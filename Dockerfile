FROM ubuntu:22.04
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update  && apt-get install -y -qq \
    gcc \
    git \
    python3 \
    python3-pip \
    libopenmpi-dev
RUN apt-get autoclean -y && apt-get autoremove -y && rm -rf /var/lib/apt/lists/*
RUN pip3 install coverage
RUN pip3 install sphinx_rtd_theme
RUN pip3 install "setuptools<=59.8.0"
RUN pip3 install wheel
RUN pip3 install "numpy<=1.23.5"

