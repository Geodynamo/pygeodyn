# pygeodyn

[![build status](https://gricad-gitlab.univ-grenoble-alpes.fr/Geodynamo/pygeodyn/badges/master/pipeline.svg)](https://gricad-gitlab.univ-grenoble-alpes.fr/Geodynamo/pygeodyn/pipelines)
[![coverage report](https://gricad-gitlab.univ-grenoble-alpes.fr/Geodynamo/pygeodyn/badges/master/coverage.svg)](https://geodynamo.gricad-pages.univ-grenoble-alpes.fr/pygeodyn/htmlcov/)
[![documentation](https://img.shields.io/website/https/geodynamo.gricad-pages.univ-grenoble-alpes.fr/pygeodyn/index.html.svg?label=documentation&up_color=%2300c5c5)](https://geodynamo.gricad-pages.univ-grenoble-alpes.fr/pygeodyn/index.html)

pygeodyn is a Python package offering data assimilation algorithms applied to geomagnetic data in the spectral domain.

Please see the [online documentation](https://geodynamo.gricad-pages.univ-grenoble-alpes.fr/pygeodyn/index.html) for further information:
* [Installation](https://geodynamo.gricad-pages.univ-grenoble-alpes.fr/pygeodyn/install.html)
* [Usage](https://geodynamo.gricad-pages.univ-grenoble-alpes.fr/pygeodyn/usage_run_algo.html)
* [API reference]((https://geodynamo.gricad-pages.univ-grenoble-alpes.fr/pygeodyn/pygeodyn.html))

### Offline documentation
It is also possible to generate the documentation locally if the source files were downloaded. For this, install first [Sphinx](http://www.sphinx-doc.org/) and the Sphinx theme [Read the Docs](https://sphinx-rtd-theme.readthedocs.io/en/stable/):
```bash
pip3 install sphinx [--user]
pip3 install sphinx_rtd_theme [--user]
```
Put `--user` if you are installing outside a virtual environment.

Then run the script building the docs:
```bash
./make_doc.sh
```
The doc will then be available for navigation in HTML format at `docs/html/index.html`.

### Scientific references
Information on the Augmented state Kalman filter can be found in:
- [Barrois, Gillet and Aubert, GJI (2017)](http://doi.org/10.1093/gji/ggx280)
- [Barrois et al. GJI (2018)](http://doi.org10.1093/gji/ggy297)
and subsequent erratum:
- [Barrois et al. GJI (2019)](http://doi.org10.1093/gji/ggy471)

For information on the further developments (dense AR-1), see:
- [Gillet, Huder and Aubert, GJI (2019)](https://doi.org/10.1093/gji/ggz313)

### Conditions of use
The work is licensed under the [GNU GPLv3](./LICENSE.txt). 

If the use of this code led to a scientific output, please cite:
- [Huder, Gillet and Thollard, GMD (2019)](https://doi.org/10.5194/gmd-12-3795-2019)

### Git repository
The source code is stored on a Git repository (https://gricad-gitlab.univ-grenoble-alpes.fr/Geodynamo/pygeodyn) which can also be used to give feedbacks through [Issues](https://gricad-gitlab.univ-grenoble-alpes.fr/Geodynamo/pygeodyn/issues).

### Contact information
For scientific inquiries, contact [Nicolas Gillet](mailto:nicolas.gillet@univ-grenoble-alpes.fr). For technical problems (including failed tests), contact [François Dall'asta](mailto:francois.dall-asta@univ-grenoble-alpes.fr) or [Franck Thollard](mailto:franck.thollard@univ-grenoble-alpes.fr).
