# Run pygeodyn on OAR cluster:

## Connect to OAR cluster:

(Requires ssh key shared with cluster)

ssh -X pseudo@ist-oar.u-ga.fr 

(dont forget -X to be able to display plots)

Once you are connected you should see : pseudo@ist-oar:~$

THE FOLLOWING WILL BE DONE ON THE OAR TERMINAL CONNECTION.

## Start Screen

Create a screen using the command : screen -R NAME_OF_SCREEN

(This step is not mandatory but is strongly advised for big runs as the code will keep running even if the terminal connection shuts down)

## Go to the working folder 

~/ is good but /data/geodynamo/pseudo/ is better because there is no storage limit.

## git clone pygeodyn with data

git clone --recursive https://gricad-gitlab.univ-grenoble-alpes.fr/Geodynamo/pygeodyn.git

## Create a virtual environment

python3 -m virtualenv name_of_virtualenv

# Activate the virtual environment

source name_of_virtualenv/bin/activate

(to deactivate it : deactivate)

# Install dependencies

pip install numpy==1.23.5 (later versions do not work)

cd pygeodyn/

pip install .

# Start an interactive job

oarsub -I --project iste-equ-geodynamo -l /nodes=1/core=10,walltime=10:00:00

Walltime: duration of the interactive job
nodes: number of nodes
core: number of core per node

(you may need to reactivate the virtual environement)

# run pygeodyn

set up the needed parameters in pygeodyn/augkf.conf

To run pygeodyn in series: 

python3 run_algo.py -conf augkf.conf (+ other parameters)

To run pygeodyn in parallel: 

mpirun -n number_of_cores python3 -m mpi4py run_algo.py -conf augkf.conf (+ other parameters)




# Run webgeodyn:

There are two ways to install webgeodyn and that may confuse the user.

## Install webgeodyn from pypi

This is the easiest way of doing.

You just need the command: pip install webgeodyn

This will install to the ACTIVE python environment library the latest version of webgeodyn found on the online server pypi.

You can also force the install of another version using : pip install webgeodyn==version

## Install webgeodyn from pypi

This is the longest way of doing but offers more flexibility (code developement).

You need to git clone webgeodyn: git clone https://gricad-gitlab.univ-grenoble-alpes.fr/Geodynamo/webgeodyn.git

Then go to webgeodyn folder: cd webgeodyn

Then use the command: pip install .

This will install to the ACTIVE python environment library the version of webgeodyn git cloned.

NOTE THAT IF BOTH WAYS SHARE THE SAME ACTIVE PYTHON ENVIRONMENT THEN WEBGEODYN WILL BE OVERWRITTEN EVERY TIME YOU PIP INSTALL!!!




# LOCAL-REMOTE development strategy:

The best way to develope code is to have a local working environment and run the computation remotely (OAR cluster).

To do so you set up pygeodyn locally (see above or "install" in the documentation).

Then you will synchronize the local folder to the remote using rsync:

rsync --exclude 'venv' -a /complete/or/relative/path/local pseudo@ist-oar.u-ga.fr:/complete/or/relative/path/remote

This command will synchronize the remote cluster /complete/or/relative/path/remote to the local /complete/or/relative/path/local without synchronizing venv/ folder (which is the virtual environment).

So basically, every time you change something locally you need to update the remote using the command above.

rsync works also from remote to local!




# Update new pygeodyn version:

## Update version number

The version number to update is in pygeodyn/_version.py

example: __version__ = '3.0.0'

## Update CHANGELOG.md

Write the changes associated with the update.

Now, to push a new pygeodyn version to gitlab you must follow the following steps.

## Create last git commit:

git add changes_done

git commit -m "Notes about the commit"

## Add a tag to last commit:

git tag -a [tag_name] HEAD -m "Tag message"

example: git tag v2.5 HEAD -m "Version 2.5 released"

Running the git log --oneline command shows that the v2.5 tag was created for the latest commit

## Push (with tags) to remote

git push origin --tags




# Add files to git LFS (Large Files Storage)

Git LFS is the best way to add data to a gitlab repository as it will only keep the last version of a dataset in the history instead of all versions.

## Track data

To add data to the lfs track list:

git lfs track "files_or_folders"

(to untrack: git lfs untrack "files_or_folders")

example: git lfs track "data_folder/**"

Then do as usual git add, commit push.




# Clean gitlab repository history polluted by data

MAKE SURE TO DOWNLOAD THE REPOSITORY AS A BACKUP IN CASE THINGS GO WRONG.

To remove complete data from a repo (including history):

git filter-repo --path folder_to_get_rid_off/ --invert-paths

Then push to remote.