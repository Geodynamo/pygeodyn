#!/usr/bin/env bash

pip3 install sphinx_rtd_theme
sphinx-apidoc -e -o docs -f pygeodyn/
cd docs
sphinx-build -a -b html . html
