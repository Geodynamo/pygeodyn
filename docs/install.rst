############
Installation
############

.. note::

    Mac installation is recent, user feedback would be much appreciated.

Linux & Mac
^^^^^^^^^^^
This section provides the installation instructions for both linux and mac system with:

* Python3 (tested under 3.10.6).
* pip (tested under 22.0.2).
* gcc (tested under 11.3.0).
* git (tested under 2.34.1). 

Linux: 
======
All can be installed through ``apt`` package manager.

.. code-block:: bash
    
    apt-get install gcc
    apt-get install git
    apt-get install python3
    apt-get install python3-pip
    sudo apt install libopenmpi-dev

Mac:
====
Gcc, git and python3 can be installed through ``Homebrew`` package manager.

.. code-block:: bash
    
    brew install gcc
    brew install git
    brew install python3

``Homebrew`` must be installed first:

.. code-block:: bash

    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

Then you can get pip if not already installed alongside python3:

.. code-block:: bash

    python3 -m ensurepip

Installation scripts:
=====================
.. note::

    The installation scripts are recent, user feedback would be much appreciated.

The script aims to install `pygeodyn`_ along with `pygeodyn_data`_ in the directory where it is run.

You must have root permission to run the script.

:download:`Linux <https://geodynamo.gricad-pages.univ-grenoble-alpes.fr/pygeodyn/linux_install.sh>`
:download:`Mac <https://geodynamo.gricad-pages.univ-grenoble-alpes.fr/pygeodyn/mac_install.sh>`

.. warning::
    A python virtual environment is set with all the required packages in it, it must be **active** when running pygeodyn code.

To activate a python environment, use the following command:

.. code-block:: bash

    source path_to_virtual_environment/bin/activate

And to deactivate it:

.. code-block:: bash

    deactivate

The installation script can be adapted to meet the user needs. 

The full process is detailled below and can help the user modify the script accordingly. 

Requirements (linux & mac):
===========================
The installation of pygeodyn requires the following packages:

* *setuptools* (tested under 59.8.0): to be able to run ``setup.py``.
* *wheel* (tested under 0.40.0): to be able to run ``setup.py``.
* *numpy* (at least 1.7, at most 1.23.5, tested under 1.23.5): to be able to wrap Fortran files with *f2py*.


The other dependencies will be automatically installed.

Get pygeodyn (linux & mac):
===========================
Sources and data
----------------
The pygeodyn package is versioned on a git repository (https://gricad-gitlab.univ-grenoble-alpes.fr/Geodynamo/pygeodyn) where the source code can be fetched by running:

.. code-block:: bash

    git clone --recursive https://gricad-gitlab.univ-grenoble-alpes.fr/Geodynamo/pygeodyn.git

This will clone the `pygeodyn`_ repository containing the sources and the `pygeodyn_data`_ repository containing several datasets (~1.5 GB) to run the algorithms.

Only sources
------------
The `pygeodyn_data`_ repository containing the datasets is handled as a git submodule of `pygeodyn`_. Therefore, it is possible to get **only the sources** of pygeodyn and not the data by running

.. code-block:: bash

    git clone https://gricad-gitlab.univ-grenoble-alpes.fr/Geodynamo/pygeodyn.git

instead. The absence of the ``--recursive`` option will prevent the cloning of the `pygeodyn_data`_ submodule.

Note however that you will not be able to run the `run_algo <https://geodynamo.gricad-pages.univ-grenoble-alpes.fr/pygeodyn/usage_run_algo.html>`_ script unless you supply your own dataset (procedure described at https://geodynamo.gricad-pages.univ-grenoble-alpes.fr/pygeodyn/usage_new_types.html).

It is still possible to download the `pygeodyn_data`_ submodule afterwards by running

.. code-block:: bash

    cd pygeodyn
    git submodule update --init

.. _pygeodyn: https://gricad-gitlab.univ-grenoble-alpes.fr/Geodynamo/pygeodyn
.. _pygeodyn_data: https://gricad-gitlab.univ-grenoble-alpes.fr/Geodynamo/pygeodyn_data

Install the pygeodyn package from sources
=========================================
Regular installation
--------------------
Once the sources are downloaded as described above (with or without data), pygeodyn can be installed by navigating to the package directory and running

.. code-block:: bash

    pip3 install . --user

If the installation succeeded, you should be able to import pygeodyn with Python 3:

.. code-block:: bash

    python3 -c "import pygeodyn; print(pygeodyn.__version__)"

This should print the current version of pygeodyn.

Note however that with this installation, any change to the source code will not be taken into account unless the package is reinstalled (see `Reinstallation`_ section).

Develop installation
--------------------
If you intend to change the source code of pygeodyn (e.g. by defining new types), you must install the package in *editable* mode:

.. code-block:: bash

    pip3 install -e . --user

This way, the sources folder will be linked to the Python library. Any modification will be immediately taken into account without the need to install the package again.

In a virtual environment
------------------------
If you are installing in a virtual environment (see the Python virtualenv_ page), you can remove the ``--user`` flag but remember to install setuptools_, numpy_ and wheel_ first:

.. code-block:: bash

    pip3 install numpy
    pip3 install [-e] .


Testing
=======
If the repository was cloned with the data, tests on the package can be run in the pygeodyn directory using

.. code-block:: bash

    python3 -m unittest discover

Please report any failed tests using the `contact info`_.

Detailed information on the coverage of the tests can be found `here <https://geodynamo.gricad-pages.univ-grenoble-alpes.fr/pygeodyn/htmlcov/>`_.


Reinstallation
==============
To reinstall the package, run the following commands in the package folder:

.. code-block:: bash

    pip3 uninstall .
    pip3 install [-e] . [--user]

.. _numpy: http://www.numpy.org
.. _wheel: https://pypi.org/project/wheel/
.. _setuptools: https://pypi.org/project/setuptools/
.. _pip: https://docs.python.org/3/installing/index.html
.. _virtualenv: https://pypi.org/project/virtualenv/

Windows
^^^^^^^
.. admonition:: Work in progress...

    We are currently working on releasing a ``pip`` or ``conda`` version to ease the install on Windows machines.

On Windows, pygeodyn requires the following:

* Python3
* pip (shipped with Python since 3.4, see https://pip.pypa.io/en/stable/installing/#do-i-need-to-install-pip)
* `MinGW <http://www.mingw.org/>`_  for ``gfortran``
* Probably additional libraries (BLAS, LAPACK...)

If those are satisfied, the installation can be done in the same manner as for Linux.

.. note::

    Currently, we are lacking feedback on the installation on Windows. Do not hesitate to give yours if you feel it can help us or future users (`contact info`_).

.. _contact info: https://geodynamo.gricad-pages.univ-grenoble-alpes.fr/pygeodyn/contact_info.html