Module reference
================

Subpackages
-----------

.. toctree::
    :maxdepth: 3

    pygeodyn.augkf
    pygeodyn.generic
    pygeodyn.inout
    pygeodyn.shear
    pygeodyn.tests

Submodules
----------

.. toctree::
   :maxdepth: 1

   pygeodyn.common
   pygeodyn.constants
   pygeodyn.corestates
   pygeodyn.pca
   pygeodyn.run
   pygeodyn.utilities
