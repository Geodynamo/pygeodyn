#####################################################
Zonal treatement of Augmented Kalman filter algorithm
#####################################################

Introduction
============

The subpackage `zonkf`_ implements variations of the `augkf`_ with zonal and non-zonal coefficients of U separately treated. This algorithm has two flavours: which be can changed by setting ``coupled_zonals`` in the configuration file:

- **Coupled ZonKF**: The zonal and non-zonal coefficients are coupled through the forecast and by cross-covariances in the analyse. Set ``coupled_zonals`` to True in the configuration file to use this algorithm.
- **Isolated ZonKF**: The zonal and non-zonal coefficients are isolated. There is no forecast of the zonal coefficients nor cross-covariances with the non-zonal coefficients in the analyse.

.. _augkf: https://geodynamo.gricad-pages.univ-grenoble-alpes.fr/pygeodyn/pygeodyn.augkf.html
.. _zonkf: https://geodynamo.gricad-pages.univ-grenoble-alpes.fr/pygeodyn/pygeodyn.zonkf.html