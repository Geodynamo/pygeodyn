pygeodyn documentation
======================

.. toctree::
    :maxdepth: 2
    :caption: General information

    install
    augkf
    shear

.. toctree::
    :maxdepth: 4
    :caption: Using pygeodyn

    usage_run_algo
    configuration

.. toctree::
    :maxdepth: 2
    :caption: Tutorials

    usage_new_types
    usage_corestate
    usage_steps

.. toctree::
    :maxdepth: 1
    :caption: Further information

    reference
    contact_info

.. toctree::
    :maxdepth: 2
    :caption: Archive

    zonkf