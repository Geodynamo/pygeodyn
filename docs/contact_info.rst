###################
Contact information
###################

For scientific inquiries, contact `Nicolas Gillet`_. For technical problems (including failed tests), contact `Francois Dall'asta`_ or `Franck Thollard`_.

.. _Nicolas Gillet: nicolas.gillet@univ-grenoble-alpes.fr
.. _Francois Dall'asta: francois.dall-asta@univ-grenoble-alpes.fr
.. _Franck Thollard: franck.thollard@univ-grenoble-alpes.fr
