import argparse
import pygeodyn.run
import os

print("""
    pygeodyn
    Copyright (C) 2019 Geodynamo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    """)

parser = argparse.ArgumentParser(description="pygeodyn")

parser.add_argument("-conf", type=str, help="config file")
parser.add_argument("-m", type=int, default=20,
                    help="Number of realisations to consider")
parser.add_argument("-algo", type=str, default='augkf', help="Algorithm to use")
parser.add_argument("-shear", type=int, default=0, help="Compute shear at analysis time: 0 NO, 1 YES")
parser.add_argument("-seed", "-s", type=int, default=None,
                    help="Seed for initializing random states")
parser.add_argument("-v", type=int, default=2,
                    help="Logging level: 1 DEBUG, 2 INFO, 3 WARNING, 4 ERROR, 5 CRITICAL, default=INFO")
parser.add_argument("-path", type=str, default='~/pygeodyn_results/', help="Path of the output folder")
parser.add_argument("-cname", type=str, default='Current_computation', help="Name of the computation")
parser.add_argument("-l", type=str, default=None, help="Name of the log file")

args = parser.parse_args()

pygeodyn.run.algorithm(os.path.expanduser(args.path),
                       os.path.expanduser(args.cname),
                       os.path.expanduser(args.conf),
                       args.m, args.shear, log_file=args.l, logging_level=int(args.v) * 10,
                       seed=args.seed, algo_name=args.algo)
