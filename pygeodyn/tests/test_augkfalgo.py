import os
import numpy as np
import unittest
from pygeodyn.augkf.algo import create_augkf_algo
from pygeodyn.inout.config import ComputationConfig


class TestAugkfAlgodiag(unittest.TestCase):
    def setUp(self):
        self.do_shear = 0
        conf_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'test_diag.conf')
        self.config = ComputationConfig(self.do_shear, config_file=conf_path)
        self.nb_realisations = 2
        self.attributed_models = np.arange(self.nb_realisations)

    def test_init(self):
        # Non-existing observation file
        self.config.obs_dir = '/path/to/nothing/gkdjgskdh'
        self.assertRaises(IOError, create_augkf_algo, self.config, self.nb_realisations, 3, self.attributed_models)

    def test_functional_init(self):
        # Correct init
        create_augkf_algo(self.config, self.nb_realisations, 5, self.attributed_models)


class TestAugkfAlgoAR1(TestAugkfAlgodiag):
    def setUp(self):
        self.do_shear = 0
        conf_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'test_AR1.conf')
        self.config = ComputationConfig(self.do_shear, config_file=conf_path)
        self.nb_realisations = 2
        self.attributed_models = np.arange(self.nb_realisations)


class TestAugkfAlgoAR3(TestAugkfAlgodiag):
    def setUp(self):
        self.do_shear = 0
        conf_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'test_AR3.conf')
        self.config = ComputationConfig(self.do_shear, config_file=conf_path)
        self.nb_realisations = 2
        self.attributed_models = np.arange(self.nb_realisations)


if __name__ == '__main__':
    SUITEA = unittest.TestLoader().loadTestsFromTestCase(TestAugkfAlgodiag)
    SUITEB = unittest.TestLoader().loadTestsFromTestCase(TestAugkfAlgoAR1)
    SUITEC = unittest.TestLoader().loadTestsFromTestCase(TestAugkfAlgoAR3)
    SUITE = unittest.TestSuite([SUITEA, SUITEB, SUITEC])
    RES = unittest.TextTestRunner(verbosity=2).run(SUITE)
    import sys
    sys.exit(max(len(RES.failures), len(RES.errors)))
