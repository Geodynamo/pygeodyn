import os
import unittest
import numpy as np
from pygeodyn import pca
import pygeodyn.inout.config as config_m
from pygeodyn.inout.reads import extract_realisations

class TestPca(unittest.TestCase):
    """ Test the pca module """

    def setUp(self):
        self.do_shear = 0
        conf_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'test_AR3.conf')
        self.cfg = config_m.ComputationConfig(self.do_shear, config_file=conf_path)
        # Extract reals of U
        real_U = extract_realisations(self.cfg.prior_dir, self.cfg.prior_type, self.cfg.dt_smoothing, measures=('U'))[1][0]
        print(real_U)
        # Take a subset of reals to avoid MemoryErrors
        real_Nu = real_U.shape[1] // 2
        self.real_U = np.concatenate((real_U[:5000, :self.cfg.Nu], real_U[:5000, real_Nu:real_Nu + self.cfg.Nu]), axis=1)

    def testPCAOperatorWithoutNorm(self):
        """
        Function tests of the transform and inverse transform method of NormedPCAOperator
        """
        # Ensure no normalisation
        self.cfg.pca_norm = None
        # Perform the PCA
        custom_pca_operator = pca.NormedPCAOperator(self.cfg)
        U_pca = custom_pca_operator.sklearn_operator.fit_transform(self.real_U)

        # Try to recover U with:
        # Regular PCA inverse transform (sklearn)
        recovered_U_sklearn = custom_pca_operator.sklearn_operator.inverse_transform(U_pca)

        # Manual inverse transform using U = S_u * U_pca + U0
        recovered_U_manual = np.transpose(np.matmul(custom_pca_operator.S_u, np.transpose(U_pca))) + custom_pca_operator.U0

        # Semi-manual (PCAOperator implements manual inverse transform)
        recovered_U_semi_manual = custom_pca_operator.inverse_transform(U_pca)

        np.testing.assert_almost_equal(recovered_U_sklearn, recovered_U_manual)
        np.testing.assert_equal(recovered_U_manual, recovered_U_semi_manual)

        # Test that inverse_transform followed by transform gives the same result
        recovered_U_pca = custom_pca_operator.transform(custom_pca_operator.inverse_transform(U_pca))
        np.testing.assert_almost_equal(recovered_U_pca, U_pca)

        # Test that transform followed by inverse_transform gives the same result
        recovered_U = custom_pca_operator.inverse_transform(custom_pca_operator.transform(self.real_U))
        recovered_U2 = custom_pca_operator.sklearn_operator.inverse_transform(custom_pca_operator.sklearn_operator.transform(self.real_U))
        np.testing.assert_almost_equal(recovered_U, recovered_U2)

    def testPCAOperatorWithNorm(self):
        """
        Function tests of the transform and inverse transform method of NormedPCAOperator
        """
        # Ensure 'energy' normalisation
        self.cfg.pca_norm = 'energy'
        # Perform the PCA
        custom_pca_operator = pca.NormedPCAOperator(self.cfg)
        U_pca = custom_pca_operator.sklearn_operator.fit_transform(self.real_U)

        # Test that inverse_transform followed by transform gives the same result
        recovered_U_pca = custom_pca_operator.transform(custom_pca_operator.inverse_transform(U_pca))
        np.testing.assert_almost_equal(recovered_U_pca, U_pca)


if __name__ == '__main__':
        SUITE = unittest.TestLoader().loadTestsFromTestCase(TestPca)
        RES = unittest.TextTestRunner(verbosity=2).run(SUITE)
        import sys
        sys.exit(max(len(RES.failures), len(RES.errors)))
