
import os
import copy
import unittest
import numpy as np
import pygeodyn.corestates as cs
import pygeodyn.inout.config as config_m
from pygeodyn.augkf.algo import create_augkf_algo


class TestAugkfAnalyserAR1(unittest.TestCase):
    def setUp(self):
        conf_path = os.path.join(
            os.path.dirname(os.path.abspath(__file__)), "test_AR1.conf"
        )
        self.do_shear = 0
        self.nb_reals = 2
        attributed_models = np.arange(self.nb_reals)
        self.config = config_m.ComputationConfig(self.do_shear, config_file=conf_path)
        augkfAlgo = create_augkf_algo(self.config, self.nb_reals, np.random.randint(0, 1000),attributed_models)
        self.analyser = augkfAlgo.analyser

    def test_analysis_step(self):

        # Test with core_state of wrong dims
        input_cs = cs.CoreState({"MF": [np.zeros(100), 1]})
        self.assertRaises(ValueError, self.analyser.analysis_step, input_cs)

        # Test with 2D core_state but not enough space for realisations
        input_cs.B = np.zeros((1, 100))
        self.assertGreater(self.nb_reals, input_cs.getMeasure("MF").shape[0])
        self.assertRaises(ValueError, self.analyser.analysis_step, input_cs)

    def test_functional_analysis_step(self):
        # Build a random core state with all measures
        input_cs = cs.CoreState(
            {
                "MF": np.random.rand(self.nb_reals, self.config.Nb),
                "U": np.random.rand(self.nb_reals, self.config.Nu2),
                "SV": np.random.rand(self.nb_reals, self.config.Nsv),
                "ER": np.random.rand(self.nb_reals, self.config.Nsv),
                "Z": np.random.rand(self.nb_reals, self.config.Nz)
            }
        )

        # Test MF and SV obs
        self.analyser.check_if_analysis_data(0)
        ana = self.analyser.analysis_step(input_cs)
        
        # Test SV obs only
        MF = copy.deepcopy(self.analyser.measure_observations["MF"])
        self.analyser.measure_observations["MF"] = {}
        self.analyser.check_if_analysis_data(0)
        ana = self.analyser.analysis_step(input_cs)
        np.testing.assert_equal(ana["MF"], input_cs["MF"])
        self.analyser.measure_observations["MF"] = MF

        # Test MF obs only
        self.analyser.measure_observations["SV"] = {}
        self.analyser.check_if_analysis_data(0)
        ana = self.analyser.analysis_step(input_cs)
        for m in ["U","ER"]:
            np.testing.assert_equal(ana[m], input_cs[m])

        # Test no obs
        self.analyser.measure_observations["MF"] = {}
        self.analyser.check_if_analysis_data(0)
        ana = self.analyser.analysis_step(input_cs)
        
        # Test that core states are equal
        for m in input_cs.measures:
            np.testing.assert_equal(ana[m], input_cs[m])

        
class TestAugkfAnalyserAR3(unittest.TestCase):
    def setUp(self):
        conf_path = os.path.join(
            os.path.dirname(os.path.abspath(__file__)), "test_AR3.conf"
        )
        self.do_shear = 0
        self.config = config_m.ComputationConfig(self.do_shear, config_file=conf_path)
        self.nb_reals = 3
        attributed_models = np.arange(self.nb_reals)
        augkfAlgo = create_augkf_algo(self.config, self.nb_reals, np.random.randint(0, 1000), attributed_models)
        self.analyser = augkfAlgo.analyser
        self.nb_t = 2*self.analyser.algo.config.dt_a_f_ratio + 1
        self.ratio = self.analyser.algo.config.dt_a_f_ratio


    def test_analysis_step(self):

        # Test with core_state of wrong dims
        input_cs = cs.CoreState({"MF": [np.zeros(100 * self.nb_t), 1]})
        Z_AR3=np.random.rand(self.nb_reals, 3, self.config.Nz)
        self.assertRaises(ValueError, self.analyser.analysis_step, input_cs, Z_AR3)

        # Test with 2D core_state but not enough space for realisations
        input_cs.B = np.zeros((1,self.nb_t, 100))
        self.assertGreater(self.nb_reals, input_cs.getMeasure("MF").shape[0])
        self.assertRaises(ValueError, self.analyser.analysis_step, input_cs, Z_AR3)

    def test_functional_analysis_step(self):
        # Build a random core state with all measures
        input_cs = cs.CoreState(
            {
                "MF": np.random.rand(self.nb_reals,self.nb_t, self.config.Nb),
                "U": np.random.rand(self.nb_reals,self.nb_t, self.config.Nu2),
                "dUdt": np.random.rand(self.nb_reals,self.nb_t, self.config.Nu2),
                "d2Udt2": np.random.rand(self.nb_reals,self.nb_t, self.config.Nu2),
                "SV": np.random.rand(self.nb_reals,self.nb_t, self.config.Nsv),
                "ER": np.random.rand(self.nb_reals,self.nb_t, self.config.Nsv),
                "dERdt": np.random.rand(self.nb_reals,self.nb_t, self.config.Nsv),
                "d2ERdt2": np.random.rand(self.nb_reals,self.nb_t, self.config.Nsv),
                "Z": np.random.rand(self.nb_reals,self.nb_t, self.config.Nz)
            }
        )

        Z_AR3=np.random.rand(self.nb_reals, 3, self.config.Nz)

        # Test MF and SV obs
        self.analyser.check_if_analysis_data(0)
        ana = self.analyser.analysis_step(input_cs, Z_AR3)[0]
        
        # Test SV obs only
        MF = copy.deepcopy(self.analyser.measure_observations["MF"])
        self.analyser.measure_observations["MF"] = {}
        self.analyser.check_if_analysis_data(0)
        ana = self.analyser.analysis_step(input_cs, Z_AR3)[0]
        np.testing.assert_equal(ana["MF"], input_cs["MF"][:,self.ratio])
        self.analyser.measure_observations["MF"] = MF

        # Test MF obs only
        self.analyser.measure_observations["SV"] = {}
        self.analyser.check_if_analysis_data(0)
        ana = self.analyser.analysis_step(input_cs, Z_AR3)[0]
        for m in ["U","ER"]:
            np.testing.assert_equal(ana[m], input_cs[m][:,self.ratio])

        # Test no obs
        self.analyser.measure_observations["MF"] = {}
        self.analyser.check_if_analysis_data(0)
        ana = self.analyser.analysis_step(input_cs, Z_AR3)[0]
        for m in input_cs.measures:
            np.testing.assert_equal(ana[m], input_cs[m][:,self.ratio])


if __name__ == "__main__":
    SUITEA = unittest.TestLoader().loadTestsFromTestCase(TestAugkfAnalyserAR1)
    SUITEB = unittest.TestLoader().loadTestsFromTestCase(TestAugkfAnalyserAR3)
    SUITE = unittest.TestSuite([SUITEA, SUITEB])
    RES = unittest.TextTestRunner(verbosity=2).run(SUITE)
    import sys

    sys.exit(max(len(RES.failures), len(RES.errors)))