#!/usr/bin/env python3
#-*- coding: utf-8 -*-

import numpy as np
from hypothesis.strategies import *
from hypothesis.extra.numpy import *
from hypothesis.strategies import composite
from pygeodyn.corestates import CoreState
from pygeodyn.inout.config import ComputationConfig


@composite
def finite_floats(draw, **kwargs):
    drawn_float = draw(floats(**kwargs, allow_nan=False, allow_infinity=False))
    return drawn_float


@composite
def square_matrices(draw, max_size, min_size=1, min_value=None, max_value=None):
    n = draw(integers(min_value=min_size, max_value=max_size))
    float_strat = finite_floats(min_value=min_value, max_value=max_value)
    sq_M = draw(arrays(elements=float_strat, shape=(n, n), dtype=np.float64, fill=float_strat))
    return sq_M

@composite
def singular_square_matrices(draw, *args, **kwargs):
    sq_M = draw(square_matrices(*args, **kwargs))
    # Set a line to 0. to ensure singularity
    sq_M[0] = 0.
    return sq_M


@composite
def decimal_dates(draw):
    year = draw(integers(min_value=0, max_value=3000))
    month = draw(integers(min_value=1, max_value=12))
    return year + month/12.


@composite
def CoreStates(draw, max_degree=30):
    Lb = draw(integers(min_value=0, max_value=max_degree))
    Lu = draw(integers(min_value=0, max_value=max_degree))
    Lsv = draw(integers(min_value=0, max_value=max_degree))

    cs = CoreState({'MF': np.zeros(Lb*(Lb+2)),
                    'U': np.zeros(2*Lu*(Lu+2)),
                    'ER': np.zeros(Lsv*(Lsv+2)),
                    'SV': np.zeros(Lsv*(Lsv+2))})
    return cs


@composite
def working_ComputationConfig_dict(draw, fill=integers(min_value=1)):
    working_dict = draw(fixed_dictionaries({k: fill for k in ComputationConfig.necessary_parameters}))
    # Ensure dt analysis is a multiple of dt forecast
    working_dict['dt_a'] = draw(integers(min_value=1))*working_dict['dt_f']

    return working_dict


def long_test(func):
    def func_wrapper(*args, **kwargs):
        print('Running {}: it will take some time... (around one minute)'.format(func.__name__))
        func(*args, **kwargs)
    return func_wrapper
