import os
import h5py
import unittest
import numpy as np
from pygeodyn.shear.shear_algo import ShearInversion, ShearError


class Dict2Class(object):
      
    def __init__(self, my_dict):
          
        for key in my_dict:
            setattr(self, key, my_dict[key])
  
  
class TestShear(unittest.TestCase):
    """
    Tests related to shear computation at the CMB
    """

    def setUp(self):
    
        self.test_dir = os.path.dirname(os.path.abspath(__file__))
        self.cfg = Dict2Class({"Ly": 18, "Lu": 18, "Lb": 13, "Lsv": 13, "prior_dir_shear": self.test_dir + "/../data/priors/CE_augkf", "prior_type_shear": "coupled_earth", "remove_spurious_shear_u":  0.09, "remove_spurious_shear_err": 0.05, "TauG":30})
        self.N_t = 57500


    def test_load_data(self):
        """
        Check ErrorValue raised if not 1D arrays
        """

        shear = ShearInversion(self.cfg, test = self.test_dir)

        U = np.zeros((10,))
        DU = np.zeros((1,10))
        MF = np.zeros((1,10, 1))
        SV = np.zeros((2,10, 1))
        
        # Check ErrorValue raised if not 1D arrays
        self.assertRaises(ValueError, shear.load_data, U, DU, MF, SV)


    def test_error_rep(self):
        """
        Check that the error of representativeness equal the reference
        """
        
        error = ShearError(
            self.cfg.Ly,
            self.cfg.Lu,
            self.cfg.Lb,
            0,
            self.cfg.prior_dir_shear,
            self.cfg.prior_type_shear,
            test=self.test_dir
            )
        
        # Precompute operators
        error.precompute_operators()
        
        # Compute error of representativeness
        U, DU, MF, SV = error.load_prior_for_err_rep()
        err_rep = error.compute_error(U[0,:], DU[0,:], MF[0,:], SV[0,:])

        # Reference error of representativeness
        ref = np.load(os.path.join(self.test_dir,"test_shear/CED_y12_err_repr_Ly_18_Lu_18_Lb_13.npy"))
        
        # Compare arrays with tolerance
        np.testing.assert_allclose(err_rep, ref[0,:], rtol=1e-5)
        

    def test_shear(self):
        """
        Compute shear and compare to reference
        """

        shear = ShearInversion(self.cfg, test = self.test_dir)

        # Precompute operators
        shear.precompute_operators()

        # Build prior
        (
            Pee_y1_tilda_with_glasso,
            Pee_y2_tilda_with_glasso,
            Pss_inv_with_glasso
        ) = shear.build_prior()

        # Build U DU MF SV data
        f = h5py.File(self.test_dir + "/../data/priors/50path/Real.hdf5", "r")
        TNM_50Path = np.array(f['U']).T
        tnm = TNM_50Path[: TNM_50Path.shape[0]//2, :]
        snm = TNM_50Path[TNM_50Path.shape[0]//2 :, :]
        gnm = np.array(f["MF"]).T
        t = np.array(f["times"])
        U = np.concatenate((-tnm[0:shear.Nu, :], snm[0:shear.Nu, :]), axis=0)
        SV = (gnm[:, 2:] - gnm[:, :-2]) / (t[2:] - t[:-2])
        DU = (U[:, 2:] - U[:, :-2]) / (t[2:] - t[:-2])
        SV = SV[:shear.Nb, self.N_t]
        U = U[:, self.N_t+1]
        MF = gnm[:shear.Nb, self.N_t+1]
        DU = DU[:,self.N_t]
        
        # Compute shear
        SH_coef = shear.compute_shear(U, DU, MF, SV, Pee_y1_tilda_with_glasso, Pee_y2_tilda_with_glasso, Pss_inv_with_glasso)
        
        # Reference shear
        ref = np.load(os.path.join(self.test_dir,"test_shear/Reference_shear.npy")).reshape(-1,)
        
        # Compare arrays with tolerance
        np.testing.assert_allclose(SH_coef, ref, rtol=1e-5)
    

if __name__ == '__main__':
    SUITE = unittest.TestLoader().loadTestsFromTestCase(TestShear)
    RES = unittest.TextTestRunner(verbosity=2).run(SUITE)
    import sys

    sys.exit(max(len(RES.failures), len(RES.errors)))
