"""
Test module for computation step.

- Test  setup
- test test_consistency_Fortran_Python_diffusion_computation
- test_building_legacy_Fortran_arrays
"""

import os.path
import unittest
import pygeodyn.generic.computer as comp
import pygeodyn.inout.config as config_m


class TestGenericComputer(unittest.TestCase):
    """Test class for the GenericComputer"""

    def setUp(self):
        """Setup for the test GenericComputer
        """
        self.do_shear = 0
        conf_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'test_diag.conf')
        self.cfg = config_m.ComputationConfig(self.do_shear, config_file=conf_path)

    def test_init(self):
        """Test the initialisation of GenericComputer
        """
        # Test setup without a ComputationConfig object
        self.assertRaises(TypeError, comp.GenericComputer, None, None, None, None)

        # Test setup without priors
        # Test setup without covariances
        # Test setup without legendre polynomials


if __name__ == '__main__':
    SUITE = unittest.TestLoader().loadTestsFromTestCase(TestGenericComputer)
    RES = unittest.TextTestRunner(verbosity=2).run(SUITE)
    import sys
    sys.exit(max(len(RES.failures), len(RES.errors)))
