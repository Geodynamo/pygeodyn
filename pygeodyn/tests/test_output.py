import h5py
import os.path
import unittest
import numpy as np
import pygeodyn.run

class TestOutput(unittest.TestCase):
    """
    Tests if all algos can be run properly on a short time span and with few realisations
    """
    def setUp(self):
        self.nb_reals = 2
        self.seed = 5
        self.do_shear = 0
        self.test_dir = os.path.dirname(os.path.abspath(__file__))

    def test_output(self):
        
        # default output file parameters
        pygeodyn.run.algorithm(os.path.join(self.test_dir, 'dummy_files'), 'test_output', os.path.join(self.test_dir, 'test_output_conf/default.conf'), self.nb_reals, 
                                                      self.do_shear, log_file=None, logging_level=1, seed=self.seed, algo_name='augkf')
        with h5py.File(os.path.join(self.test_dir, 'dummy_files', 'test_output','test_output.hdf5'),'r') as f:
            self.assertEqual(list(f.keys()), ['analysed'])
            self.assertEqual('float32', np.array(f['analysed']['U']).dtype)
            self.assertEqual('float32', np.array(f['analysed']['times']).dtype)
        
        # all output parameters = 1 and double precision
        pygeodyn.run.algorithm(os.path.join(self.test_dir, 'dummy_files'), 'test_output', os.path.join(self.test_dir, 'test_output_conf/all_out.conf'), self.nb_reals, 
                                                      self.do_shear, log_file=None, logging_level=1, seed=self.seed, algo_name='augkf')
        with h5py.File(os.path.join(self.test_dir, 'dummy_files', 'test_output','test_output.hdf5'),'r') as f:
            self.assertEqual(list(f.keys()), ['analysed','computed','forecast','misfits'])
            self.assertEqual('float64', np.array(f['analysed']['U']).dtype)
            self.assertEqual('float64', np.array(f['analysed']['times']).dtype)

        # all output parameters = 0
        pygeodyn.run.algorithm(os.path.join(self.test_dir, 'dummy_files'), 'test_output', os.path.join(self.test_dir, 'test_output_conf/no_out.conf'), self.nb_reals, 
                                                      self.do_shear, log_file=None, logging_level=1, seed=self.seed, algo_name='augkf')
        with h5py.File(os.path.join(self.test_dir, 'dummy_files', 'test_output','test_output.hdf5'),'r') as f:
            self.assertEqual(list(f.keys()), [])


if __name__ == '__main__':
    SUITE = unittest.TestLoader().loadTestsFromTestCase(TestOutput)
    RES = unittest.TextTestRunner(verbosity=2).run(SUITE)
    import sys

    sys.exit(max(len(RES.failures), len(RES.errors)))
