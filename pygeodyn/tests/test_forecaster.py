import os
import unittest
import numpy as np
import pygeodyn.corestates as cs
from pygeodyn.augkf.algo import create_augkf_algo
from pygeodyn.inout.config import ComputationConfig
from pygeodyn.augkf.forecaster import AugkfForecasterAR1, AugkfForecasterAR3


class TestAugkfForecasterAR1(unittest.TestCase):
    def setUp(self):
        """
        Setup for testing Forecaster of the Augmented state Kalman Filter algo
        """

        self.do_shear = 0
        conf_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'test_AR1.conf')
        self.config = ComputationConfig(self.do_shear, config_file=conf_path)
        self.nb_reals = 2
        self.attributed_models = np.arange(self.nb_reals)
        self.augkfalgo = create_augkf_algo(self.config, self.nb_reals, 4321, self.attributed_models)
        self.forecaster = self.augkfalgo.forecaster
        self.seed = 0
        self.i_t = 0
        self.i_real = 0

    def test_init(self):
        """
        Tests if the initialisation fails if not all covariances are present.
        """
        # Test init without CholU
        incomplete_augkfalgo = create_augkf_algo(self.config, self.nb_reals, 1234, self.attributed_models)
        incomplete_augkfalgo.cov_prior.pop('Chol')

        self.assertRaises(KeyError, AugkfForecasterAR1, incomplete_augkfalgo)

    def test_forecast_step(self):
        """
        Tests the augkf forecast step
        """
        
        Z_AR = np.random.rand(self.augkfalgo.config.Nz)
        # Not a CoreState
        not_a_core_state = np.zeros(self.config.Nb)
        self.assertRaises(TypeError, self.forecaster.forecast_step, not_a_core_state, Z_AR, self.seed, self.i_real, self.i_t)

        # Wrong dims of CoreStates
        wrong_dim_B = np.zeros((2, self.config.Nb))
        input_core_state = cs.CoreState({'MF': wrong_dim_B,
                                         'U': np.zeros(self.config.Nu2),
                                         'SV': np.zeros(self.config.Nsv),
                                         'ER': np.zeros(self.config.Nsv),
                                         'Z': np.zeros(self.config.Nz)})
        self.assertRaises(ValueError, self.forecaster.forecast_step, input_core_state, Z_AR, self.seed, self.i_real, self.i_t)

        # Have a proper core_state
        input_core_state.B = np.zeros(self.config.Nb)

        wrong_Z_AR = np.random.rand(2,self.augkfalgo.config.Nz)
        # 2D Z_AR
        self.assertRaises(ValueError, self.forecaster.forecast_step, input_core_state, wrong_Z_AR, self.seed, self.i_real, self.i_t)

        # Do a simple forecast step
        self.forecaster.forecast_step(input_core_state, Z_AR, self.seed, self.i_real, self.i_t)


class TestAugkfForecasterAR3(unittest.TestCase):
    def setUp(self):
        """
        Setup for testing Forecaster of the Augmented state Kalman Filter algo
        """
        self.do_shear = 0
        conf_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'test_AR3.conf')
        self.config = ComputationConfig(self.do_shear, config_file=conf_path)
        self.nb_reals = 2
        self.attributed_models = np.arange(self.nb_reals)
        self.augkfalgo = create_augkf_algo(self.config, self.nb_reals, 4321, self.attributed_models)
        self.forecaster = self.augkfalgo.forecaster
        self.seed = 0
        self.i_t = 0
        self.i_real = 0

    def test_init(self):
        """
        Tests if the initialisation fails if not all covariances are present.
        """
        # Test init without CholU
        incomplete_augkfalgo = create_augkf_algo(self.config, self.nb_reals, 1234, self.attributed_models)
        incomplete_augkfalgo.cov_prior.pop('Chol')

        self.assertRaises(KeyError, AugkfForecasterAR3, incomplete_augkfalgo)

    def test_forecast_step(self):
        """
        Tests the augkf forecast step
        """
        
        Z_AR = np.random.rand(3,self.augkfalgo.config.Nz)
        # Not a CoreState
        not_a_core_state = np.zeros(self.config.Nb)
        self.assertRaises(TypeError, self.forecaster.forecast_step, not_a_core_state, Z_AR, self.seed, self.i_real, self.i_t)
        
        # Wrong dims of CoreStates
        wrong_dim_B = np.zeros((2, self.config.Nb))
        input_core_state = cs.CoreState({'MF': wrong_dim_B,
                                         'U': np.zeros(self.config.Nu2),
                                         'SV': np.zeros(self.config.Nsv),
                                         'ER': np.zeros(self.config.Nsv),
                                         'Z': np.zeros(self.config.Nz)})
        self.assertRaises(ValueError, self.forecaster.forecast_step, input_core_state, Z_AR, self.seed, self.i_real, self.i_t)

        # Have a proper core_state
        input_core_state.B = np.zeros(self.config.Nb)
        
        wrong_Z_AR = np.random.rand(self.augkfalgo.config.Nz)
        # 1D Z_AR
        self.assertRaises(ValueError, self.forecaster.forecast_step, input_core_state, wrong_Z_AR, self.seed, self.i_real, self.i_t)

        wrong_Z_AR = np.random.rand(2,self.augkfalgo.config.Nz)
        # 2D but no 3 times
        self.assertRaises(ValueError, self.forecaster.forecast_step, input_core_state, wrong_Z_AR, self.seed, self.i_real, self.i_t)
        
        Z_AR = np.random.rand(3,self.augkfalgo.config.Nz)
        # Do a simple forecast step
        self.forecaster.forecast_step(input_core_state, Z_AR, self.seed, self.i_real, self.i_t)


if __name__ == '__main__':
    SUITEA = unittest.TestLoader().loadTestsFromTestCase(TestAugkfForecasterAR1)
    SUITEB = unittest.TestLoader().loadTestsFromTestCase(TestAugkfForecasterAR3)
    SUITE = unittest.TestSuite([SUITEA, SUITEB])
    RES = unittest.TextTestRunner(verbosity=2).run(SUITE)
    import sys

    sys.exit(max(len(RES.failures), len(RES.errors)))
