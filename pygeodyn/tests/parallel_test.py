import unittest
import os.path
import h5py
import numpy as np
import subprocess
import mpi4py
mpi4py.rc(initialize=False, finalize=False)
from mpi4py import MPI

class Parallel(unittest.TestCase):
    """
    Tests if all algos can be run properly on a short time span and with few realisations
    Small note: seeds in parallel processing is tricky to use.
    Event with the same seed, launching pygeodyn now does not give the same result with a different number of 
    processors. if the number of calls of RandomState changes with the number of processors, for instance
    normal_noise = random_state.normal(0, 1, size=dim/n_proc)
    with n_proc the number of processors, then the random generator might be shifted between each process.
    
    WARNING: This test is voluntarily named in such way that unittest.discover put it in its suite, i.e.
             it will not be executed with the command python3 -m unittest discover. It is called independently
             in .gitlab-ci.yml with the command python3 -m unittest pygeodyn.tests.parallel_test
    """
    def setUp(self):
        self.test_dir = os.path.dirname(os.path.abspath(__file__))
        suite = unittest.TestSuite()
        
        # if mpi is invoke (for instance through "from mpi4py import MPI"), this can cause conflicts
        # with the mpi call with subprocess
        if MPI.Is_initialized():
            MPI.Finalize()

    def parallel_single_core(self):

        # define all paths
        conf_file = os.path.join(self.test_dir, 'test_parallel.conf')
        seed = 0
        reals = 2
        run_script = os.path.join(self.test_dir, '../../run_algo.py')
        output_path = os.path.join(self.test_dir, 'dummy_files/')
        name_parallel = 'test_parallel'
        sim_parallel = os.path.join(self.test_dir, os.path.join(output_path, f'{name_parallel}/{name_parallel}.hdf5'))
        
        name_single_core = 'test_single_core'
        sim_single_core = os.path.join(self.test_dir, os.path.join(output_path, f'{name_single_core}/{name_single_core}.hdf5'))

        cmd_single_process = ['python3', run_script, '-m', str(reals), '-conf', conf_file, '-cname', name_parallel, '-path', output_path, '-seed', str(seed)]
        cmd_parallel_process = [run_script, '-m', str(reals), '-conf', conf_file, '-cname', name_single_core, '-path', output_path, '-seed', str(seed)]
        cmd_parallel_process = ['/usr/bin/mpiexec', '-np', '2', '--oversubscribe', 'python3'] + cmd_parallel_process

        # removing previous files
        if os.path.isfile(sim_parallel):
            subprocess.call(f'rm {sim_parallel}', shell=True)
        if os.path.isfile(sim_single_core):
            subprocess.call(f'rm {sim_single_core}', shell=True)

        subprocess.run(cmd_parallel_process, check=True, capture_output=False)
        subprocess.run(cmd_single_process)

        # launch single core simulation
        # subprocess.check_call(cmd_single_process, shell=True)

        self.compare_simu_files(sim_parallel, sim_single_core)

    def compare_simu_files(self, sim1, sim2, abs_tol=1e-4, rel_tol=1e-4):
        """
        Compare if the two simulations files sim1 and sim2 results are equal.
        This test compare, for each measure, if the means of the to simulations
         on all realizations is closer than abs_tol in absolute and rel_tol in relative.
        """
        for measure in ('MF', 'SV', 'ER', 'U'):
            with h5py.File(sim1) as file1, h5py.File(sim2) as file2:
                for state_type in ('forecast', 'analysed', 'computed'):
                    qty1 = np.array(file1[state_type][measure]).mean(axis=0)
                    qty2 = np.array(file2[state_type][measure]).mean(axis=0)
                    self.assertTrue(np.allclose(qty1, qty2, rtol=rel_tol, atol=abs_tol))


if 'parallel_test' in __name__: # when it is called as python3 -m unittest pygeodyn.tests.test_parallel

    def make_suite():
        suite = unittest.TestSuite()
        suite.addTest(Parallel('parallel_single_core'))
        return suite

    suite_parallel = make_suite()
    runner = unittest.TextTestRunner(failfast=True)
    runner.run(suite_parallel)

    # SUITE = unittest.TestLoader().loadTestsFromTestCase(Parallel)
    # RES = unittest.TextTestRunner(verbosity=2).run(SUITE)
    # import sys

    # sys.exit(max(len(RES.failures), len(RES.errors)))
