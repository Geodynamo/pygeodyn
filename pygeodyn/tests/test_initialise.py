import os
import unittest
import pygeodyn.inout.config as config_m


class TestAlgoInit(unittest.TestCase):
    """ Test the Augkf algorithm initialisation. TODO... """

    def setUp(self):
        conf_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'test_diag.conf')
        self.cfg = config_m.ComputationConfig(config_file=conf_path)


if __name__ == '__main__':
        SUITE = unittest.TestLoader().loadTestsFromTestCase(TestAlgoInit)
        RES = unittest.TextTestRunner(verbosity=2).run(SUITE)
        import sys
        sys.exit(max(len(RES.failures), len(RES.errors)))
