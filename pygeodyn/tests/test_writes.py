import os.path
import unittest
import pygeodyn.inout.writes as writes
import pygeodyn.inout.config as config_m


class TestWrites(unittest.TestCase):
    def setUp(self):
        self.do_shear = 0
        conf_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'test_diag.conf')
        self.config = config_m.ComputationConfig(self.do_shear, config_file=conf_path)
        self.algo_dict = {'name': 'augkf', 'nb_reals': 2}

    def test_saveReadme(self):
        not_a_valid_path = '/path/to/nothing/kjhsjhgsdgsd'
        self.assertFalse(os.path.exists(not_a_valid_path))
        git_info = ('mastère', 'super modification', 'version & sha1, 1.0.0-405-abcd1234')
        seed = 0
        result_save = writes.saveReadme(not_a_valid_path, config=self.config, algo_name='test', 
                                        nb_realisations=2, git_info=git_info, elapsed_time=0, parallel=False, seed=seed)
        self.assertEqual(-1, result_save)

        valid_path = './dummy_files'
        if not os.path.exists(valid_path):
            os.mkdir(valid_path)
        self.assertTrue(os.path.exists(valid_path))

        result_save = writes.saveReadme(valid_path, config=self.config, algo_name='test', 
                                        nb_realisations=2, git_info=git_info, elapsed_time=0, parallel=False, seed=seed)
        self.assertEqual(0, result_save)

    def test_saveHandlers(self):
        pass


if __name__ == '__main__':
    SUITE = unittest.TestLoader().loadTestsFromTestCase(TestWrites)
    RES = unittest.TextTestRunner(verbosity=2).run(SUITE)
    import sys

    sys.exit(max(len(RES.failures), len(RES.errors)))
