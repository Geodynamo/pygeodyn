"""
Test the corestate module.

"""

import random
import os.path
import unittest
import numpy as np
from hypothesis import given, assume
from pygeodyn.tests import strategies
from pygeodyn.augkf.algo import create_augkf_algo
from pygeodyn.inout.config import ComputationConfig
from pygeodyn.corestates import CoreState, coef_print
from pygeodyn.inout.reads import read_analysed_states_hdf5

GH_MEASURE_SIZE = 195
TS_MEASURE_SIZE = 720


class TestCoreState(unittest.TestCase):
    """ Test the building of the corestate """

    def setUp(self):
        # Build a 1D full CoreState
        self.do_shear = 0
        self.nb_realisations = 2
        self.attributed_models = np.arange(self.nb_realisations)
        self.full_cs = CoreState({'MF': np.zeros(GH_MEASURE_SIZE),
                                  'U': np.zeros(TS_MEASURE_SIZE),
                                  'SV': np.zeros(GH_MEASURE_SIZE),
                                  'ER': np.zeros(GH_MEASURE_SIZE)})

    @given(strategies.CoreStates(), strategies.integers(), strategies.floats())
    def test_coef_print(self, core_state, int_index, not_int_index):
        """ Test that coef_print throws an error if not given a CoreState or an int"""
        not_core_state_array = np.linspace(0, 10, 20)
        self.assertRaises(TypeError, coef_print, not_core_state_array, 0)

        # Test error if empty CoreState
        empty_core_state = CoreState()
        self.assertRaises(ValueError, coef_print, empty_core_state, 0)
        # Test error if given a non-int
        self.assertRaises(AssertionError, coef_print, core_state, not_int_index)

        if int_index < 0:
            # Test error if given a negative int
            self.assertRaises(AssertionError, coef_print, core_state, int_index)
        else:
            if (int_index >= len(core_state.B) or int_index >= len(core_state.U)) or int_index >= len(core_state.SV):
                # Test return empty string if out of bounds
                self.assertEqual(coef_print(core_state, int_index), '')
            else:
                # Test ok
                result = coef_print(core_state, int_index)
                self.assertTrue(type(result) == str and len(result) != 0)

    @given(strategies.integers(min_value=0, max_value=1000))
    def test_addMeasureMF(self, Lb):
        """Test the adding of m_pygeodyneasures of CoreState
        """
        cs = CoreState()
        # Test with a dim != Lb*(Lb+2)
        wrong_measure_data = np.zeros(Lb*(Lb+2) + 1)
        self.assertRaises(ValueError, cs.addMeasure, 'MF', wrong_measure_data)

        # Test with dim = Lb*(Lb+2)
        right_measure_data = np.zeros(Lb*(Lb+2))
        cs.addMeasure('MF', right_measure_data)
        self.assertIn('MF', cs.measures)
        self.assertEqual(Lb, cs.Lb)

    @given(strategies.integers(min_value=0, max_value=1000))
    def test_addMeasureU(self, Lu):
        cs = CoreState()
        # U dim = 2*Lu*(Lu+2)
        right_measure_data = np.zeros(2*Lu*(Lu+2))
        cs.addMeasure('U', right_measure_data)
        self.assertIn('U', cs.measures)
        self.assertEqual(Lu, cs.Lu)

    @given(strategies.integers(min_value=-1000, max_value=1000))
    def test_addMeasureAny(self, Lany):
        cs = CoreState()
        # Any data: need to set max_degree even if it has no meaning
        any_data = np.zeros(158)
        measure_name = 'ANY'
        cs.addMeasure(measure_name, any_data, Lany)
        self.assertIn(measure_name, cs.measures)
        self.assertEqual(cs._getMaxDegree(measure_name), Lany)
    
    def test_update_corestate(self):
        #Setup random [0 1] and constant [1000] 3D corestate with 3 random measures among : U, B, SV, ER, S
        cs = CoreState()
        cs_empty = CoreState()
        exclude = random.sample(["U","ER","SV","MF","S"], 2)
        for id in ["U","ER","SV","MF","S"]:
            if id == "U" or id == "S":
                N = 720
            else:
                N = 195

            cs.addMeasure(id, np.random.rand(10, 10, N))
            cs_empty.addMeasure(id, np.ones((10, 10, N))*1000) #impossible to be equal to random [0 1]
        
        cs_empty.update_corestate(2, cs[:,5,:], exclude = exclude)

        for id in ["U","ER","SV","MF","S"]:
            if id not in exclude:
                np.testing.assert_array_equal(np.array(cs_empty._measures[id][:,2,:]),np.array(cs._measures[id][:,5,:]))

    def test_getMaxDegree(self):
        cs = CoreState()
        # Test getMaxDegree gives 0 for a measure not set
        not_a_measure = 'YAR'
        self.assertNotIn(not_a_measure, cs.measures)
        self.assertEqual(cs._getMaxDegree(not_a_measure), 0)

    @given(strategies.integers(min_value=0, max_value=GH_MEASURE_SIZE-1), strategies.finite_floats())
    def test_get_set_item(self, index, value):
        # Test dict-like behaviour of get_item
        for measure in self.full_cs.keys():
            np.testing.assert_equal(self.full_cs[measure], self.full_cs.getMeasure(measure))

        # Test get_item & set_item
        truncate = 10
        truncated_cs = self.full_cs[:truncate]
        self.assertEqual(truncated_cs.measures, self.full_cs.measures)
        for measure in truncated_cs.measures:
            self.assertEqual(len(truncated_cs.getMeasure(measure)), truncate)
            np.testing.assert_equal(truncated_cs.getMeasure(measure),
                                    self.full_cs.getMeasure(measure)[:10])
        # Test copy
        copied_cs = self.full_cs.copy()
        self.assertEqual(copied_cs.measures, self.full_cs.measures)
        for measure in self.full_cs.measures:
            np.testing.assert_equal(copied_cs.getMeasure(measure), self.full_cs.getMeasure(measure))

        # Test set_item with index
        assume(value != 0.) # Exclude the value zero to be able to test the fact that full_cs is not updated
        copied_cs[index] = value
        for measure in self.full_cs.measures:
            # Test that item was properly set in the copy
            self.assertEqual(copied_cs.getMeasure(measure)[index], value)
            # Test that the original was not updated
            self.assertNotEqual(self.full_cs.getMeasure(measure)[index], value)

    def test_initialisation_from_file_AR3(self):

        #compute algo from conf file
        conf_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'test_corestate/test_AR3_init_file.conf')
        self.config = ComputationConfig(self.do_shear, config_file=conf_path)
        self.algo = create_augkf_algo(self.config, self.nb_realisations, 0, self.attributed_models)

        # Read the data that will be used for initialisation
        file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'test_corestate/test_AR3_init_file.hdf5')
        file_data = read_analysed_states_hdf5(file_path)

        # Take a random date for initialisation
        i_date = 0
        U = self.algo.config.pcaU_operator.transform(file_data["U"][:, i_date])
        dU = self.algo.config.pcaU_operator.transform_deriv(file_data["dUdt"][:, i_date])
        d2U = self.algo.config.pcaU_operator.transform_deriv(file_data["d2Udt2"][:, i_date])

        Z = np.concatenate((U, file_data["ER"][:, i_date] - self.algo.avg_prior["ER"]),axis=1)
        dZ = np.concatenate((dU, file_data["dERdt"][:, i_date]),axis=1)
        d2Z = np.concatenate((d2U, file_data["d2ERdt2"][:, i_date]),axis=1)

        Z_AR_ref = self.algo.analyser.init_forecast(Z, dZ, d2Z)

        # Initialise from file
        cs, _, _, _, Z_AR = self.algo.init_corestates()
        
        # Check that the Z_AR are the same
        np.testing.assert_allclose(Z_AR, Z_AR_ref, rtol=1.e-10)

        # Check that the initialisation worked
        for measure in ["U", "ER", "SV", "MF"]:
            np.testing.assert_equal(cs[measure][:, 0], file_data[measure][:self.nb_realisations, i_date])
    

    def test_initialisation_from_file_AR1(self):
        
        #compute algo from conf file
        conf_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'test_corestate/test_AR1_init_file.conf')
        self.config = ComputationConfig(self.do_shear, config_file=conf_path)
        self.algo = create_augkf_algo(self.config, self.nb_realisations, 0, self.attributed_models)

        # Read the data that will be used for initialisation
        file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'test_corestate/test_AR1_init_file.hdf5')
        file_data = read_analysed_states_hdf5(file_path)

        # Take a random date for initialisation
        i_date = 0

        # Initialise from file
        cs = self.algo.init_corestates()[0]

        # Check that the initialisation worked
        for measure in ["U", "ER", "SV", "MF"]:
            np.testing.assert_equal(cs[measure][:, 0], file_data[measure][:self.nb_realisations, i_date])
    
    
    def test_initialisation_from_normal_noise_AR3(self):
        
        #compute algo from conf file
        conf_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'test_corestate/test_AR3_init_noise.conf')
        self.config = ComputationConfig(self.do_shear, config_file=conf_path)
        self.algo = create_augkf_algo(self.config, self.nb_realisations, 0, self.attributed_models)

        # Initialise from normal noise
        self.algo.init_corestates()

    def test_initialisation_from_normal_noise_AR1(self):
        
        #compute algo from conf file
        conf_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'test_corestate/test_AR1_init_noise.conf')
        self.config = ComputationConfig(self.do_shear, config_file=conf_path)
        self.algo = create_augkf_algo(self.config, self.nb_realisations, 0, self.attributed_models)

        # Initialise from normal noise
        self.algo.init_corestates()
        

if __name__ == '__main__':
        SUITE = unittest.TestLoader().loadTestsFromTestCase(TestCoreState)
        RES = unittest.TextTestRunner(verbosity=2).run(SUITE)
        import sys
        sys.exit(max(len(RES.failures), len(RES.errors)))
