import h5py
import os.path
import logging
import unittest
import numpy as np
import pygeodyn.run
from pygeodyn.corestates import CoreState
from pygeodyn.tests.strategies import long_test


class TestRunAlgos(unittest.TestCase):
    """
    Tests if all algos can be run properly on a short time span and with few realisations
    """
    def setUp(self):
        self.do_shear = 0
        self.nb_reals = 2
        self.seed = 5
        self.test_dir = os.path.dirname(os.path.abspath(__file__))

    @long_test
    def test_run_diag(self):
        """ diag AR1"""
        c, f, a = pygeodyn.run.algorithm(os.path.join(self.test_dir, 'dummy_files'), 'test_run_diag', os.path.join(self.test_dir, 'test_diag.conf'), self.nb_reals, 
                                                      self.do_shear, log_file=None, logging_level=1, seed=self.seed, algo_name='augkf')
        self.basic_functional_test_resulting_states(c)

    @long_test
    def test_run_AR1(self):
        """ dense AR1"""
        c, f, a = pygeodyn.run.algorithm(os.path.join(self.test_dir, 'dummy_files'), 'test_run_AR1', os.path.join(self.test_dir, 'test_AR1.conf'), self.nb_reals, 
                                                      self.do_shear, log_file=None, logging_level=1, seed=self.seed, algo_name='augkf')
        self.basic_functional_test_resulting_states(c)


    @long_test
    def test_run_AR3(self):
        """ dense AR3"""
        c, f, a = pygeodyn.run.algorithm(os.path.join(self.test_dir, 'dummy_files'), 'test_run_AR3', os.path.join(self.test_dir, 'test_AR3.conf'), self.nb_reals, 
                                                      self.do_shear, log_file=None, logging_level=1, seed=self.seed, algo_name='augkf')
        self.basic_functional_test_resulting_states(c)

    @long_test
    def test_run_obs(self):
        """ GVO obs"""
        c, f, a = pygeodyn.run.algorithm(os.path.join(self.test_dir, 'dummy_files'), 'test_run_obs', os.path.join(self.test_dir, 'test_obs.conf'), self.nb_reals, 
                                                      self.do_shear, log_file=None, logging_level=1, seed=self.seed, algo_name='augkf')
        self.basic_functional_test_resulting_states(c)


    def basic_functional_test_resulting_states(self, computed_states, nb_reals=None):
        """ Basic health checks of the results """
        self.assertTrue(isinstance(computed_states, CoreState))
        for measure_to_test in ['MF', 'SV', 'ER', 'U']:
            self.assertIn(measure_to_test, computed_states.measures)

        nb_reals = nb_reals if nb_reals is not None else self.nb_reals
        self.assertTrue(computed_states.B.shape[0] == nb_reals)

        # Test values of the first coef for which we have a good idea of the lower/upper values
        is_acceptable = lambda arr, low, high: np.all(np.logical_and(low < arr[:, 2:, 0], arr[:, 2:, 0] < high))
        test_value_B = is_acceptable(computed_states.B, -35000., -25000.)
        test_value_SV = is_acceptable(computed_states.SV, -5., 30.)
        test_value_ER = is_acceptable(computed_states.ER, -30., 30.)
        test_value_U = is_acceptable(computed_states.U, -20., 5.)

        if not all((test_value_B, test_value_SV, test_value_ER, test_value_U)):
            err = 'At least one of the following quantities deviate from acceptable values, B, SV, ER, U: {}'
            logging.warning(err.format((test_value_B, test_value_SV, test_value_ER, test_value_U)))
        self.assertTrue(test_value_SV)
        self.assertTrue(test_value_ER)
        self.assertTrue(test_value_B)
        self.assertTrue(test_value_U)


    def functional_test_resulting_states(self, basename):
        """ Health checks of the results using a benchmark file.
         Only works for regular AugKF for now :
            - Pb with PCA of U for dense AR
            - Pb with non controlled noising of GOVO observations for obs
        """
        benchmark_file = os.path.join(self.test_dir, 'benchmarks', basename+'.hdf5')
        output_file = os.path.join(self.test_dir, 'dummy_files', basename, basename+'.hdf5')
        state = 'computed'

        with h5py.File(benchmark_file) as benchmark:
            with h5py.File(output_file) as output:
                for measure_to_test in benchmark[state].keys():
                    with self.subTest(measure=measure_to_test):
                        np.testing.assert_allclose(output[state][measure_to_test], benchmark[state][measure_to_test])


if __name__ == '__main__':
    SUITE = unittest.TestLoader().loadTestsFromTestCase(TestRunAlgos)
    RES = unittest.TextTestRunner(verbosity=2).run(SUITE)
    import sys

    sys.exit(max(len(RES.failures), len(RES.errors)))
