import os
import unittest
import pygeodyn.inout.config as config_m

class TestComputationConfig(unittest.TestCase):
    """ Test the ComputationConfig class """

    def setUp(self):
        """Setup for the test of ComputationConfig
        """
        self.do_shear = 0
        self.conf_path = os.path.join(
            os.path.dirname(os.path.abspath(__file__)), "test_config/test_config.conf"
        )
        pass

    def test_empty_init_raises_ValueError(self):
        """Test setup method raises ValueError if both config_file
        and config_dict are None
        """
        self.assertRaises(ValueError, config_m.ComputationConfig, self.do_shear)

if __name__ == "__main__":
    SUITE = unittest.TestLoader().loadTestsFromTestCase(TestComputationConfig)
    RES = unittest.TextTestRunner(verbosity=2).run(SUITE)
    import sys
    sys.exit(max(len(RES.failures), len(RES.errors)))

