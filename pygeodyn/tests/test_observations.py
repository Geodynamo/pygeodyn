import unittest
import numpy as np
from hypothesis import given
from pygeodyn.tests import strategies
from pygeodyn.inout import observations


class TestObservations(unittest.TestCase):
    @given(strategies.integers(min_value=1, max_value=1000))
    def test_observation_object(self, nb_obs):
        obs_data = np.zeros((10, nb_obs))

        wrong_H = np.zeros((nb_obs//2, 224))
        right_H = np.zeros((nb_obs, 224))

        wrong_R = np.zeros((nb_obs//2, nb_obs//2))
        right_R = np.zeros((nb_obs, nb_obs))

        # Assert errors on wrong data
        self.assertRaises(ValueError, observations.Observation, obs_data, wrong_H, right_R)
        self.assertRaises(ValueError, observations.Observation, obs_data, right_H, wrong_R)
        # Test with right inputs
        right_obs = observations.Observation(obs_data, right_H, right_R)
        self.assertIsInstance(right_obs, observations.Observation)


if __name__ == '__main__':
    SUITE = unittest.TestLoader().loadTestsFromTestCase(TestObservations)
    RES = unittest.TextTestRunner(verbosity=2).run(SUITE)
    import sys

    sys.exit(max(len(RES.failures), len(RES.errors)))
