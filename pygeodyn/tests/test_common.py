import unittest
import numpy as np
from scipy import linalg
from pygeodyn import common
from hypothesis import given
from pygeodyn.tests import strategies
from pygeodyn.constants import r_earth


class TestCommon(unittest.TestCase):
    """ Test class for functions in common """

    def test_compute_direct_obs_operator(self):
        """ Checks that the shape of H is consistent with the input """

        positions = [r_earth, np.pi, 0.]
        max_degree = 10
        H = common.compute_direct_obs_operator(positions, max_degree)
        self.assertEqual(H.shape, (len(positions), max_degree*(max_degree+2)))

    
    def test_compute_diag_AR1_coefs(self):
        """ Test that compute diag AR1 coefs raises errors """
        cov_U = np.identity(10)
        cov_ER = np.identity(10)
        Tau_U = 1
        Tau_E = 1

        # Test 3D
        wrong_cov_U = cov_U.reshape(2,10,5)
        wrong_cov_ER = cov_ER.reshape(2,10,5)
        self.assertRaises(ValueError, common.compute_diag_AR1_coefs, wrong_cov_U, wrong_cov_ER, Tau_U, Tau_E)
        # Test different shape
        wrong_cov_ER = cov_ER.reshape(1,100)
        self.assertRaises(ValueError, common.compute_diag_AR1_coefs, cov_U, wrong_cov_ER, Tau_U, Tau_E)


    def test_compute_derivative(self):
        
        X = np.zeros((1,6,1))
        dt=1
        
        # X 2D
        wrong_X = X.reshape(2,3)
        np.testing.assert_raises(ValueError, common.compute_derivative, wrong_X, dt)
        # X first dim < 3
        wrong_X = X.reshape(3,2,1)
        np.testing.assert_raises(ValueError, common.compute_derivative, wrong_X, dt)
        # check that it runs
        common.compute_derivative(X, dt)


    def test_prep_AR_matrix(self):
        """ Test that prep_AR_matrix raises errors """
        X = np.zeros((4,20))
        dt = 1
        Nt = 3

        # Test 3D
        wrong_X = X.reshape(4,10,2)
        self.assertRaises(ValueError, common.prep_AR_matrix, wrong_X, dt, Nt)
        # Test first dim = 2
        wrong_X = X.reshape(2,40)
        self.assertRaises(ValueError, common.prep_AR_matrix, wrong_X, dt, Nt)


    def test_compute_AR_coefs(self):
        """ Test that compute_AR_coefs raises errors """
        
        X = np.identity(4)
        dt = 1
        Nt = 3
        AR_type = "AR1"
    
        # 1D dX
        wrong_dX = np.ones((4))
        self.assertRaises(ValueError, common.compute_AR_coefs_avg, [[X, wrong_dX, dt, Nt]], AR_type)
        # not same shape X, dX
        wrong_dX = np.identity(5)
        self.assertRaises(ValueError, common.compute_AR_coefs_avg, [[X, wrong_dX, dt, Nt]], AR_type)
        

    def test_compute_AR1_coefs_forecast(self):
        """ Test that compute AR-1 coefs forecast raises an error if wrong args are supplied"""
        Ncoef = 10
        dt_forecast = 1
        A = np.ones((Ncoef,Ncoef))
        Chol = np.tril(np.ones((Ncoef,Ncoef+1)))

        #Matrices not same size
        self.assertRaises(ValueError,common.compute_AR1_coefs_forecast, A, Chol, dt_forecast, Ncoef)
        #No lower triangle Chol
        Chol = np.tril(A.shape).T
        self.assertRaises(ValueError,common.compute_AR1_coefs_forecast, A, Chol.T, dt_forecast, Ncoef)


    def test_compute_AR3_coefs_forecast(self):
        """ Test that compute AR-1 coefs forecast raises an error if wrong args are supplied"""

        Ncoef = 10
        dt_forecast = 1
        A = np.ones((Ncoef,Ncoef))
        B = np.ones((Ncoef,Ncoef))
        C = np.ones((Ncoef,Ncoef+1))
        Chol = np.tril(A)

        #Matrices not same size
        self.assertRaises(ValueError, common.compute_AR3_coefs_forecast, A, B, C, Chol, dt_forecast, Ncoef)
        #No lower triangle Chol
        self.assertRaises(ValueError, common.compute_AR3_coefs_forecast, A, B, C, Chol.T, dt_forecast, Ncoef)


    def test_functional_Kalman_matrix_computation(self):
        """ Test that the function returns the appropriate matrix result """
        Nc = 224
        No = 20
        P = np.random.rand(Nc, Nc)
        H = np.random.rand(No, Nc)
        R = np.random.rand(No, No)

        # K = P*trans(H)*inv(H*P*trans(H)+R)
        PHT = np.matmul(P, np.transpose(H))
        K_by_hand = np.matmul(PHT, linalg.inv(np.matmul(H, PHT) + R))

        K_by_function = common.compute_Kalman_gain_matrix(P, H, R)

        self.assertEqual(K_by_function.shape, (Nc, No))
        np.testing.assert_almost_equal(K_by_function, K_by_hand)


    def test_ar3_process(self):
        """ Test that AR-3 raises an error if wrong args are supplied"""
        N = 100
        X = np.random.rand(3,N)
        A = np.ones((N,N))
        B = np.ones((N,N))
        C = np.ones((N,N))
        Chol = np.tril(A.shape)
        
        # 3D X
        wrong_X = X.reshape((6,2,25))
        self.assertRaises(ValueError, common.ar3_process, wrong_X, A, B, C, Chol)
        # first dim not 3
        wrong_X = X.reshape((15,20))
        self.assertRaises(ValueError, common.ar3_process, wrong_X, A, B, C, Chol)
        # 3D A
        wrong_A = A.reshape((10,1000))
        self.assertRaises(ValueError, common.ar3_process, X, wrong_A, B, C, Chol)
        # Upper Chol
        self.assertRaises(ValueError, common.ar3_process, X, A, B, C, Chol.T)


    def test_ar1_process(self):
        """ Test that AR-1 raises an error if wrong args are supplied"""
        N = 100
        X = np.random.rand(N)
        A = np.ones((N,N))
        Chol = np.tril(A.shape)
        
        # 3D X
        wrong_X = X.reshape((2,2,25))
        self.assertRaises(ValueError, common.ar1_process, wrong_X, A, Chol)
        # 2D X
        wrong_X = X.reshape((5,20))
        self.assertRaises(ValueError, common.ar1_process, wrong_X, A, Chol)
        # 3D A
        wrong_A = A.reshape((10,1000))
        self.assertRaises(ValueError, common.ar1_process, X, wrong_A, Chol)
        # Upper Chol
        self.assertRaises(ValueError, common.ar1_process, X, A, Chol.T)


    @given(strategies.singular_square_matrices(max_size=100))
    def test_Cholesky_inversion(self, non_invertible):
        """
        Test that mat_inv_using_Cholesky raises an error if the matrix is not invertible
        """
        # Check that det = 0
        self.assertEqual(linalg.det(non_invertible), 0)
        # Numpy will also throw a LinAlgError if the input is not definite positive
        self.assertRaises(np.linalg.LinAlgError, common.mat_inv_using_Cholesky, non_invertible)

    def test_functional_Cholesky_inversion(self):
        """ Test that matrix inversion using Cholesky gives the same result as scipy.linalg.inv """

        # The test is done on a random generated matrix meaning it may produce an ill-conditionned matrix so that the test fails for numeric reasons.
        # In consequence, the test is done twice with different generated matrices and must pass at least once.
        result_test = [False, False]

        for i in range(0, 2):
            M = np.zeros((100, 100))

            # Build a random definite-positive invertible matrix
            while linalg.det(M) == 0:
                A = np.random.rand(100, 100)
                M = np.matmul(np.transpose(A), A)

            # Invert using both methods
            invM_Cholesky = common.mat_inv_using_Cholesky(M)
            invM = linalg.inv(M)

            # Store the comparison result in result_test
            result_test[i] = np.allclose(invM, invM_Cholesky, rtol=1e-06)

        # Check if a least one test passed
        self.assertTrue(result_test[0] or result_test[1])


if __name__ == '__main__':
    SUITE = unittest.TestLoader().loadTestsFromTestCase(TestCommon)
    RES = unittest.TextTestRunner(verbosity=2).run(SUITE)
    import sys
    sys.exit(max(len(RES.failures), len(RES.errors)))
