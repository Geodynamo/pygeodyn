import os
import unittest
from pygeodyn.generic.algo import GenericAlgo
from pygeodyn.inout.config import ComputationConfig


class TestGenericAlgo(unittest.TestCase):
    def setUp(self):
        self.do_shear = 0
        conf_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'test_diag.conf')
        self.config = ComputationConfig(self.do_shear, config_file=conf_path)
        self.nb_reals = 2

    def test_init(self):
        # Not a config as arg
        not_a_config = 'not_a_config'
        self.assertRaises(TypeError, GenericAlgo, not_a_config, self.nb_reals)

        # Wrong number of realisations
        bad_nb = -10
        self.assertRaises(ValueError, GenericAlgo, self.config, bad_nb)


if __name__ == '__main__':
    SUITE = unittest.TestLoader().loadTestsFromTestCase(TestGenericAlgo)
    RES = unittest.TextTestRunner(verbosity=2).run(SUITE)
    import sys
    sys.exit(max(len(RES.failures), len(RES.errors)))
