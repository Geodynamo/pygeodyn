import os.path
import unittest
import pygeodyn
import pygeodyn.inout.reads as reads

class TestReads(unittest.TestCase):
    def test_read_observatories_cdf_data(self):
        # Test that an error is raised
        # ...if the measure_type is not valid
        obs_file = 'GObs_4M_19970301T000000_20201101T000000_0101.cdf'
        path_to_data = 'pygeodyn/data/observations/GO-VO_2020_cdf/'
        filepath = os.path.join(pygeodyn._package_directory, os.path.join(path_to_data, obs_file))
        # self.assertTrue(os.path.exists(filepath))
        self.assertRaises(ValueError, reads.read_observatories_cdf_data, filepath, 'Not a valid measure type', 'GO')
        self.assertRaises(ValueError, reads.read_observatories_cdf_data, filepath, 'MF', 'Not a valid obs')
        # ...if the path is not valid (handled by genfromtxt)
        not_a_valid_path = '/not/a/valid/path/kishksdg'
        self.assertFalse(os.path.exists(filepath))
        self.assertRaises(IOError, reads.read_observatories_cdf_data, not_a_valid_path, 'MF', 'GO')


if __name__ == '__main__':
    SUITE = unittest.TestLoader().loadTestsFromTestCase(TestReads)
    RES = unittest.TextTestRunner(verbosity=2).run(SUITE)
    import sys

    sys.exit(max(len(RES.failures), len(RES.errors)))
