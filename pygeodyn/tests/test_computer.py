import os
import unittest
import numpy as np
from pygeodyn import corestates as cs
from pygeodyn.generic.algo import GenericAlgo
from pygeodyn.inout.config import ComputationConfig
from pygeodyn.generic.computer import GenericComputer


class TestGenericComputer(unittest.TestCase):
    def setUp(self):
        self.do_shear = 0
        conf_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'test_diag.conf')
        config = ComputationConfig(self.do_shear, config_file=conf_path)
        algo = GenericAlgo(config, 10)
        self.computer_object = GenericComputer(algo)

    def test_compute_Ab(self):
        """ Test that compute_Ab raises an Error with wrong inputs. """

        # Checks exception is raised if Xb dim !=1
        multidim_core_state = cs.CoreState({'MF': [np.random.rand(1, 20), 1]})
        self.assertRaises(ValueError, self.computer_object.compute_Ab, multidim_core_state, None)


if __name__ == '__main__':
    SUITE = unittest.TestLoader().loadTestsFromTestCase(TestGenericComputer)
    RES = unittest.TextTestRunner(verbosity=2).run(SUITE)
    import sys

    sys.exit(max(len(RES.failures), len(RES.errors)))
