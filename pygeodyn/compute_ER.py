# load required modules

import os
import time
import h5py
import logging
import numpy as np
from mpi4py import MPI
import geodyn_fortran as fortran
from pygeodyn.common import compute_legendre_polys

"""
Compute ER from U and SV. Writes the output file ".npy" file in the prior_dir.
"""

#directory of the script
dir = os.path.dirname(os.path.abspath(__file__))

#computation parameters
Lu = 18
Lb = 13
Lsv = 13
#prior_dir = dir + "/data/priors/0path" #coupled_earth
#prior_type = "0path"                 #coupled_earth
#prior_dir = dir + "/data/priors/50path"  #midpath
#prior_type = "50path"                       #midpath
prior_dir = dir + "/data/priors/71path"   #71path
prior_type = "71path"                        #71path
#prior_dir = dir + "/data/priors/100path"   #100path
#prior_type = "100path"                        #100path

def compute_ER(Lu, Lb, Lsv, prior_dir, prior_type):
    """
    Compute ER for all prior times and save it in prior_dir

    :param Lu: Maximal spherical harmonic degree of the core flow
    :type Lu: int
    :param Lb: Maximal spherical harmonic degree of the magnetic field
    :type Lb: int
    :param Lsv: Maximal spherical harmonic degree of the secular variation
    :type Lsv: int
    :param prior_dir: Directory containing the prior data
    :type prior_dir: str
    :param prior_type: Prior type
    :type prior_type: str
    """

    # to improve performance in parrallel
    os.environ["MKL_NUM_THREADS"] = "1"
    os.environ["NUMEXPR_NUM_THREADS"] = "1"
    os.environ["OMP_NUM_THREADS"] = "1"

    # Init MPI
    if not MPI.Is_initialized():
        MPI.Init()
    comm = MPI.COMM_WORLD
    nb_proc = comm.Get_size()
    rank = comm.Get_rank()

    def first_process():
        return rank == 0

    # Log
    logging_level = logging.INFO
    log_format = '%(asctime)s - Process {} - %(levelname)s - %(message)s'.format(rank)
    logging.basicConfig(format=log_format, level=logging_level)
    
    if first_process:
        begin_time = time.time()
    
    Nu = Lu*(Lu+2)
    Nu2 = 2*Nu
    Nsv = Lsv*(Lsv+2)
    Nb = Lb*(Lb+2)
    tmax = 64

    #read Prior T U MF SV (can be adapted to the user's prior data)
    if prior_type == "71path" or prior_type == "100path" or prior_type == "midpath" or prior_type == "S1dynamo":
        with h5py.File(prior_dir + "/Real.hdf5", "r") as hf:
            T = np.array(hf["times"])
            U = np.array(hf["U"])
            MF = np.array(hf["MF"])
            SV = np.gradient(MF,T,axis=0)

    #select max degree
    tnm = U[:, :int(np.shape(U)[1]/2)]
    snm = U[:, int(np.shape(U)[1]/2):]
    U = np.concatenate([tnm[:, :Nu],snm[:, :Nu]],axis=1)
    MF = MF[:, :Nb]
    SV = SV[:, :Nsv]

    #number of times
    N_times = T.shape[0]
    
    #compute legendre polynomes
    legendre_polys = compute_legendre_polys(tmax, Lb, Lu, Lsv)

    gauss_th = np.asfortranarray(legendre_polys['thetas'])
    gauss_weights = np.asfortranarray(legendre_polys['weights'])

    lpsv = np.asfortranarray(legendre_polys['SV'][0])
    dlpsv = np.asfortranarray(legendre_polys['SV'][1])
    d2lpsv = np.asfortranarray(legendre_polys['SV'][2])
    LLsv = d2lpsv.shape[0]

    lpu = np.asfortranarray(legendre_polys['U'][0])
    dlpu = np.asfortranarray(legendre_polys['U'][1])
    d2lpu = np.asfortranarray(legendre_polys['U'][2])
    LLu = d2lpu.shape[0]

    lpb = np.asfortranarray(legendre_polys['MF'][0])
    dlpb = np.asfortranarray(legendre_polys['MF'][1])
    d2lpb = np.asfortranarray(legendre_polys['MF'][2])
    LLb = d2lpb.shape[0]

    # Build the attribution of times if several processes in parallel
    attributed_times = range(rank, N_times, nb_proc)

    ER = np.zeros((N_times,Nsv)) 

    for i in attributed_times:
        logging.info("Computing ER at time index " + str(i) + " out of " + str(N_times))
        # Function radmats computes Ar(b)^T for the radial induction equation
        #       dBr/dt = Ar(b)*U
        ArbT = np.zeros((Nu2, Nsv), order='F')
        fortran.integral.radmats(gauss_th, gauss_weights, lpsv, dlpsv, d2lpsv,
                                lpu, dlpu, d2lpu, lpb, dlpb, d2lpb,
                                ArbT, MF[i,:],
                                Lsv, Lu, Lb,
                                Nsv, Nu2 // 2, Nb,
                                LLsv, LLu, LLb, tmax)

        Arb = np.transpose(ArbT)

        # SV from unresolved field --> unresolved SV =  total SV - resolved SV
        ER[i, :] = SV[i, :] - np.dot(Arb,U[i, :])

    # Recover all errors by summing all arrays
    # Note that Allreduce updates the array of each process with the result so no need to broadcast afterwards.
    logging.info("Waiting all process to finish computing ER")
    comm.Allreduce(MPI.IN_PLACE, ER, op=MPI.SUM)
    logging.info("Sync done !".format(rank))

    if first_process:
        # Save ER in prior_dir_shear
        np.save(prior_dir + "/ER_Lu_{}_Lb_{}_Lsv_{}.npy".format(Lu,Lb,Lsv), ER)

        end_time = time.time()
        elapsed_time = end_time - begin_time
        logging.info("Elapsed time : {:.2f}".format(elapsed_time))

if __name__ == '__main__':
    compute_ER(Lu, Lb, Lsv, prior_dir, prior_type)

    