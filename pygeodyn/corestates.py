import numpy as np
import scipy as sp
from mpi4py import MPI
from functools import wraps
import pygeodyn.common as common
from pygeodyn.utilities import date_to_decimal
from pygeodyn.inout.reads import read_analysed_states_hdf5

comm = MPI.COMM_WORLD
rank = comm.Get_rank()

class CoreState:
    """
    CoreState object. It stores the measure datas in a dict of ndarrays.
    """
    def __init__(self, init_measures=None):
        """
        Initiates the CoreState. Initial measures can be given in the init_measures arg:
            - Either with only the data (max_degree will be inferred from the data)
                Ex: CoreState({SV: np.zeros(224), ...})
            - Or by giving also the max_degree as a second member of a 2-tuple/list (the first being the data):
                Ex: CoreState({SV: [np.zeros(224), 14], ...})

        :param init_measures: The measures to add in a dict with members can be np.ndarray or 2-tuple/lists.
        :type init_measures: dict or None
        """
        self._measures = {}
        self._max_degrees = {}

        if init_measures is not None and isinstance(init_measures, dict):
            for meas_id, measure_data in init_measures.items():
                # If 2-tuple/list, assume data in first member and Lmax in second
                if len(measure_data) == 2 and (isinstance(measure_data, list) or isinstance(measure_data, tuple)):
                    self.addMeasure(meas_id, measure_data[0], measure_data[1])
                # Else add the data with Lmax inferred from the data
                else:
                    self.addMeasure(meas_id, measure_data)

    def create_CoreState(self):
        """
        Create an instatiation of the class CoreState.
        Convenience function created for inheritance purposes.
        """
        return CoreState()

    def addMeasure(self, meas_id, meas_data, meas_max_degree=None):
        """
        Adds a measure to the CoreState.

        :param meas_id: name of the measure. Used as key of dict for internal storing.
        :type meas_id: str
        :param meas_data: data of the measure.
        :type meas_data: np.ndarray or list
        :param meas_max_degree: Max degree of the measure. If None (default), it will be inferred from the last dimension of the data.
        :type meas_max_degree: int or None
        """
        # Try to infer the max degree from last dimension of data N=L(L+2)
        if meas_max_degree is None:
            if meas_id != 'Z':
                nb_coeffs = meas_data.shape[-1]
                # If a measure derived from U or S, the equation is N/2 = L(L+2)
                if ('U' == meas_id) or ('dUdt' == meas_id) or ('d2Udt2' == meas_id) or ('S' == meas_id):
                    assert nb_coeffs % 2 == 0
                    nb_coeffs = nb_coeffs//2
                computed_Lmax = np.sqrt(nb_coeffs+1) - 1
                if computed_Lmax != int(computed_Lmax):
                    raise ValueError('Last dimension {} of the data given for {} does not lead to an integer max degree ! Got {} instead.'
                                    .format(meas_data.shape[-1], meas_id, computed_Lmax))
                meas_max_degree = int(computed_Lmax)
            else:
                meas_max_degree = -1

        assert isinstance(meas_max_degree, int)
        self._max_degrees[meas_id] = meas_max_degree
        self._measures[meas_id] = meas_data


    def __getitem__(self, item):
        # If the item is a measure name, return the measure (dict-like behaviour)
        if isinstance(item, str):
            return self.getMeasure(item)
        # Else, delegate the __getitem__ to the measures stored to return a sliced CoreState (ndarray-like behaviour)
        new_cs = self.create_CoreState()
        for id_qty, qty in self._measures.items():
            new_cs.addMeasure(id_qty, qty.__getitem__(item), self._max_degrees[id_qty])
        return new_cs

    def __setitem__(self, key, value):
        for meas_id, meas_data in self._measures.items():
            # If the value is a CoreState, set the items measure by measure
            if isinstance(value, CoreState):
                meas_data.__setitem__(key, value.getMeasure(meas_id))
            # Else delegate the __setitem__ to the measures as it is
            else:
                meas_data.__setitem__(key, value)

    def update_corestate(self, i_t, corestate, exclude = ["S"]):
        """
        :param i_t: time_iteration
        :type i_t: integer
        :param corestate: corestate to update from
        :type corestate: Corestate
        :param exclude: list of measures not updated
        :type exclude: list
        """
        for meas_id in self._measures.keys():
            if meas_id not in exclude:
                self._measures[meas_id][:,i_t,:] = corestate._measures[meas_id]

    def copy(self):
        """
        :return: a CoreState with copied measures
        :rtype: CoreState
        """
        new_cs = self.create_CoreState()
        for meas_id, meas_data in self._measures.items():
            new_cs.addMeasure(meas_id, meas_data.copy(), self._max_degrees[meas_id])
        return new_cs

    def mean(self, *args, **kwargs):
        """
        :return: a new CoreState with averaged measures
        :rtype: CoreState
        """
        new_cs = self.create_CoreState()
        for meas_id, meas_data in self._measures.items():
            new_cs.addMeasure(meas_id, meas_data.mean(*args, **kwargs), self._max_degrees[meas_id])
        return new_cs

    def std(self, **kwargs):
        """
        :return: a new CoreState with the standard deviation of the measures
        :rtype: CoreState
        """
        new_cs = self.create_CoreState()
        for meas_id, meas_data in self._measures.items():
            new_cs.addMeasure(meas_id, meas_data.std(**kwargs), self._max_degrees[meas_id])
        return new_cs

    @property
    def measures(self):
        """
        :return: A *sorted* list of the measures of the CoreState
        :rtype: list
        """
        return sorted(self._measures.keys())

    def keys(self):
        """
        Convenience function to use CoreState as a dict

        :return: A *sorted* list of the measures of the CoreState
        :rtype: list
        """
        return self.measures

    def _getMaxDegree(self, measure_type):
        """
        :param measure_type: type of the measure
        :type measure_type: str
        :return: the max degree of the asked type or 0 if the measure is not loaded
        :rtype: int
        """
        # Returns 0 if not in _max_degrees
        if measure_type not in self._max_degrees:
            return 0
        else:
            return self._max_degrees[measure_type]

    @property
    def Lb(self):
        return self._getMaxDegree('MF')

    @property
    def Nb(self):
        return self.Lb*(self.Lb + 2)

    @property
    def Lu(self):
        return self._getMaxDegree('U')

    @property
    def Nu2(self):
        return 2*self.Lu*(self.Lu + 2)

    @property
    def Lsv(self):
        return self._getMaxDegree('SV')

    @property
    def Nsv(self):
        return self.Lsv*(self.Lsv + 2)


    # Getters/Setters for CoreState quantities
    def getMeasure(self, measure_type):
        """
        Generic getter for measures. Raises a KeyError if the measure is not loaded.

        :param measure_type: type of the measure to get
        :type measure_type: str
        :return: the measure data
        :rtype: np.ndarray
        """
        if measure_type not in self._measures:
            raise KeyError('{} is not a valid measure to get from CoreState.'.format(measure_type))
        return self._measures[measure_type]

    def setMeasure(self, measure_type, value):
        """
        Generic setter for measures. Raises a KeyError if the measure is not loaded.

        :param measure_type: type of the measure to get
        :type measure_type: str
        :param value: the new measure data
        :type measure: np.ndarray
        """
        if measure_type not in self._measures:
            raise KeyError('{} is not a valid measure to set from CoreState.'.format(measure_type))
        self._measures[measure_type] = value

    @property
    def B(self):
        """
        Get the magnetic field (B) part of the CoreState.

        :return: Magnetic field data
        :rtype: np.ndarray
        """
        return self._measures['MF']

    @B.setter
    def B(self, mag_field):
        self._measures['MF'] = mag_field

    @property
    def U(self):
        """
        Get the core flow (U) part of the CoreState.

        :return: Core flow data
        :rtype: np.ndarray
        """
        return self._measures['U']

    @U.setter
    def U(self, core_flow):
        self._measures['U'] = core_flow

    @property
    def dUdt(self):
        """
        Get time derivative of core flow (dUdt) part of the CoreState.

        :return: time derivative of core flow
        :rtype: np.ndarray
        """
        return self._measures['dUdt']

    @dUdt.setter
    def dUdt(self, time_derivative_core_flow):
        self._measures['dUdt'] = time_derivative_core_flow

    @property
    def d2Udt2(self):
        """
        Get  second time derivative of core flow (d2Udt2) part of the CoreState.

        :return: second time derivative of core flow
        :rtype: np.ndarray
        """
        return self._measures['d2Udt2']

    @d2Udt2.setter
    def d2Udt2(self, second_time_derivative_core_flow):
        self._measures['d2Udt2'] = second_time_derivative_core_flow

    @property
    def S(self):
        """
        Get the shear (S) part of the CoreState.

        :return: Shear data
        :rtype: np.ndarray
        """
        return self._measures['S']

    @S.setter
    def S(self, shear):
        self._measures['S'] = shear

    @property
    def ER(self):
        """
        Get the subgrid errors (ER) part of the CoreState.

        :return: Subgrid errors data
        :rtype: np.ndarray
        """
        return self._measures['ER']

    @ER.setter
    def ER(self, subgrid_error):
        self._measures['ER'] = subgrid_error

    @property
    def dERdt(self):
        """
        Get the derivative of subgrid errors (dERdt) part of the CoreState.

        :return: derivative of subgrid errors
        :rtype: np.ndarray
        """
        return self._measures['dERdt']

    @dERdt.setter
    def dERdt(self, derivative_subgrid_error):
        self._measures['dERdt'] = derivative_subgrid_error

    @property
    def d2ERdt2(self):
        """
        Get the second derivative of subgrid errors (d2ERdt2) part of the CoreState.

        :return: second derivative of subgrid errors
        :rtype: np.ndarray
        """
        return self._measures['d2ERdt2']

    @d2ERdt2.setter
    def d2ERdt2(self, second_derivative_subgrid_error):
        self._measures['d2ERdt2'] = second_derivative_subgrid_error


    @property
    def Z(self):
        """
        Get the Augmented state (Z) part of the CoreState.

        :return: Coreflow and subgrid errors data
        :rtype: np.ndarray
        """
        return self._measures['Z']

    @Z.setter
    def Z(self, augmented_state):
        self._measures['Z'] = augmented_state

    @property
    def SV(self):
        """
        Get the Core secular variation (SV) part of the CoreState.

        :return: Secular variation data
        :rtype: np.ndarray
        """
        return self._measures['SV']

    @SV.setter
    def SV(self, secular_variation):
        self._measures['SV'] = secular_variation


    def initialise_from_noised_priors(self, algo, random_state=None):
        """
        Initialise the core state Z MF U ER SV at t=0 with a random normal draw around the average priors.
        
        :param algo: algo instance
        :type algo: Augkf.algo
        :param random_state: if not None, is used for the normal draw. sets the B and U part to the average prior.
        :type random_state: numpy.RandomState or None
        :return: nothing. Simply update self.
        """

        # THE CORESTATE HAS A NUMBER OF REALISATIONS THAT MATCHES THE NUMBER OF ATTRIBUTED MODELS

        #Averages
        avg_b = algo.avg_prior['B']
        AR_type = algo.config.AR_type
        Nz = algo.config.Nz
        dt_f = algo.config.dt_f

        # Lower Cholesky matrices
        L_bb = sp.linalg.cholesky(algo.cov_prior['B,B'], lower=True)
        L_zz = sp.linalg.cholesky(algo.cov_prior['Z,Z'], lower=True)
        
        if AR_type == "AR3":
            L_dzdz = sp.linalg.cholesky(algo.cov_prior['dZ,dZ'], lower=True)
            L_d2zd2z = sp.linalg.cholesky(algo.cov_prior['d2Z,d2Z'], lower=True)

        # Set random draw
        if random_state is not None:
            normal_draw = random_state.normal
        else:
            normal_draw = np.random.normal
        
        # Loop over attributed models
        for i_idx, i_real in enumerate(algo.attributed_models):
            # Set normal draw
            w_b = normal_draw(size=algo.config.Nb)
            w_z = normal_draw(size=Nz)

            # Initialise B part of core state by normal distrib N(mean_b, sigma_b)
            self.B[i_idx, 0] = avg_b + np.matmul(L_bb, w_b)
            # Initialise Z part of core state by normal distrib N(0, sigma_z) 
            self.Z[i_idx, 0] = np.matmul(L_zz, w_z)
            # Z to U ER
            self.U[i_idx, 0], self.ER[i_idx, 0] = algo.Z_to_U_ER(self.Z[i_idx, 0], 1)
            # SV = A(b) U + ER
            self.SV[i_idx, 0] = algo.analyser.compute_Ab(self[i_idx, 0]) @ self.U[i_idx, 0] + self.ER[i_idx, 0]

        if AR_type == "AR3":
            # Compute Z_AR3
            Z_AR3 = np.zeros((algo.attributed_models.shape[0],3,Nz))
            w_dz = normal_draw(size=(algo.attributed_models.shape[0],Nz))
            w_d2z = normal_draw(size=(algo.attributed_models.shape[0],Nz))
            # Set dZ d2Z normal distribution
            dZ = w_dz @ L_dzdz.T
            d2Z = w_d2z @ L_d2zd2z.T
            # Taylor series of order 2
            Z_AR3[:, 2] = self.Z[:, 0]
            Z_AR3[:, 1] = self.Z[:, 0] - dt_f * dZ  + dt_f**2 / 2 * d2Z
            Z_AR3[:, 0] = self.Z[:, 0] - (2*dt_f) * dZ  + (2*dt_f)**2 / 2 * d2Z

            return Z_AR3
        else:
            return None


    def initialise_from_file(self, algo):
        """
        Initialise the core state Z MF U ER SV at t=0 from the CoreState in a file at a given date.
        
        :param algo: algo instance
        :type algo: Augkf.algo
        :param file_path: path of the hdf5 file containing the computed states to use for initialisation
        :type file_path: str
        :param date: date of the CoreState to use for the initialisation
        :type date: datetime64
        :return: nothing. Simply update self.
        """

        # THE CORESTATE HAS A NUMBER OF REALISATIONS THAT MATCHES THE NUMBER OF ATTRIBUTED MODELS

        file_path = algo.config.init_file
        decimal_date = algo.config.init_date
        AR_type = algo.config.AR_type
        file_data = read_analysed_states_hdf5(file_path)
        Nz = algo.config.Nz

        date_indexes = (np.where((file_data['times'] <= decimal_date + 0.1) & (file_data['times'] > decimal_date - 0.1)))[0]
        
        if date_indexes.shape[0] == 0:
            raise(ValueError, "{} analysed state not found in {}, check the input date".format(decimal_date, file_path))
        
         # do some checks
        i_date = date_indexes[0]

        for measure in ["MF", "SV", "U", "ER"]:
            # Check that measures match
            assert measure in file_data
            cs_nb_reals = algo.nb_realisations
            file_nb_reals = file_data[measure][:, i_date].shape[0]
            assert cs_nb_reals <= file_nb_reals
            cs_nb_coefs = self._measures[measure][:, 0].shape[1]
            file_nb_coefs = file_data[measure][:, i_date].shape[1]
            assert cs_nb_coefs <= file_nb_coefs

            # If so, initialise with the file_data
            self._measures[measure][:, 0] = file_data[measure][algo.attributed_models, i_date, :cs_nb_coefs]
        
        # build Z
        Z = np.zeros((algo.attributed_models.shape[0],Nz))
        for i_idx, i_real in enumerate(algo.attributed_models):
            if algo.check_PCA():
                # perform PCA
                U = algo.config.pcaU_operator.transform(file_data["U"][i_real, i_date])
            else:
                U = file_data["U"][i_real, i_date] - algo.avg_prior["U"]
            ER = file_data["ER"][i_real, i_date] - algo.avg_prior["ER"]
            Z[i_idx] = np.concatenate((U, ER))
        self._measures["Z"][:, 0] = Z   

        if AR_type == "AR3":
            dt_f = algo.config.dt_f
            
            # If derivatives in file_data
            if 'dUdt' in file_data.keys() and 'dERdt' in file_data.keys() and 'd2Udt2' in file_data.keys() and 'd2ERdt2' in file_data.keys():
                
                Z_AR3 = np.zeros((algo.attributed_models.shape[0],3,Nz))
                for i_idx, i_real in enumerate(algo.attributed_models):
                    #compute dZ d2Z
                    if algo.check_PCA():
                        # perform PCA
                        dUdt = algo.config.pcaU_operator.transform_deriv(file_data["dUdt"][i_real, i_date])
                        d2Udt2 = algo.config.pcaU_operator.transform_deriv(file_data["d2Udt2"][i_real, i_date])
                    else:
                        dUdt = file_data["dUdt"][i_real, i_date]
                        d2Udt2 = file_data["d2Udt2"][i_real, i_date]
                    dERdt = file_data["dERdt"][i_real, i_date]
                    d2ERdt2 = file_data["d2ERdt2"][i_real, i_date]
                    dZdt = np.concatenate((dUdt, dERdt))
                    d2Zdt2 = np.concatenate((d2Udt2, d2ERdt2))
                    # computes Z_AR3
                    # taylor series of order 2
                    Z_AR3[i_idx, 2] = Z[i_idx]
                    Z_AR3[i_idx, 1] = Z_AR3[i_idx, 2] - dt_f * dZdt  + dt_f**2 / 2 * d2Zdt2
                    Z_AR3[i_idx, 0] = Z_AR3[i_idx, 2] - (2*dt_f) * dZdt  + (2*dt_f)**2 / 2 * d2Zdt2
            
            # If no derivatives in file_data NOT TESTED YET
            else:    
                if len(date_indexes) < 3:
                    raise ValueError('Initialisation from file requires at least 3 analysed states in {} but contains only {}'.format(file_path, len(date_indexes)))
                
                # compute dZ and d2Z
                dt_a = file_data['times'][1] - file_data['times'][0]
                dZdt, d2Zdt2 = common.compute_derivative(file_data.Z[algo.attributed_models, i_date - 1 : i_date + 2, :cs_nb_coefs], dt_a)
                
                # compute Z_AR3
                Z_AR3 = np.zeros((algo.attributed_models.shape[0],3,Nz))

                # taylor series of order 2
                Z_AR3[:, 2] = Z
                Z_AR3[:, 1] = Z - dt_f * dZdt  + dt_f**2 / 2 * d2Zdt2
                Z_AR3[:, 0] = Z - (2*dt_f) * dZdt  + (2*dt_f)**2 / 2 * d2Zdt2

            return Z_AR3
        else:
            return None


def with_core_state_of_dimensions(ndim_desired, arg_pos=1):
    """
    Decorator checking if methods have input core state of desired dimensions

    :param ndim_desired: number of desired dimensions for the CoreState
    :type ndim_desired: int
    :param arg_pos: position of the CoreState arg in the decorated function
    :type arg_pos: int
    """
    def checking_decorator(method):
        @wraps(method)
        def checked_method(*args, **kwargs):
            corestate = args[arg_pos]
            if not isinstance(corestate, CoreState):
                raise TypeError('Expected CoreState object in {}, got {} instead'.format(method.__name__, type(corestate)))
            if len(corestate.measures) == 0:
                raise ValueError('Expected {} dimensions for the core_state, got an empty core state instead'.format(ndim_desired))
            # Checks that all measures have the desired dimensions
            for measure_id, measure in corestate._measures.items():
                ndim_measure = measure.ndim
                if measure.ndim != ndim_desired:
                    raise ValueError('Expected {} dimensions for the core_state, got {} instead for {}'.format(ndim_desired, ndim_measure, measure_id))
            return method(*args, **kwargs)
        return checked_method

    return checking_decorator


@with_core_state_of_dimensions(1, arg_pos=0)
def coef_print(core_state_1D, n_coef):
    """
    Convenience function to print the coef of all core state quantities of a certain index. Note that the index should therefore be lower than the length of the smallest quantity.

    :param core_state_1D: 1D Core state with all quantities
    :type core_state_1D: corestates.CoreState
    :param n_coef: index of the coef to print
    :type n_coef: int
    :returns: a string with the 'n_coef'-th coefficients of all measures of the input Corestate
    :rtype: str
    """
    assert type(n_coef) is int and n_coef >= 0, 'Coef_print : {} is not a valid index'.format(n_coef)

    to_print = ''
    # Checks that n_coef is not out of bounds
    if n_coef >= core_state_1D.Nb or (n_coef >= core_state_1D.Nu2 or n_coef >= core_state_1D.Nsv):
        return to_print

    for measure_id in core_state_1D.measures:
        to_print += "{}: {} ".format(measure_id, core_state_1D.getMeasure(measure_id)[n_coef])
    to_print = to_print[:-1]+"\n"
    return to_print
