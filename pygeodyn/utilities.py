#-*- coding: utf-8 -*-
"""
Utility functions
"""
import h5py
import math
import cdflib
import numpy as np
import scipy.interpolate

def decimal_to_date(decimal_date, month_shift=0):
    """
    Converts a decimal date in a datetime64 object.
    Assumes decimal_date = YEAR + (MONTH + SHIFT)/12.

    :param decimal_date: date to convert
    :type decimal_date: float
    :param month_shift: shift of the month. Integer dates have Dec as month with 0, whereas they have Jan with 1.
    :type month_shift: 0 or 1
    :return: date converted
    :rtype: np.datetime64
    """
    year = int(decimal_date)
    month = int(round((decimal_date - year) * 12, 0)) + month_shift
    # For Dec (year + 12/12), the previous formula returns 0 (if shift is 0) so need to treat the case apart
    if month == 0:
        year = year - 1
        month = 12
    return np.datetime64('{}-{:02}'.format(year, month))


def date_to_decimal(date, nb_decimals=None):
    """
    Converts a datetime64 object in a decimal date.
    Returns YEAR + MONTH/12. rounded to the number of decimals given

    :param date: date to convert
    :type date: np.datetime64
    :param nb_decimals: number of decimals for round-off (default: None)
    :type nb_decimals: int or None
    :return: date in decimal format
    :rtype: float
    """
    if date != date: # special case for np.nan
        return np.float('nan')

    year, month = str(date).split('-')
    if nb_decimals is None:
        return int(year) + (int(month)) / 12.
    else:
        return round(int(year) + (int(month)) / 12., nb_decimals)


def cdf_times_to_np_date(times):
    """
    Transform the times in cdf epoch into numpy dates, rounding month to nearest.
    """
    dates = []
    for t in times:
        if t != t:
            # if time is a nan, date should be a nan as well
            dates.append(np.float('nan'))
            continue
        year, month, day = cdflib.cdfepoch.breakdown_epoch(t)[:3]
        if day > 15:
            if month == 12:
                year += 1
                month = 1
            else:
                month += 1
        dates.append(np.datetime64('{}-{:02}'.format(year, month)))
    return dates

def date_array_to_decimal(date_array, nb_decimals=None):
    """
    Uses date_to_decimal to convert a 1D array of datetime64 in a 1D array of floating numbers.

    :param date_array: array to convert
    :type date_array: 1D np.array of np.datetime64
    :param nb_decimals: Number of decimals for round-off (default: None)
    :type nb_decimals: int or None
    :return: Same array with dates as floating numbers
    :rtype: 1D np.array
    """
    return np.array([date_to_decimal(date, nb_decimals) for date in date_array])


def eval_Plm(z, thetas, legendre_poly_values_at_thetas, parity):
    """
    Evaluate the Legendre polynomial at l,m at an arbitrary z using interpolation.

    :param z: Value where the Legendre polynomial must be evaluated
    :type z: float
    :param thetas: List of the angles used for the computation of Legendre polynomials
    :type thetas: np.array
    :param legendre_poly_values_at_thetas: List of the Legendre polynomials value for all cos(thetas) (at fixed l,m) (same dimension as thetas)
    :type legendre_poly_values_at_thetas: np.array
    :return: Interpolated value of the Legendre polynomial at z
    :rtype: float
    """
    # Treat obvious cases first
    if z == 1:
        return 1
    elif z == -1:
        return (-1) ** parity
    else:
        f = interpolate_Plm(thetas, legendre_poly_values_at_thetas, kind='linear')
        return f(z)


def get_degree_order(k):
    """
    Gets the degree, order and coefficient name ("g" or "h") of a coefficient.

    :param k: index of the coefficient
    :type k: int
    :return: degree, order and coef name of the coefficient
    :rtype: int, int, str
    """

    assert (int(k) == k and k >= 0)

    # For m = 0, k = l**2 -1.
    floating_sqrt_result = math.sqrt(k+1)
    degree = int(floating_sqrt_result)
    if degree == floating_sqrt_result:
        return degree, 0, "g"

    # We need now to find m verifying:
    #    for g : k = l**2 + 2m - 2 => 2m = k - l**2 + 2
    # OR for h : k = l**2 + 2m - 1 => 2m = k - l**2 + 1
    twice_order = k - degree*degree + 2
    if twice_order % 2 == 0:
        coef = "g"
    else:
        coef = "h"
        # If twice_order is not divisible by 2, it means that the 2m is in fact (twice_order - 1)
        twice_order = twice_order - 1

    order = twice_order // 2
    return degree, order, coef

def north_geomagnetic_pole_angle(g_10, g_11, h_11):
    """
    Given the first three elements of the spherical harmonic decomposition,
    return the angle theta and phi of the geomagnetic pole.
    Details in:
    Hulot, G., et al. "The present and future geomagnetic field." (2015): 33-78.
    """
    g_vec = np.array([g_10, g_11, h_11])
    m_0 = np.sqrt(g_vec @ g_vec)
    return np.pi - np.arccos(g_10 / m_0), -np.pi + np.arctan2(h_11, g_11)

def geomagnetic_latitude(theta, phi, theta_0, phi_0):
    """
    Computes the geomagnetic (or dipole) latitude.
    Angle must be given in radians.
    theta_0: latitude of the north geomagnetic pole
    phi_0: longitude of the north geomagnetic pole
    """
    cos_term = np.cos(theta) * np.cos(theta_0)
    sin_term = np.sin(theta) * np.sin(theta_0) * np.cos(phi - phi_0)
    return np.arccos(cos_term + sin_term)

def extract_dipole_chaos(chaos_file):
    """
    Extract the coefficients of the dipole (g_10, g_11, h_11) from chaos_file,
    at all available times.
    """
    with h5py.File(chaos_file, 'r') as hf:
        gnms = np.array(hf['gnm'])
        times = np.array(hf['times'])
    return times, gnms[:, :3]

def interpolate_Plm(thetas, legendre_poly_values_at_thetas, l, m, **kwargs):
    # Set fill values for extremal values (-1, 1)
    if m == 0:
        fill_values = ((-1) ** l, 1)
    else:
        fill_values = 0

    return scipy.interpolate.interp1d(np.cos(thetas), legendre_poly_values_at_thetas, bounds_error=False,
                                      fill_value=fill_values, **kwargs)


def zonal_indexes(max_degree):
    """
    Generates the indexes of zonal coefficients t_n^0.

    :param max_degree: Maximum value of n
    :type max_degree: int
    """
    for n in range(max_degree):
        yield n*(n+2)


def zonal_mask(max_degree):
    """
    Generates a mask to select indexes of zonal coefficients t_n^(m=0).

    :param max_degree: Maximum value of n
    :type max_degree: int
    """
    Nu2 = 2 * max_degree * (max_degree + 2)
    zonal_mask = np.zeros(Nu2, bool)  # False everywhere...
    zonal_mask[[i_z for i_z in zonal_indexes(max_degree)]] = True  # ...except for zonal indexes
    return zonal_mask


def non_zonal_mask(max_degree):
    """
    Generates a mask to select indexes of non zonal coefficients t_n^(m!=0).

    :param max_degree: Maximum value of n
    :type max_degree: int
    """
    return np.logical_not(zonal_mask(max_degree))


def spectral_to_znz_matrix(max_degree):
    """
    Generates the permutation matrix P linking U stored in spectral order to U stored in zonal-non_zonal order

    znz_U = P @ spectral_U

    :param max_degree: Maximum value of n
    :type max_degree: int
    :return: Matrix linking spectral U to zonal-non_zonal U
    :rtype: np.array (dim: Nu2 x Nu2)
    """
    zonals = zonal_mask(max_degree)
    non_zonals = non_zonal_mask(max_degree)
    Nu2 = len(zonals)

    P = np.zeros((Nu2, Nu2))
    # Link the first members to zonal coefs
    P[[i for i in range(max_degree)], zonals] = 1
    # Link the rest to non zonal coefs
    P[[i for i in range(max_degree, Nu2)], non_zonals] = 1

    return P

def get_seeds_for_obs(seed, obs_type_to_use):
    """
    Get a seed for each observatory to noise the MF or SV data
    """
    # maximum number of seeds is equal to 2 times the number of keys in the dict
    # even are for MF, odds for SV
    np.random.seed(seed)
    seeds = np.random.randint(0, 50000, size=len(obs_type_to_use.keys())*2)
    seeds_dict = {}
    for i, (obs, measure) in enumerate(obs_type_to_use.items()):
        if type(measure) == str: # if keys of obs_select are written in the simple form 'MF'
            seeds_dict[obs, measure] = seeds[2*i]
        else: # else the keys of obs_select are written as ('MF',) or ('MF', 'SV')
            seeds_dict[obs, measure[0]] = seeds[2*i]
            if len(measure) > 1:
                seeds_dict[obs, measure[1]] = seeds[2*i+1]
    return seeds_dict
