import os

# to improve performance in parrallel
os.environ["MKL_NUM_THREADS"] = "1"
os.environ["NUMEXPR_NUM_THREADS"] = "1"
os.environ["OMP_NUM_THREADS"] = "1"

import time
import h5py
import logging
import subprocess
import numpy as np
from mpi4py import MPI
from .inout import writes
from . import corestates as cs
from .augkf.algo import create_augkf_algo
from .inout.config import ComputationConfig
from .shear.shear_algo import ShearInversion

def send_receive(corestate, comm, rank, handler, attributed_models):
    """
    Gather corestate to rank 0 (and broadcast to all ranks if do_bcast=True) 
    without comm.Gather that had issues with large matrices

    :param corestate: Corestate to be gathered
    :type corestate: Cs.Corestate
    :param comm: MPI communicator
    :type comm: MPI.comm
    :param rank: process rank
    :type rank: int
    :param handler: hdf5 handler
    :param attributed_models: hdf5 handler
    :type attributed_models: 1D numpy array
    """
    if rank == 0:
        handler.update_all(corestate, attributed_models)

    for i in range(1,comm.Get_size()):
        if rank == i:
            comm.send(corestate, dest=0, tag=0)
            comm.send(attributed_models, dest=0, tag=1)
        if rank == 0:
            corestate_recv = comm.recv(source=i, tag=0)
            attributed_models_recv = comm.recv(source=i, tag=1)
            handler.update_all(corestate_recv, attributed_models_recv)
        comm.Barrier()

def gather_states(corestate, attributed_models, comm, rank, do_bcast=True):
    """
    Gather corestate to rank 0 (and broadcast to all ranks if do_bcast=True)

    :param corestate: Corestate to be gathered
    :type corestate: Cs.Corestate
    :param attributed_models: models handled by rank process 
    :type attributed_models: 1D numpy array
    :param comm: MPI communicator
    :type comm: MPI.comm
    :param rank: process rank
    :type rank: int
    :param do_bcast: Controls whether the gathered corestate is broadcasted to all process
    :type do_bcast: boolean
    """
    #synchronyze all processes
    comm.Barrier()
    #gather from all cores to rank 0
    attributed_models_gather = comm.gather(attributed_models, root=0)
    #in rank 0
    if rank == 0:
        # concatenate the contribution from all cores
        attributed_models_gather_concat = np.array(attributed_models_gather[0])
        for i in range(1,len(attributed_models_gather)):
            attributed_models_gather_concat = np.concatenate((attributed_models_gather_concat,attributed_models_gather[i]),axis=0)
        #get sorting indices
        attributed_models_gather_sorted = np.argsort(attributed_models_gather_concat)
    #for each measure in corestate
    for meas in corestate._measures:
        #synchronyze all processes
        comm.Barrier()
        #gather from all cores to rank 0
        X_gather = comm.gather(corestate._measures[meas], root=0)
        #in rank 0
        if rank == 0:
            X_gather_concat = np.array(X_gather[0])
            for i in range(1,len(X_gather)):
                X_gather_concat = np.concatenate((X_gather_concat,X_gather[i]),axis=0)
            #order X_gather_concat using the sorting indices
            X_gather_concat = X_gather_concat[attributed_models_gather_sorted]
        else:
            X_gather_concat = None
        if do_bcast:
            #synchronyze all processes
            comm.Barrier()
            #broadcast state from rank 0 to all ranks
            corestate._measures[meas] = comm.bcast(X_gather_concat, root=0)
        else:
            #synchronyze all processes
            comm.Barrier()
            corestate._measures[meas] = X_gather_concat
    return corestate

def choose_algorithm(algo_name, config_file, nb_models, global_seed, attributed_models, do_shear):
    if algo_name == 'augkf':
        logging.info('Using augmented state Kalman filter algorithm')
        return create_augkf_algo(ComputationConfig(do_shear, config_file), nb_models, global_seed, attributed_models)
    else:
        raise ValueError('Algorithm name {} was not recognised. Aborting computation.'.format(algo_name))

def algorithm(output_path, computation_name, config_file, nb_models, do_shear, seed=None, log_file=None, logging_level=None, algo_name='augkf'):
    """
    Runs the chosen algorithm : takes care of running the forecasts, the analysis, of logging and of saving the data

    :param output_path: path where the data should be saved
    :type output_path: str
    :param computation_name: will create a folder of this name to store the output files
    :type computation_name: str
    :param config_file: path to the configuration file
    :type config_file: str
    :param nb_models: number of realisations/models to consider
    :type nb_models: int
    :param do_shear: control parameter of the shear computation
    :type do_shear: int
    :param seed: seed to use for theCs.Corestateel (https://docs.python.org/3.6/library/logging.html?highlight=logging#levels)
    :param algo_name: name of the algorithm. Supported algorithms is 'augkf'
    :type algo_name: str
    :return: CoreStates containing all the results of the computation, the forecasts and analysis.
    :rtype: CoreState, CoreState, CoreState
    """
    # Init MPI variables
    if not MPI.Is_initialized():
        MPI.Init()
    comm = MPI.COMM_WORLD
    nb_proc = comm.Get_size()
    rank = comm.Get_rank()
    
    def first_process():
        return rank == 0
    
    def compute_shear():
        return do_shear == 1
    
    # set output folder
    folder_name = os.path.join(output_path, computation_name)
    os.makedirs(folder_name, exist_ok=True)

    # Set log
    if logging_level is None:
        logging_level = logging.INFO
    log_format = '%(asctime)s - Process {} - %(levelname)s - %(message)s'.format(rank)
    if log_file is None:
        logging.basicConfig(format=log_format, level=logging_level)
        print("Logs will be displayed in the console.")
    else:
        logging.basicConfig(format=log_format, level=logging_level,
                            filename=os.path.join(folder_name, log_file))
        print("Logs will be saved in {}".format(os.path.join(folder_name, log_file)))
    
    # Start time
    begin_time = time.time()

    # INITIALISATION OF ALGO

    # Set seed
    process_seeds = None
    if first_process():
        if seed is None:
            seed = np.random.randint(0, 50000)
        logging.info("Global seed = {}".format(seed))
        # Get sha1 and human readable info about git commit and branch
        current_dir = os.path.dirname(os.path.realpath(__file__))
        git_branch = (subprocess.check_output(["git", "rev-parse", "--abbrev-ref", "HEAD"], cwd=current_dir).strip()).decode("utf-8")
        git_commit_mess = subprocess.check_output(["git", "log", "-1", "--pretty=%B"], cwd=current_dir).strip().decode("utf-8")
        git_description = subprocess.check_output(["git", "describe", "--always"], cwd=current_dir).strip().decode("utf-8")
        logging.info("Git branch: " + git_branch)
        logging.info("Git last commit message: \"" + git_commit_mess + "\" and description: " + git_description)
        # Generate a seed for each MPI processes from the global seed
        # If the same seed was given to each process, then the same random numbers would appear 
        # on each process
        process_seeds = np.random.RandomState(seed).randint(0, 50000, size=nb_proc)

    # Broadcast the process seed for each MPI process
    seed = comm.bcast(seed, root=0)
    process_seeds = comm.bcast(process_seeds, root=0)
    pseed = process_seeds[rank]
    logging.info("Process {} seed = {}".format(rank, pseed))
    # Initialise the random state using the process seed
    process_rstate = np.random.RandomState(pseed)

    # Build the attribution of realisations if several processes in parallel
    attributed_models = np.array(list(range(rank, nb_models, nb_proc)))
    logging.debug('Process {} will process realisations: {}'.format(rank, list(attributed_models)))
    
    # Set algo
    algo = choose_algorithm(algo_name, config_file, nb_models, pseed, attributed_models, do_shear)

    # Initialization of states is done in each process 
    # Each process has its own attributed models so that there is less transfer of arrays
    computed_states, forecast_states, analysed_states, misfits, Z_AR3 = algo.init_corestates(random_state=process_rstate)
    
    if first_process():
        logging.info("Forecast will be performed at following times: {}"
                    .format(algo.config.t_forecasts))

        logging.info("Analyses will be performed at following times: {}"
                        .format(algo.config.t_analyses))
    
    # LAUNCH THE ALGORITHM
    # INIT
    i_t = 0
    i_analysis = 0
    ratio = algo.config.dt_a_f_ratio
    idx_max = algo.config.t_forecasts.shape[0] - 1

    # Loop over all time indices
    while i_t < idx_max:

        t = algo.config.t_forecasts[i_t]
        
        # PREPARE
        # Check if obs data is available and if next analysis on mf and/or sv is performed
        algo.analyser.check_if_analysis_data(i_analysis, do_backward=False)

        if bool(algo.config.last_analysis_backward) is True:
            if np.abs(t - algo.config.t_analyses[-2]) < algo.config.dt_f / 2:
                logging.info("Preparing states for backward analysis scheme.")
                algo.analyser.check_if_analysis_data(i_analysis, do_backward=True)
        
        # Adapt forecast range R to eventual analysis and AR type
        if algo.analyser.sv_analysis() and algo.config.AR_type == "AR3":
            R = 2*ratio
        else:
            R = ratio
        if i_t + R > idx_max:
            R = idx_max - i_t
        
        # FORECAST
        for i in range(R):
            # Increment i_t
            i_t += 1
            t = algo.config.t_forecasts[i_t]

            # Compute forecast 
            if algo.config.AR_type == "AR3":   
                forecast_states[:, i_t], Z_AR3 = algo.forecaster.parallel_forecast_step(computed_states[:,i_t-1], Z_AR3, pseed, i_t)
            else:
                forecast_states[:, i_t] = algo.forecaster.parallel_forecast_step(computed_states[:,i_t-1], pseed, i_t)
            
            # Update the computed core_state array with the forecast result
            computed_states[:, i_t] = forecast_states[:, i_t]
        
        # ANALYSIS
        if algo.analyser.sv_analysis() and algo.config.AR_type == "AR3":
            # Set i_t back from t_a+ to t_a
            i_t = i_t - ratio
            t = algo.config.t_forecasts[i_t]
        
        # If at least mf or sv analysis
        if algo.analyser.mf_analysis() or algo.analyser.sv_analysis():
            logging.info("Starting analysis #{} at time {}...".format(i_analysis+1, t))

            # Compute analysis
            if algo.config.AR_type == "AR3":
                
                # Gather computed_states to get all realisations for analysis

                # Issue #79
                # Doing backward analysis step for the last analysis
                # Avoiding biased forecast
                if bool(algo.config.last_analysis_backward) is True and np.abs(t - algo.config.t_analyses[-1]) < algo.config.dt_f / 2:
                    logging.warning("Performing an analysis using backward scheme.")
                    gather_computed_states = gather_states(computed_states[:, i_t - 2 * ratio:i_t + 1], attributed_models, comm, rank)
                    analysed_states[:, i_analysis], Z_AR3 = algo.analyser.analysis_step(gather_computed_states, Z_AR3, do_backward=True)
                else:
                    gather_computed_states = gather_states(computed_states[:, i_t - ratio:i_t + ratio + 1], attributed_models, comm, rank)
                    analysed_states[:, i_analysis], Z_AR3 = algo.analyser.analysis_step(gather_computed_states, Z_AR3, do_backward=False)
            else:
                gather_computed_states = gather_states(computed_states[:, i_t], attributed_models, comm, rank)
                analysed_states[:, i_analysis] = algo.analyser.analysis_step(gather_computed_states)

            if first_process():
                print('A', t, cs.coef_print(analysed_states[0, i_analysis], 0))
            
            # Update the computed core_state array with the analysis result (overwrites forecast result)
            computed_states[:, i_t] = analysed_states[:, i_analysis]

            # Update the misfits
            for measure in misfits.measures:
                misfits[measure][:, i_analysis] = algo.get_current_misfits(measure)
            
            logging.info("Analysis #{} finished !".format(i_analysis+1))

            # Issue #70
            # Exporting data
            if bool(algo.config.export_every_analysis) is True:
                logging.info("Exporting...")

                if first_process():
                    # Creating file
                    output_file = h5py.File(os.path.join(folder_name, computation_name)+'.hdf5', mode='w')
                    algo.config.save_hdf5(output_file)

                    # Saving data (only `computed` and `misfits`)
                    if algo.config.out_computed:
                        computed_handler = writes.Hdf5GroupHandler(
                            algo.config.t_forecasts, nb_models, computed_states, output_file, "computed", algo.config.out_format, 
                            exclude=["Z","S","dUdt","dERdt","d2Udt2","d2ERdt2"]
                        )
                    if algo.config.out_misfits: 
                        misfits_handler = writes.Hdf5GroupHandler(
                            algo.config.t_analyses, 1, misfits, output_file, "misfits", algo.config.out_format
                        ) 
                        misfits_handler.update_all(misfits, [0])

                    # Closing
                    output_file.close()

                logging.info("Done!")
                logging.info(f"Data was saved inside {os.path.join(folder_name, computation_name) + '.hdf5'}")
        
        if i_analysis < algo.config.nb_analyses:
            if abs(t - algo.config.t_analyses[i_analysis]) < algo.config.dt_f/2: # if analysis time
                # Update the analysed core_state array with the computed core_state
                analysed_states[:, i_analysis] = computed_states[:, i_t]
                # Increment i_analysis
                i_analysis += 1
        
    # END OF ALGO TIME LOOP

    # START SHEAR COMPUTATION
    if compute_shear():
        
        logging.info("Initializing shear inversion")

        # Init the Class ShearInversion
        shear = ShearInversion(algo.config)

        # Precompute operators
        shear.precompute_operators()

        # Build prior
        (
            Pee_y1_tilda_with_glasso,
            Pee_y2_tilda_with_glasso,
            Pss_inv_with_glasso
        ) = shear.build_prior()
        
        # - Sign Toroidal convention for shear
        U_state = np.copy(analysed_states.U)
        U_state[:, :, :algo.config.Nu] = -U_state[:, :, :algo.config.Nu]

        # Loop over analyses
        for i_t, t in enumerate(algo.config.out_analysis):
            
            logging.info("Starting shear computation #{} at time {}...".format(i_t, t))

            store_shear = np.zeros((analysed_states.S[:, 0,:].shape))
            
            # Compute shear and store every realisation
            for i_idx, i_real in enumerate(attributed_models):
                if i_t == 0:
                    DU = (U_state[i_idx, i_t+1] - U_state[i_idx, i_t]) / (algo.config.dt_a)
                elif i_t == algo.config.out_analysis.shape[0] - 1:
                    DU = (U_state[i_idx, i_t] - U_state[i_idx, i_t-1]) / (algo.config.dt_a)
                else :
                    DU = (U_state[i_idx, i_t+1] - U_state[i_idx, i_t-1]) / (2*algo.config.dt_a)
                    
                U = U_state[i_idx, i_t,:]
                MF = analysed_states.B[i_idx, i_t]
                SV = analysed_states.SV[i_idx, i_t]
                store_shear[i_idx,:] = shear.compute_shear(U, DU, MF, SV, Pee_y1_tilda_with_glasso, Pee_y2_tilda_with_glasso, Pss_inv_with_glasso)
            
            analysed_states.S[:, i_t] = store_shear

            if first_process():
                print('A', t, cs.coef_print(analysed_states[0, i_t], 0))

    # END SHEAR COMPUTATION
    
    if first_process():
        # Create FileHandlers for saving
        output_file = h5py.File(os.path.join(folder_name, computation_name)+'.hdf5', mode='w')
        algo.config.save_hdf5(output_file)
        # Save data if not done beforehand
        if algo.config.out_computed:
            computed_handler = writes.Hdf5GroupHandler(
                algo.config.t_forecasts, nb_models, computed_states, output_file, "computed", algo.config.out_format, 
                exclude=["Z","S","dUdt","dERdt","d2Udt2","d2ERdt2"])
        if algo.config.out_forecast:
            forecast_handler = writes.Hdf5GroupHandler(
                algo.config.t_forecasts, nb_models, forecast_states, output_file, "forecast", algo.config.out_format, 
                exclude=["Z","S","dUdt","dERdt","d2Udt2","d2ERdt2"])
        if algo.config.out_analysis:
            analysis_handler = writes.Hdf5GroupHandler(
                algo.config.t_analyses, nb_models, analysed_states, output_file, "analysed", algo.config.out_format, 
                exclude=["Z"])
        if algo.config.out_misfits: 
            misfits_handler = writes.Hdf5GroupHandler(
                algo.config.t_analyses, 1, misfits, output_file, "misfits", algo.config.out_format) 
            misfits_handler.update_all(misfits, [0]) 
    else:
        computed_handler, forecast_handler, analysis_handler= None, None, None

    if algo.config.out_computed:
        send_receive(computed_states, comm, rank, computed_handler, attributed_models)
    if algo.config.out_forecast:
        send_receive(forecast_states, comm, rank, forecast_handler, attributed_models)
    if algo.config.out_analysis:
        send_receive(analysed_states, comm, rank, analysis_handler, attributed_models)

    # Final saving tasks (closing files and writing readme)
    if first_process():
        # End time
        end_time = time.time()
        elapsed_time = end_time - begin_time
        logging.info("Elapsed time : {:.2f}".format(elapsed_time))

        # Close file handlers
        output_file.close()

        logging.info("Data are saved in {}".format(os.path.join(folder_name, computation_name)+'.hdf5'))

        git_info = (git_branch, git_commit_mess, git_description)

        # Write Readme file
        writes.saveReadme(folder_name, algo.config, algo.name, nb_models, git_info, elapsed_time, nb_proc, seed)
        
        return computed_states, forecast_states, analysed_states
    else:
        return None, None, None