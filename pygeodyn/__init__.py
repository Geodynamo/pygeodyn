import os.path
from pygeodyn._version import __version__

_package_directory = os.path.join(os.path.dirname(os.path.realpath(__file__)))
