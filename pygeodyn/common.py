#!/usr/bin/python3

""" Functions that are common to all algo steps. """

import math
import numpy as np
from . import utilities
from scipy import linalg
from .constants import r_earth
import geodyn_fortran as fortran

def compute_direct_obs_operator(positions, max_degree, internal_source=True, eps=1e-7):
    """
    Computes the direct observation operator H of a measure of given max degree for a list of given positions.

    :param positions: List of the positions stored under the form [r1, th1, ph1, r2, th2, ph2 ...]
    :type positions: list or 1D numpy.ndarray
    :param max_degree: Maximal degree of the observed measure
    :type max_degree: int
    :param internal_source: whether the Gauss coefficient represent an internal or an external source (a/r or r/a dependency)
    :type internal_source: bool
    :return: the direct observation operator H
    :rtype: 2D numpy.ndarray (nb_observations x nb_coefs)
    """

    No = len(positions)
    Ncoefs = max_degree * (max_degree + 2)

    H = np.zeros([No, Ncoefs])

    for i in range(0, No, 3):
        r = positions[i]
        th = positions[i + 1]
        ph = positions[i + 2]
        H[i:i+3, :] = compute_lines_direct_obs_operator(r, th, ph, internal_source, eps, max_degree)
    return H

def compute_lines_direct_obs_operator(r, th, ph, internal_source, eps, max_degree):
    """
    Convenience function, as most of the the observations positions are similar through time (GVOs are unchanged for each satellite era),
    a cache is used to avoid computing the same coefficients for each time step.
    """
    Ncoefs = max_degree * (max_degree + 2)
    r_th_ph_coefs = np.zeros(shape=(3, Ncoefs))
    # Compute prefactors
    if internal_source:
        radius_to_earth = r_earth / r

    sin_th = math.sin(th)
    if abs(sin_th) < eps: # theta is either O or pi
        sin_th_0 = True
        sign_cos_th = np.sign(math.cos(th))
    else:
        sin_th_0 = False

    # Evaluate the Legendre coeffs at a given th using the Fortran function
    plm, dplm, d2plm = eval_Plm_at_theta(th, max_degree)
        # computed_plms[th] = plm
        # computed_dplms[th] = dplm

    for k in range(0, Ncoefs):
        l, m, coef = utilities.get_degree_order(k)
        k_legendre = (l + 1) * l // 2 + m

        Plm_at_z = plm[k_legendre]
        dPlm_at_z = dplm[k_legendre]

        cos_mph = math.cos(m * ph)
        sin_mph = math.sin(m * ph)

        if internal_source:
            if coef == "g":
                r_th_ph_coefs[0, k] = (l + 1) * radius_to_earth ** (l + 2) * Plm_at_z * cos_mph
                r_th_ph_coefs[1, k] = - radius_to_earth ** (l + 2) * dPlm_at_z * cos_mph
                if sin_th_0:
                    assert sin_mph == 0 or Plm_at_z == 0, 'One of these values should be 0, {}, {}'.format(sin_mph, Plm_at_z)
                    r_th_ph_coefs[2, k] = radius_to_earth ** (l + 2) * m * sign_cos_th * dPlm_at_z * sin_mph
                else:
                    r_th_ph_coefs[2, k] = radius_to_earth ** (l + 2) * m * Plm_at_z * sin_mph / sin_th
            else:
                r_th_ph_coefs[0, k] = (l + 1) * radius_to_earth ** (l + 2) * Plm_at_z * sin_mph
                r_th_ph_coefs[1, k] = -radius_to_earth ** (l + 2) * dPlm_at_z * sin_mph
                if sin_th_0:
                    assert cos_mph == 0 or Plm_at_z == 0, 'One of these values should be 0, {}, {}'.format(cos_mph, Plm_at_z)
                    r_th_ph_coefs[2, k] = -radius_to_earth ** (l + 2) * m * sign_cos_th * dPlm_at_z * cos_mph
                else:
                    r_th_ph_coefs[2, k] = -radius_to_earth ** (l + 2) * m * Plm_at_z * cos_mph / sin_th
    return r_th_ph_coefs

def compute_direct_obs_operator_SOLA(r, th, ph, max_degree, internal_source=True, eps=1e-7):
    """
    Computes the direct observation operator H of a measure of given max degree for a list of given positions.

    :param positions: List of the positions stored under the form [r1, th1, ph1, r2, th2, ph2 ...]
    :type positions: list or 1D numpy.ndarray
    :param max_degree: Maximal degree of the observed measure
    :type max_degree: int
    :param internal_source: whether the Gauss coefficient represent an internal or an external source (a/r or r/a dependency)
    :type internal_source: bool
    :return: the direct observation operator H
    :rtype: 2D numpy.ndarray (nb_observations x nb_coefs)
    """

    No = r.shape[0]
    Ncoefs = max_degree * (max_degree + 2)

    H = np.zeros([No, Ncoefs])

    for i in range(No):
        H[i, :] = compute_lines_direct_obs_operator_SOLA(r[i], th[i], ph[i], internal_source, eps, max_degree)
    return H

def compute_lines_direct_obs_operator_SOLA(r, th, ph, internal_source, eps, max_degree):
    Ncoefs = max_degree * (max_degree + 2)
    r_th_ph_coefs = np.zeros(Ncoefs)
    # Compute prefactors
    if internal_source:
        radius_to_earth = r_earth / r

    # Evaluate the Legendre coeffs at a given th using the Fortran function
    plm, dplm, d2plm = eval_Plm_at_theta(th, max_degree)
        # computed_plms[th] = plm
        # computed_dplms[th] = dplm

    for k in range(0, Ncoefs):
        l, m, coef = utilities.get_degree_order(k)
        k_legendre = (l + 1) * l // 2 + m

        Plm_at_z = plm[k_legendre]

        cos_mph = math.cos(m * ph)
        sin_mph = math.sin(m * ph)

        if internal_source:
            if coef == "g":
                r_th_ph_coefs[k] = (l + 1) * radius_to_earth ** (l + 2) * Plm_at_z * cos_mph
            else:
                r_th_ph_coefs[k] = (l + 1) * radius_to_earth ** (l + 2) * Plm_at_z * sin_mph
    return r_th_ph_coefs


def sample_timed_quantity(times, X_quantity, sampling_dt):
    """
    Samples a timed quantity according to sampling_dt.
    First, generates sampling times from the times array and then evaluates the X quantity at these times using interpolation.

    :param times: times of X quantity
    :type times: 1D numpy.array (dim: Ntimes)
    :param X_quantity: array containing values of the quantity X at times
    :type X_quantity: 2D numpy.array (dim: Ntimes x Ncoefs)
    :param sampling_dt: time step to use for the sampling (in years)
    :type sampling_dt: float
    :return: 2-tuple containing the sampling times and the quantity X evaluated at these sampling times
    :rtype: 1D numpy.array (dim: Nsamples), 2D numpy.array (dim: Nsamples x Ncoefs)
    """
    nb_coeffs = X_quantity.shape[1]
    sampling_times = np.arange(times.min(), times.max(), sampling_dt)
    nb_samples = len(sampling_times)
    sampled_X = np.zeros(shape=(nb_samples, nb_coeffs))

    # TODO Replace by scipy method ?
    for i_coef in range(nb_coeffs):
        sampled_X[:, i_coef] = np.interp(sampling_times, times, X_quantity[:, i_coef])

    return sampling_times, sampled_X


def compute_legendre_polys(tmax, Lb, Lu, Lsv):
    """
    Computes the coefficients of Legendre polynomials, first derivative and second derivative of B, U and SV using a wrapped Fortran function.

    :param tmax: Number of angles for computation
    :type tmax: int
    :param Lb: Max degree of magnetic field
    :type Lb: int
    :param Lu: Max degree of core flow
    :type Lu: int
    :param Lsv: Max degree of secular variation
    :type Lsv: int
    :return: A dictionary containing the angles used for the computations and the Legendre coefs of B, U and SV.
    :rtype: dict
    """

    # Compute number of coefs
    LLb = ((Lb + 1) * (Lb + 2)) // 2
    LLu = ((Lu + 1) * (Lu + 2)) // 2
    LLsv = ((Lsv + 1) * (Lsv + 2)) // 2

    # Init of arrays storing the legendre polynomials, their derivative and second derivative
    # For magnetic field
    lp_b = np.zeros((LLb, tmax), order='F')
    d_lp_b = np.zeros((LLb, tmax), order='F')
    d2_lp_b = np.zeros((LLb, tmax), order='F')
    # For core flow
    lp_u = np.zeros((LLu, tmax), order='F')
    d_lp_u = np.zeros((LLu, tmax), order='F')
    d2_lp_u = np.zeros((LLu, tmax), order='F')
    # For secular variation
    lp_sv = np.zeros((LLsv, tmax), order='F')
    d_lp_sv = np.zeros((LLsv, tmax), order='F')
    d2_lp_sv = np.zeros((LLsv, tmax), order='F')

    # Compute Gauss-Legendre quadrature : returns tmax gauss_points in [-1, -1] with their associated weights
    gauss_points, gauss_weights = fortran.legendre.gauleg(np.float64(-1.), np.float64(1.), tmax)

    # For each gauss point x_i, compute legendre associated functions at x_i
    for i, x_i in enumerate(gauss_points):
        # Last arg of plmbar2 is 1 to have Schmidt quasi-normalisation
        fortran.legendre.plmbar2(lp_b[:, i], d_lp_b[:, i], d2_lp_b[:, i], x_i, Lb, 1)
        fortran.legendre.plmbar2(lp_u[:, i], d_lp_u[:, i], d2_lp_u[:, i], x_i, Lu, 1)
        fortran.legendre.plmbar2(lp_sv[:, i], d_lp_sv[:, i], d2_lp_sv[:, i], x_i, Lsv, 1)

    # Convert the values used for the polynomial computation in angles
    gauss_thetas = np.arccos(gauss_points)

    return {'thetas': gauss_thetas,
            'weights': gauss_weights,
            'MF': np.array((lp_b, d_lp_b, d2_lp_b)),
            'U': np.array((lp_u, d_lp_u, d2_lp_u)),
            'SV': np.array((lp_sv, d_lp_sv, d2_lp_sv))}


def compute_Kalman_gain_matrix(P, H, R, use_cholesky=False):
    """
    Computes the Kalman gain matrix from the correlation matrix, observation operator and observation error matrix:
        K = P*trans(H)*inv(H*P*trans(H)+R)
    The inversion can be carried out using Cholesky decomposition.

    :param P: Correlation matrix
    :type P: 2D numpy.ndarray (dim: Nstate x Nstate)
    :param H: Observation operator
    :type H: 2D numpy.ndarray (dim: Nobs x Nstate)
    :param R: Observation error matrix
    :type R: 2D numpy.ndarray (dim: Nobs x Nobs)
    :param use_cholesky: if True, matrix inversion is done using Cholesky decomposition. Uses scipy.linalg.inv otherwise.
    :type use_cholesky: bool
    :return: Kalman gain matrix K = P*trans(H)*inv(H*P*trans(H)+R)
    :rtype: 2D numpy.ndarray (dim: Nstate x Nobs)
    """

    # Compute PHT = P*transpose(H) that is needed twice
    PHT = np.matmul(P, np.transpose(H))

    # Compute the Kalman gain K = P*trans(H)*inv(H*P*trans(H)+R)
    matrix_to_invert = np.matmul(H, PHT) + R
    if use_cholesky:
        invert_matrix = mat_inv_using_Cholesky(matrix_to_invert)
        return np.matmul(PHT, invert_matrix)
    else:
        # invert_matrix = linalg.inv(matrix_to_invert)
        # return np.matmul(PHT, invert_matrix)
        # Solve K = PHT*inv(H*PHT+R) <=> (H*PHT+R)^T*K^T = PHT^T
        return np.transpose(linalg.solve(np.transpose(matrix_to_invert), np.transpose(PHT)))


def compute_Kalman_huber(b_f, y, Pbb_inv, H, Rbb, max_steps=50):
    """
    Solve iteratively the least square problem with a Huber norm, the equation is written (cf Walker and jackson 2000):
    b^k+1 = b^f + (P^{-1/2} + H^T R^{-1/2} W R^{-1/2} H)^-1 ( H^T R^{-1/2} W R^{-1/2}) (y - H b^f)

    :param max_steps: maximum number of realisations
    :type max_steps: int (dim: N_real x N_coefs_A)
    """
    sq_R = 1 / np.sqrt(np.diag(Rbb))
    R_H = np.multiply(sq_R[:, None], H); H_R = R_H.T
    W = np.ones(Rbb.shape[0])
    ones_min = np.ones(Rbb.shape[0])
    c = 1.5
    cutoff = 1e-8 # just to avoid division by zero
    y_minus_H_b = y - H @ b_f

    for i in range(max_steps):
        b_kplus1 = b_f + np.linalg.solve(H_R @ np.multiply(W[:, None], R_H) + Pbb_inv, 
                                         np.multiply(H_R, (W * sq_R)[None, :])) @ y_minus_H_b
        # if difference between last iteration and this one is less than 2% of the deviation, break the loop
        if i and np.amax(b_kplus1 - b_k) < 1e-4 * np.amax(b_k):
            break
        b_k = b_kplus1
        epsilon = abs(y - H @ b_kplus1) * sq_R
        epsilon[epsilon < cutoff] = cutoff

        W = np.min([c / epsilon, ones_min], axis=0) # if residual is bigger than one, weight is replaced by one
        # conditions to break the loop to make it faster
        if np.all(W == ones_min): # then nothing will change in the loop, LS solution because no outliers
            break
    return b_kplus1


def compute_Kalman_huber_parameter_basis(b_f, y, HPzzHT, PzzHT, H, Rbb, norm='huber', max_steps=50):
    """
    very similar to compute_Kalman_huber but written in the parameter basis.
    Solve iteratively the least square problem with a Huber norm, the equation is written (cf Walker and jackson 2000):
    b^k+1 = b^f + (P^{-1/2} + H^T R^{-1/2} W R^{-1/2} H)^-1 (H^T R^{-1/2} W R^{-1/2}) (y - H b^f)

    :param max_steps: maximum number of realisations
    :type max_steps: int (dim: N_real x N_coefs_A)
    """
    sq_R = np.sqrt(np.diag(Rbb))
    W = np.ones(Rbb.shape[0])
    ones_min = np.ones(Rbb.shape[0])
    c = 1.5
    cutoff = 1e-8 # just to avoid division by zero
    y_minus_H_b = y - H @ b_f
    if norm in ('l2', 'L2'):
        max_steps = 1 # first step just computes least square, one iteration in the loop is fine

    for i in range(max_steps):
        R_k = np.diag(sq_R / W) * sq_R
        b_kplus1 = b_f + PzzHT @ np.linalg.solve(R_k + HPzzHT, y_minus_H_b)
        # if difference between last iteration and this one is less than 2% of the deviation, break the loop
        epsilon = abs(y - H @ b_kplus1) / sq_R
        epsilon[epsilon < cutoff] = cutoff

        prev_W = W
        W = np.min([c / epsilon, ones_min], axis=0) # if residual is bigger than c, weight is replaced by one
        # conditions to break the loop to make it faster
        if np.all(W == ones_min): # then nothing will change in the loop, LS solution because no outliers
            break

        if i and np.amax(W - prev_W) < 1e-4 * np.amax(W):
            break

    return b_kplus1


def cov(x):
    return np.cov(x, rowvar=False)
    

def corr(x):
    return np.corrcoef(x, rowvar=False)


def compute_average(X, Nt, mode = "valid"):
    # Perform a convolution of X by a blackman smoothing window of size Nt (NOT A PROPER AVERAGE)
    X_avg = np.zeros((X.shape[0] - Nt + 1,X.shape[1]))
    for i in range(X.shape[1]):
        X_avg[:,i] = np.convolve(X[:,i], np.blackman(Nt), mode)
    # PROPER AVERAGE = X_avg/np.sum(np.blackman(Nt))
    return X_avg


def compute_derivative(X,dt):
 
    if X.ndim != 3:
        raise ValueError("X must be 3D")
    if X.shape[1]<3:
        raise ValueError("X first dimension must be at least 3")
    
    # compute second order 1st and 2nd derivatives
    dX = (X[:,2:,:]-X[:,:-2,:])/(2*dt)
    d2X = (X[:,2:,:]-2*X[:,1:-1,:]+X[:,:-2,:])/(dt**2)
    
    return dX, d2X

def compute_misfit(realisations, mean, inverse_weight_matrix):
    """
    Computes the misfit of realisations with respect to the mean.
    The misfit is a weighted norm according to the weight_matrice (Mahalanobis distance).
    :warning: The inverse weight matrix must be supplied

    :param realisations: Array of realisations (dim: nb_real x nb_coefs)
    :type realisations: np.array
    :param mean: Mean that will be subtracted to the realisations array (dim: nb_coefs)
    :type mean: np.array
    :param inverse_weight_matrix: Inverse of the weight matrix (WM^{-1}, dim: nb_coefs x nb_coefs)
    :type inverse_weight_matrix: np.array
    :return: sqrt(1/(nb_real(nb_coefs-1))*sum[(realisations - mean)^T WM^{-1} (realisations-mean)])
    :rtype: float
    """
    nb_realisations = realisations.shape[0]
    nb_coefs = realisations.shape[1]

    # Regular distance D (this step will fail if the dimensions are not matching)
    distance = realisations - mean

    # Compute the sum over realisations (assumed axis=0) of D^T * WM^{-1} * D that is the squared weighted norm.
    non_norm_squared_norm = 0
    for k in range(0, nb_realisations):
        non_norm_squared_norm += distance[k].T @ (inverse_weight_matrix @ distance[k])
    return np.sqrt(1./(nb_realisations * (nb_coefs - 1)) * non_norm_squared_norm)


def get_BLUE(X, Y, P, H, R, K=None, cholesky=False):
    """
    Returns the Best Linear Unbiased Estimate (BLUE) for X from the observations Y and the covariances matrices.

    :param X: current state vector
    :type X: 1D numpy.ndarray (dim: Nstate)
    :param Y: observation vector
    :type Y: 1D numpy.ndarray (dim: Nobs)
    :param P: covariance of forecast priors
    :type P: 2D numpy.ndarray (dim: Nstate x Nstate)
    :param H: Observation operator
    :type H: 2D numpy.ndarray (dim: Nobs x Nstate)
    :param R: covariance of observations errors
    :type R: 2D numpy.ndarray (dim: Nobs x Nobs)
    :param K: Kalman gain matrix. Optionnal: if not supplied, will be computed from other matrices.
    :type K: 2D numpy.ndarray (dim: Nstate x Nobs)
    :param cholesky: if True, uses Cholesky decomposition to compute matrices' inverses
    :type cholesky: bool
    :return: Best Linear Unbiased Estimate of X
    :rtype: 1D numpy.ndarray (dim: Nstate)
    """

    # Compute the innovation vector first
    D = Y - np.matmul(H, X)

    # Compute Kalman gain matrix if not provided
    if K is None:
        K = compute_Kalman_gain_matrix(P, H, R, use_cholesky=cholesky)

    return X + np.matmul(K, D)


def mat_inv_using_Cholesky(M):
    """
    Returns the inverse of a matrix computed by Cholesky decomposition

    :param M: a square positive-definite matrix
    :type M: ndarray
    :return: Inverse of M
    :rtype: ndarray of the dimensions of M
    """
    if linalg.det(M) == 0:
        raise np.linalg.LinAlgError('The inversion matrix method using Cholesky was called on an non-invertible matrix: {}!'.format(M))

    LCMat_tuple = linalg.cho_factor(M, lower=True)
    return linalg.cho_solve(LCMat_tuple, np.identity(M.shape[0]))

def eval_Plm_at_theta(theta, lmax, norm=1):
    """
    Evaluates the coeffs of the legendre polynomials (and 1st, and 2nd derivative) at a given theta.

    :param theta: angle where to evaluate the Legendre polys (in rad)
    :type theta: float
    :param lmax: Max degree described by the Legendre polys
    :type lmax: int
    :param norm: Optional arg to set the normalisation of the polys (1=Schmidt semi-norm (default), 2=full norm)
    :type norm: int
    :return: 3 numpy.array with the coefs of the Legendre polys, of its 1st derivative and of its 2nd derivative
    :rtype: np.array, np.array, np.array
    """

    nb_legendre_coefs = int((lmax+1)*(lmax+2)/2)

    coefs_plm = np.zeros(nb_legendre_coefs)
    coefs_dplm = np.zeros_like(coefs_plm)
    coefs_d2plm = np.zeros_like(coefs_plm)

    fortran.legendre.plmbar2(coefs_plm, coefs_dplm, coefs_d2plm, math.cos(theta), lmax, norm)

    return coefs_plm, coefs_dplm, coefs_d2plm

    
def compute_diag_AR1_coefs(cov_U, cov_ER, Tau_U, Tau_E):
    """
    Computes the matrices for a diagonal AR-1 process

    :param correlation_matrix: Correlation matrix of the AR-1 quantity
    :type correlation_matrix: np.array (dim: N x N)
    :param ar_timestep: Timestep of the AR-1 process in years (also called Tau)
    :type ar_timestep: float
    :return: The drift matrix and Cholesky lower matrix of the correlation matrix
    :rtype: np.array (dim: N x N), np.array (dim: N x N)
    """

    if not cov_U.ndim == 2:
        raise ValueError("cov_U must be 2D but is {}D".format(cov_U.ndim))

    Nu = cov_U.shape[0]
    Ner = cov_ER.shape[0]
    Nz = Nu + Ner
    diag = np.ones((Nz))
    diag[:Nu] = 1/Tau_U
    diag[Nu:] = 1/Tau_E
    A = np.diag(diag)

    Chol_U = np.sqrt(2. / Tau_U) * linalg.cholesky(cov_U, lower=True)
    Chol_ER = np.sqrt(2. / Tau_E) * linalg.cholesky(cov_ER, lower=True)
    Chol = linalg.block_diag(Chol_U, Chol_ER)
    
    return A, Chol


def prep_AR_matrix(Z, dt_samp, Nt):
    """
    Compute time derivatives

    :Z param: state array
    :Z type: ndarray (Ntimes x Ncoeffs)
    :dt_samp param: time step
    :dt_samp type: float
    :Nt param: average window size
    :Nt type: int
    :X return: X
    :X rtype: ndarray (Ntimes x Ncoeffs)
    :dX return: first time derivative X
    :dX rtype: ndarray (Ntimes x Ncoeffs)
    :d2X return: second time derivative X
    :d2X rtype: ndarray (Ntimes x Ncoeffs)
    :d3X return: third time derivative X
    :d3X rtype: ndarray (Ntimes x Ncoeffs)
    """

    if not Z.ndim == 2:
        raise ValueError("Z must be 2D but is {}D".format(Z.ndim))
    if Z.shape[0] < 4:
        raise ValueError("First dimension of Z must be at least 4 but is {}".format(Z.shape[0]))

    x0 = Z[:-3,:]
    x1 = Z[1:-2,:]
    x2 = Z[2:-1,:]
    x3 = Z[3:,:]

    # compute dX d2X d3X derivatives
    dX = (x1-x0)/dt_samp
    d2X = (x2-2*x1+x0)/dt_samp**2
    d3X = (x3-3*x2+3*x1-x0)/dt_samp**3
    
    # compute average (not divided by smoothing window total weigth !!!)
    # requires to be divided by the smoothing window total weigth to get a proper average
    x0 = compute_average(x0, Nt)
    dX = compute_average(dX, Nt)
    d2X = compute_average(d2X, Nt)
    d3X = compute_average(d3X, Nt)
    
    if not x0.shape == dX.shape == d2X.shape == d3X.shape:
        raise ValueError("x0, dX, d2X and d3X must be the same shape")
    
    return x0, dX, d2X, d3X


def compute_AR_coefs_avg(container, AR_type):
    """
    Compute the dense AR coefs:
    A,Chol if AR1
    A,B,C,Chol if AR3
    
    :param container: list containing the stored variables [X, dX, dt_sampling] for every prior.
    :type container: list
    :param AR_type: type of AR process
    :type AR_type: str
    """

    for i in range(len(container)):
        if not container[i][0].shape == container[i][1].shape:
            raise ValueError("X, dX must be the same shape")
        if not container[i][0].ndim == 2:
            raise ValueError("Z must be 2D but is {}D".format(container[i][0].ndim))
    
    if AR_type == "AR1":
        NZ = container[0][0].shape[0]
        tmp1 = np.zeros((NZ,NZ))
        tmp2 = np.zeros((NZ,NZ))
    elif AR_type == "AR3":
        NZ = container[0][0].shape[0] // 3
        tmp1 = np.zeros((3*NZ,3*NZ))
        tmp2 = np.zeros((3*NZ,3*NZ))

    Dt = container[0][2]

    # compute AA
    NT = 0
    for i in range(len(container)):
        X = container[i][0]
        dX = container[i][1]
        Nt = container[i][3]
        Mt = np.sum(np.power(np.blackman(Nt),2))
        NT += round(X.shape[1]/Nt)
        XXT = X @ X.T / (Mt*Nt)
        dXXT = dX @ X.T / (Mt*Nt)
        tmp1 += dXXT
        tmp2 += XXT

    AA = -1/Dt * tmp1 @ np.linalg.inv(tmp2)                   
    
    # compute WWT
    WWT = []
    for i in range(len(container)):
        X = container[i][0]
        dX = container[i][1]
        dt = container[i][2]
        Nt = container[i][3]
        Mt = np.sum(np.power(np.blackman(Nt),2))
        W = (dX + dt * AA @ X)/np.sqrt(dt)
        WWT.append(W @ W.T / (Mt*Nt))

    if AR_type == "AR1":
        S = (sum(WWT) / NT)
        L_S = np.linalg.cholesky(S) #Warning:: return the LOWER triangle of the Choleski decomposition!
        return AA.T, L_S
    elif AR_type == "AR3":
        C = AA[2*NZ:3*NZ,:NZ]
        B = AA[2*NZ:3*NZ,NZ:2*NZ]
        A = AA[2*NZ:3*NZ,2*NZ:3*NZ]
        S = (sum(WWT) / NT) [2*NZ:3*NZ,2*NZ:3*NZ]
        L_S = np.linalg.cholesky(S) #Warning:: return the LOWER triangle of the Choleski decomposition!
        return A.T, B.T, C.T, L_S
    

def compute_AR1_coefs_forecast(A, Chol, dt_forecast, Ncoef):
    """
    Compute the forecast coeffcients A, and Chol for AR-1 process
    """

    if not (A.shape == Chol.shape == (Ncoef,Ncoef)):
        raise ValueError("A, Chol matrices must be the same shape")
    if not np.allclose(Chol, np.tril(Chol)):
            raise ValueError('Cholesky matrix supplied in AR-1 is not a lower triangular matrix ! Did you supply the upper Cholesky matrix instead ?')
    
    # matrices pour AR1 schéma décentré
    Id = np.identity(Ncoef)
    A_forecast = dt_forecast*A-Id
    Chol_forecast = dt_forecast**(1/2) * Chol

    return A_forecast, Chol_forecast


def compute_AR3_coefs_forecast(A, B, C, Chol, dt_forecast, Ncoef):
    """
    Compute the forecast coeffcients A, B, C and Chol for AR-3 process
    """

    if not (A.shape == B.shape == C.shape == Chol.shape == (Ncoef,Ncoef)):
        raise ValueError("A, B, C, Chol matrices must have the same shape")
    if not np.allclose(Chol, np.tril(Chol)):
            raise ValueError('Cholesky matrix supplied in AR-3 is not a lower triangular matrix ! Did you supply the upper Cholesky matrix instead ?')

    # matrices pour AR3 schéma décentré
    Id = np.identity(Ncoef)
    A_forecast = dt_forecast*A-3*Id
    B_forecast = (dt_forecast**2)*B - 2*dt_forecast*A + 3*Id
    C_forecast = (dt_forecast**3)*C - (dt_forecast**2)*B + dt_forecast*A - Id
    Chol_forecast = dt_forecast**(5/2) * Chol

    return A_forecast, B_forecast, C_forecast, Chol_forecast


def ar1_process(X, A, Chol, random_state=None, check_Cholesky=True):
    """
    Applies an Auto-Regressive process of order 1 to the augmented state Z.

    X1 = - X0 @ A + Chol @ normal_noise

    :param X: quantity on which the AR-1 process will be applied (X0 above)
    :type X: 1D numpy.ndarray (dim: Nz)
    :param A: AR-1 operator (can be diagonal or dense)
    :type A: 2D numpy.ndarray (dim: Nz x Nz)
    :param random_state: RandomState to use for normal distribution draw. If None (default), draws will be done with np.random.normal.
    :type: None or numpy.random.RandomState
    :param check_Cholesky: If True (default), checks that the Cholesky is lower triangular. Setting to False may enhance performance.
    :type: bool
    :return: vector containing the result of the AR-1 process (X1 above)
    :rtype: 1D numpy.ndarray (dim: Nz)
    """

    Ncoef = X.shape[0]

    if X.ndim != 1:
        raise ValueError('Input quantity in AR-1 process should be 1D ! Got {} dims instead'.format(X.ndim))
    if A.shape != (Ncoef, Ncoef):
        raise ValueError('A matrix in AR-1 should have dimensions ({0},{0}). Got {1} instead'.format(Ncoef, A.shape))
    if check_Cholesky:  
        if not np.allclose(Chol, np.tril(Chol)):
            raise ValueError('Cholesky matrix supplied in AR-1 is not a lower triangular matrix ! Did you supply the upper Cholesky matrix instead ?')

    # Returns samples of the same size as qty from normal distribution with zero mean and unit variance N(0,1)
    if random_state is not None:
        normal_noise = random_state.normal(0, 1, size=Ncoef)
    else:
        normal_noise = np.random.normal(0, 1, size=Ncoef)

    #build a scaled noise
    scaled_noise = Chol @ normal_noise

    # Compute qty at t+1 from qty at t and the scaled noise (Euler-Maruyama)
    return - X @ A + scaled_noise


def ar3_process(X, A, B, C, Chol, random_state=None, check_Cholesky=True):
    """
    Applies an Auto-Regressive process of order 3 to the augmented state Z.

    X3 = - X0 @ A - X1 @ B - X2 @ C + Chol @ normal_noise

    :param X: quantity on which the AR-3 process will be applied (X0, X1, X2 above)
    :type X: 2D numpy.ndarray (dim: 3 x Nz)
    :param A: AR-3 operator (must be dense)
    :type A: 2D numpy.ndarray (dim: Nz x Nz)
    :param B: AR-3 operator (must be dense)
    :type B: 2D numpy.ndarray (dim: Nz x Nz)
    :param C: AR-3 operator (must be dense)
    :type C: 2D numpy.ndarray (dim: Nz x Nz)
    :param random_state: RandomState to use for normal distribution draw. If None (default), draws will be done with np.random.normal.
    :type: None or numpy.random.RandomState
    :param check_Cholesky: If True (default), checks that the Cholesky is lower triangular. Setting to False may enhance performance.
    :type: bool
    :return: vector containing the result of the AR-3 process (X3 above)
    :rtype: 1D numpy.ndarray (dim: Nz)
    """

    Ncoef = X.shape[-1]

    if X.ndim != 2:
        raise ValueError('Input quantity in AR-3 process should be 2D ! Got {} dims instead'.format(X.ndim))
    if X.shape[0] != 3:
        raise ValueError('First dimension of input quantity in AR-3 process should be 3 ! Got {} dims instead'.format(X.shape[0]))
    if A.shape != (Ncoef, Ncoef):
        raise ValueError('A matrix in AR-3 should have dimensions ({0},{0}). Got {1} instead'.format(Ncoef, A.shape))
    if B.shape != (Ncoef, Ncoef):
        raise ValueError('B matrix in AR-3 should have dimensions ({0},{0}). Got {1} instead'.format(Ncoef, B.shape))
    if C.shape != (Ncoef, Ncoef):
        raise ValueError('C matrix in AR-3 should have dimensions ({0},{0}). Got {1} instead'.format(Ncoef, C.shape))
    if check_Cholesky:  
        if not np.allclose(Chol, np.tril(Chol)):
            raise ValueError('Cholesky matrix supplied in AR-3 is not a lower triangular matrix ! Did you supply the upper Cholesky matrix instead ?')

    # Returns samples of the same size as qty from normal distribution with zero mean and unit variance N(0,1)
    if random_state is not None:
        normal_noise = random_state.normal(0, 1, size=Ncoef)
    else:
        normal_noise = np.random.normal(0, 1, size=Ncoef)

    #build a scaled noise
    scaled_noise = Chol @ normal_noise

    # Compute X3 from X0, X1, X2 and the scaled noise (Euler-Maruyama)
    return - X[2,:] @ A - X[1,:] @ B - X[0,:] @ C + scaled_noise
