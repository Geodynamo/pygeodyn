import numpy as np
import logging
import scipy as sc
from .. import common, corestates as cs, pca
from ..inout import reads
from ..generic.algo import GenericAlgo
from pygeodyn.augkf.forecaster import AugkfForecasterAR1, AugkfForecasterAR3
from pygeodyn.augkf.analyser import AugkfAnalyserAR1, AugkfAnalyserAR3
from fractions import Fraction
from ..inout.config import ComputationConfig

def create_augkf_algo(config, nb_realisations, seed, attributed_models):
    """
    Factory function that returns the AugKF algo

    :param config: Configuration of the algo
    :type config: ComputationConfig
    :param nb_realisations: number of realisations to consider in the algo
    :type nb_realisations: int
    :return: Algorithm object
    :rtype: AugkfAlgo
    """

    return AugkfAlgo(config, nb_realisations, seed, attributed_models)


class AugkfAlgo(GenericAlgo):
    """
    Augmented State Kalman Filter algorithm with DIFF treated as a contribution to ER.
    """
    name = 'augkf'


    def __eq__(self, other):
        """
        Implementation of equality for two different AugkfAlgos

        :param: other
        """
        are_all_equal = True
        def print_equal_message(eq, key1, key2):
            if eq:
                pass
            else:
                logging.debug('vals are not equal for key = {}, {}'.format(key1, key2))

        logging.debug('testing equalities of algo')
        # loop on all items of the two instantiation of the classes
        for (key, val), (key_other, val_other) in zip(self.__dict__.items(), other.__dict__.items()):
            # if val is an instance, skip it, these classes have their own implementation
            if isinstance(val, AugkfAnalyserAR3) or isinstance(val, AugkfForecasterAR3) or isinstance(val, ComputationConfig):
                logging.debug('Skipping {}, {}, an instantiation of a class, val = {}'.format(key, key_other, val))
                continue
            if type(val) == type(dict()):
                for (key_val, val_avg), (key_val2, val_avg2) in zip(val.items(), val_other.items()):
                    eq = np.allclose(val_avg, val_avg2)
                    print_equal_message(eq, key_val, key_val2)
            else:
                eq = val == val_other
                print_equal_message(eq, key, key_other)

        return are_all_equal


    def __init__(self, cfg, nb_realisations, seed, attributed_models):
        super().__init__(cfg, nb_realisations)
        self.attributed_models = attributed_models
        self.avg_prior, self.cov_prior = self.extract_prior_and_covariances()
        self.legendre_polys = common.compute_legendre_polys(cfg.Nth_legendre, cfg.Lb, cfg.Lu, cfg.Lsv)
        self.forecaster = self.create_forecaster()
        self.seed = seed
        self.analyser = self.create_analyser()


    def check_PCA(self):
        return self.config.pca


    def create_forecaster(self):
        """
        Factory method to create the forecaster.

        :return: AugkfForecaster
        """
        if self.config.AR_type == "AR3":
            return AugkfForecasterAR3(self)
        else:
            return AugkfForecasterAR1(self)


    def create_analyser(self):
        """
        Factory method to create the analyser.

        :return: AugkfAnalyser
        """
        if self.config.AR_type == "AR3":
            return AugkfAnalyserAR3(self)
        else:
            return AugkfAnalyserAR1(self)


    def get_current_misfits(self, measure):
        """ Forwards the getting of current_misfits to analyser """
        return self.analyser.current_misfits[measure]


    def init_corestates(self, random_state=None):
        """
        Sets up the corestates needed for the AugKF algorithm.
        Returns CoreStates of the adequate form and initialisation to perform the AugKF.

        :param random_state: Random state to use for normal draw (only used when init from noised priors)
        :type random_state: np.random.RandomState
        :return: computed_states, forecast_states, analysed_states, misfits, Z_AR3
        :rtype: CoreState, CoreState, CoreState, CoreState, 3D numpy array (N_real x 3 x Ncoef) or None (if not AR3)
        """

        corestate_measures={}

        # Define the measures used for the computation and their number of coeffs
        corestate_measures.update({'MF': self.config.Nb,
                                   'U': self.config.Nu2,
                                   'SV': self.config.Nsv,
                                   'ER': self.config.Nsv,
                                   'Z': self.config.Nz})
        
        # Add derivatives needed to AR3 to measures
        if self.config.AR_type == "AR3":
            corestate_measures.update({'dUdt': self.config.Nu2,
                                    'd2Udt2': self.config.Nu2,
                                    'dERdt': self.config.Nsv,
                                    'd2ERdt2': self.config.Nsv})
            
        # Add shear measure ('S') if do_shear = 1
        if self.config.do_shear == 1:
            corestate_measures.update({'S': self.config.Nu2})

        # Build the array of computed states
        computed_states = cs.CoreState()
        analysed_states = cs.CoreState()

        for meas_id, Nmeas in corestate_measures.items():
            computed_states.addMeasure(meas_id, np.zeros((self.attributed_models.shape[0], self.config.nb_forecasts, Nmeas)))

        # Initialize the core state at t=0
        if self.config.core_state_init == 'from_file':
            Z_AR3 = computed_states.initialise_from_file(self)
            str_init = self.config.init_file
        else:
            Z_AR3 = computed_states.initialise_from_noised_priors(self, random_state=random_state)
            str_init = 'normal draw around average priors'

        logging.debug('Computed states initialised from {}'.format(str_init))

        # Create the array storing the result of only forecasts (also copies the value at t=0)
        forecast_states = computed_states.copy()
        
        for meas_id, Nmeas in corestate_measures.items():
            analysed_states.addMeasure(meas_id, np.zeros((self.attributed_models.shape[0], self.config.nb_analyses, Nmeas)))

        # Create the CoreState (1 realisation and 1 coef (max_degree forced to 0)) storing the misfits of analyses
        misfits = cs.CoreState()
        for meas_id in ['MF', 'SV']:
            misfits.addMeasure(meas_id, np.zeros((1, self.config.nb_analyses, 1)), meas_max_degree=0)
            
        logging.info("AugKF CoreStates ready !")
        return computed_states, forecast_states, analysed_states, misfits, Z_AR3


    def extract_prior_and_covariances(self):
        """
        Extracts the priors from the files in the config prior directory. Also sets
        the covariance matrices by computation from priors or by reading them.

        :return: average priors and covariances matrices as dictionaries.
        :rtype: dict, dict
        """
        AR_type = self.config.AR_type
        Nz = self.config.Nz
        Nb = self.config.Nb
        Nsv = self.config.Nsv
        Nuz = self.config.Nuz
        dt_f = self.config.dt_f

        MF, U, ER, times, dt_samp, tag = reads.extract_realisations(self.config.prior_dir, self.config.prior_type, self.config.dt_smoothing)
        
        if not (len(times) == len(MF) == len(U) == len(ER) == len(dt_samp) == len(tag)):
            raise ValueError("times, MF, U, ER, dt_samp, tag lists must be the same length but are : {}, {}, {}, {}, {}, {}".format(len(times),len(MF),len(U),len(ER),len(dt_samp),len(tag)))
        
        # Applying unique function on array to get list of tags
        res,ind = np.unique(np.array(tag), return_index=True)
        tag_list = res[np.argsort(ind)]

        # Store covariance
        cov_prior = {}
        cov_prior['Z,Z'] = np.zeros((Nz,Nz))
        cov_prior['B,B'] = np.zeros((Nb,Nb))
        cov_prior['U,U'] = np.zeros((Nuz,Nuz))
        cov_prior['ER,ER'] = np.zeros((Nsv,Nsv))
        if AR_type == "AR3":
            cov_prior['dZ,dZ'] = np.zeros((Nz,Nz))
            cov_prior['d2Z,d2Z'] = np.zeros((Nz,Nz))

        # Store average
        avg_prior = {}
        
        # Init container
        container = []
        # If U ER not combined then a second container is needed
        if not self.config.combined_U_ER_forecast:
            container2 = []
        # Loop over tag list 
        # 100path MUST NOT BE THE FIRST IN TAG LIST THIS WILL RESULT IN AN ERROR
        # BECAUSE PCA FIT (AND AVERAGES) MUST BE DONE ON A GEODYNAMO WITH LONG TIME SERIES (LIKE 71PATH)
        for t in tag_list:
            # Init matrices to store iteratively if many time series in tag 
            # (case for 100path which combines 100p and 71p priors) 
            # Thus we can mix geodynamo priors
            Z, dZ, d2Z, d3Z = np.empty((0,Nz)), np.empty((0,Nz)), np.empty((0,Nz)), np.empty((0,Nz))
            
            for i in range(len(times)):
                if t == tag[i]:

                    # Set prior size 
                    U[i] = self.set_prior_size(U[i], self.config.Lu, 'U')
                    MF[i] = self.set_prior_size(MF[i], self.config.Lb, 'MF')
                    ER[i] = self.set_prior_size(ER[i], self.config.Lsv, 'ER')
                    
                    dt_sampling = dt_samp[i] # time sampling
                    
                    if AR_type == "AR3":
                        dt_prior = times[i][1]-times[i][0] # geodynamo sampling
                        # Compute blackman smoothing window length as the rounded ratio dt_sampling/dt_prior
                        Nt = round(dt_sampling / dt_prior)
                        # Compute blackman smoothing window total weight by summing all blackman window coeffs
                        blackman_w = np.sum(np.blackman(Nt))
                    else:
                        # No smoothing
                        Nt = 1
                        blackman_w = 1
                        # Perform a subsampling of U MF ER times according to self.config.dt_sampling
                        U[i] = common.sample_timed_quantity(times[i], U[i], self.config.dt_sampling)[1]
                        MF[i] = common.sample_timed_quantity(times[i], MF[i], self.config.dt_sampling)[1]
                        times[i], ER[i] = common.sample_timed_quantity(times[i], ER[i], self.config.dt_sampling)
                        dt_prior = times[i][1]-times[i][0] # geodynamo sampling
                    
                    if t != "100path": #We consider average of 70path for 100path because longer time series
                        # Compute U, B, ER averages
                        avg_prior['U'] = U[i].mean(axis=0)
                        avg_prior['ER'] = ER[i].mean(axis=0)
                        avg_prior['B'] = MF[i].mean(axis=0)

                    # Center data
                    if self.check_PCA():
                        if t != "100path": # PCA fit over 71path because longer time series
                            self.config.pcaU_operator = pca.NormedPCAOperator(self.config)
                            self.config.pcaU_operator.fit(U[i])
                        # PCA transform of U
                        U[i] = self.config.pcaU_operator.transform(U[i])
                    else:
                        # Remove mean U
                        U[i] -= avg_prior['U']
                    # Remove mean ER
                    ER[i] -= avg_prior['ER']

                    if not AR_type == "diag":
                        # compute U ER B and time derivatives and concatenate Z
                        u, du, d2u, d3u = common.prep_AR_matrix(U[i], dt_prior, Nt)
                        e, de, d2e, d3e = common.prep_AR_matrix(ER[i], dt_prior, Nt)
                        b = common.prep_AR_matrix(MF[i], dt_prior, Nt)[0]
                        
                        Z = np.concatenate((Z,self.U_ER_to_Z(u,e)), axis=0)
                        dZ = np.concatenate((dZ,self.U_ER_to_Z(du,de)), axis=0)
                        d2Z = np.concatenate((d2Z,self.U_ER_to_Z(d2u,d2e)), axis=0)
                        d3Z = np.concatenate((d3Z,self.U_ER_to_Z(d3u,d3e)), axis=0)
                    else:
                        u = np.copy(U[i])
                        e = np.copy(ER[i])
                        b = np.copy(MF[i])
            
                    if t != "100path": #We consider covariance of 70path for 100path because longer time series
                        # Compute U, B, ER covariance matrices
                        cov_prior['U,U'] = common.cov(u/blackman_w)
                        cov_prior['B,B'] = common.cov(b/blackman_w)
                        cov_prior['ER,ER'] = common.cov(e/blackman_w)

                        if AR_type == "diag":
                            # Diag is independant for forecast so diag block U,U and ER,ER
                            cov_prior['Z,Z'] = sc.linalg.block_diag(cov_prior['U,U'],cov_prior['ER,ER'])
                        else:
                            # If U and ER dependant for forecast
                            if self.config.combined_U_ER_forecast :
                                cov_prior['Z,Z'] = common.cov(Z/blackman_w)
                                cov_prior['d2Z,d2Z'] = common.cov(d2Z/blackman_w)
                                cov_prior['dZ,dZ'] = common.cov(dZ/blackman_w)
                            else:
                                # Concatenate
                                cov_prior['Z,Z'] = sc.linalg.block_diag(cov_prior['U,U'],cov_prior['ER,ER'])
                                cov_prior['dZ,dZ'] = sc.linalg.block_diag(common.cov(du/blackman_w),common.cov(de/blackman_w))
                                cov_prior['d2Z,d2Z'] = sc.linalg.block_diag(common.cov(d2u/blackman_w),common.cov(d2e/blackman_w))

            if not AR_type == "diag":
                # If U and ER independant for forecast
                if not self.config.combined_U_ER_forecast:
                    # Split U ER parts of Z
                    Uz = Z[:,:Nuz]
                    dUz = dZ[:,:Nuz]
                    d2Uz = d2Z[:,:Nuz]
                    d3Uz = d3Z[:,:Nuz]
                    ERz = Z[:,Nuz:]
                    dERz = dZ[:,Nuz:]
                    d2ERz = d2Z[:,Nuz:]
                    d3ERz = d3Z[:,Nuz:]
                    
                if AR_type == "AR1":  
                    # If U and ER dependant for forecast
                    if self.config.combined_U_ER_forecast:
                        # Append container for Z
                        container.append([Z.T, dZ.T * dt_prior, dt_prior, Nt])
                    else:
                        # Append container for U
                        container.append([Uz.T, dUz.T * dt_prior, dt_prior, Nt])
                        # Append container 2 for ER
                        container2.append([ERz.T, dERz.T * dt_prior, dt_prior, Nt])
                elif AR_type == "AR3":  
                    # If U and ER dependant for forecast
                    if self.config.combined_U_ER_forecast:
                        # Append container for Z
                        container.append([np.concatenate((Z.T,dZ.T,d2Z.T),axis=0),
                                            np.concatenate((dZ.T,d2Z.T,d3Z.T),axis=0) * dt_prior,
                                            dt_prior, Nt])
                    else:
                        # Append container for U
                        container.append([np.concatenate((Uz.T,dUz.T,d2Uz.T),axis=0),
                                            np.concatenate((dUz.T,d2Uz.T,d3Uz.T),axis=0) * dt_prior,
                                            dt_prior, Nt])
                        # Append container 2 for ER
                        container2.append([np.concatenate((ERz.T,dERz.T,d2ERz.T),axis=0),
                                            np.concatenate((dERz.T,d2ERz.T,d3ERz.T),axis=0) * dt_prior,
                                            dt_prior, Nt])

        if AR_type == "AR1":
            # compute A, Chol of U or Z
            (A, Chol) = common.compute_AR_coefs_avg(container, AR_type)
            if not self.config.combined_U_ER_forecast:  
                # compute A, Chol of ER
                (A2, Chol2) = common.compute_AR_coefs_avg(container2, AR_type)
                # diag block 
                A = sc.linalg.block_diag(A,A2)
                Chol = sc.linalg.block_diag(Chol,Chol2)
            # compute A, Chol for forecast
            (cov_prior['A'], 
             cov_prior['Chol']) = common.compute_AR1_coefs_forecast(A, Chol, dt_f, Nz)
        elif AR_type == "diag":
            # compute A, Chol
            A, Chol = common.compute_diag_AR1_coefs(cov_prior['U,U'], 
                                                    cov_prior['ER,ER'], 
                                                    self.config.TauU, 
                                                    self.config.TauE)
            # compute A, Chol for forecast
            (cov_prior['A'], 
             cov_prior['Chol']) = common.compute_AR1_coefs_forecast(A, Chol, dt_f, Nz)
        elif AR_type == "AR3":
            # compute A, B, C, Chol of U or Z
            (A,B,C,Chol) = common.compute_AR_coefs_avg(container, AR_type)
            if not self.config.combined_U_ER_forecast:  
                # compute A, B, C, Chol of ER
                (A2,B2,C2,Chol2) = common.compute_AR_coefs_avg(container2, AR_type)
                # diag block 
                A = sc.linalg.block_diag(A,A2)
                B = sc.linalg.block_diag(B,B2)
                C = sc.linalg.block_diag(C,C2)
                Chol = sc.linalg.block_diag(Chol,Chol2)
            # compute A, B, C, Chol forecast
            (cov_prior['A'], 
             cov_prior['B'], 
             cov_prior['C'], 
             cov_prior['Chol']) = common.compute_AR3_coefs_forecast(A, B, C, Chol, dt_f, Nz)

        logging.debug('Reading and computation of priors/covariances of AugkfAlgo OK !')

        return avg_prior, cov_prior


    def check_coef_size(self, X, asked_max_degree, asked_coeffs):
        # Take only data corresponding to the asked coefs (and check that they are not bigger than the data...)
        if X.shape[1] < asked_coeffs:
            raise ValueError(
                'Asked max degree ({}) is too big to be handled by the prior data in {}. Please retry with a lower max degree.'.format(
                    asked_max_degree, self.config.prior_dir))
    
    
    def set_prior_size(self, X, asked_max_degree, measure):
        asked_coeffs = asked_max_degree * (asked_max_degree + 2)
        if measure in ['MF', 'SV', 'ER']:
            self.check_coef_size(X, asked_max_degree, asked_coeffs)
            return X[:, :asked_coeffs]
        else:
            self.check_coef_size(X, asked_max_degree, asked_coeffs)
            read_Nu = X.shape[1] // 2
            return np.concatenate((X[:, :asked_coeffs], X[:, read_Nu:read_Nu + asked_coeffs]), axis=1)

    
    def Z_to_U_ER(self, Z, dimension):
        """
        Compute U ER from Z 

        :param Z: Augmented state Z
        :type Z: numpy array, dim Ncoef (1D) or Nreal x Ncoef (2D)
        :param dimension: dimension of Z
        :type dimension: int 
        :return: U and ER states
        :rtype: 1D or 2D U and ER states
        """

        if dimension == 1: # Ncoef
            if self.check_PCA():
                ER = Z[self.config.N_pca_u:] + self.avg_prior['ER']
                U = self.config.pcaU_operator.inverse_transform(Z[:self.config.N_pca_u])
            else:
                U = Z[:self.config.Nu2] + self.avg_prior['U']
                ER = Z[self.config.Nu2:] + self.avg_prior['ER']
        elif dimension == 2: # Nreal x Ncoef
            if self.check_PCA():
                ER = Z[:, self.config.N_pca_u:] + self.avg_prior['ER']
                U = self.config.pcaU_operator.inverse_transform(Z[:, :self.config.N_pca_u])
            else:
                U = Z[:, :self.config.Nu2] + self.avg_prior['U']
                ER = Z[:, self.config.Nu2:] + self.avg_prior['ER']
        else:
            raise ValueError("dimension must be equal to 1 or 2 but is {}".format(dimension))
        return U, ER
    

    def dZ_to_dU_dER(self, dZ):
        """
        Compute dU dER from dZ or d2U d2ER from d2Z

        :param dZ: derivative of augmented state (dZ)
        :type times: numpy array, Nreal x Ncoef (2D)
        :return: derivatives of U and ER states
        :rtype: 2D dU and dER states
        """

        # Nreal x Ncoef
        if self.check_PCA():
            dER = dZ[:, self.config.N_pca_u:]
            dU = self.config.pcaU_operator.inverse_transform_deriv(dZ[:, :self.config.N_pca_u])
        else:
            dU = dZ[:, :self.config.Nu2]
            dER = dZ[:, self.config.Nu2:]
        return dU, dER
    

    def U_ER_to_Z(self, U, ER):
        """
        Compute Z from U ER

        :param U: U state
        :type U: numpy array, dim Ncoef (1D) or Ntimes x Ncoef (2D) or Nreal x Ntimes x Ncoef (3D)
        :return: U and ER states
        :rtype: 1D or 2D or 3D Z states
        """
        if U.ndim == ER.ndim:
            if U.ndim == 1: # Ncoef
                return np.concatenate((U, ER),axis=0)
            elif U.ndim == 2: # Ntimes x Ncoef
                return np.concatenate((U, ER),axis=1)
            elif U.ndim == 3: # Nreal x Ntimes x Ncoef
                return np.concatenate((U, ER),axis=2)
            else: 
                raise ValueError("U and ER must be 1D, 2D or 3D arrays but are {}, {}".format(U.ndim, ER.ndim))
        else:
            raise ValueError("U and ER arrays must be the same number of dimensions but are {}, {}".format(U.ndim, ER.ndim))