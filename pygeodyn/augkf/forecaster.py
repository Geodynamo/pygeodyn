import numpy as np
from .. import common
from ..generic.computer import GenericComputer
from pygeodyn.corestates import with_core_state_of_dimensions
from mpi4py import MPI
from .. import corestates as cs

comm = MPI.COMM_WORLD
rank = comm.Get_rank()

class AugkfForecasterAR1(GenericComputer):
    """
    Class that implements the forecasts using AugKF (Augmented state Kalman Filter) algorithm with DIFF treated as a contribution to ER.
    """
    def __init__(self, algo):
        """
        :param algo: Algorithm object
        :type algo: Algo
        """
        super().__init__(algo)

        for k in self.needed_covariance_matrices():
            if k not in algo.cov_prior:
                raise KeyError('{} matrix not found in covariance matrices while needed for Forecast !'.format(k))

        # Bool to deactivate checks on AR processes
        self.Cholesky_AR_check = False

    def needed_covariance_matrices(self):
        #needed_covariance_matrices for forecast step
        return ['A', 'Chol']

    @with_core_state_of_dimensions(1)
    def forecast_step(self, input_core_state, Z_AR, seed, i_real, i_t):
        """
        Forecasts the input_core_state using AR processes for Z, computation of SV and Euler scheme for B.

        :param input_core_state: core_state of a single realisation at a single date 
        :type input_core_state: corestates.CoreState
        :param Z_AR: forecast state AR
        :type Z_AR: np.array 1D if AR1 (Ncoef) or 2D if AR3 (3 x Ncoef)
        :param seed: random seed
        :type seed: int
        :param i_real: model realisation index
        :type i_real: int
        :param i_t: time index
        :type i_t: int
        :return: CoreState containing the result from the forecast
        :rtype: corestates.CoreState
        """
        
        # copy input core state
        next_core_state = input_core_state.copy()
        
        # set random state
        N = self.algo.nb_realisations
        rstate = np.random.default_rng(seed + i_real + N * i_t)
        
        # Compute Z(t+1)
        next_core_state.Z = self.forecast_Z(Z_AR, rstate=rstate)
        next_core_state.U, next_core_state.ER = self.algo.Z_to_U_ER(next_core_state.Z, 1)
        
        # Compute A(b)
        Ab = self.compute_Ab(next_core_state)
      
        # Compute SV(t+1) = A(b)U(t+1) + E(t+1)
        next_core_state.SV = np.matmul(Ab, next_core_state.U) + next_core_state.ER

        # Compute B(t+1) = B(t) + dt*SV(t+1) (Euler scheme as SV=dB/dt)
        next_core_state.B = input_core_state.B + self.cfg.dt_f * next_core_state.SV
        
        # Returns the updated core_state at t+1
        return next_core_state


    def forecast_Z(self, Z_AR1, rstate=None):
        """
        Forecast Z state with AR-1 process.

        :param Z_AR1: AR-1 forecast state
        :type Z_AR1: np.array (Ncoef)
        :param rstate: Random state to use for the AR-1 process
        :type rstate: np.random.RandomState
        :return: forecasted Z state
        :rtype: np.array(Ncoef)
        """
        # AR1 process for Z
        return common.ar1_process(Z_AR1, 
                            self.algo.cov_prior['A'], 
                            self.algo.cov_prior['Chol'], 
                            random_state=rstate)
    

    def parallel_forecast_step(self, input_core_state, seed, i_t):
        """
        parallelize the AR1 forecast step

        :param input_core_state: input_core_state at time t
        :type input_core_states: corestates.Corestate
        :param seed: random seed
        :type seed: int
        :param i_t: time index
        :type i_t: int
        :return: CoreState containing the result from the forecast
        :rtype: corestates.CoreState
        """

        t = self.algo.config.t_forecasts[i_t]
        
        # copy input core state
        forecast_at_t = input_core_state.copy()
        # set all measures to 0
        forecast_at_t[:] = 0

        # Each process computes its attributed models
        for i_idx, i_model in enumerate(self.algo.attributed_models):
            forecast_at_t[i_idx] = self.forecast_step(input_core_state[i_idx], input_core_state.Z[i_idx], seed, i_idx, i_t)

            print('P'+str(rank), t, cs.coef_print(forecast_at_t[i_idx], 0))

        return forecast_at_t
    

class AugkfForecasterAR3(AugkfForecasterAR1):

    def needed_covariance_matrices(self):
        return ['A', 'B', 'C', 'Chol']

    def forecast_Z(self, Z_AR3, rstate=None):
        """
        Forecast Z state with AR-3 process.

        :param Z_AR3: AR-3 forecast state
        :type Z_AR3: np.array (3 x Ncoef)
        :param rstate: Random state to use for the AR-3 process
        :type rstate: np.random.RandomState
        :return: forecasted Z state
        :rtype: np.array(Ncoef)
        """
        # AR3 process for Z
        return common.ar3_process(Z_AR3, 
                            self.algo.cov_prior['A'], 
                            self.algo.cov_prior['B'], 
                            self.algo.cov_prior['C'], 
                            self.algo.cov_prior['Chol'], 
                            random_state=rstate)
    
    
    def update_Z_AR3(self, Z, Z_AR3):
        """
        Update forecast Z state for AR3
        :param Z: forecasted Z
        :type Z: np.array (nreal x Ncoef)
        :param Z_AR3: AR-3 forecast state
        :type Z_AR3: np.array (3 x Ncoef)
        :return Z_AR3: AR-3 forecast state
        :rtype Z_AR3: np.array (3 x Ncoef)
        """
        Z_AR3 = np.concatenate((Z_AR3, Z[:,np.newaxis,:]), axis=1)
        assert Z_AR3[:,1:,:].shape[1] == 3
        return Z_AR3[:,1:,:]
    
    def parallel_forecast_step(self, input_core_state, Z_AR3, seed, i_t):
        """
        parallelize the AR3 forecast step

        :param input_core_state: input_core_state at time t
        :type input_core_states: corestates.Corestate
        :param Z_AR3: AR-3 forecast state
        :type Z_AR3: np.array (3 x Ncoef)
        :param seed: random seed
        :type seed: int
        :param i_t: time index
        :type i_t: int
        :return: CoreState containing the result from the forecast
        :rtype: corestates.CoreState
        """

        t = self.algo.config.t_forecasts[i_t]

        # copy input core state
        forecast_at_t = input_core_state.copy()
        # set all measures to 0
        forecast_at_t[:] = 0

        # Each process computes its attributed models
        for i_idx, i_model in enumerate(self.algo.attributed_models):
            forecast_at_t[i_idx] = self.forecast_step(input_core_state[i_idx], Z_AR3[i_idx], seed, i_idx, i_t)

            print('P'+str(rank), t, cs.coef_print(forecast_at_t[i_idx], 0))

        # update Z_AR3 
        Z_AR3 = self.update_Z_AR3(forecast_at_t.Z, Z_AR3)

        return forecast_at_t, Z_AR3