import numpy as np
import os.path
import pygeodyn
import logging
from scipy import linalg
from mpi4py import MPI
from .. import common
from ..inout import observations as observations_module
from ..generic.computer import GenericComputer
from sklearn import covariance as skcov
from .. import common

comm = MPI.COMM_WORLD
rank = comm.Get_rank()

class AugkfAnalyserAR1(GenericComputer):
    """
    Class that handles the analyses of the Augmented State Kalman Filter algorithm with DIFF treated as a contribution to ER.
    """

    def __init__(self, algo):
        """
        Sets all internal members, observation operator and covariance matrices for Z=UE.

        :param algo: Algorithm object
        :type algo: Algo
        """
        super().__init__(algo)

        # Date-based dicts for observations, stored in measure_observations dict
        self.measure_observations = {}

        nb_obs_mf, nb_obs_sv = self.extract_observations()
        if nb_obs_mf == 0:
            logging.error(
                "No observation was extracted for MF! Analyses on b will be completely skipped"
                )
        elif nb_obs_sv == 0:
            logging.error(
                "No observation was extracted for SV! Analyses on u, e and d/dt b will be completely skipped"
                )
        else:
            logging.info(
                "Finished extracting the observations: MF ({}) and SV ({})!".format(
                    nb_obs_mf, nb_obs_sv
                )
            )

        # Dict to store the misfits of the current analysis
        self.current_misfits = {}
        self.current_misfits = self.invalid_misfits(keys=["MF", "SV", "U", "ER", "Z"]) # initialisation

        # Performing or not a backward analysis
        self.do_backward_analysis = False


    def invalid_misfits(self, keys):
        possible_keys = ["MF", "SV", "U", "ER", "Z"]
        err = 'invalid key passed in invalid_mifits(), keys: {}, possible keys: {}'
        assert all([k in possible_keys for k in keys]), err.format(keys, possible_keys)
        try:
            for k in keys:
                self.current_misfits[k] = np.nan
            return self.current_misfits
        except NameError:
            return {k: np.nan for k in keys}


    def extract_observations(self):
        """
        Extracts the observations for all obs types in the config. Updates the internal dictionaries observations_mf and observations_sv.

        :return: The numbers of dates for MF and SV for eventual checking.
        :rtype: int, int
        """

        # building function can be either, build_go_vo_observations, build_covobs_observations,
        # build_covobs_observations or build_covobs_hdf5_observations
        building_function = getattr(
            observations_module,
            "build_{}_observations".format(self.cfg.obs_type.lower()),
            None,
        )

        if building_function is None:
            logging.error(
                "No loading function was defined for {}. The extraction of observations was skipped for this type.".format(
                    self.cfg.obs_type
                )
            )
        logging.info("Reading {} data as observations...".format(self.cfg.obs_type))

        for measure_type in ["SV", "MF"]:
            self.measure_observations[measure_type] = building_function(
                self.cfg, self.algo.nb_realisations, measure_type, self.algo.seed
            )
            if hasattr(self.cfg, "obs_mod_errors_dir"):
                self.add_modelisation_errors(
                    os.path.join(
                        pygeodyn._package_directory, self.cfg.obs_mod_errors_dir
                    ),
                    measure_type,
                )
        return (
            len(self.measure_observations["MF"].keys()),
            len(self.measure_observations["SV"].keys()),
        )

    def add_modelisation_errors(self, obs_mod_errors_dir, measure_type):
        """
        Add another contribution to the observation errors. The errors have to be stored in two text files,
        one for the MF and one for the SV, see for instance 'pygeodyn/data/observations/COVOBS-x2_maglat/mod_errors'
        """
        if self.cfg.obs_type == 'GO_VO':
            logging.warning('mod_error is made for synthetic data, not for GVO observatories')
            return None

        try:
            mod_errors = np.genfromtxt(
                os.path.join(obs_mod_errors_dir, "{}.err".format(measure_type))
            )
        except IOError:
            logging.error(
                "No {0} modelisation error file ({0}.err) was found in {1} ! Modelisation errors will be set to 0 for {0}.".format(
                    measure_type, obs_mod_errors_dir
                )
            )
            return

        logging.debug(
            "Adding modelisation errors stored in {1} for {0}.".format(
                measure_type, obs_mod_errors_dir
            )
        )
        for date, obs in self.measure_observations[measure_type].items():
            No = len(obs.Rxx)
            print('No, measure, mod_errors', No, date, measure_type, mod_errors.shape)
            self.measure_observations[measure_type][date].Rxx = obs.Rxx + np.diag(
                mod_errors[:No]
            )

    def sv_analysis(self):
        """
        Shortcut to check if sv_analysis is performed
        """
        # 1 means analysis on sv, 0 means no analysis on sv
        return np.all(self.ana_sv)
    
    def mf_analysis(self):
        """
        Shortcut to check if mf_analysis is performed
        """
        # 1 means analysis on mf, 0 means no analysis on mf
        return np.all(self.ana_mf)
    
    def check_if_analysis_data(self, i_analysis, do_backward=False):
        """
        check if there is mf and/or sv observation at next analysis time (times if AR3)
        and prepare observation data for either AR1 or AR3 analysis

        :param i_analysis: analysis time iteration
        :type i_analysis: int
        :return: update self.ana_sv and self.ana_mf
        """

        AR_type = self.algo.config.AR_type
        
        # set number of times (Nt) involved in analysis depending on AR_type
        if AR_type == "AR3":
            Nt = 3
        else:
            Nt = 1
        
        # init boolean vectors 
        self.ana_sv = np.zeros((Nt,), dtype=bool)
        self.ana_mf = np.zeros((Nt,), dtype=bool)
        
        if not i_analysis < self.algo.config.nb_analyses:
            return self.ana_mf, self.ana_sv
            
        # set times vector depending on AR_type
        if AR_type == "AR3":
            if do_backward is True:
                times = [
                    i_analysis - 1,
                    i_analysis,
                    i_analysis + 1
                ]
            else:
                times = [i_analysis, 
                    i_analysis + 1, 
                    i_analysis + 2]
        else:
            times = [i_analysis+1]

        # loop over Nt
        for i in range(Nt):
            t = str(times[i])
            # if observation found
            if t in self.measure_observations["SV"].keys():
                #update self.ana_sv
                self.ana_sv[i] = True  
            if t in self.measure_observations["MF"].keys():
                #update self.ana_sv
                self.ana_mf[i] = True  
        #setup self.measure_observations
        if AR_type == "AR3":
            if self.sv_analysis():
                self.sv_X = [self.measure_observations["SV"][str(times[0])].X,
                             self.measure_observations["SV"][str(times[1])].X,
                             self.measure_observations["SV"][str(times[2])].X]
                self.sv_H = [self.measure_observations["SV"][str(times[0])].H,
                             self.measure_observations["SV"][str(times[1])].H,
                             self.measure_observations["SV"][str(times[2])].H]
                self.sv_Rxx = [self.measure_observations["SV"][str(times[0])].Rxx,
                               self.measure_observations["SV"][str(times[1])].Rxx,
                               self.measure_observations["SV"][str(times[2])].Rxx]
            if self.mf_analysis():
                self.mf_X = [self.measure_observations["MF"][str(times[0])].X,
                             self.measure_observations["MF"][str(times[1])].X,
                             self.measure_observations["MF"][str(times[2])].X]
                self.mf_H = [self.measure_observations["MF"][str(times[0])].H,
                             self.measure_observations["MF"][str(times[1])].H,
                             self.measure_observations["MF"][str(times[2])].H]
                self.mf_Rxx = [self.measure_observations["MF"][str(times[0])].Rxx,
                               self.measure_observations["MF"][str(times[1])].Rxx,
                               self.measure_observations["MF"][str(times[2])].Rxx]
        else:
            if self.sv_analysis():
                self.sv_X = self.measure_observations["SV"][str(times[0])].X                   
                self.sv_H = self.measure_observations["SV"][str(times[0])].H 
                self.sv_Rxx = self.measure_observations["SV"][str(times[0])].Rxx 

            if self.mf_analysis():
                self.mf_X = self.measure_observations["MF"][str(times[0])].X                      
                self.mf_H = self.measure_observations["MF"][str(times[0])].H 
                self.mf_Rxx = self.measure_observations["MF"][str(times[0])].Rxx
        
        # if no mf analysis
        if not self.mf_analysis():
            logging.critical(
                "Skipping MF analysis"
            )
            self.current_misfits = self.invalid_misfits(keys=["MF"])
        # if no sv analysis
        if not self.sv_analysis():
            logging.critical(
                "Skipping SV analysis"
            )
            self.current_misfits = self.invalid_misfits(keys=["SV"])     


    def analysis_step(self, input_core_state):
        """ Does the analysis at time t on the B and Z=[UE] part of the input_core_state.
        Updates SV = A(B)U - ER in consequence.

        :param input_core_state: Core state at time t
        :type input_core_state: corestates.CoreState (dim: nb_realisations x Ncorestate)
        :return: the analysed core state
        :rtype: corestates.CoreState (dim: nb_realisations x Ncorestate)
        """

        # Check corestate is 2D
        if input_core_state.B.ndim != 2:
            raise ValueError("Corestate must be 2D but is ({}D)".format(input_core_state.B.ndim))
        
        # Check that number of reals match
        if input_core_state.B.shape[0] != self.algo.nb_realisations:
            raise ValueError(
                "Number of realisations in input_core_state ({}) is not equal to expected number ({})".format(
                    input_core_state.B.shape[0], self.algo.nb_realisations))
        
        # copy core state
        ana_core_state = input_core_state.copy()
        
        # if no analysis
        if not self.sv_analysis() and not self.mf_analysis(): # no analysis of mf and sv
            return ana_core_state[self.algo.attributed_models]

        if self.mf_analysis():
            # perform MF analysis
            ana_core_state.B = self.analyse_B(ana_core_state.B, self.mf_X, self.mf_H, self.mf_Rxx)
        if self.sv_analysis():
            # perform SV analysis
            (ana_core_state.Z,
            ana_core_state.U,
            ana_core_state.ER,
            ana_core_state.SV) = self.analyse_Z(ana_core_state, self.sv_X, self.sv_H, self.sv_Rxx)
        return ana_core_state[self.algo.attributed_models]
    
    def analyse_B(self, input_core_state, mf_X, mf_H, mf_Rxx):
        """
        Returns the analysed data for B by a BLUE given the observations.

        :param input_core_state: NumPy array containing the coefficient data of B
        :type input_core_state: np.array (dim: nb_realisations x Nb)
        :param mf_X: Observation data to use for the BLUE
        :type mf_X: Observation
        :param mf_H: Observation matrix to use for the BLUE
        :type mf_H: Observation
        :param mf_Rxx: Observation error to use for the BLUE
        :type mf_Rxx: Observation
        :return: NumPy array containing the analysed coefficient data of B
        :rtype: np.array (dim: nb_realisations x Nb)
        """
        # obs operator
        Hb = mf_H
        # obs error
        Rbb = mf_Rxx
        # compute Pbb from B state
        Pbb_forecast = self.remove_small_correlations(input_core_state)
        # Updates the B part of the core_state by the result of the Kalman filter for each model
        logging.debug("Getting best linear unbiased estimate of B...")
        analysed_B = np.zeros((self.algo.nb_realisations, self.algo.config.Nb))

        if self.cfg.kalman_norm == 'l2': # for non least square norm, iteration are needed
            Kbb = common.compute_Kalman_gain_matrix(
                Pbb_forecast, Hb, Rbb
            )
            for i_idx, i_real in enumerate(self.algo.attributed_models):
                analysed_B[i_real] = common.get_BLUE(
                    input_core_state[i_real],
                    mf_X[i_real],
                    Pbb_forecast,
                    Hb,
                    Rbb,
                    Kbb,
                )

        elif self.cfg.kalman_norm == 'huber':
            # compute inverse of P_bb before loop on reals using its symmetry
            P_eig_val, P_eig_vec = np.linalg.eigh(Pbb_forecast)
            P_eig_val[P_eig_val < 1e-10] = 1e-10 # in case if matrix is not full rank, which should not happen
            Pbb_inv = P_eig_vec @ np.diag(1 / P_eig_val) @ P_eig_vec.T
            for i_idx, i_real in enumerate(self.algo.attributed_models):
                analysed_B[i_real] = common.compute_Kalman_huber(input_core_state[i_real], mf_X[i_real], 
                                                Pbb_inv, Hb, Rbb)
        else:
            raise ValueError('Invalid value of param kalman_norm, should be equal to huber or l2, got {}'.format(self.cfg.kalman_norm))
        
        comm.Allreduce(MPI.IN_PLACE, analysed_B, op=MPI.SUM)

        # Compute the misfits for B (Y - HX)
        HX_b = np.transpose(np.matmul(Hb, np.transpose(analysed_B)))
        self.current_misfits["MF"] = common.compute_misfit(
            mf_X, HX_b, linalg.inv(Rbb)
        )

        return analysed_B
    

    def setup_Hz(self,Ab, Nu):
        """
        Compute the matrix [Ab | I_e] where Ab is the contains the Gaunt elasser integrals,
        while I_e is the identity matrix of size Nsv
        
        :param Nu: dimension of the flow
        :type Nu: int
        :return: matrix Nsv x (Nu + Ne)
        :rtype: numpy array
        """
        assert Ab.shape[1] == Nu, "mismatch in dimension of Ab {} and flow {} ".format(Ab.shape[1], Nu)
        Nsv = self.cfg.Nsv

        Hz = np.zeros((Nsv, Nu + Nsv), order="F")

        # Set the observation operator to A(B) for U
        Hz[:Nsv, :Nu] = Ab
        # and identity for E
        Hz[:Nsv, Nu:Nu + Nsv] = np.identity(Nsv) * self.cfg.compute_e
        return Hz

    def remove_small_correlations(self, input_core_state, eps=1e-10):
        """
        Apply the graphical lasso to the correlation matrix. The correlation matrix is computed from
        the covariance matrix, either Pzz or Pbb in practice, with C[i, j] = P[i, j] / (P[i, i] P[j, j]).
        Warning: In some cases, some variance elements can be zero, for instance if the initialisation
        parameter, core_state_init, is set to constant. Then the correlation matrix cannot be computed
        and the Glasso is not applied.

        If the glasso parameter, self.cfg.remove_spurious, is set to 0 (np.inf), then the resp. diagonal (empirical)
        covariance matrix is returned.
        Otherwise the glasso is applied on the correlation matrix.

        :param input_core_state: Corestate which can either be Z or B,
                                 at a given time for all realizations
        :param eps: threshold that determines if a value should be considered as null. During the division to
                    get the correlation matrix, null values are replaced by eps
        :type eps: float
        """

        # computation of the empirical Pzz_forecast
        P_forecast = common.cov(input_core_state)

        if self.cfg.remove_spurious == 0: # extreme case, keep the sample covariance matrix
            return P_forecast
        if self.cfg.remove_spurious == np.inf: # other extreme case, take only the diagonal of the covariance matrix
            return np.diag(np.diag(P_forecast))
        
        if np.any(np.diag(P_forecast) == 0):
            # avoid division by zeros (exactly zeros, small numbers are left) by regularization
            # with many zeros, might give a hard time to the graphical lasso algo, as result may not converge.
            zeros = np.squeeze(np.argwhere(np.diag(P_forecast) == 0))
            logging.warning('Some coefficients in the diagonal sample covariance matrix are very close to zero at {}'.format(zeros))
            diag_mask = np.zeros_like(P_forecast, dtype=bool)
            np.fill_diagonal(diag_mask, True)
            P_forecast[np.logical_and(P_forecast==0, diag_mask)] = eps # only modifies zeros present in the diagonal

        # if some values in the diagonal of the covariance matrix are zero, it will still give a 1 in the diagonal of the correlation matrix
        # compute correlation matrix
        diag_sq_inv = lambda M: np.diag(1 / np.sqrt(np.diag(M)))
        C_forecast = diag_sq_inv(P_forecast) @ P_forecast @ diag_sq_inv(P_forecast)

        # Compute the lasso approximation
        C_lasso = skcov.graphical_lasso(C_forecast, self.cfg.remove_spurious, max_iter=100)[0]

        # compute the P_lasso from P_forecast
        diag_sq = lambda M: np.diag(np.sqrt(np.diag(M)))

        return diag_sq(P_forecast) @ C_lasso @ diag_sq(P_forecast)
    

    def analyse_Z(self, input_core_state, sv_X, sv_H, sv_Rxx):
        """
        Returns the analysed data for the augmented state Z = [U ER] and SV by a BLUE given the observations.

        :param input_core_state: 2D CoreState containing the coefficient data
        :type input_core_state: CoreState
        :param sv_X: Observation data to use for the BLUE
        :type sv_X: Observation
        :param sv_H: Observation matrix to use for the BLUE
        :type sv_H: Observation
        :param sv_Rxx: Observation error to use for the BLUE
        :type sv_Rxx: Observation
        :return: 2D analysed Z U ER SV
        :rtype: 2D arrays (Nreal x Ncoef)

        """
        
        # compute necessary matrices for Kalman filter
        Pzz_forecast = self.remove_small_correlations(input_core_state.Z)

        analysed_Z = np.zeros((self.algo.nb_realisations, self.algo.config.Nz))
        analysed_SV = np.zeros((self.algo.nb_realisations, self.algo.config.Nsv))
        analysed_ER = np.zeros((self.algo.nb_realisations, self.algo.config.Nsv))
        analysed_U = np.zeros((self.algo.nb_realisations, self.algo.config.Nu2))
        sv_X_real = np.zeros(sv_X.shape)
        for i_idx, i_real in enumerate(self.algo.attributed_models):
            
            Ab = self.compute_Ab(input_core_state[i_real])

            # The complete H operator (No x Ncoefs of U) is:
            # Hsv (No x (Ncoefs of core + Ncoefs SV)) * Hz ((Ncoefs of core + Ncoefs SV) x Ncoefs of U)
            if self.algo.check_PCA():
                # if PCA
                complete_H = sv_H @ self.setup_Hz(Ab @ self.cfg.pcaU_operator.S_u, self.cfg.N_pca_u)
            else:
                # if no PCA
                complete_H = sv_H @ self.setup_Hz(Ab, self.cfg.Nu2)

            PzzHT = Pzz_forecast @ complete_H.T
            HPzzHT = complete_H @ PzzHT

            # Z is centered on 0 so we must remove the mean from the observation data
            # Y = Ab (U+U0) + (ER + ER0) => Y - Ab U0 - ER0 = Ab U + ER
            sv_X_real[i_real] = sv_X[i_real] - (sv_H @ Ab @ self.algo.avg_prior["U"] + sv_H @ self.algo.avg_prior["ER"])

            analysed_Z[i_real] = common.compute_Kalman_huber_parameter_basis(input_core_state.Z[i_real],
                                                                                sv_X_real[i_real],
                                                                                HPzzHT,
                                                                                PzzHT,
                                                                                complete_H,
                                                                                sv_Rxx)
            analysed_U[i_real], analysed_ER[i_real] = self.algo.Z_to_U_ER(analysed_Z[i_real],1) 

            analysed_SV[i_real] = Ab @ analysed_U[i_real] + analysed_ER[i_real]
        
        comm.Allreduce(MPI.IN_PLACE, analysed_Z, op=MPI.SUM)
        comm.Allreduce(MPI.IN_PLACE, analysed_U, op=MPI.SUM)
        comm.Allreduce(MPI.IN_PLACE, analysed_ER, op=MPI.SUM)
        comm.Allreduce(MPI.IN_PLACE, analysed_SV, op=MPI.SUM)
        comm.Allreduce(MPI.IN_PLACE, sv_X_real, op=MPI.SUM)

        # Compute the misfits for SV (Y - HX)
        HX_z = (complete_H @ np.transpose(analysed_Z)).T
        self.current_misfits["SV"] = common.compute_misfit(
                sv_X_real, HX_z, linalg.inv(sv_Rxx)
                )

        return analysed_Z, analysed_U, analysed_ER, analysed_SV
    

class AugkfAnalyserAR3(AugkfAnalyserAR1):
    
    def analysis_step(self, input_core_state, Z_AR3, do_backward=False):
        """ Does the analysis at time t on the B and Z=[UE] part of the input_core_state.
        Updates SV = A(B)U - ER in consequence.

        :param input_core_state: Core state at time t
        :type input_core_state: corestates.CoreState (dim: nb_realisations x Ncorestate)
        :param ZAR3: AR3 forecast state
        :type ZAR3: np.array (dim: nb_realisations x 3 x Nz)
        :return: the analysed core state and updated ZAR3
        :rtype: corestates.CoreState (dim: nb_realisations x Ncorestate), np.array (dim: nb_realisations x 3 x Nz)
        """

        self.do_backward_analysis = do_backward

        # Check corestate is 3D
        if input_core_state.B.ndim != 3:
            raise ValueError("Corestate must be 3D but is ({}D)".format(input_core_state.B.ndim))
        
        # Check the dim of core_state
        if input_core_state.B.shape[0] != self.algo.nb_realisations:
            raise ValueError(
                "Number of realisations in input_core_state ({}) is not equal to expected number ({})".format(
                    input_core_state.B.shape[0], self.algo.nb_realisations
                )
            )
        
        ratio = self.algo.config.dt_a_f_ratio
        
        # copy core state
        if self.do_backward_analysis is True:
            ana_core_state = input_core_state[:,2*ratio].copy()
        else:
            ana_core_state = input_core_state[:,ratio].copy()
        
        # if no analysis
        if not self.sv_analysis() and not self.mf_analysis(): # no analysis of mf and sv
            return ana_core_state[self.algo.attributed_models], Z_AR3
        

        if self.mf_analysis():
            # compute the full state B B' B'' at analysis time by fitting B forecast curves
            B_full = self.compute_full_state(input_core_state.B)

            # compute analysis on B B' B'' and return b_minus b b_plus
            b_minus, b, b_plus = self.analyse_B(B_full, self.mf_X, self.mf_H, self.mf_Rxx)
            
            # update ana B state
            if self.do_backward_analysis is True:
                ana_core_state.B = b_plus
            else:
                ana_core_state.B = b
        else:
            #else we simply pick b_minus b b_plus from forecast values
            b_minus = input_core_state.B[:, 0]
            b = input_core_state.B[:, ratio]
            b_plus = input_core_state.B[:, 2*ratio]

        if self.sv_analysis():
            # compute the full state Z Z' Z'' at analysis time by fitting Z forecast curves
            Z_full = self.compute_full_state(input_core_state.Z)

            # perform the analysis on Z Z' Z''
            (ana_Z,
            ana_dZ,
            ana_d2Z,
            ana_core_state.U,
            ana_core_state.ER,
            ana_core_state.SV) = self.analyse_Z(Z_full, self.sv_X, self.sv_H, self.sv_Rxx, b_minus, b, b_plus, input_core_state[0,ratio])
            
            # update ana Z state
            ana_core_state.Z = ana_Z
            # update ana dUdt and ana dERdt state
            ana_core_state.dUdt, ana_core_state.dERdt = self.algo.dZ_to_dU_dER(ana_dZ)
            # update ana d2Udt2 and ana d2ERdt2 state
            ana_core_state.d2Udt2, ana_core_state.d2ERdt2 = self.algo.dZ_to_dU_dER(ana_d2Z)

            # update Z_AR3
            Z_AR3 = self.init_forecast(ana_Z, ana_dZ, ana_d2Z)

            return ana_core_state[self.algo.attributed_models], Z_AR3[self.algo.attributed_models]
        
        else:
            return ana_core_state[self.algo.attributed_models], Z_AR3
        

    def build_H_operator(self, X, row_diff = False):
        """
        Compute observation H operator

        :param X: X matrix (2D or 3D)
        :param row_diff: tells if X is the same everywhere (2D X) or different (3D X)
        """

        dt_a = self.algo.config.dt_a


        # Issue #79
        # Changing state accordingly, whether we use backward or centered method
        if self.do_backward_analysis is False:
            if row_diff == False:
                o = np.zeros(X.shape)
                return np.concatenate((np.concatenate((X, -dt_a * X, dt_a**2/2*X),axis=1),
                                np.concatenate((X, o, o),axis=1),
                                np.concatenate((X, dt_a * X, dt_a**2/2*X),axis=1))
                                ,axis=0)
            else:
                o = np.zeros(X[1].shape)
                return np.concatenate((np.concatenate((X[0], -dt_a * X[0], dt_a**2/2*X[0]),axis=1),
                                np.concatenate((X[1], o, o),axis=1),
                                np.concatenate((X[2], dt_a * X[2], dt_a**2/2*X[2]),axis=1)),axis=0)
        else:
            if row_diff == True:
                o = np.zeros(X[1].shape)

                return np.concatenate((
                    np.concatenate((X[0], -2 * dt_a * X[0], 2 * dt_a**2 * X[0]), axis=1),
                    np.concatenate((X[1], -dt_a * X[1], dt_a**2 / 2 * X[1]), axis=1),
                    np.concatenate((X[2], o, o), axis=1)
                ), axis=0)
            else:
                o = np.zeros(X.shape)

                return np.concatenate((
                    np.concatenate((X, -2 * dt_a * X, 2 * dt_a**2 * X),axis=1),
                    np.concatenate((X, -dt_a * X, dt_a**2 / 2 * X),axis=1),
                    np.concatenate((X, o, o),axis=1)
                ), axis=0)

    def build_obs_operator(self, obs):
        """
        Compute observation data operator

        :param obs: Observation data
        """

        return np.concatenate((obs[0],obs[1],obs[2]),axis=1)
    

    def build_err_operator(self, error):
        """
        Compute error operator

        :param error: Observation error
        """
        
        return linalg.block_diag(error[0], error[1], error[2])
        

    def compute_full_state(self, X_state):
        """
        Compute Least Square fit of X_ta, dX_ta and d2X_ta

        :param X_state: X_state
        :type X_state: 3D np array (dim: n_reals x nb_forecasts x ncoefs)
        :return: Xfull
        :rtype: 2D np array (dim: n_reals x 3*ncoefs)
        """
        N_coef = X_state.shape[2]
        ratio = self.algo.config.dt_a_f_ratio
        dt_f = self.algo.config.dt_f

        dt = np.arange(start = -ratio, stop = ratio + 1, step = 1) * dt_f

        # Issue #79
        # When doing the backward analysis, we retrieve (ta - 2dt, ta - dt, ta) states instead
        # of the usual centered (ta - dt, ta, ta + dt) states.
        if self.do_backward_analysis is True:
            dt = np.arange(start = -2 * ratio, stop = 1, step = 1) * dt_f

        dt = dt.reshape(-1,1)
        
        #build G operator
        G = np.ones(dt.shape)
        max_order = 2
        for i in range(max_order):
            i = i + 1
            dG = np.power(dt,i)/np.math.factorial(i)
            G = np.concatenate((G, dG), axis=1)

        X_full = np.zeros((self.algo.nb_realisations, 3 * N_coef))
        for i_idx, i_real in enumerate(self.algo.attributed_models):
            Y = X_state[i_real]
            # perform best fit L2 norm
            S = np.linalg.inv(G.T @ G) @ G.T @ Y
            X_full[i_real] = S[:3].flatten()

        comm.Allreduce(MPI.IN_PLACE, X_full, op=MPI.SUM)

        return X_full
    
    
    def analyse_B(self, B_full, mf_X, mf_H, mf_Rxx):
        """
        Returns the analysed data for B by a BLUE given the observations.

        :param input_B: NumPy array containing the coefficient data of B
        :type input_B: np.array (dim: nb_realisations x Nb)
        :param mf_X: Observation data to use for the BLUE
        :type mf_X: Observation
        :param mf_H: Observation matrix to use for the BLUE
        :type mf_H: Observation
        :param mf_Rxx: Observation error to use for the BLUE
        :type mf_Rxx: Observation
        :return: b_minus, b, b_plus
        :rtype: np.array (dim: nb_realisations x Nb), np.array (dim: nb_realisations x Nb), np.array (dim: nb_realisations x Nb)
        """

        Nb = self.cfg.Nb

        #obs operator
        Hb = self.build_H_operator(mf_H, row_diff = True)
        #obs data
        Yb = self.build_obs_operator(mf_X)
        #obs error
        Rbb = self.build_err_operator(mf_Rxx)
        # compute Pbb from B_full state (B B' B'')
        Pbb = self.remove_small_correlations(B_full)

        # Updates the B part of the core_state by the result of the Kalman filter for each model
        logging.debug("Getting best linear unbiased estimate of B...")
        analysed_B = np.zeros((self.algo.nb_realisations, 3*Nb))

        if self.cfg.kalman_norm == 'l2': # for non least square norm, iteration are needed
            Kbb = common.compute_Kalman_gain_matrix(
                Pbb, Hb, Rbb
            )
            for i_idx, i_real in enumerate(self.algo.attributed_models):
                analysed_B[i_real] = common.get_BLUE(
                    B_full[i_real],
                    Yb[i_real],
                    Pbb,
                    Hb,
                    Rbb,
                    K=Kbb,
                )

        elif self.cfg.kalman_norm == 'h875ber':
            # compute inverse of P_bb before loop on reals using its symmetry
            P_eig_val, P_eig_vec = np.linalg.eigh(Pbb)
            P_eig_val[P_eig_val < 1e-10] = 1e-10 # in case if matrix is not full rank, which should not happen
            Pbb_inv = P_eig_vec @ np.diag(1 / P_eig_val) @ P_eig_vec.T
            for i_idx, i_real in enumerate(self.algo.attributed_models):
                analysed_B[i_real] = common.compute_Kalman_huber(B_full[i_real], Yb[i_real], 
                                                Pbb_inv, Hb, Rbb)
        else:
            raise ValueError('Invalid value of param kalman_norm, should be equal to huber or l2, got {}'.format(self.cfg.kalman_norm))
        
        # gather process
        comm.Allreduce(MPI.IN_PLACE, analysed_B, op=MPI.SUM)

        # Compute the misfits for B (Y - HX)
        HX_b = np.transpose(np.matmul(Hb, np.transpose(analysed_B)))
        self.current_misfits["MF"] = common.compute_misfit(
            Yb, HX_b, linalg.inv(Rbb)
        )
        
        # compute b_minus b b_plus from B,B'B''
        B_a = analysed_B @ self.build_H_operator(np.identity(Nb)).T
        
        #extract b_minus b b_plus
        b_minus = B_a[:,:Nb]
        b = B_a[:,Nb:2*Nb]
        b_plus = B_a[:,2*Nb:]

        return b_minus, b, b_plus
    

    def build_full_Hz(self,input_core_state, b_minus, b, b_plus, sv_H):
        """
        Build the full observation matrix H for Z
        """
        #loop over ta_minus ta ta_plus
        for i in range(3):
            if i == 0:
                input_core_state.B = b_minus
            elif i == 1: 
                input_core_state.B = b
            elif i == 2:
                input_core_state.B = b_plus
            Ab = self.compute_Ab(input_core_state)
            if self.algo.check_PCA():
                # if PCA
                h = sv_H[i] @ self.setup_Hz(Ab @ self.cfg.pcaU_operator.S_u, self.cfg.N_pca_u)
            else:
                # if no PCA
                h = sv_H[i] @ self.setup_Hz(Ab, self.cfg.Nu2)
            try:
                H = H + [h]
                Ab_out = Ab_out + [Ab]
            except:
                H = [h]
                Ab_out = [Ab]
        return H, Ab_out
    

    def analyse_Z(self, Z_full, sv_X, sv_H, sv_Rxx, b_minus, b, b_plus, input_core_state):
        """
        Returns the analysed data for the augmented state Z and SV that maximizes the
        likelihood (of the Gaussian or Huber distribution)

        :param input_core_state: Z_full 
        :type input_core_state: np.array (dim: nb_realisations x 3*Nz)
        :param sv_X: Observation data to use for the BLUE
        :type sv_X: Observation
        :param sv_H: Observation matrix to use for the BLUE
        :type sv_H: Observation
        :param sv_Rxx: Observation error to use for the BLUE
        :type sv_Rxx: Observation
        :param b_minus: b at ta-
        :type b_minus: np.array (dim: nb_realisations x Nb)
        :param b: b at ta
        :type b: np.array (dim: nb_realisations x Nb)
        :param b_plus: b at ta+
        :type b_plus: np.array (dim: nb_realisations x Nb)
        :return: ana_Z, ana_dZ, ana_d2Z, ana_U, ana_ER, ana_SV
        :rtype: np.array (dim: nb_realisations x Ncoef)
        """
        
        Nz = self.cfg.Nz

        # build obs error
        Rzz = self.build_err_operator(sv_Rxx)

        # compute necessary matrices for Kalman filter
        PZZ = self.remove_small_correlations(Z_full)

        analysed_Z = np.zeros((self.algo.nb_realisations, 3*Nz))
        analysed_SV = np.zeros((self.algo.nb_realisations, self.algo.config.Nsv))
        analysed_ER = np.zeros((self.algo.nb_realisations, self.algo.config.Nsv))
        analysed_U = np.zeros((self.algo.nb_realisations, self.algo.config.Nu2))
        sv_X_real = [np.zeros(sv_X[0].shape), np.zeros(sv_X[1].shape), np.zeros(sv_X[2].shape)]
        Yz = np.zeros((self.algo.nb_realisations, sv_X[0].shape[1] + sv_X[1].shape[1] + sv_X[2].shape[1]))
        for i_idx, i_real in enumerate(self.algo.attributed_models):

            # build obs operator complete
            H, Ab = self.build_full_Hz(input_core_state, b_minus[i_real], b[i_real], b_plus[i_real], sv_H)
            Hz = self.build_H_operator(H, row_diff = True)
            # Z is centered on 0 so we must remove the mean from the observation data
            # Y = Ab (U+U0) + (ER + ER0) => Y - Ab U0 - ER0 = Ab U + ER
            for i in range(3):
                sv_X_real[i][i_real] = sv_X[i][i_real] - (sv_H[i] @ Ab[i] @ self.algo.avg_prior["U"] + sv_H[i] @ self.algo.avg_prior["ER"])
  
            # build obs data
            Yz[i_real] = np.concatenate((sv_X_real[0][i_real],sv_X_real[1][i_real],sv_X_real[2][i_real]),axis=0)
            
            PzzHT = PZZ @ Hz.T
            HPzzHT = Hz @ PzzHT

            analysed_Z[i_real] = common.compute_Kalman_huber_parameter_basis(Z_full[i_real],
                                                                                Yz[i_real],
                                                                                HPzzHT,
                                                                                PzzHT,
                                                                                Hz,
                                                                                Rzz)

            analysed_U[i_real], analysed_ER[i_real] = self.algo.Z_to_U_ER(analysed_Z[i_real,:Nz],1) 
            
            if self.do_backward_analysis is True:
                analysed_SV[i_real] = Ab[2] @ analysed_U[i_real] + analysed_ER[i_real]
            else:
                analysed_SV[i_real] = Ab[1] @ analysed_U[i_real] + analysed_ER[i_real]

        # gather process
        comm.Allreduce(MPI.IN_PLACE, analysed_U, op=MPI.SUM)
        comm.Allreduce(MPI.IN_PLACE, analysed_ER, op=MPI.SUM)
        comm.Allreduce(MPI.IN_PLACE, analysed_SV, op=MPI.SUM)
        comm.Allreduce(MPI.IN_PLACE, analysed_Z, op=MPI.SUM)
        comm.Allreduce(MPI.IN_PLACE, Yz, op=MPI.SUM)

        # Compute the misfits for SV (Y - HX)
        HX_z = (Hz @ np.transpose(analysed_Z)).T
        self.current_misfits["SV"] = common.compute_misfit(
                Yz, HX_z, linalg.inv(Rzz)
                )
        
        # extract ana_Z ana_dZ ana_d2Z
        ana_Z = analysed_Z[:,:Nz]
        ana_dZ = analysed_Z[:,Nz:2*Nz]
        ana_d2Z = analysed_Z[:,2*Nz:]

        return ana_Z, ana_dZ, ana_d2Z, analysed_U, analysed_ER, analysed_SV
    
    def init_forecast(self, Z, dZ, d2Z):
        """
        init ZAR3 state for forecast

        :param Z: Z state
        :type Z: 2D np array (dim: n_reals x nz)
        :param dZ: dZ state
        :type dZ: 2D np array (dim: n_reals x nz)
        :param d2Z: d2Z state
        :type d2Z: 2D np array (dim: n_reals x nz)
        :return: init_Z
        :rtype: 3D np array (dim: n_reals x 3 x nz)
        """
        init_Z = np.zeros((self.algo.nb_realisations, 3, self.algo.config.Nz))
        dt_f = self.algo.config.dt_f
    
        init_Z[:, 2] = Z
        init_Z[:, 1] = Z - dt_f * dZ + dt_f**2 / 2 * d2Z
        init_Z[:, 0] = Z - (2*dt_f) * dZ + (2*dt_f)**2 / 2 * d2Z

        return init_Z