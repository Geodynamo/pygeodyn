import numpy as np
import os
import time
import logging
from pygeodyn.corestates import with_core_state_of_dimensions
from pygeodyn.utilities import date_array_to_decimal
import pygeodyn

class Hdf5GroupHandler:
    def __init__(self, dates, nb_realisations, core_state, hdf5_file, group_name, format, exclude = ['Z']):
        self.mother_file = hdf5_file
        self.group = self.mother_file.create_group(group_name)
        self.dates = dates
        # Convert unsupported type datetime64 in strings
        if self.dates.dtype == 'datetime64[M]':
            self.dates = date_array_to_decimal(self.dates, 2)
        self.group.create_dataset('times', data=self.dates, dtype=format)

        self.handlers = {}
        self.exclude = exclude
        for measure in core_state.measures:
            if measure not in exclude:
                self.handlers[measure] = self.group.create_dataset(measure, data = np.zeros((nb_realisations,core_state._measures[measure].shape[1],core_state._measures[measure].shape[2])), dtype=format)

    @with_core_state_of_dimensions(3)
    def update_all(self, core_state, attributed_models):
        for measure in self.handlers:
            for i_idx, i_real in enumerate(attributed_models):
                self.handlers[measure][i_real] = core_state._measures[measure][i_idx]
    
    def close(self):
        self.mother_file.close()


def saveReadme(readme_path, config, algo_name, nb_realisations, git_info, elapsed_time, parallel, seed):
    try:
        readme = open(os.path.join(readme_path, 'README.md'), 'w')
    except IOError:
        logging.error('Problem in creating README at {} ! Other files will be saved but no README will be generated.'.format(readme_path))
        return -1

    if parallel > 1:
        readme.write('Parallel algorithm with {} process'.format(parallel))
    else:
        readme.write('Sequential algorithm ')

    readme.write('{} (pygeodyn v{}) finished at {} in {:.2f} s\n'.format(algo_name.upper(), pygeodyn.__version__, time.asctime(), elapsed_time))
    readme.write('---\n')
    readme.write('Git branch: {}, last message: "{}", version and sha1: {}'.format(*git_info))
    readme.write('---\n')
    readme.write('{} realisations were considered with seed {}'.format(nb_realisations, seed))

    wo = 'with' if config.compute_e else 'without'
    readme.write(' {} computation of sub-grid errors\n'.format(wo))

    readme.write('-------Config file----------\n\n')
    write_conf(readme, config)

    readme.write('----------------------------\n')

    readme.close()
    return 0

def conf_dict(config):
    """
    Rewrite the config.__dict__ in a form that is ready to be printed in a text file.
    """

    file_attrs = config.__dict__

    keys_without_conversion = ['dt_f', 'dt_a_f_ratio', 'dt_smoothing', 't_start_analysis', 't_end_analysis', 'compute_e', 'N_pca_u', 'AR_type', 'combined_U_ER_forecast', 'core_state_init',
                                'Lb', 'Lu', 'Lsv', 'TauU', 'TauE',
                                'prior_dir', 'prior_type', 'obs_dir', 'obs_type',
                                'Nth_legendre']
    optional_keys = ['remove_spurious', 'pca_norm', 'init_file', 'obs_select', 'discard_high_lat']
    for key in optional_keys:
        if key in file_attrs:
            keys_without_conversion.append(key)
    extracted_dict = {k: file_attrs[k] for k in keys_without_conversion}

    # Converting dt stored as fractions of years in timedelta64 months.
    dt_to_convert = ['TauU', 'TauE']
    for dt in dt_to_convert:
        # Add decimal to the dt name except for Tau
        key = dt
        try:
            extracted_dict[dt] = np.timedelta64(int(file_attrs[key]*12), 'M')
        except:
            raise KeyError('Key not found')
    return extracted_dict

def write_conf(readme, config):
    extracted_dict = conf_dict(config)

    for k, v in extracted_dict.items():
        if type(v).__module__ == 'numpy':

            if k[:3] == 'Tau':
                v_type = 'years'
            else:
                # convert the int64,float32... to native python type, int, float...
                v_type = type(v.item()).__name__
        else:
            if k == 'obs_select':
                v_type = 'dict'
            else:
                v_type = type(v).__name__

        readme.write('{} {} {} \n'.format(k, v_type, v))
