""" Contains generic reading functions """
import numpy as np
from collections import defaultdict
import os.path
import logging
import h5py
from . import priors as priors_module
from .. import utilities
import cdflib
import h5py
from operator import add

def extract_realisations(prior_directory, prior_type, dt_sampling, measures=None):
    """
    Return prior data arrays (nb_realisations x nb_coefs) by dynamically getting the reading function from the prior type.

    :param prior_directory: directory where the prior data is stored
    :type prior_directory: str
    :param prior_type: type of the prior data
    :type prior_type: str
    :param dt_sampling: smoothing window duration in second
    :type dt_sampling: float
    :param measures: sequence of measures to extract, allowed values are a selection of 'times', 'B', 'U' and 'ER'
    :return: MF, U, ER, times, dt_samp, tag
    :rtype: lists
    """
    # statically get the function name from the prior type

    if prior_type == '0path':
        func_name = 'read_0_path'
    elif prior_type == '50path' or prior_type == '71path':
        func_name = 'read_50_and_71_path'
    elif prior_type == '100path':
        func_name = 'read_100_path'
    else:
        func_name = None
    reading_prior_function = getattr(priors_module, func_name, None)

    if reading_prior_function is None:
        raise NotImplementedError('No loading function was defined for {} prior'.format(prior_type))
    logging.info('Reading {} data for the priors {}...'.format(prior_type, measures))

    # Read and return prior data arrays (nb_realisations x nb_coefs)
    logging.info('function name {}'.format(reading_prior_function.__name__))
    return reading_prior_function(prior_directory, prior_type, dt_sampling, measures=measures)


def read_analysed_states_hdf5(filepath):
    if not os.path.isfile(filepath):
        raise IOError('{} is not a valid file ! Please check the given path'.format(filepath))

    output_dict = {}
    with h5py.File(filepath, 'r') as hdf_file:
        if 'analysed' in  hdf_file.keys():
            init_data = hdf_file['analysed']
        else:
            raise ValueError('{} does not contain "analysed" data! Initialisation from file impossible'.format(filepath))

        for measureName, measureData in init_data.items():
            output_dict[measureName] = np.array(measureData)

    return output_dict


def parse_config_file(file_name=None):
    """ Reads the config file and returns a dictionary of the config. Supported types for reading: int, float, str, bool

    :param file_name: file name storing the config
    :type file_name: str
    :return: the configuration read: dict["parameter"] = value
    :rtype: dict
    """
    if file_name is None:
        raise IOError('Cannot read configuration : no file path was given !')
    if not os.path.isfile(file_name):
        raise IOError('{} is not a valid file ! Please check the given path'.format(file_name))

    config = {}
    with open(file_name, "r") as handle:
        for l in handle:
            if l.strip().startswith("#") or l.strip() == '':
                continue
            parameter, p_type = l.split()[:2]
            vals = l.split()[2:]
            p_val = vals[0] if len(vals) == 1 else vals
            if p_type == "int":
                config[parameter] = int(p_val)
            elif p_type == "float":
                config[parameter] = float(p_val)
            elif p_type == "str":
                config[parameter] = p_val
            elif p_type == "bool":
                if p_val == '0' or p_val.lower() == 'false' or p_val.lower() == 'no':
                    config[parameter] = False
                else:
                    config[parameter] = bool(p_val)
            elif p_type == "csv":
                config[parameter] = np.array(p_val.split(','))
            # Reading of date objects
            elif p_type == "date":
                # If only year is given, assume Jan
                if len(p_val.split('-')) <= 1:
                    p_val += "-01"
                config[parameter] = np.datetime64(p_val, 'M')
            elif p_type == "months":
                config[parameter] = np.timedelta64(p_val, 'M')
            elif p_type == "years":
                # Convert in months
                config[parameter] = np.timedelta64(int(p_val)*12, 'M')
            elif p_type == "dict":
                p_val = ' '.join(p_val) # taking care of spaces
                config[parameter] = eval(p_val)
            else:
                raise ValueError("type not (yet) supported: {}".format(p_type))

    return config


def read_prior(filename, max_cols=None, **kwargs):
    """
    Uses genfromtxt to read the prior data. Returns the average :math:`<X>`, the RMS
    :math:`\sqrt{<(X-<X>)^2>}` and deviation :math:`X-<X>` of the prior.

    :param filename: path of the prior file to read
    :type filename: str
    :param max_cols: number of columns to read
    :type max_cols: int or None
    :param kwargs: keyword args passed to genfromtxt.
    :type kwargs: kwargs
    :return: tuple containing the average, the root-mean-square and the deviation with respect to the average of the prior. All arrays have dimensions (max_cols x max_rows).
    :rtype: (ndarray, ndarray, ndarray)
    """

    # Setting usecols in genfromtxt is faster than reading the full prior and slicing it.
    # If max_cols is ill-defined, read all cols
    if type(max_cols) is not int:
        raw_prior = np.genfromtxt(filename, **kwargs)
    else:
        raw_prior = np.genfromtxt(filename, usecols=range(0, max_cols), **kwargs)

    # Compute average
    avg_prior = np.average(raw_prior, axis=0)
    # Compute rms
    rms_prior = np.std(raw_prior, axis=0)
    # Compute deviation
    dev_prior = raw_prior - avg_prior

    return avg_prior, rms_prior, dev_prior


def read_single_computed_realisation(filename):
    dates = []
    data = []
    with open(filename, 'r') as f:
        for line in f:
            values = line.split()
            dates.append(values[0])
            data.append(values[1:])

    return np.array(dates, dtype=np.float64), np.array(data, dtype=np.float64)


def read_all_computed_realisations(basename, nb_realisations):
    all_measures = []
    dates = []
    for i in range(0, nb_realisations):
        real_name = basename.replace('*', str(i))
        print('Reading {}...'.format(real_name))
        dates, measure_data = read_single_computed_realisation(real_name)
        all_measures.append(measure_data)

    return dates, np.array(all_measures, dtype=np.float64)


def read_observatories_cdf_data(cdf_filename, measure_type, obs, remove_bias_crust=True,
                                huge_sigma=99999, discard=False):
    """
    Reads the data of observatories file in cdf format.

    huge_sigma: Replace Nans in sigma by this big value

    :param cdf_filename: path of the cdf observation file to read
    :type filename: str
    :param measure_type: either 'MF' or 'SV' for main field or secular variation
    :type filename: str
    :param obs: abreviation of the satellite name, possible values 'GO' (ground observations), 'CH' (Champ), 'SW' (Swarm), 'OR' (oersted), 'CR' (cryosat), 'CO' (composite)
    :type filename: str
    :param huge_sigma: Replace Nans in sigma by this big value
    :type cdf_filename: str
    :param cdf_filename: name of the .cdf file
    """
    possible_obs = np.array(['GO', 'CH', 'SW', 'OR', 'CR', 'GR', 'CO'])
    if not np.any(obs == possible_obs):
        raise ValueError('Got {} as obs value, possible values are {}'.format(obs, possible_obs))
    if not np.any(measure_type == np.array(['MF', 'SV'])):
        raise ValueError('Got {} as measure_type, possible values are {}'.format(measure_type, ('MF', 'SV')))

    cdf_file = cdflib.CDF(cdf_filename)

    thetas = 90 - cdf_file.varget("Latitude") # convert in colatitude
    if np.any(thetas > 180) or np.any(thetas < 0):
        raise ValueError('angle th {} represent the colatitude, did you use latitudes instead?'.format(thetas))

    thetas = thetas * np.pi / 180 # convert in rad
    phis = cdf_file.varget("Longitude") * np.pi / 180
    radii = cdf_file.varget('Radius') / 1000 # to convert in kms
    assert np.amax(radii) < 10000 and np.amin(radii) > 6000, 'Are you sure radii are stored in meters?'

    if measure_type == 'MF':
        times = cdf_file.varget('Timestamp')
        Bs, sigma2 = cdf_file.varget('B_CF'), cdf_file.varget('sigma_CF')**2
        if remove_bias_crust and obs == 'GO':
            Bs -= cdf_file.varget('bias_crust')
    elif measure_type == 'SV':
        times = cdf_file.varget('Timestamp_SV')
        Bs, sigma2 = cdf_file.varget('B_SV'), cdf_file.varget('sigma_SV')**2
    Bs[np.where(Bs==0)] = np.nan
    sigma2[np.where(sigma2==0)] = np.nan

    err = 'At least some invalid values of sigma correspond to valid values in B, file, measure and obs {}, {}, {}'
    err = err.format(cdf_filename.split('/')[-1], measure_type, obs)

    if obs == 'GO':
        locs = [''.join([l[0] for l in loc]) for loc in cdf_file.varget('Obs')]
        N_VO = len(list(dict.fromkeys(zip(thetas, phis, locs)))) # small trick that counts the number of unique pairs of theta/phi/locs
    else:
        N_VO = len(dict.fromkeys(zip(thetas, phis)).keys()) # small trick that counts the number of unique pairs of theta/phi
    dates = utilities.cdf_times_to_np_date(times)
    # dates = [np.datetime64('{}-{:02}'.format(*cdflib.cdfepoch.breakdown_epoch(t)[:2])) for t in times]
    # Precise the first date as cdf files are not human readable and therefore it is not easy to find the date manually
    # logging.info('First data available at {}'.format(dates[0]))

    obs_data = defaultdict(list) # list returns []
    obs_positions = defaultdict(list)
    var_data = defaultdict(list)

    if discard:
        to_deg = lambda alpha: 180 * alpha / np.pi
        to_rad = lambda alpha: np.pi * alpha / 180
        chaos_file = '../data/observations/CHAOS-7/CHAOS-7.hdf5' # takes Chaos file from usual data location
        chaos_file = os.path.join(os.path.dirname(__file__), chaos_file) # relative path to absolute
        times_chaos, dipoles = utilities.extract_dipole_chaos(chaos_file)

    for i, date in enumerate(dates):
        if discard: # if discard high_lat set to 0, will pass.
            g_10, g_11, h_11 = dipoles[np.argmin(abs(times_chaos - times[i])), :3]
            th_0, ph_0 = utilities.north_geomagnetic_pole_angle(g_10, g_11, h_11)
            theta_d = to_deg(utilities.geomagnetic_latitude(thetas[i], phis[i], th_0, ph_0)) # all angles should be in rad
            if theta_d > 90 + discard or theta_d < 90 - discard:
                radii[i] = np.nan
                thetas[i] = np.nan
                phis[i] = np.nan
                Bs[i] = np.nan
                sigma2[i] = np.nan

        # Add positions
        # Convert angles in rad
        obs_positions[date].extend((radii[i], thetas[i], phis[i]))
        # Add measurement data
        obs_data[date].extend(tuple(Bs[i]))
        # add errors
        var_data[date].extend(tuple(sigma2[i, :]))
    
    # if MF measure, compute a simple time average to sync MF and SV dates 
    if measure_type == "MF":
        obs_data_sync = defaultdict(list) # list returns []
        obs_positions_sync = defaultdict(list)
        var_data_sync = defaultdict(list)
        mask_data_sync = defaultdict(list)
        sat_name_sync = defaultdict(list)
        unique = np.unique(np.array(dates))
        for i, date in enumerate(unique):
            if i < unique.shape[0]-1:
                dt = (unique[i+1] - unique[i])/2
                date_sync = unique[i] + dt
                 
                # time i
                pos1 = np.array(obs_positions[unique[i]])
                data1 = np.array(obs_data[unique[i]])
                var1 = np.array(var_data[unique[i]])
                
                # time i+1
                pos2 = np.array(obs_positions[unique[i+1]])
                data2 = np.array(obs_data[unique[i+1]])
                var2 = np.array(var_data[unique[i+1]])
                
                # compute average
                obs_positions_sync[date_sync] = (pos1 + pos2)/2
                obs_data_sync[date_sync] = (data1 + data2)/2
                var_data_sync[date_sync] = (var1 + var2)/4
                
                #update sync dicts
                mask = np.logical_not(np.isnan(obs_positions_sync[date_sync]) | np.isnan(obs_data_sync[date_sync]) | np.isnan(var_data_sync[date_sync]))
                obs_positions_sync[date_sync] = list(obs_positions_sync[date_sync][mask])
                obs_data_sync[date_sync] = list(obs_data_sync[date_sync][mask])
                var_data_sync[date_sync] = list(var_data_sync[date_sync][mask])
                if obs == "SW":
                    mask_data_sync[date_sync] = mask
                else:
                    mask_data_sync[date_sync] = np.array([])
                sat_name_sync[date_sync] = list(obs) * len(obs_data_sync[date_sync])
    else:
        obs_data_sync = defaultdict(list) # list returns []
        obs_positions_sync = defaultdict(list)
        var_data_sync = defaultdict(list)
        mask_data_sync = defaultdict(list)
        sat_name_sync = defaultdict(list)
        unique = np.unique(np.array(dates))
        for i, date in enumerate(unique):
            
            # time i
            obs_positions_sync[unique[i]] = np.array(obs_positions[unique[i]])
            obs_data_sync[unique[i]] = np.array(obs_data[unique[i]])
            var_data_sync[unique[i]] = np.array(var_data[unique[i]])
            
            #update sync dicts
            mask = np.logical_not(np.isnan(obs_positions_sync[unique[i]]) | np.isnan(obs_data_sync[unique[i]]) | np.isnan(var_data_sync[unique[i]]))
            obs_positions_sync[unique[i]] = list(obs_positions_sync[unique[i]][mask])
            obs_data_sync[unique[i]] = list(obs_data_sync[unique[i]][mask])
            var_data_sync[unique[i]] = list(var_data_sync[unique[i]][mask])
            if obs == "SW":
                mask_data_sync[unique[i]] = mask
            else:
                mask_data_sync[unique[i]] = np.array([])
            sat_name_sync[unique[i]] = list(obs) * len(obs_data_sync[unique[i]])

    return obs_positions_sync, obs_data_sync, var_data_sync, mask_data_sync, sat_name_sync
