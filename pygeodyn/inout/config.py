import numpy as np
import logging
from pygeodyn.inout import reads
import pygeodyn
import os
from os.path import isfile, join

class ComputationConfig(object):
    """
    Defines the configuration of the computation, namely:
        - All core state degrees and number of coefs
        - Physical constants
        - Forecast and Analysis times
        - Paths of the prior and observation data
        - Output file features
    """

    # Initialization methods
    def __init__(self, do_shear, config_file=None):
        """
        Loads the config according to parameters. First tries to load config file, then config dict if the config_file is None.
        
        :param do_shear: control if we add shear config parameters to config dict
        :type do_shear: int
        :param config_file: filename containing the config (see format hereafter)
        :type config_file: str (default None)

        config file format:
            name type value

        Example :
        Lb int 10

        supported format: int/float/str/bool
        lines starting by # are not considered.
        """
        self.Lb = None
        self.Lsv = None
        self.Lu = None
        self.Ly = None

        self.do_shear = do_shear

        # data is a string, assuming the filename of a config file
        if config_file is not None:
            self.read_from_file(config_file)

        if self.Lb is None:
            raise ValueError("No maximal degree was read for the magnetic field !")
        if self.Lsv is None:
            raise ValueError("No maximal degree was read for the secular variation !")
        if self.Lu is None:
            raise ValueError("No maximal degree was read for the flow !")

        # Once the data was loaded, build all the relevant variables
        self.init_bools()
        self.init_out()
        self.init_physical_constants()
        self.init_paths()
        self.init_times()
        self.init_corestate_inits()
        self.init_observations()
        if self.do_shear:
            if self.Ly is None:
                raise ValueError("No maximal degree was read for the constraint on the shear !")
        
            self.init_shear()

    @property
    def Nb(self):
        return self.Lb * (self.Lb + 2)

    @property
    def Nsv(self):
        return self.Lsv * (self.Lsv + 2)

    @property
    def Nu(self):
        return self.Lu * (self.Lu + 2)
    
    @property
    def Ny(self):
        return self.Ly * (self.Ly + 2)

    @property
    def Nu2(self):
        return 2*self.Nu
    
    @property
    def Ny2(self):
        return 2*self.Ny
    
    @property
    def Nz(self):
        if self.pca:
            return self.N_pca_u + self.Nsv
        else:
            return self.Nu2 + self.Nsv
            
    @property
    def Nuz(self):
        if self.pca:
            return self.N_pca_u
        else:
            return self.Nu2

    def init_bools(self):
        """
        Init computation booleans.
        """
        if not hasattr(self, 'compute_e'):
            logging.warning("Compute ER was not set in the config file! Assuming True.")
            self.compute_e = 1

        if not hasattr(self, 'kalman_norm'):
            logging.warning("Kalman_norm was not set in the config file! Assuming l2 norm.")
            self.kalman_norm = 'l2'
        else:
            self.kalman_norm = self.kalman_norm.lower() # tolerance with caps
            
        if not hasattr(self, 'N_pca_u'):
            logging.warning("Number of coefficients for PCA of U was not set in the config file! Assuming no PCA.")
            self.N_pca_u = 0
            self.pca = 0
        else:
            if self.N_pca_u == 0:
                self.pca = 0
            else:
                self.pca = 1

        if not hasattr(self, 'remove_spurious'):
            logging.warning("Value to discard spurious correlations was not set! Assuming diagonal.")
            self.remove_spurious = np.inf

        if not hasattr(self, 'AR_type'):
            logging.warning("Type of AR process was not set in the config file! Assuming diagonal.")
            self.AR_type = 'diag'

        if self.N_pca_u == 0 and self.AR_type == "AR3":
            raise ValueError("AR3 process requires N_pca_u to be non-zero")
        
        if not hasattr(self, 'combined_U_ER_forecast'):
            logging.warning("U ER forecast dependancy was not set in the config file! Assuming independant.")
            self.combined_U_ER_forecast = 0

        if self.combined_U_ER_forecast == 1 and self.AR_type == 'diag':
            raise ValueError("U ER must be independant for diag forecast")
        
        # Checking if we export the data every analysis
        # Issue #70
        if not hasattr(self, "export_every_analysis"):
            logging.warning("Data exportation will be done at the end of calculations.")
            self.export_every_analysis = 0

        # Issue #79
        # Enabling backward analysis or not (defaulting to True)
        if not hasattr(self, "last_analysis_backward"):
            logging.warning("Doing last analysis using backward scheme")
            self.last_analysis_backward = 1

    def init_out(self):
        if not hasattr(self, 'out_computed'):
            logging.warning("Out_computed was not set in the config file ! Assuming computed states not saved.")
            self.out_computed = 0
        
        if not hasattr(self, 'out_analysis'):
            logging.warning("Out_analysis was not set in the config file ! Assuming analysis states saved.")
            self.out_analysis = 1

        if not hasattr(self, 'out_forecast'):
            logging.warning("Out_forecast was not set in the config file ! Assuming forecast states not saved.")
            self.out_forecast = 0

        if not hasattr(self, 'out_misfits'):
            logging.warning("Out_misfits was not set in the config file ! Assuming misfits not saved.")
            self.out_misfits = 0

        if not hasattr(self, 'out_format'):
            logging.warning("Out_format was not set in the config file ! Assuming single precision.")
            self.out_format = 'float32'
        


    def init_corestate_inits(self):
        if not hasattr(self, 'core_state_init'):
            logging.warning("Type of CoreState initialisation was not set in the config file! Assuming normal draw around average priors.")
            self.core_state_init = 'normal'

        if self.core_state_init == 'from_file':
            if not (hasattr(self, 'init_file') and hasattr(self, 'init_date')):
                logging.warning(
                    "CoreState initialisation was set to 'from_file' but no file or date were given. Falling back to normal draw initialisation.")
                self.core_state_init = 'normal'

    def init_paths(self):
        """
        Builds the full path according to the path given in conf or set default.
        """
        if not hasattr(self, 'prior_dir'):
            logging.warning("Prior directory was not set, assuming 71path.")
            self.prior_dir = 'data/priors/71path'
            self.prior_type = '71path'
        
        if self.prior_type == '0path':
            if not self.AR_type == "diag":
                raise ValueError('AR_type must be diag for 0path prior')


        if not hasattr(self, 'prior_type'):
            raise ValueError('Prior directory was set ({}) without prior type ! Please set the prior type.'.format(self.prior_dir))

        # Default behaviour is to read the COVOBS-x2 model as observations
        if not hasattr(self, 'obs_dir'):
            self.obs_dir = 'data/observations/COVOBS-x2_maglat'
            self.obs_type = 'COVOBS_hdf5'

        paths = ['prior_dir', 'obs_dir']
        if hasattr(self, 'init_file'):
            paths.append('init_file')

        for path_key in paths:
            # If not absolute, build the absolute path from the given relative path
            given_path = self.__dict__[path_key]
            if not os.path.isabs(given_path):
                given_path = os.path.normpath(os.path.join(pygeodyn._package_directory, given_path))

            if not os.path.exists(given_path):
                # Try to correct the path
                corrected_path = os.path.join(pygeodyn._package_directory, given_path.split('pygeodyn/')[-1])
                # If the corrected path does not exist either, throw an error
                if not os.path.exists(corrected_path):
                    raise IOError('{} given in config does not exist ! Did you supply a relative path instead of an absolute one ?'.format(given_path))
                given_path = corrected_path
            self.__dict__[path_key] = given_path


    def init_observations(self):
        if self.obs_type == 'GO_VO':
            if not hasattr(self, 'obs_select'):
                logging.warning('No values of govo_obs_list found, default take all obs files in observation directory')
                self.obs_select = all
            if not hasattr(self, 'discard_high_lat'):
                logging.warning("Suppression of high latitude data was not set, assuming no suppression")
                self.discard_high_lat = False
            if not hasattr(self, 'SW_err'): # FOR 4MONTHS SWARM DATA ONLY!!!
                logging.warning('SW_err was not set, assuming diag error matrix for SWARM')
                self.SW_err = "diag"
        else:
            self.obs_select = None
        
            

    def init_physical_constants(self):
        """
        Builds the physical constants of the computation.
        """

        # Check for theta steps for Legendre polynomial
        if "Nth_legendre" not in self.__dict__:
            logging.warning("No number of theta steps was read ! Using default value 64.")
            self.Nth_legendre = 64

        # Check the presence of times
        if "TauU" not in self.__dict__:
            logging.warning("No characteristic time was read for the core flow ! Using default value 30 yrs.")
            self.TauU = np.timedelta64(30, 'Y')
        if "TauE" not in self.__dict__:
            logging.warning("No characteristic time was read for the subgrid errors ! Using default value 10 yrs.")
            self.TauE = np.timedelta64(10, 'Y')

        # Convert TauU and TauE in decimal values (in years)
        year_timedelta64 = np.timedelta64(1, 'Y')
        self.TauU = self.TauU / year_timedelta64
        self.TauE = self.TauE / year_timedelta64

    def init_times(self):
        """ Builds the variables linked to the times of computation (forecasts and analysis). """

        if "t_start_analysis" not in self.__dict__:
            raise ValueError("No analysis starting time was read !")

        if "t_end_analysis" not in self.__dict__:
            raise ValueError("No analysis end time was read !")

        if "dt_f" not in self.__dict__:
            raise ValueError("No forecast timestep was read !")

        if self.dt_f == 0:
            raise ValueError("Forecast timestep cannot be zero !")
        
        if "dt_a_f_ratio" not in self.__dict__:
            raise ValueError("No dt_analysis/dt_forecast ratio was read !")

        if int(self.dt_a_f_ratio) != self.dt_a_f_ratio:
            raise ValueError("dt_a_f_ratio must be an integer!")
        else:
            self.dt_a = self.dt_a_f_ratio * self.dt_f

        if "N_dta_start_forecast" not in self.__dict__:
            self.N_dta_start_forecast = 1
        else:
            if self.N_dta_start_forecast < 1:
                raise ValueError("N_dta_start_forecast must be greater or equal to 1")
            
        if self.AR_type == "AR3":
            if "N_dta_end_forecast" not in self.__dict__:
                self.N_dta_end_forecast = 1
            else:
                if self.N_dta_end_forecast < 1:
                    raise ValueError("N_dta_end_forecast must be greater or equal to 1")
        else:
            if "N_dta_end_forecast" not in self.__dict__:
                self.N_dta_end_forecast = 0

        if "dt_smoothing" not in self.__dict__:
            logging.warning("No prior dt smoothing was read ! Using default value 3.2 years.")
            self.dt_smoothing = 3.2

        if "dt_sampling" not in self.__dict__:
            logging.warning("No prior dt_sampling was read ! Using default value 5 years.")
            self.dt_sampling = 5
        
        t_start_forecast = self.t_start_analysis - self.dt_a * self.N_dta_start_forecast
        t_end_forecast = self.t_end_analysis + self.dt_a * self.N_dta_end_forecast + self.dt_f

        # Build time arrays
        # First forecast at t_start + dt_f
        self.t_forecasts = np.arange(t_start_forecast, t_end_forecast, self.dt_f)
        # First analysis at t_start
        self.t_analyses = np.arange(self.t_start_analysis, self.t_end_analysis + self.dt_f, self.dt_a)
        if self.AR_type == "AR3":
            self.t_analyses_full = np.arange(self.t_start_analysis - self.dt_a, self.t_end_analysis + self.dt_a + self.dt_f, self.dt_a)
        else:
            self.t_analyses_full = np.arange(self.t_start_analysis - self.dt_a, self.t_end_analysis + self.dt_f, self.dt_a)

    def init_shear(self):

        if not hasattr(self, 'TauG'):
            logging.warning("TauG was not set in the config file! Using default value 20 yrs.")
            self.TauG = np.timedelta64(20, 'Y')

        year_timedelta64 = np.timedelta64(1, 'Y')
        self.TauG = self.TauG / year_timedelta64

        if not hasattr(self, 'remove_spurious_shear_u'):
            logging.warning("Value to discard shear U spurious correlations was not set! Assuming diagonal.")
            self.remove_spurious_shear_u = np.inf

        if not hasattr(self, 'remove_spurious_shear_err'):
            logging.warning("Value to discard shear error spurious correlations was not set! Assuming diagonal.")
            self.remove_spurious_shear_err = np.inf

        if not hasattr(self, 'prior_dir_shear'):
            logging.warning("Shear prior directory was not set, assuming same prior as prior_dir.")
            self.prior_dir_shear = self.prior_dir
            self.prior_type_shear = self.prior_type

        if not hasattr(self, 'prior_type_shear'):
            raise ValueError('Shear prior directory was set ({}) without shear prior type ! Please set the shear prior type.'.format(self.prior_dir_shear))

        paths = ['prior_dir_shear']

        for path_key in paths:
            # If not absolute, build the absolute path from the given relative path
            given_path = self.__dict__[path_key]
            if not os.path.isabs(given_path):
                given_path = os.path.normpath(os.path.join(pygeodyn._package_directory, given_path))

            if not os.path.exists(given_path):
                # Try to correct the path
                corrected_path = os.path.join(pygeodyn._package_directory, given_path.split('pygeodyn/')[-1])
                # If the corrected path does not exist either, throw an error
                if not os.path.exists(corrected_path):
                    raise IOError('{} given in config does not exist ! Did you supply a relative path instead of an absolute one ?'.format(given_path))
                given_path = corrected_path
            self.__dict__[path_key] = given_path

    @property
    def nb_forecasts(self):
        return len(self.t_forecasts)

    @property
    def nb_analyses(self):
        return len(self.t_analyses)

    # Config methods
    def read_from_file(self, filename):
        """ Reads the config file from a filename and sets the read dictonary as object dict.

        :param filename: the file from which we load the data (see format at end)
        :type filename: str
        """
        file_conf = reads.parse_config_file(filename)
        self.update(file_conf)

    def update(self, config_dict):
        """ Updates the internal dictionary of the object with the config_dict

        :param config_dict: Configuration dictionary
        :type config_dict: dict
        """
        self.__dict__.update(config_dict)

    def save_hdf5(self, hdf5file):
        """ save necessary attributes for webgeodyn reading
        """
        hdf5file.attrs["Lb"] = self.Lb
        hdf5file.attrs["Lu"] = self.Lu
        hdf5file.attrs["Lsv"] = self.Lsv
            