#-*- coding: utf-8 -*-
""" Contains the reading methods for prior types """
import h5py
import os.path
import numpy as np

def read_0_path(data_directory, prior_type, dt_sampling, measures=None):
    """
    Reads prior data under the 0 path format.

    :param data_directory: directory where the prior data is stored
    :type data_directory: str
    :param prior_type: type of the prior data
    :type prior_type: str
    :param dt_sampling: smoothing window duration in second
    :type dt_sampling: float
    :param measures: sequence of string saying which measures should be extracted. Allowed
              values are 'times', 'B', 'U' and 'ER', order do not matter.
    :return: MF, U, ER, times, dt_samp, tag
    :rtype: lists
    """
    allowed_measures = ('times', 'B', 'U', 'ER')
    if measures is None:
        measures = allowed_measures

    err_mess = """Invalid measure given in measures, {0}, allowed values are {1}.
               """.format(measures, allowed_measures)
    assert any([m in measures for m in allowed_measures]), err_mess

    # Read the prior data 
    MF, U, ER, times, dt_samp, tag = [], [], [], [], [], []
    for m in measures:
        if m == 'times':
            times.append(None)
        measure_fn = 'Real' + m + '.dat'
        if m == 'U':
            # Need to change the sign convention for U prior if read from Coupled-Earth
            U.append((-1) * np.genfromtxt(os.path.join(data_directory, measure_fn)))
        elif m == 'B':
            MF.append(np.genfromtxt(os.path.join(data_directory, measure_fn)))
        elif m == 'ER':
            ER.append(np.genfromtxt(os.path.join(data_directory, measure_fn)))
    
    dt_samp.append(None)
    tag.append(prior_type)
    return MF, U, ER, times, dt_samp, tag


def read_50_and_71_path(data_directory, prior_type, dt_sampling, measures=None):
    """
    Reads prior data under the 50% or 71% path format.

    :param data_directory: directory where the prior data is stored
    :type data_directory: str
    :param prior_type: type of the prior data
    :type prior_type: str
    :param dt_sampling: smoothing window duration in second
    :type dt_sampling: float
    :param measures: sequence of string saying which measures should be extracted. Allowed
              values are 'times', 'B', 'U' and 'ER', order do not matter.
    :return: MF, U, ER, times, dt_samp, tag
    :rtype: lists
    """
    if measures is None:
        measures = ('times', 'B', 'U', 'ER')

    err_mess = """Invalid measure given in measures, {0}, allowed values are {1}.
               """.format(measures, ('times', 'B', 'U', 'ER'))
    assert any([m in measures for m in ('times', 'B', 'U', 'ER')]), err_mess

    measures = ['MF' if m == 'B' else m for m in measures]
    
    MF, U, ER, times, dt_samp, tag = [], [], [], [], [], []
    with h5py.File(os.path.join(data_directory, 'Real.hdf5'), 'r') as hdf_file:
        times.append(np.array(hdf_file["times"]))
        if prior_type == "71path":
            MF.append(-np.array(hdf_file["MF"]))
        else:
            MF.append(np.array(hdf_file["MF"]))
        U.append(np.array(hdf_file["U"]))
        ER.append(np.array(hdf_file["ER"]))
    tag.append(prior_type)
    dt_samp.append(dt_sampling)
    return MF, U, ER, times, dt_samp, tag


def read_100_path(data_directory, prior_type, dt_sampling, measures=None):
    """
    Reads prior data under 100% path format.

    :param data_directory: directory where the prior data is stored
    :type data_directory: str
    :param prior_type: type of the prior data
    :type prior_type: str
    :param dt_sampling: smoothing window duration in second
    :type dt_sampling: float
    :param measures: sequence of string saying which measures should be extracted. Allowed
              values are 'times', 'B', 'U' and 'ER', order do not matter.
    :return: MF, U, ER, times, dt_samp, tag
    :rtype: lists
    """
    if measures is None:
        measures = ('times', 'B', 'U', 'ER')

    err_mess = """Invalid measure given in measures, {0}, allowed values are {1}.
               """.format(measures, ('times', 'B', 'U', 'ER'))
    assert any([m in measures for m in ('times', 'B', 'U', 'ER')]), err_mess

    measures = ['MF' if m == 'B' else m for m in measures]
    
    # list of 100path time series
    R = ["Real_A1.hdf5",
     "Real_A2.hdf5",
     "Real_A3.hdf5",
     "Real_A4.hdf5",
     "Real_B1.hdf5",
     "Real_B2.hdf5",
     "Real_B3_clean_1.hdf5",
     "Real_B3_clean_2.hdf5",
     "Real_B4.hdf5"]
    
    #loop over time series
    MF, U, ER, times, dt_samp, tag = [], [], [], [], [], []
    for i in range(9):
        with h5py.File(os.path.join(data_directory, R[i]), 'r') as hdf_file:
            times.append(np.array(hdf_file["times"][50:]))
            MF.append(np.array(hdf_file["MF"][50:]))
            U.append(np.array(hdf_file["U"][50:]))
            ER.append(np.array(hdf_file["ER"][50:]))
            dt = hdf_file["times"][1] - hdf_file["times"][0]
        tag.append(prior_type)
        dt_samp.append(dt)

    #add 71path prior data
    MF_71p, U_71p, ER_71p, times_71p, dt_samp_71p, tag_71p = read_50_and_71_path(os.path.join(data_directory, '../71path'),'71path', dt_sampling)
    
    #concatenate lists
    MF = MF_71p + MF
    U = U_71p + U
    ER = ER_71p + ER
    times = times_71p + times
    tag = tag_71p + tag
    dt_samp = dt_samp_71p + dt_samp
    
    return MF, U, ER, times, dt_samp, tag