#!/usr/bin/env bash

# Paths are relative to the repo folder
VERSION_NB=$(grep -Eo "[0-9]+\.[0-9]+\.[0-9]+" pygeodyn/_version.py)
#CHANGES=$(cat changes.log)

git rm pygeodyn/release/RELEASE.md
git add pygeodyn/_version.py
git add CHANGELOG.md
git commit -m "Update to $VERSION_NB"
git tag "$VERSION_NB"
git push --tags # tags are not pushed by default