Copy this file and rename it as RELEASE.md and complete the parts in italics. 
To trigger the gitlab CI release, include [new-release] (brackets included) in the commit message.

RELEASE_TYPE: *major, minor or patch*

*Keep the blank line above and write the description of changes here*