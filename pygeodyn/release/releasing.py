#!/usr/bin/env python3
#-*- coding: utf-8 -*-
import os.path
import datetime

valid_release_types = ['major', 'minor', 'patch']
file_extension = '.md'


def do_release():
    release_type, release_changes = parse_release_file()
    new_version = generate_version_number(release_type)
    generate_change_log(new_version, release_changes)
    update_version_file(new_version)
    return new_version, release_changes


def generate_change_log(version_number, changes, date=None):
    now = datetime.datetime.now()
    with open(os.path.join(os.path.dirname(__file__), '../../CHANGELOG'+file_extension), 'r+') as changelog:
        changelog_header = changelog.readline()
        old_changes = changelog.readlines()
        changelog.seek(0)  # go back to the beginning of the file
        # Write new changes at the beginning
        if date is None:
            date = now.strftime("%Y-%m-%d")
        header = '{} - {}\n'.format(version_number, date)
        changelog.write(changelog_header+'\n')
        changelog.write(header)
        changelog.write('-'*(len(header)-1)+'\n')
        changelog.write(changes)
        if not changes.endswith('\n'):
            changelog.write('\n')
        for line in old_changes:
            changelog.write(line)
        changelog.close()
    return 0


def generate_version_number(release_type):
    # Parse current version
    with open(os.path.join(os.path.dirname(__file__), '../_version.py')) as version_file:
        version = version_file.read().strip().split("'")[1]
    version_numbers = version.split('.')

    # Find the release_type: 0=major, 1=minor, 2=patch
    try:
        i_release = valid_release_types.index(release_type)
    except ValueError:
        raise TypeError('Found "{}" as release type which is not a valid release type !'.format(release_type))

    # Increment corresponding version number
    version_numbers[i_release] = '{}'.format(int(version_numbers[i_release])+1)

    # Set subordinate version numbers to 0
    for i_nb, nb in enumerate(version_numbers[i_release+1:]):
        version_numbers[i_release+1+i_nb] = '0'

    return ".".join(version_numbers)


def parse_release_file():
    release_file_path = os.path.join(os.path.dirname(__file__), 'RELEASE'+file_extension)
    if not os.path.isfile(release_file_path):
        raise IOError('No RELEASE{} file was found !'.format(file_extension))

    release_type = None
    with open(release_file_path) as release_file:
        for line in release_file:
            if 'RELEASE_TYPE' in line:
                release_type = line.strip().split()[-1].lower()
                break
        # Verify that we found a valid release_type
        if release_type not in valid_release_types:
            raise TypeError('Found "{}" as release type which is not a valid release type !'.format(release_type))
        # Parse the blank line
        line = release_file.readline()
        if len(line.strip()) != 0:
            raise IOError('No blank line were found after the line defining the release type !')
        # Parse the changes (assumed to be the rest of the file)
        changes = release_file.read()

    return release_type, changes


def update_version_file(version_string):
    with open(os.path.join(os.path.dirname(__file__), '../_version.py'), 'w') as version_file:
        version_file.write("__version__ = '{}'".format(version_string))
    return 0


if __name__ == "__main__":
    version_number, version_changes = do_release()
    print(version_changes)
