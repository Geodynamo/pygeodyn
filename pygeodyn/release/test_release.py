import unittest
import releasing


class TestRelease(unittest.TestCase):
    def test_parsing_release_file(self):
        release_type, changes = releasing.parse_release_file()
        self.assertEqual(len(release_type), 5)
        self.assertGreater(len(changes), 0)


if __name__ == '__main__':
    SUITE = unittest.TestLoader().loadTestsFromTestCase(TestRelease)
    RES = unittest.TextTestRunner(verbosity=2).run(SUITE)
    import sys

    sys.exit(max(len(RES.failures), len(RES.errors)))
