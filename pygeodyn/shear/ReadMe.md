Shear at the CMB (Firsov et al., submitted):
============================================

Standards used for shear computation:
-------------------------------------

Toroidal shear = - Toroidal pygeodyn   !!!!!

shear_algo.py
-------------

Comprises 2 classes: **ShearInversion** and **ShearError**.

**ShearInversion** computes the shear at the CMB given a prior (U and error of representativeness) , a galvanic time TauG and an analysed state: U, DU, MF, SV with DU computed using finite difference of U. The output is the toroidal/poloidal schmidt semi normalized coefficients of the shear at the CMB.

**ShearInversion** computes the error of representativeness given a prior (U and MF), TauG (must remain 0 so far because we don't have prior of SV and DU), Ly, Lu and Lb. The output is the toroidal/poloidal schmidt semi normalized coefficients of the error of representativeness.

compute_error.py
----------------

Parallel script that computes the error of representativeness for every time in the prior. Computation for every time is independant and can be computed in parallel. The computation parameters can be modified to meet the user's needs (except TauG must remain 0).

To run it in series:

``python3 compute_error.py``

To run it in parallel using mpi4py:

``mpirun -n <number of cores> python3 -m mpi4py compute_error.py``

blackBoxFormula2temp.py and templib_temp.py
-------------------------------------------

Python modules containing functions that are called in **ShearInversion** and **ShearError**.