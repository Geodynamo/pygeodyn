from pygeodyn.shear import blackBoxFormula2temp as bbf
import numpy as np


def openOrCalculateDBrDtForward(
    Lb,
    Lsv,
    Lq,
    tmax,
    Nu_real_imag_form,
    Br_data_matrix,
    u_in_real_imag_form_dict,
    gauss_thetas,
    gauss_weights,
    phis,
    u_plus_forward_matrix_complex,
    u_minus_forward_matrix_complex,
    print_text="Opening or calculating dBr/dt forward matrix...",
    debug=True,
):
   

    u_plus_data_matrices, u_minus_data_matrices = bbf.calculate_u_data_matrices(
        Nu_real_imag_form,
        u_in_real_imag_form_dict,
        gauss_thetas,
        phis,
        debug=debug,
        u_plus_forward_matrix=u_plus_forward_matrix_complex,
        u_minus_forward_matrix=u_minus_forward_matrix_complex,
        forward_type="complex",
    )

    (
        v_plus_matrices,
        v_minus_matrices,
    ) = bbf.calculate_v_plus_minus(
        Lsv,
        Nu_real_imag_form,
        Br_data_matrix,
        u_plus_data_matrices,
        u_minus_data_matrices,
        gauss_thetas,
        gauss_weights,
        tmax,
        debug=debug,
    )

    (
        SV_in_real_imag_form_dict,
        Nsv_real_imag_form,
        SV_cos_sin_dict,
        Nsv_cossin_form,
    ) = bbf.build_real_imag_and_cossin_dict_pos(Lsv)

    dBr_dt_matrix_from_u_minus_plus = bbf.calculating_SV_from_v_matrix_ulm(
        Lsv,
        Nsv_real_imag_form,
        SV_in_real_imag_form_dict,
        v_plus_matrices,
        v_minus_matrices,
    )

    return (
        v_plus_matrices,
        v_minus_matrices,
        dBr_dt_matrix_from_u_minus_plus,
    )


def flowToMMatrix(u, Lu):
    u_m = np.array(u)
    n = 0
    for l_ in range(1, Lu + 1):
        for m_ in range(0, l_ + 1):
            u_m[:, n] = u[:, n] * ((-1) ** m_)
            n += 1
            if m_ != 0:
                u_m[:, n] = u[:, n] * ((-1) ** m_)
                n += 1
    for l_ in range(1, Lu + 1):
        for m_ in range(0, l_ + 1):
            u_m[:, n] = u[:, n] * ((-1) ** m_)
            n += 1
            if m_ != 0:
                u_m[:, n] = u[:, n] * ((-1) ** m_)
                n += 1

    return u_m


def flowToM(tlmclm, Lu):
    tlmclm_m = np.array(tlmclm.reshape((-1, 1)))
    n = 0
    for l_ in range(1, Lu + 1):
        for m_ in range(0, l_ + 1):
            tlmclm_m[n, 0] = tlmclm[n] * ((-1) ** m_)
            n += 1
            if m_ != 0:
                tlmclm_m[n, 0] = tlmclm[n] * ((-1) ** m_)
                n += 1
    for l_ in range(1, Lu + 1):
        for m_ in range(0, l_ + 1):
            tlmclm_m[n, 0] = tlmclm[n] * ((-1) ** m_)
            n += 1
            if m_ != 0:
                tlmclm_m[n, 0] = tlmclm[n] * ((-1) ** m_)
                n += 1

    return tlmclm_m


def magnFieldFromRaToRaRcM(glm, Lb):
    Br_glm_Rc_2 = np.array(glm.reshape((-1, 1)))
    Br_glm_Ra_2 = np.array(glm.reshape((-1, 1)))
    Br_glm_Ra = np.array(glm.reshape((-1, 1)))
    n = 0
    for l_ in range(1, Lb + 1):
        for m_ in range(0, l_ + 1):
            Br_glm_Ra_2[n, 0] = Br_glm_Ra[n, 0] * ((-1) ** m_)
            n += 1
            if m_ != 0:
                Br_glm_Ra_2[n, 0] = Br_glm_Ra[n, 0] * ((-1) ** m_)
                n += 1
    n = 0
    for l_ in range(1, Lb + 1):
        for m_ in range(0, l_ + 1):
            Br_glm_Rc_2[n, 0] = (
                Br_glm_Ra[n, 0] * ((bbf.Ra / bbf.Rc) ** (l_ + 2)) * ((-1) ** m_)
            )
            n += 1
            if m_ != 0:
                Br_glm_Rc_2[n, 0] = (
                    Br_glm_Ra[n, 0] * ((bbf.Ra / bbf.Rc) ** (l_ + 2)) * ((-1) ** m_)
                )
                n += 1

    return Br_glm_Rc_2, Br_glm_Ra_2, Br_glm_Ra


def magnFieldFromRaToRaRcMMatrix(glm, Lsv):
    err_Ra_coeffs = np.array(glm)
    err_Rc_coeffs_2 = np.array(glm)
    err_Ra_coeffs_2 = np.array(glm)
    n = 0
    for l_ in range(1, Lsv + 1):
        for m_ in range(0, l_ + 1):
            err_Ra_coeffs_2[:, n] = err_Ra_coeffs[:, n] * ((-1) ** m_)
            n += 1
            if m_ != 0:
                err_Ra_coeffs_2[:, n] = err_Ra_coeffs[:, n] * ((-1) ** m_)
                n += 1
    n = 0
    for l_ in range(1, Lsv + 1):
        for m_ in range(0, l_ + 1):
            err_Rc_coeffs_2[:, n] = (
                err_Ra_coeffs[:, n] * ((bbf.Ra / bbf.Rc) ** (l_ + 2)) * ((-1) ** m_)
            )
            n += 1
            if m_ != 0:
                err_Rc_coeffs_2[:, n] = (
                    err_Ra_coeffs[:, n] * ((bbf.Ra / bbf.Rc) ** (l_ + 2)) * ((-1) ** m_)
                )
                n += 1

    return err_Rc_coeffs_2, err_Ra_coeffs_2, err_Ra_coeffs


def lvFromVMatrix(v_plus_matrices_2, v_minus_matrices_2, Lsv):
    lv_plus_matrix = np.array(v_plus_matrices_2)
    lv_minus_matrix = np.array(v_minus_matrices_2)
    n = 0
    for lv in range(1, Lsv):
        for mv in range(-lv, lv + 1):
            lv_plus_matrix[n, :] = lv_plus_matrix[n, :] * -lv
            lv_minus_matrix[n, :] = lv_minus_matrix[n, :] * -lv
            n += 1
            lv_plus_matrix[n, :] = lv_plus_matrix[n, :] * -lv
            lv_minus_matrix[n, :] = lv_minus_matrix[n, :] * -lv
            n += 1

    n = 0
    for l_ in range(1, Lsv):
        for m_ in range(-l_, l_ + 1):
            lv_plus_matrix[n, :] = (
                lv_plus_matrix[n, :] * (bbf.Ra ** (l_ + 2)) / (bbf.Rc ** (l_ + 2))
            )
            lv_minus_matrix[n, :] = (
                lv_minus_matrix[n, :] * (bbf.Ra ** (l_ + 2)) / (bbf.Rc ** (l_ + 2))
            )
            n += 1
            # if m_ != 0:
            lv_plus_matrix[n, :] = (
                lv_plus_matrix[n, :] * (bbf.Ra ** (l_ + 2)) / (bbf.Rc ** (l_ + 2))
            )
            lv_minus_matrix[n, :] = (
                lv_minus_matrix[n, :] * (bbf.Ra ** (l_ + 2)) / (bbf.Rc ** (l_ + 2))
            )
            n += 1

    return lv_plus_matrix, lv_minus_matrix


def calculate_TlmClm_to_UminusUplus_matrix(
    Lu,
):
    (
        u_in_real_imag_form_dict,
        Nu_real_imag_form,
        u_cos_sin_dict,
        Nu_cossin_form,
    ) = bbf.build_real_imag_and_cossin_dict_pos(Lu)

    Matrix_multiply_by_i = bbf.building_transformation_matrix_multiply_by_i(
        Nu_real_imag_form, u_in_real_imag_form_dict
    )

    u_Matrix_from_cossin_to_R_I_form = (
        bbf.building_transformation_matrix_from_cossin_to_R_I_form(
            Nu_real_imag_form, Nu_cossin_form, u_in_real_imag_form_dict, u_cos_sin_dict
        )
    )

    u_Matrix_from_R_I_from_cossin = (
        bbf.building_transformation_matrix_from_R_I_to_cossin_form(
            Nu_real_imag_form, Nu_cossin_form, u_in_real_imag_form_dict, u_cos_sin_dict
        )
    )

    #########################################

    TtU = np.zeros((Nu_real_imag_form, Nu_real_imag_form))
    CtU = np.zeros((Nu_real_imag_form, Nu_real_imag_form))

    nu = 0
    ntc = 0
    for lu in range(1, Lu + 1):
        temp = np.sqrt(lu * (lu + 1) / 2)
        for mu in range(-lu, lu + 1):
            CtU[nu, nu] = temp
            TtU[nu, nu] = temp
            nu += 1
            CtU[nu, nu] = temp
            TtU[nu, nu] = temp
            nu += 1

    TCtUp = np.concatenate((-Matrix_multiply_by_i.dot(TtU), CtU), axis=1)
    TCtUm = np.concatenate((Matrix_multiply_by_i.dot(TtU), CtU), axis=1)

    tc_from_cossin_to_R_I = np.zeros((Nu_real_imag_form * 2, Nu_cossin_form * 2))
    tc_from_cossin_to_R_I[
        :Nu_real_imag_form, :Nu_cossin_form
    ] = u_Matrix_from_cossin_to_R_I_form
    tc_from_cossin_to_R_I[
        Nu_real_imag_form:, Nu_cossin_form:
    ] = u_Matrix_from_cossin_to_R_I_form

    TCtUp_from_cossin = TCtUp.dot(tc_from_cossin_to_R_I)
    TCtUm_from_cossin = TCtUm.dot(tc_from_cossin_to_R_I)

    TCtUmUp = np.concatenate((TCtUm_from_cossin, TCtUp_from_cossin), axis=0)

    return TCtUmUp


def calculate_QtT_QtC_Lu_form_Q_Lq(Lq):
    (
        Q_in_real_imag_form_dict_Lq,
        Nq_real_imag_form_Lq,
        Q_cos_sin_dict_Lq,
        Nq_cossin_form_Lq,
    ) = bbf.build_real_imag_and_cossin_dict_pos(Lq, q=True)

    Q_Matrix_from_cossin_to_R_I_form = (
        bbf.building_transformation_matrix_from_cossin_to_R_I_form(
            Nq_real_imag_form_Lq,
            Nq_cossin_form_Lq,
            Q_in_real_imag_form_dict_Lq,
            Q_cos_sin_dict_Lq,
        )
    )

    Q_Matrix_from_R_I_to_cossin_form = (
        bbf.building_transformation_matrix_from_R_I_to_cossin_form(
            Nq_real_imag_form_Lq,
            Nq_cossin_form_Lq,
            Q_in_real_imag_form_dict_Lq,
            Q_cos_sin_dict_Lq,
        )
    )

    QtC_matrix_Re_Im_Lq = bbf.build_qlm_to_clm_matrix_Re_Im(Lq)
    QtT_matrix_part_Re_Im_Lq = bbf.build_qlm_to_tlm_matrix_Re_Im(Lq)
    QtT_matrix_zonal_Re_Im_Lq = bbf.build_qlm_to_tlm_zonal_matrix_Re_Im_v2(Lq)
    QtT_matrix_real_imag_form_Lq = np.array(QtT_matrix_zonal_Re_Im_Lq)
    QtT_matrix_real_imag_form_Lq[:, 2:] += QtT_matrix_part_Re_Im_Lq

    temp = np.zeros(QtT_matrix_real_imag_form_Lq.shape)
    temp[: Nq_real_imag_form_Lq - 2, 2:] += QtC_matrix_Re_Im_Lq
    QtC_matrix_real_imag_form_Lq = temp

    return QtT_matrix_real_imag_form_Lq, QtC_matrix_real_imag_form_Lq


def calculate_psi1_to_delta_plus_minus_matrix(
    Lu, Lq, QtT_matrix_real_imag_form_Lq, QtC_matrix_real_imag_form_Lq
):

    (
        Q_in_real_imag_form_dict_Lu,
        Nq_real_imag_form_Lu,
        Q_cos_sin_dict_Lu,
        Nq_cossin_form_Lu,
    ) = bbf.build_real_imag_and_cossin_dict_pos(Lu, q=True)

    (
        Q_in_real_imag_form_dict_Lq,
        Nq_real_imag_form_Lq,
        Q_cos_sin_dict_Lq,
        Nq_cossin_form_Lq,
    ) = bbf.build_real_imag_and_cossin_dict_pos(Lq, q=True)

    (
        u_in_real_imag_form_dict,
        Nu_real_imag_form,
        u_cos_sin_dict,
        Nu_cossin_form,
    ) = bbf.build_real_imag_and_cossin_dict_pos(Lu)

    Matrix_multiply_by_i = bbf.building_transformation_matrix_multiply_by_i(
        Nu_real_imag_form, u_in_real_imag_form_dict
    )

    (
        ulm_plus_from_qlm_matrix_Lq,
        ulm_minus_from_qlm_matrix_Lq,
    ) = bbf.calculating_ulm_plus_minus_from_qlm(
        Lu,
        Nu_real_imag_form,
        QtC_matrix_real_imag_form_Lq[:, :],
        QtT_matrix_real_imag_form_Lq[:, :],
        Matrix_multiply_by_i,
    )

    ulm_minus_plus_from_qlm_matrix = np.zeros(
        (Nu_real_imag_form * 2, Nq_real_imag_form_Lq)
    )
    ulm_minus_plus_from_qlm_matrix[:Nu_real_imag_form, :] = ulm_minus_from_qlm_matrix_Lq
    ulm_minus_plus_from_qlm_matrix[Nu_real_imag_form:, :] = ulm_plus_from_qlm_matrix_Lq

    psi1_to_psi2_data = bbf.calculate_psi1_to_psi2(Lu)
    # psi1_to_psi2_data = calculate_psi1_to_psi2(Lu)

    psi1_to_psi2_matrix = bbf.calculate_psi1_to_psi2_matrix(Lu, psi1_to_psi2_data)

    psi1_to_psi3_matrix = psi1_to_psi2_matrix + np.eye(Nq_real_imag_form_Lu) * 6

    (
        psi3_to_s_tilda_matrix_plus,
        psi3_to_s_tilda_matrix_minus,
    ) = bbf.calculate_s_tilda_from_qlm_tilda(Lu)

    psi1_to_s_tilda_matrix_plus_temp = psi3_to_s_tilda_matrix_plus.dot(
        psi1_to_psi3_matrix
    )
    psi1_to_s_tilda_matrix_minus_temp = psi3_to_s_tilda_matrix_minus.dot(
        psi1_to_psi3_matrix
    )
    psi1_to_s_tilda_matrix_plus = psi1_to_s_tilda_matrix_plus_temp
    psi1_to_s_tilda_matrix_minus = psi1_to_s_tilda_matrix_minus_temp

    (
        psi1_to_s_hat_matrix_plus,
        psi1_to_s_hat_matrix_minus,
    ) = bbf.calculate_s_hat_from_qlm(Lu)
    # psi1_to_s_hat_matrix_plus[: (Lu-1) * (Lu -1 + 2) * 2 + 2, :(Lu-1) * (Lu -1 + 2) * 2 + 2] = psi1_to_s_hat_matrix_plus_temp
    # psi1_to_s_hat_matrix_minus[: (Lu-1) * (Lu -1 + 2) * 2 + 2, :(Lu-1) * (Lu -1 + 2) * 2 + 2] = psi1_to_s_hat_matrix_minus_temp

    psi1_to_delta_plus_temp = psi1_to_s_tilda_matrix_plus + psi1_to_s_hat_matrix_plus
    psi1_to_delta_minus_temp = psi1_to_s_tilda_matrix_minus + psi1_to_s_hat_matrix_minus
    psi1_to_delta_plus = np.zeros((Nq_real_imag_form_Lu, Nq_real_imag_form_Lq))
    psi1_to_delta_minus = np.zeros((Nq_real_imag_form_Lu, Nq_real_imag_form_Lq))
    psi1_to_delta_plus = psi1_to_delta_plus_temp[:, :Nq_real_imag_form_Lq]
    psi1_to_delta_minus = psi1_to_delta_minus_temp[:, :Nq_real_imag_form_Lq]

    return psi1_to_delta_plus, psi1_to_delta_minus


def calculate_u_minus_plus_from_u_minus(Lu):
    (
        u_in_real_imag_form_dict,
        Nu_real_imag_form,
        u_cos_sin_dict,
        Nu_cossin_form,
    ) = bbf.build_real_imag_and_cossin_dict_pos(Lu)

    u_plus_from_u_minus = np.zeros((Nu_real_imag_form, Nu_real_imag_form))

    for lu in range(1, Lu + 1):
        for mu in range(-lu, lu + 1):
            temp = (-1) ** mu

            u_pos_reim_plus = list(u_in_real_imag_form_dict.keys())[
                list(u_in_real_imag_form_dict.values()).index((lu, mu, "r"))
            ]
            u_pos_reim_minus = list(u_in_real_imag_form_dict.keys())[
                list(u_in_real_imag_form_dict.values()).index((lu, -mu, "r"))
            ]

            u_plus_from_u_minus[u_pos_reim_plus, u_pos_reim_minus] = temp
            u_plus_from_u_minus[u_pos_reim_plus + 1, u_pos_reim_minus + 1] = -temp

    u_minus_plus_from_u_minus = np.concatenate(
        (np.eye(Nu_real_imag_form), u_plus_from_u_minus), axis=0
    )

    return u_minus_plus_from_u_minus


def calculate_u_minus_from_u_minus_plus(Lu):
    u_minus_plus_from_u_minus = calculate_u_minus_plus_from_u_minus(Lu)
    u_minus_from_u_minus_plus = np.linalg.inv(
        u_minus_plus_from_u_minus.T.dot(u_minus_plus_from_u_minus)
    ).dot(u_minus_plus_from_u_minus.T)
    return u_minus_from_u_minus_plus


def from_Ra_to_Rc(matr, Lsv):
    matr_res = np.array(matr)
    n = 0
    for l_ in range(1, Lsv):
        for m_ in range(-l_, l_ + 1):
            matr_res[n, :] = matr[n, :] * (bbf.Ra ** (l_ + 2)) / (bbf.Rc ** (l_ + 2))
            n += 1
            matr_res[n, :] = matr[n, :] * (bbf.Ra ** (l_ + 2)) / (bbf.Rc ** (l_ + 2))
            n += 1
    return matr_res


def calculate_corelation(Aij, Bij, thetas, tmax, pmax):
    sin_theta_map = np.zeros((tmax, pmax))

    for th in range(tmax):
        sin_theta = np.sin(thetas[th])
        for ph in range(pmax):
            sin_theta_map[th, ph] = sin_theta

    Aij_map = np.array(Aij.real.reshape((tmax, pmax)))
    Bij_map = np.array(Bij.real.reshape((tmax, pmax)))

    covar_ij = np.zeros((tmax, pmax))

    norm = 0
    for th in range(tmax):
        for ph in range(pmax):
            if th > 0 and th < tmax - 1:
                dtheta = np.abs(thetas[th + 1] - thetas[th - 1]) / 2
            else:
                if th == 0:
                    dtheta = np.abs(thetas[th + 1] - thetas[th])
                else:
                    dtheta = np.abs(thetas[th - 1] - thetas[th])
            temp_A2 = dtheta * (Aij_map[th, ph] ** 2) * sin_theta_map[th, ph]
            temp_B2 = dtheta * (Bij_map[th, ph] ** 2) * sin_theta_map[th, ph]

            norm += np.sqrt(temp_A2 * temp_B2)

    norm = norm * 2 * np.pi / pmax

    value = 0
    for th in range(tmax):
        for ph in range(pmax):
            if th > 0 and th < tmax - 1:
                dtheta = np.abs(thetas[th + 1] - thetas[th - 1]) / 2
            else:
                if th == 0:
                    dtheta = np.abs(thetas[th + 1] - thetas[th])
                else:
                    dtheta = np.abs(thetas[th - 1] - thetas[th])
            temp_AB = (
                dtheta * (Aij_map[th, ph] * Bij_map[th, ph]) * sin_theta_map[th, ph]
            )

            value += temp_AB

    value = value * 2 * np.pi / pmax

    return value / norm


def calculate_normalised_misfit(Aij, Bij, thetas, tmax, pmax):
    sin_theta_map = np.zeros((tmax, pmax))

    for th in range(tmax):
        sin_theta = np.sin(thetas[th])
        for ph in range(pmax):
            sin_theta_map[th, ph] = sin_theta

    Aij_map = np.array(Aij.real.reshape((tmax, pmax)))
    Bij_map = np.array(Bij.real.reshape((tmax, pmax)))

    covar_ij = np.zeros((tmax, pmax))

    norm = 0
    for th in range(tmax):
        for ph in range(pmax):
            if th > 0 and th < tmax - 1:
                dtheta = np.abs(thetas[th + 1] - thetas[th - 1]) / 2
            else:
                if th == 0:
                    dtheta = np.abs(thetas[th + 1] - thetas[th])
                else:
                    dtheta = np.abs(thetas[th - 1] - thetas[th])
            temp_A2 = dtheta * (Aij_map[th, ph] ** 2) * sin_theta_map[th, ph]
            temp_B2 = dtheta * (Bij_map[th, ph] ** 2) * sin_theta_map[th, ph]

            norm += np.sqrt(temp_A2 * temp_B2)
            # norm += temp_B2

    norm = norm * 2 * np.pi / pmax

    value = 0
    for th in range(tmax):
        for ph in range(pmax):
            if th > 0 and th < tmax - 1:
                dtheta = np.abs(thetas[th + 1] - thetas[th - 1]) / 2
            else:
                if th == 0:
                    dtheta = np.abs(thetas[th + 1] - thetas[th])
                else:
                    dtheta = np.abs(thetas[th - 1] - thetas[th])
            temp_AB = (
                dtheta
                * ((Aij_map[th, ph] - Bij_map[th, ph]) ** 2)
                * sin_theta_map[th, ph]
            )

            value += temp_AB

    value = value * 2 * np.pi / pmax

    return value / norm


def calculate_norm(Aij, thetas, tmax, pmax):

    sin_theta_map = np.zeros((tmax, pmax))
    for th in range(tmax):
        sin_theta = np.sin(thetas[th])
        for ph in range(pmax):
            sin_theta_map[th, ph] = sin_theta

    temp = 0
    for th in range(tmax):
        temp_theta = 0
        for ph in range(pmax):
            temp_theta += (Aij[th, ph] ** 2) * sin_theta_map[th, ph]
        temp += temp_theta

    return np.sqrt(temp)


def calculate_spectrum_re_im_flow(coef, L):
    spectrum = np.zeros((L,))
    iter = 0
    for l in range(1, L + 1):
        temp = 0
        for m in range(-l, l + 1):
            c1 = coef[iter]
            c2 = coef[iter + 1]
            if m != 0:
                c1 = c1 / np.sqrt(2)
                c2 = c2 / np.sqrt(2)
            temp += c1**2
            temp += c2**2
            iter += 2
        spectrum[l - 1] += temp * (l + 1)
    for l in range(1, L + 1):
        temp = 0
        for m in range(-l, l + 1):
            c1 = coef[iter]
            c2 = coef[iter + 1]
            if m != 0:
                c1 = c1 / np.sqrt(2)
                c2 = c2 / np.sqrt(2)
            temp += c1**2
            temp += c2**2
            iter += 2
        spectrum[l - 1] += temp * (l + 1)

    return spectrum


def calculate_spectrum_re_im_SV2_pos(coef, L):
    spectrum = np.zeros((L,))
    iter = 0
    for l in range(1, L + 1):
        temp = 0
        for m in range(0, l + 1):
            if m == 0:
                temp += coef[iter] ** 2
                iter += 1
            else:
                temp += coef[iter] ** 2
                temp += coef[iter + 1] ** 2
                iter += 2
        spectrum[l - 1] += temp * (l + 1)

    return spectrum


def calculate_spectrum_re_im_SV2_pos_all_m(coef, L):
    spectrum = np.zeros((L,))
    iter = 0
    for l in range(1, L + 1):
        temp = 0
        for m in range(-l, l + 1):
            c = coef[iter]
            if m != 0:
                c = c / np.sqrt(2)
            temp += c**2

            c = coef[iter + 1]
            if m != 0:
                c = c / np.sqrt(2)
            temp += c**2
            iter += 2

        spectrum[l - 1] += temp * (l + 1)

    return spectrum
