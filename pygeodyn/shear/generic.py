import numpy as np
from pygeodyn.shear import blackBoxFormula2temp as bbf

class Generic():
    """
    Base level class
    """

    def __init__(self, cfg, test):
        """
        Init instance variables

        :param cfg: variable containing configuration dictionary
        """

        self.tau1 = cfg.TauG

        self.Lu = cfg.Lu
        self.Ly = cfg.Ly
        self.Lb = cfg.Lb
        self.Lsv = cfg.Lsv
        self.prior_dir_shear = str(cfg.prior_dir_shear)
        self.prior_type_shear = cfg.prior_type_shear
        self.glasso_lambda_u = cfg.remove_spurious_shear_u
        self.glasso_lambda_err = cfg.remove_spurious_shear_err

        self.tmax = 64
        self.tpmax = 2 *  self.tmax**2
        self.pmax = 2 *  self.tmax
        self.gauss_thetas,  self.gauss_weights = bbf.gaussPoints(0, np.pi,  self.tmax)
        self.phis = np.linspace(0, 2 * np.pi, self.pmax, endpoint=False)
        self.Nb = self.Lb * (self.Lb + 2)
        self.Nsv = self.Lsv * (self.Lsv + 2)
        self.Nu = self.Lu * (self.Lu + 2)
        self.Nu2 = 2 * self.Nu
        self.Ny = self.Ly * (self.Ly + 2)
        self.Ny2 = 2 * self.Ny
        self.Lq = self.Lu - 1
        self.test = test