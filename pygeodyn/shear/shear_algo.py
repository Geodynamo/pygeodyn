import numpy as np
import h5py
import os
import sklearn.covariance as skcov
from pygeodyn.shear import blackBoxFormula2temp as bbf
from pygeodyn.shear import templib_temp as templib
from pygeodyn.shear.generic import Generic

class ShearInversion(Generic):
    """
    Compute the shear at CMB at analysis time
    """

    def __init__(self, cfg, test=""):
        """
        Init instance of ShearInverion

        :param cfg: variable containing configuration dictionary
        :type cfg: dict
        :param test: select test prior directory (default: "")
        :type test: str
        """

        Generic.__init__(self, cfg, test)


    def build_prior(self):
        
        # Load prior
        u_prior = self.load_prior_flow()
        y1_error_of_representativeness, y2_error_of_representativeness = self.load_prior_err()
        
        # Compute prior matrices
        Pss_inv_with_glasso = self.compute_prior_shear(u_prior)
        (
            Pee_y1_tilda_with_glasso,
            Pee_y2_tilda_with_glasso
        ) = self.compute_prior_error_shear(y1_error_of_representativeness, y2_error_of_representativeness)

        return Pee_y1_tilda_with_glasso, Pee_y2_tilda_with_glasso, Pss_inv_with_glasso


    def load_prior_err(self):
        """
        Load prior error data
        :return: y1_error_of_representativeness, y2_error_of_representativeness
        :rtype: ndarray (Ntimes x Ly(Ly + 2)), ndarray (Ntimes x Ly(Ly + 2))
        """
         
        # Load prior error
        if self.test == "":
            filename = self.prior_dir_shear + "/y12_err_repr_Ly_{}_Lu_{}_Lb_{}.npy".format(self.Ly, self.Lu, self.Lb)
        else:
            filename = self.test + "/test_shear/CED_y12_err_repr_Ly_18_Lu_18_Lb_13.npy"

        if os.path.isfile(filename):
                y12_error_of_representativeness = np.load(filename)
                y1_error_of_representativeness = y12_error_of_representativeness[:,:self.Ny]
                y2_error_of_representativeness = y12_error_of_representativeness[:,self.Ny:]
        else:
            raise IOError(filename + " is not found you must provide the shear error of representativeness with the coefficients Ly = {}, Lu = {}, Lb = {} and for a prior = {}. To compute it, you can use the routine parallel code compute_error.py in pygeodyn/pygeodyn/shear/".format(self.Ly, self.Lu, self.Lb, self.prior_type_shear))
        
        return y1_error_of_representativeness, y2_error_of_representativeness


    def load_prior_flow(self):
        """
        Load prior flow
        :return: u_prior
        :rtype: ndarray (Ntimes x 2Lu(Lu + 2))
        """

        # Load prior flow
        if self.test == "":
            if self.prior_type_shear == "coupled_earth":
                #sign convention coupled earth U
                TNM = -np.loadtxt(self.prior_dir_shear + "/RealU.dat").T
            elif self.prior_type_shear == "geodynamo" or self.prior_type_shear == "midpath" or self.prior_type_shear == "71path":
                f = h5py.File(self.prior_dir_shear + "/Real.hdf5", "r")
                TNM = np.array(f['U']).T
        else:
            TNM = np.loadtxt(self.test + "/test_shear/tnmsnm").T

        tnm = TNM[: TNM.shape[0]//2, :]
        snm = TNM[TNM.shape[0]//2 :, :]
        flowcoefs = np.concatenate((-tnm[0:self.Nu, :], snm[0:self.Nu, :]), axis=0)
        u_prior = flowcoefs.T

        return u_prior


    def precompute_operators(self):
        """
        Precompute operators
        """

        #########################################
        # Calculating forward matrices to calculate 
        # theta phi maps 
        #########################################

        self.delta_plus_forward_matrix_complex = bbf.calculate_u_forward_complex(
            self.Lu, self.gauss_thetas, self.phis, bbf.generalised_Plm_plus, debug=True
        )

        self.delta_minus_forward_matrix_complex = bbf.calculate_u_forward_complex(
            self.Lu, self.gauss_thetas, self.phis, bbf.generalised_Plm_minus, debug=True
        )

        self.u_plus_forward_matrix_complex = np.array(self.delta_plus_forward_matrix_complex)
        self.u_minus_forward_matrix_complex = np.array(self.delta_minus_forward_matrix_complex)


        #########################################
        # Defining parameters to rebuild the
        # forward matrices from the form
        # [c10 cos, c11 cos, c11 sin, ... ]
        # to
        # [c1-1 real, c1-1 imag, c10 real, c10 imag, ... ]
        #########################################

        (
            self.u_in_real_imag_form_dict,
            self.Nu_real_imag_form,
            u_cos_sin_dict,
            Nu_cossin_form,
        ) = bbf.build_real_imag_and_cossin_dict_pos(self.Lu)

        #########################################
        # Transformation matrix is built this  way
        # [c10 cos, c11 cos, c11 sin, ... ]
        # to
        # [c1-1 real, c1-1 imag, c10 real, c10 imag, ... ]
        #########################################

        #u_Matrix_from_cossin_to_R_I_form = (
        #    bbf.building_transformation_matrix_from_cossin_to_R_I_form(
        #        self.Nu_real_imag_form, Nu_cossin_form, self.u_in_real_imag_form_dict, u_cos_sin_dict
        #    )
        #)
        #
        #u_Matrix_from_R_I_to_cossin = (
        #    bbf.building_transformation_matrix_from_R_I_to_cossin_form(
        #        self.Nu_real_imag_form, Nu_cossin_form, self.u_in_real_imag_form_dict, u_cos_sin_dict
        #    )
        #)

        #########################################
        # Matrix, that describes multiplication by 1j
        #########################################

        #Matrix_multiply_by_i = bbf.building_transformation_matrix_multiply_by_i(
        #    self.Nu_real_imag_form, self.u_in_real_imag_form_dict
        #)
        #########################################
        # Defining parameters to rebuild the
        # forward matrices from the form
        # [g10 cos, g11 cos, g11 sin, ... ]
        # to
        # [g1-1 real, g1-1 imag, g10 real, g10 imag, ... ]
        # for Br
        #########################################

        (
            b_in_real_imag_form_dict,
            Nb_real_imag_form,
            b_cos_sin_dict,
            Nb_cossin_form,
        ) = bbf.build_real_imag_and_cossin_dict_pos(self.Lb)

        # Same for the flow coefficients

        #(
        #    u_in_complex_form_dict,
        #    Nu_complex_form,
        #    u_cos_sin_dict,
        #    Nu_cossin_form,
        #) = bbf.build_complex_and_cossin_dict_pos(self.Lu)

        # Same for coefficients of change of magnetic field in time

        (
            _,
            _,
            _,
            self.Nsv_cossin_form,
        ) = bbf.build_real_imag_and_cossin_dict_pos(self.Lsv)

        #########################################
        # Transformation matrix is built this  way
        # [g10 cos, g11 cos, g11 sin, ... ]
        # to
        # [g1-1 real, g1-1 imag, g10 real, g10 imag, ... ]
        # for Br
        #########################################

        self.b_Matrix_from_cossin_to_R_I_form = (
            bbf.building_transformation_matrix_from_cossin_to_R_I_form_B(
                Nb_real_imag_form, Nb_cossin_form, b_in_real_imag_form_dict, b_cos_sin_dict
            )
        )


        #########################################
        # Pre calculating intermediate maps for 
        # the flow
        #########################################


        self.u_plus_data_matrices, self.u_minus_data_matrices = bbf.calculate_u_data_matrices(
            self.Nu_real_imag_form,
            self.u_in_real_imag_form_dict,
            self.gauss_thetas,
            self.phis,
            debug=True,
            u_plus_forward_matrix=self.u_plus_forward_matrix_complex,
            u_minus_forward_matrix=self.u_minus_forward_matrix_complex,
            forward_type="complex",
        )


        #########################################
        # The same but for "s" term in calculation 
        # of the shear
        #########################################

        (
            self.u_plus_forward_matrix_for_s,
            self.u_minus_forward_matrix_for_s,
        ) = bbf.calculate_u_plus_minus_forward_temp_for_s(self.Lu, self.gauss_thetas, self.phis, debug=True)


        #########################################
        # The same but for "v0" term in calculation 
        # of the shear
        #########################################


        self.u_zero_data_matrices = bbf.calculate_u_zero_data_matrices(
            self.Nu_real_imag_form, self.u_in_real_imag_form_dict, self.gauss_thetas, self.phis, debug=True
        )

        #########################################
        # Matricies that allow to recalculate 
        # u+lm and u-lm coefficients from tlm and clm
        #########################################

        self.TCtUmUp = templib.calculate_TlmClm_to_UminusUplus_matrix(self.Lu)

        self.UmUptTC = np.linalg.inv(self.TCtUmUp.T.dot(self.TCtUmUp)).dot(self.TCtUmUp.T)

        #########################################
        # Matricies to calculate u+lm u-lm 
        # from u-lm and backwards
        #########################################

        self.u_minus_plus_from_u_minus = templib.calculate_u_minus_plus_from_u_minus(self.Lu)

        self.u_minus_from_u_minus_plus = templib.calculate_u_minus_from_u_minus_plus(self.Lu)

        # Calculation of some supporting information 
        # for inverse problem for the shear
        # (Generally should be removed to some library)

        (
            Y_in_real_imag_form_dict,
            self.Ny_real_imag_form,
            _,
            _,
        ) = bbf.build_real_imag_and_cossin_dict_pos(self.Ly)

        REMOVE_LINES_Y_Real = []
        self.STAYED_LINES_Y_Real = []
        n = 0
        for l_ in range(1, self.Ly + 1):
            for m_ in range(-l_, l_ + 1):
                if m_ == 0:
                    self.STAYED_LINES_Y_Real.append(n)
                    REMOVE_LINES_Y_Real.append(n + 1)
                else:
                    self.STAYED_LINES_Y_Real.append(n)
                    self.STAYED_LINES_Y_Real.append(n + 1)
                n += 2

        Y_positive_to_full_real = np.zeros(
            (
                self.Ny_real_imag_form,
                self.Ly * (self.Ly + 2)
            )
        )

        ny_positive_real = 0
        for ly in range(1, self.Ly + 1):
            for my in range(0, ly + 1):
                temp = (-1) ** my
                
                y_pos_reim_pos = list(Y_in_real_imag_form_dict.keys())[
                    list(Y_in_real_imag_form_dict.values()).index((ly, my, "r"))
                ]
                y_pos_reim_neg = list(Y_in_real_imag_form_dict.keys())[
                    list(Y_in_real_imag_form_dict.values()).index((ly, -my, "r"))
                ]

                Y_positive_to_full_real[y_pos_reim_pos, ny_positive_real] = 1
                Y_positive_to_full_real[y_pos_reim_neg, ny_positive_real] = temp
                ny_positive_real += 1

                if my != 0:
                    Y_positive_to_full_real[y_pos_reim_pos+1, ny_positive_real] = 1
                    Y_positive_to_full_real[y_pos_reim_neg+1, ny_positive_real] = -temp
                    ny_positive_real += 1

        Y_positive_to_full_real = Y_positive_to_full_real[self.STAYED_LINES_Y_Real]

        self.Y_full_to_positive_real = np.linalg.inv(Y_positive_to_full_real.T.dot(Y_positive_to_full_real)).dot(Y_positive_to_full_real.T)


        REMOVE_LINES_Y_Imag = []
        self.STAYED_LINES_Y_Imag = []
        n = 0
        for l_ in range(1, self.Ly + 1):
            for m_ in range(-l_, l_ + 1):
                if m_ == 0:
                    REMOVE_LINES_Y_Imag.append(n)
                    self.STAYED_LINES_Y_Imag.append(n + 1)
                else:
                    self.STAYED_LINES_Y_Imag.append(n)
                    self.STAYED_LINES_Y_Imag.append(n + 1)
                n += 2

        Y_positive_to_full_imag = np.zeros(
            (
                self.Ny_real_imag_form,
                self.Ly * (self.Ly + 2)
            )
        )

        ny_positive_imag = 0
        for ly in range(1, self.Ly + 1):
            for my in range(0, ly + 1):
                temp = (-1) ** my

                if my != 0:
                    y_pos_reim_pos = list(Y_in_real_imag_form_dict.keys())[
                        list(Y_in_real_imag_form_dict.values()).index((ly, my, "r"))
                    ]
                    y_pos_reim_neg = list(Y_in_real_imag_form_dict.keys())[
                        list(Y_in_real_imag_form_dict.values()).index((ly, -my, "r"))
                    ]

                    Y_positive_to_full_imag[y_pos_reim_pos, ny_positive_imag] = 1
                    Y_positive_to_full_imag[y_pos_reim_neg, ny_positive_imag] = -temp
                    ny_positive_imag += 1

                    Y_positive_to_full_imag[y_pos_reim_pos+1, ny_positive_imag] = 1
                    Y_positive_to_full_imag[y_pos_reim_neg+1, ny_positive_imag] = temp
                    ny_positive_imag += 1


                else:
                    y_pos_reim_pos = list(Y_in_real_imag_form_dict.keys())[
                        list(Y_in_real_imag_form_dict.values()).index((ly, my, "i"))
                    ]
                    y_pos_reim_neg = list(Y_in_real_imag_form_dict.keys())[
                        list(Y_in_real_imag_form_dict.values()).index((ly, -my, "i"))
                    ]

                    Y_positive_to_full_imag[y_pos_reim_pos, ny_positive_imag] = 1
                    Y_positive_to_full_imag[y_pos_reim_neg, ny_positive_imag] = temp
                    ny_positive_imag += 1

        Y_positive_to_full_imag = Y_positive_to_full_imag[self.STAYED_LINES_Y_Imag]

        self.Y_full_to_positive_imag = np.linalg.inv(Y_positive_to_full_imag.T.dot(Y_positive_to_full_imag)).dot(Y_positive_to_full_imag.T)

        (
            delta_in_re_im_form_dict,
            self.N_delta_re_im_form,
            _,
            _,
        ) = bbf.build_real_imag_and_cossin_dict_pos(self.Lu, q=True)

        s_plus_from_s_minus = np.zeros(
            (
                self.N_delta_re_im_form,
                self.N_delta_re_im_form
            )
        )

        for lu in range(0, self.Lu + 1):
            for mu in range(-lu, lu + 1):
                temp = (-1) ** mu
                
                s_pos_reim_plus = list(delta_in_re_im_form_dict.keys())[
                    list(delta_in_re_im_form_dict.values()).index((lu, mu, "r"))
                ]
                s_pos_reim_minus = list(delta_in_re_im_form_dict.keys())[
                    list(delta_in_re_im_form_dict.values()).index((lu, -mu, "r"))
                ]

                s_plus_from_s_minus[s_pos_reim_plus, s_pos_reim_minus] = temp
                s_plus_from_s_minus[s_pos_reim_plus + 1, s_pos_reim_minus + 1] = -temp

        self.s_minus_plus_from_s_minus = np.concatenate((np.eye(self.N_delta_re_im_form), s_plus_from_s_minus), axis=0)

        #s_minus_from_s_minus_plus = np.linalg.inv(self.s_minus_plus_from_s_minus.T.dot(self.s_minus_plus_from_s_minus)).dot(self.s_minus_plus_from_s_minus.T)

        self.ulm_to_clm_matrix = np.zeros((self.Nu_real_imag_form, self.Nu_real_imag_form))
        nu = 0
        for lu in range(1, self.Lu + 1):
            for mu in range(-lu, lu + 1):
                temp = np.sqrt(2) / np.sqrt(lu * (lu + 1))
                self.ulm_to_clm_matrix[nu, nu] = temp
                nu += 1
                self.ulm_to_clm_matrix[nu, nu] = temp
                nu += 1


    def compute_prior_shear(self, u_prior):
        """
        Compute a prior matrix for the shear 

        :param u_prior: Toroidal/poloidal flow Schmidt semi-normalized spherical harmonic coefficients
        :type u_prior: ndarray (Ntimes x 2Lu(Lu + 2))
        :return Pss_inv_with_glasso: Covariance matrix Toroidal/poloidal flow Schmidt semi-normalized spherical harmonic coefficients
        :rtype Pss_inv_with_glasso: ndarray (2Lu(Lu + 2) x 2Lu(Lu + 2))
        """
        
        # Calculating a priory matrix for the shear 

        u_covar_coefs = np.array(u_prior)
        u_m_covar_coefs = templib.flowToMMatrix(u_covar_coefs, self.Lu)
        Pss_from_dynamo_covar_cossin = np.matmul(u_m_covar_coefs.T, u_m_covar_coefs) / (u_m_covar_coefs.shape[0] - 1) 

        # Applying graphical lasso to shear prior information

        Css_temp = np.linalg.inv(np.diag(np.sqrt(np.diag(Pss_from_dynamo_covar_cossin)))).dot(Pss_from_dynamo_covar_cossin).dot(np.linalg.inv(np.diag(np.sqrt(np.diag(Pss_from_dynamo_covar_cossin)))))
        Css_tilda = skcov.graphical_lasso(Css_temp, self.glasso_lambda_u, max_iter=100)[0]
        Pss_tilda = np.diag(np.sqrt(np.diag(Pss_from_dynamo_covar_cossin))).dot(Css_tilda).dot(np.diag(np.sqrt(np.diag(Pss_from_dynamo_covar_cossin))))
        Pss_inv_with_glasso = np.linalg.inv(self.u_minus_from_u_minus_plus.dot(self.TCtUmUp).dot(Pss_tilda).dot(self.TCtUmUp.T).dot(self.u_minus_plus_from_u_minus))
        
        return Pss_inv_with_glasso


    def compute_prior_error_shear(self, y1_error_of_representativeness, y2_error_of_representativeness):
        """
        Computing a priory error matrix for the shear product  

        :param y1_error_of_representativeness: Toroidal flow error Schmidt semi-normalized spherical harmonic coefficients
        :type y1_error_of_representativeness: ndarray (Ntimes x Lu(Lu + 2))
        :param y2_error_of_representativeness: Poloidal flow error Schmidt semi-normalized spherical harmonic coefficients
        :type y2_error_of_representativeness: ndarray (Ntimes x Lu(Lu + 2))
        :return Pee_y1_tilda_with_glasso: Covariance matrix Toroidal flow error Schmidt semi-normalized spherical harmonic coefficients
        :rtype Pee_y1_tilda_with_glasso: ndarray (Lu(Lu + 2) x Lu(Lu + 2))
        :return Pee_y2_tilda_with_glasso: Covariance matrix Poloidal flow error Schmidt semi-normalized spherical harmonic coefficients
        :rtype Pee_y2_tilda_with_glasso: ndarray (Lu(Lu + 2) x Lu(Lu + 2))
        """

        # Calculating a priory error matrix for the shear product  
        Pee_y1_apriory_temp = y1_error_of_representativeness.T.dot(y1_error_of_representativeness) / y1_error_of_representativeness.shape[0]
        Pee_y2_apriory_temp = y2_error_of_representativeness.T.dot(y2_error_of_representativeness) / y2_error_of_representativeness.shape[0]

        # Applying graphical lasso to shear prior information
        Cee_y1_temp = np.linalg.inv(np.diag(np.sqrt(np.diag(Pee_y1_apriory_temp)))).dot(Pee_y1_apriory_temp).dot(np.linalg.inv(np.diag(np.sqrt(np.diag(Pee_y1_apriory_temp)))))
        Cee_y2_temp = np.linalg.inv(np.diag(np.sqrt(np.diag(Pee_y2_apriory_temp)))).dot(Pee_y2_apriory_temp).dot(np.linalg.inv(np.diag(np.sqrt(np.diag(Pee_y2_apriory_temp)))))

        Cee_y1_tilda = skcov.graphical_lasso(Cee_y1_temp, self.glasso_lambda_err, max_iter=100)[0]
        Cee_y2_tilda = skcov.graphical_lasso(Cee_y2_temp, self.glasso_lambda_err, max_iter=100)[0]

        Pee_y1_tilda_with_glasso = np.diag(np.sqrt(np.diag(Pee_y1_apriory_temp))).dot(Cee_y1_tilda).dot(np.diag(np.sqrt(np.diag(Pee_y1_apriory_temp))))
        Pee_y2_tilda_with_glasso = np.diag(np.sqrt(np.diag(Pee_y2_apriory_temp))).dot(Cee_y2_tilda).dot(np.diag(np.sqrt(np.diag(Pee_y2_apriory_temp))))
        
        return Pee_y1_tilda_with_glasso, Pee_y2_tilda_with_glasso


    def compute_shear(self, U, DU, MF, SV, Pee_y1_tilda_with_glasso, Pee_y2_tilda_with_glasso, Pss_inv_with_glasso):
        """
        Compute the shear 

        :param U: Toroidal/poloidal flow in Schmidt semi-normalized spherical harmonic coefficients
        :type U: ndarray (2Lu(Lu + 2))
        :param DU: Toroidal/poloidal rate of change flow in Schmidt semi-normalized spherical harmonic coefficients
        :type DU: ndarray (2Lu(Lu + 2))
        :param MF: Radial Main Field in Schmidt semi-normalized spherical harmonic coefficients
        :type MF: ndarray (Lb(Lb + 2))
        :param SV: Radial Secular Variation in Schmidt semi-normalized spherical harmonic coefficients
        :type SV: ndarray (Lsv(Lsv + 2))
        :param Pee_y1_tilda_with_glasso: Covariance matrix Toroidal flow error Schmidt semi-normalized spherical harmonic coefficients
        :type Pee_y1_tilda_with_glasso: ndarray (Lu(Lu + 2) x Lu(Lu + 2))
        :param Pee_y2_tilda_with_glasso: Covariance matrix Poloidal flow error Schmidt semi-normalized spherical harmonic coefficients
        :type Pee_y2_tilda_with_glasso: ndarray (Lu(Lu + 2) x Lu(Lu + 2))
        :param Pss_inv_with_glasso: Covariance matrix Toroidal/poloidal flow Schmidt semi-normalized spherical harmonic coefficients
        :type Pss_inv_with_glasso: ndarray (2Lu(Lu + 2) x 2Lu(Lu + 2))
        :return SH_coef: Toroidal/poloidal shear in Schmidt semi-normalized spherical harmonic coefficients
        :rtype SH_coef: ndarray (2Lu(Lu + 2))
        """

        U, DU, MF, SV = self.load_data(U, DU, MF, SV)

        (
            Br_glm_Rc_r_i,
            Br_data_matrix,
            Br_dt_data_matrix, 
            u_true, 
            u_dt_true
        ) = self.prepare_data(U, DU, MF, SV)

        self.build_forward_matrices(Br_glm_Rc_r_i, Br_data_matrix, Br_dt_data_matrix)

        SH_coef = self.compute_inverse(
            Pee_y1_tilda_with_glasso,
            Pee_y2_tilda_with_glasso,
            Pss_inv_with_glasso, 
            u_true,
            u_dt_true
            )

        return SH_coef


    def load_data(self, U, DU, MF, SV):
        """
        Load U DU MF and SV coefficient vectors at required time
        """

        measures = ["U", "DU", "MF", "SV"]
        for i, data in enumerate([U, DU, MF, SV]):
            if np.max(data.shape) == data.reshape(-1,).shape[0]:
                data = data.reshape(-1,)
            else:
                raise ValueError(measures[i] + " must be 1D but has dimensions " + str(data.shape))
            
        return U, DU, MF, SV


    def prepare_data(self, U, DU, MF, SV):
        """
        Perform U, DU, MF, SV transformations
        """
         
        #########################################
        # Defining coefficients that are used 
        # in calculation 
        # glm - main field coefficients
        # dtglm - coefficients of change of 
        #         the magnetic field
        # tlmclm - flow coefficients
        #########################################

        glm = np.array(MF)
        dtglm = np.array(SV)

        # multiplying magnetic field coefficients by -1^m
        # and recalcuting it to the Earth Core radius

        (Br_glm_Rc_2, _, _) = templib.magnFieldFromRaToRaRcM(glm, self.Lb)
        (gdot_Rc_coeffs_2, _, _) = templib.magnFieldFromRaToRaRcM(dtglm, self.Lb)
        
        # recalcilating to another representation and building
        # physical values on theta phi

        Br_glm_Rc_r_i = self.b_Matrix_from_cossin_to_R_I_form.dot(Br_glm_Rc_2)
        Br_data_matrix = bbf.calculate_Br_from_ReIm(self.Lb, self.gauss_thetas, self.phis, Br_glm_Rc_r_i)

        Br_glm_Rc_dt_r_i = self.b_Matrix_from_cossin_to_R_I_form.dot(gdot_Rc_coeffs_2)
        Br_dt_data_matrix = bbf.calculate_Br_from_ReIm(self.Lb, self.gauss_thetas, self.phis, Br_glm_Rc_dt_r_i)

        #########################################
        # multiplying all flow coefficients by -1^m

        u_m = templib.flowToM(U, self.Lu)

        # Doing the same for the time change of 
        # the flow coefficients

        u_m_dt = templib.flowToM(DU, self.Lu)

        # caclculating u+lm and u-lm from tlm clm

        u_true = self.TCtUmUp.dot(u_m)
        u_dt_true = self.TCtUmUp.dot(u_m_dt)
        
        return Br_glm_Rc_r_i, Br_data_matrix, Br_dt_data_matrix, u_true, u_dt_true
    

    def build_forward_matrices(self, Br_glm_Rc_r_i, Br_data_matrix, Br_dt_data_matrix):
        """
        Build forward matrices
        """
        
        #########################################
        self.t_plus_matrix_from_ulm, self.t_minus_matrix_from_ulm = bbf.calculate_t_from_u(
            self.Ly,
            self.Lb,
            self.Nu_real_imag_form,
            self.gauss_thetas,
            self.gauss_weights,
            self.phis,
            np.array(Br_glm_Rc_r_i),
            self.u_plus_data_matrices,
            self.u_minus_data_matrices,
            tmax=self.tmax,
            debug=True,
        )

        #########################################
        self.v_zero_matrix_from_ulm = bbf.calculate_v0_from_u_minus_plus(
            self.Ly,
            self.Lb,
            self.Ny_real_imag_form,
            self.gauss_thetas,
            self.gauss_weights,
            self.phis,
            np.array(Br_glm_Rc_r_i),
            self.u_plus_data_matrices,
            self.u_minus_data_matrices,
            tmax=self.tmax,
            debug=True,
        )

        #########################################
        self.p_plus_matrix_from_clm, self.p_minus_matrix_from_clm = bbf.calculate_p_from_clm(
            self.Ly,
            self.Lb,
            self.Ny_real_imag_form,
            self.Nu_real_imag_form,
            np.array(Br_glm_Rc_r_i),
            self.gauss_thetas,
            self.gauss_weights,
            self.phis,
            self.u_zero_data_matrices,
            tmax=self.tmax,
            debug=True,
        )

        #########################################
        self.s_plus_matrix_from_delta, self.s_minus_matrix_from_delta = bbf.calculate_s_from_delta(
            self.Ly,
            self.Lb,
            self.Lu,
            np.array(Br_glm_Rc_r_i),
            self.gauss_thetas,
            self.gauss_weights,
            self.phis,
            self.u_plus_forward_matrix_for_s,
            self.u_minus_forward_matrix_for_s,
            tmax=self.tmax,
            debug=True,
        )

        #############################

        #(
        #    _,
        #    _,
        #    dBr_dt_matrix_from_u_minus_plus,
        #) = templib.openOrCalculateDBrDtForward(
        #    self.Lb,
        #    self.Lsv,
        #    self.Lq,
        #    self.tmax,
        #    self.Nu_real_imag_form,
        #    self.Br_data_matrix,
        #    self.u_in_real_imag_form_dict,
        #    self.gauss_thetas,
        #    self.gauss_weights,
        #    self.phis,
        #    self.u_plus_forward_matrix_complex,
        #    self.u_minus_forward_matrix_complex,
        #    print_text="Opening or calculating dBr/dt forward matrix Br...",
        #)

        (
            self.v_plus_matrices,
            self.v_minus_matrices,
            _,
        ) = templib.openOrCalculateDBrDtForward(
            self.Lb,
            self.Ly,
            self.Lq,
            self.tmax,
            self.Nu_real_imag_form,
            Br_data_matrix,
            self.u_in_real_imag_form_dict,
            self.gauss_thetas,
            self.gauss_weights,
            self.phis,
            self.u_plus_forward_matrix_complex,
            self.u_minus_forward_matrix_complex
        )

        self.v_plus_matrices = templib.from_Ra_to_Rc(self.v_plus_matrices, self.Ly)
        self.v_minus_matrices = templib.from_Ra_to_Rc(self.v_minus_matrices, self.Ly)

        (
            self.v_plus_matrices_dBdt,
            self.v_minus_matrices_dBdt,
            _,
        ) = templib.openOrCalculateDBrDtForward(
            self.Lb,
            self.Ly,
            self.Lq,
            self.tmax,
            self.Nu_real_imag_form,
            Br_dt_data_matrix,
            self.u_in_real_imag_form_dict,
            self.gauss_thetas,
            self.gauss_weights,
            self.phis,
            self.u_plus_forward_matrix_complex,
            self.u_minus_forward_matrix_complex
        )

        self.v_plus_matrices_dBdt = templib.from_Ra_to_Rc(self.v_plus_matrices_dBdt, self.Ly)
        self.v_minus_matrices_dBdt = templib.from_Ra_to_Rc(self.v_minus_matrices_dBdt, self.Ly)


        (
            v_plus_matrices_2,
            v_minus_matrices_2,
            _,
        ) = templib.openOrCalculateDBrDtForward(
            self.Lb,
            self.Ly,
            self.Lq,
            self.tmax,
            self.Nu_real_imag_form,
            Br_data_matrix,
            self.u_in_real_imag_form_dict,
            self.gauss_thetas,
            self.gauss_weights,
            self.phis,
            self.u_plus_forward_matrix_complex,
            self.u_minus_forward_matrix_complex
        )


        ###############################
        self.lv_plus_matrix = np.array(v_plus_matrices_2)
        self.lv_minus_matrix = np.array(v_minus_matrices_2)
        n = 0
        for lv in range(1, self.Ly):
            for mv in range(-lv, lv + 1):
                self.lv_plus_matrix[n, :] = self.lv_plus_matrix[n, :] * -lv
                self.lv_minus_matrix[n, :] = self.lv_minus_matrix[n, :] * -lv
                n += 1
                self.lv_plus_matrix[n, :] = self.lv_plus_matrix[n, :] * -lv
                self.lv_minus_matrix[n, :] = self.lv_minus_matrix[n, :] * -lv
                n += 1

        self.lv_plus_matrix = templib.from_Ra_to_Rc(self.lv_plus_matrix, self.Ly)
        self.lv_minus_matrix = templib.from_Ra_to_Rc(self.lv_minus_matrix, self.Ly)


    def compute_inverse(self, Pee_y1_tilda_with_glasso, Pee_y2_tilda_with_glasso, Pss_inv_with_glasso, u_true, u_dt_true):
        """
        Solve the shear inverse problem
        """

        #Ny_real_imag_stayed = len(self.STAYED_LINES_Y_Real)

        A1 = (
            -np.concatenate((self.t_minus_matrix_from_ulm, self.t_plus_matrix_from_ulm), axis=1)
            + self.v_zero_matrix_from_ulm
            - np.concatenate(
                (
                    self.p_minus_matrix_from_clm.dot(self.ulm_to_clm_matrix),
                    self.p_plus_matrix_from_clm.dot(self.ulm_to_clm_matrix),
                ),
                axis=1,
            )
        )
        A1 = np.array(A1[:self.Ny_real_imag_form, :])[self.STAYED_LINES_Y_Real]
        A1 = self.Y_full_to_positive_real.dot(A1)

        B1 = np.concatenate((self.s_minus_matrix_from_delta, self.s_plus_matrix_from_delta), axis=1)
        B1 = np.array(B1[:self.Ny_real_imag_form, :])[self.STAYED_LINES_Y_Real]
        B1_minus = (B1.dot(self.s_minus_plus_from_s_minus))[:, 2:]
        B1_minus = self.Y_full_to_positive_real.dot(B1_minus)

        A1_conducting_dBdt = (
            self.tau1 * np.concatenate((self.v_minus_matrices_dBdt, self.v_plus_matrices_dBdt), axis=1)
        )
        A1_conducting_dBdt = np.array(A1_conducting_dBdt[:self.Ny_real_imag_form, :])[self.STAYED_LINES_Y_Real]
        A1_conducting_dBdt = self.Y_full_to_positive_real.dot(A1_conducting_dBdt)

        A1_conducting_Br = (
            self.tau1 * np.concatenate((self.v_minus_matrices, self.v_plus_matrices), axis=1)
        )
        A1_conducting_Br = np.array(A1_conducting_Br[:self.Ny_real_imag_form, :])[self.STAYED_LINES_Y_Real]
        A1_conducting_Br = self.Y_full_to_positive_real.dot(A1_conducting_Br)

        #y1_true = A1.dot(u_true)
        y1_true_conducting = A1.dot(u_true) + A1_conducting_dBdt.dot(u_true) + A1_conducting_Br.dot(u_dt_true)

        ###############################

        A2 = (
            -np.concatenate((-self.t_minus_matrix_from_ulm, self.t_plus_matrix_from_ulm), axis=1)
            - np.concatenate(
                (
                    -self.p_minus_matrix_from_clm.dot(self.ulm_to_clm_matrix),
                    self.p_plus_matrix_from_clm.dot(self.ulm_to_clm_matrix),
                ),
                axis=1,
            )
            - np.concatenate((-self.lv_minus_matrix, self.lv_plus_matrix), axis=1)
        )

        A2 = np.array(A2[:self.Ny_real_imag_form, :])[self.STAYED_LINES_Y_Imag]
        A2 = self.Y_full_to_positive_imag.dot(A2)

        # A2_conducting_dBdt = (
        #     tau1 * np.concatenate((-v_minus_matrices_dBdt, v_plus_matrices_dBdt), axis=1)
        # )
        # A2_conducting_dBdt = np.array(A2_conducting_dBdt[:Ny_real_imag_form, :])[STAYED_LINES_Y_Imag]
        # A2_conducting_dBdt = Y_full_to_positive_imag.dot(A2_conducting_dBdt)

        # A2_conducting_Br = (
        #     tau1 * np.concatenate((-v_minus_matrices, v_plus_matrices), axis=1)
        # )
        # A2_conducting_Br = np.array(A2_conducting_Br[:Ny_real_imag_form, :])[STAYED_LINES_Y_Imag]
        # A2_conducting_Br = Y_full_to_positive_imag.dot(A2_conducting_Br)

        B2 = np.concatenate((-self.s_minus_matrix_from_delta, self.s_plus_matrix_from_delta), axis=1)
        B2 = np.array(B2[:self.Ny_real_imag_form, :])[self.STAYED_LINES_Y_Imag]
        B2_minus = (B2.dot(self.s_minus_plus_from_s_minus))[:, 2:]
        B2_minus = self.Y_full_to_positive_imag.dot(B2_minus)

        y2_true_conducting = A2.dot(u_true)
        # y2_true_conducting = A2.dot(u_true) + A2_conducting_dBdt.dot(u_true) + A2_conducting_Br.dot(u_dt_true)

        ################################

        R1_with_glasso = Pee_y1_tilda_with_glasso

        ###############################

        R2_with_glasso = Pee_y2_tilda_with_glasso

        ###############################

        y12_true_conducting = np.concatenate((y1_true_conducting, y2_true_conducting), axis=0)
        B12 = np.concatenate((B1_minus, B2_minus), axis=0)
        R12_with_glasso = np.concatenate((np.concatenate((R1_with_glasso, np.zeros(R1_with_glasso.shape)), axis=0), np.concatenate((np.zeros(R2_with_glasso.shape), R2_with_glasso), axis=0)), axis=1)
        R12_inv_with_glasso = np.linalg.inv(R12_with_glasso)

        Pss1212_hathat_with_glasso = np.linalg.inv(B12.T.dot(R12_inv_with_glasso).dot(B12) + Pss_inv_with_glasso)

        s12hat_true_conducting_temp = Pss1212_hathat_with_glasso.dot(B12.T).dot(R12_inv_with_glasso).dot(y12_true_conducting)
        s12hat_true_conducting = np.zeros((self.Nu_real_imag_form, ))
        s12hat_true_conducting = s12hat_true_conducting_temp
        s12hat_true_conducting = self.u_minus_plus_from_u_minus.dot(s12hat_true_conducting)

        # Convert complex SH basis to schmidt semi normalized coefficients
        SH_coef = self.UmUptTC.dot(s12hat_true_conducting)
        SH_coef = templib.flowToM(SH_coef, self.Lu)
        SH_coef[:self.Nu] = -SH_coef[:self.Nu]
        SH_coef = SH_coef.reshape(-1,)

        return SH_coef



class ShearError(ShearInversion):
    """
    Compute the error of representativeness at CMB
    """

    def __init__(self, Ly, Lu, Lb, TauG, prior_dir_shear, prior_type_shear, test=""):
        """
        Init instance of ShearError
        """

        self.Ly = Ly
        self.Lu = Lu
        self.Lb = Lb
        self.Lsv = self.Lb

        self.prior_dir_shear = prior_dir_shear
        self.prior_type_shear = prior_type_shear
        self.tau1 = TauG
        self.test = test

        self.tmax = 64
        self.tpmax = 2 *  self.tmax**2
        self.pmax = 2 *  self.tmax
        self.gauss_thetas,  self.gauss_weights = bbf.gaussPoints(0, np.pi,  self.tmax)
        self.phis = np.linspace(0, 2 * np.pi, self.pmax, endpoint=False)
        self.Nb = self.Lb * (self.Lb + 2)
        self.Nsv = self.Lsv * (self.Lsv + 2)
        self.Nu = self.Lu * (self.Lu + 2)
        self.Nu2 = 2 * self.Nu
        self.Ny = self.Ly * (self.Ly + 2)
        self.Ny2 = 2 * self.Ny
        self.Lq = self.Lu - 1


    def load_prior_for_err_rep(self):
        """
        Load prior to compute error of representativeness

        :return U: Toroidal/poloidal flow in Schmidt semi-normalized spherical harmonic coefficients
        :rtype U: ndarray (2Lu(Lu + 2))
        :return DU: Toroidal/poloidal rate of change flow in Schmidt semi-normalized spherical harmonic coefficients
        :rtype DU: ndarray (2Lu(Lu + 2))
        :return MF: Radial Main Field in Schmidt semi-normalized spherical harmonic coefficients
        :rtype MF: ndarray (Lb(Lb + 2))
        :return SV: Radial Secular Variation in Schmidt semi-normalized spherical harmonic coefficients
        :rtype SV: ndarray (Lb(Lb + 2))
        """

        if self.test == "":
            if self.prior_type_shear == "coupled_earth":
                #sign convention coupled earth U
                u_full = -np.loadtxt(self.prior_dir_shear + "/RealU.dat")
                mf_full = np.loadtxt(self.prior_dir_shear + "/RealB.dat")
            elif self.prior_type_shear == "geodynamo" or self.prior_type_shear == "midpath" or self.prior_type_shear == "71path":
                f = h5py.File(self.prior_dir_shear + "/Real.hdf5", "r")
                u_full = np.array(f['U'])
                mf_full = np.array(f['MF'])
        else:
            u_full = np.loadtxt(self.test + "/test_shear/tnmsnm")
            mf_full = np.loadtxt(self.test + "/test_shear/gnm")

        tnm = u_full[:, : u_full.shape[1]//2]
        snm = u_full[:, u_full.shape[1]//2 :]
        U = np.concatenate((-tnm[:, 0:self.Nu], snm[:, 0:self.Nu]), axis=1)
        MF = mf_full[:, :self.Nb]
        DU=np.zeros(np.shape(U)) # because TauG = 0
        SV=np.zeros(np.shape(MF)) # because TauG = 0

        if not U.shape[0] == DU.shape[0] == MF.shape[0] == SV.shape[0]:
            raise ValueError("U, DU, MF, SV time series are not the same length, respectively {}, {}, {}, {}".format(U.shape[0],DU.shape[0],MF.shape[0],SV.shape[0]))
        
        return U, DU, MF, SV
    

    def compute_derivative(self,A,N,dt):
        out = np.zeros(A.shape)
        N = A.shape[1]
        for i in range(N):
            if i == 0 :
                out[:, i] = (A[:, i+1] - A[:, i]) / (dt)
            elif i == N - 1 :
                out[:, i] = (A[:, i] - A[:, i-1]) / (dt)
            else :
                out[:, i] = (A[:, i+1] - A[:, i-1]) / (2*dt)

        return out
    

    def compute_error(self, U, DU, MF, SV):
        """
        Compute the error of representativeness

        :param U: Toroidal/poloidal flow in Schmidt semi-normalized spherical harmonic coefficients
        :type U: ndarray (2Lu(Lu + 2))
        :param DU: Toroidal/poloidal rate of change flow in Schmidt semi-normalized spherical harmonic coefficients
        :type DU: ndarray (2Lu(Lu + 2))
        :param MF: Radial Main Field in Schmidt semi-normalized spherical harmonic coefficients
        :type MF: ndarray (Lb(Lb + 2))
        :param SV: Radial Secular Variation in Schmidt semi-normalized spherical harmonic coefficients
        :type SV: ndarray (Lb(Lb + 2))
        :return err: Toroidal/poloidal error of representativeness in Schmidt semi-normalized spherical harmonic coefficients
        :rtype: ndarray (Ntimes x 2Ly(Ly + 2))
        """

        U, DU, MF, SV = self.load_data(U, DU, MF, SV)

        (
            Br_glm_Rc_r_i,
            Br_data_matrix,
            Br_dt_data_matrix, 
            u_true, 
            u_dt_true
        ) = self.prepare_data(U, DU, MF, SV)

        self.build_forward_matrices(Br_glm_Rc_r_i, Br_data_matrix, Br_dt_data_matrix)

        err = self.solve_error(
            u_true,
            u_dt_true
            )

        return err
        


    def solve_error(self, u_true, u_dt_true):
        """
        Solve for the error
        """

        #Ny_real_imag_stayed = len(self.STAYED_LINES_Y_Real)

        A1 = (
            -np.concatenate((self.t_minus_matrix_from_ulm, self.t_plus_matrix_from_ulm), axis=1)
            + self.v_zero_matrix_from_ulm
            - np.concatenate(
                (
                    self.p_minus_matrix_from_clm.dot(self.ulm_to_clm_matrix),
                    self.p_plus_matrix_from_clm.dot(self.ulm_to_clm_matrix),
                ),
                axis=1,
            )
        )
        A1 = np.array(A1[:self.Ny_real_imag_form, :])[self.STAYED_LINES_Y_Real]
        A1 = self.Y_full_to_positive_real.dot(A1)

        B1 = np.concatenate((self.s_minus_matrix_from_delta, self.s_plus_matrix_from_delta), axis=1)
        B1 = np.array(B1[:self.Ny_real_imag_form, :])[self.STAYED_LINES_Y_Real]
        B1_minus = (B1.dot(self.s_minus_plus_from_s_minus))[:, 2:]
        B1_minus = self.Y_full_to_positive_real.dot(B1_minus)

        A1_conducting_dBdt = (
            self.tau1 * np.concatenate((self.v_minus_matrices_dBdt, self.v_plus_matrices_dBdt), axis=1)
        )
        A1_conducting_dBdt = np.array(A1_conducting_dBdt[:self.Ny_real_imag_form, :])[self.STAYED_LINES_Y_Real]
        A1_conducting_dBdt = self.Y_full_to_positive_real.dot(A1_conducting_dBdt)

        A1_conducting_Br = (
            self.tau1 * np.concatenate((self.v_minus_matrices, self.v_plus_matrices), axis=1)
        )
        A1_conducting_Br = np.array(A1_conducting_Br[:self.Ny_real_imag_form, :])[self.STAYED_LINES_Y_Real]
        A1_conducting_Br = self.Y_full_to_positive_real.dot(A1_conducting_Br)

        #y1_true = A1.dot(u_true)
        y1_true_conducting = A1.dot(u_true) + A1_conducting_dBdt.dot(u_true) + A1_conducting_Br.dot(u_dt_true)

        ###############################

        A2 = (
            -np.concatenate((-self.t_minus_matrix_from_ulm, self.t_plus_matrix_from_ulm), axis=1)
            - np.concatenate(
                (
                    -self.p_minus_matrix_from_clm.dot(self.ulm_to_clm_matrix),
                    self.p_plus_matrix_from_clm.dot(self.ulm_to_clm_matrix),
                ),
                axis=1,
            )
            - np.concatenate((-self.lv_minus_matrix, self.lv_plus_matrix), axis=1)
        )

        A2 = np.array(A2[:self.Ny_real_imag_form, :])[self.STAYED_LINES_Y_Imag]
        A2 = self.Y_full_to_positive_imag.dot(A2)

        # A2_conducting_dBdt = (
        #     tau1 * np.concatenate((-v_minus_matrices_dBdt, v_plus_matrices_dBdt), axis=1)
        # )
        # A2_conducting_dBdt = np.array(A2_conducting_dBdt[:Ny_real_imag_form, :])[STAYED_LINES_Y_Imag]
        # A2_conducting_dBdt = Y_full_to_positive_imag.dot(A2_conducting_dBdt)

        # A2_conducting_Br = (
        #     tau1 * np.concatenate((-v_minus_matrices, v_plus_matrices), axis=1)
        # )
        # A2_conducting_Br = np.array(A2_conducting_Br[:Ny_real_imag_form, :])[STAYED_LINES_Y_Imag]
        # A2_conducting_Br = Y_full_to_positive_imag.dot(A2_conducting_Br)

        B2 = np.concatenate((-self.s_minus_matrix_from_delta, self.s_plus_matrix_from_delta), axis=1)
        B2 = np.array(B2[:self.Ny_real_imag_form, :])[self.STAYED_LINES_Y_Imag]
        B2_minus = (B2.dot(self.s_minus_plus_from_s_minus))[:, 2:]
        B2_minus = self.Y_full_to_positive_imag.dot(B2_minus)

        y2_true_conducting = A2.dot(u_true)
        # y2_true_conducting = A2.dot(u_true) + A2_conducting_dBdt.dot(u_true) + A2_conducting_Br.dot(u_dt_true)

        y12_true_conducting = np.concatenate((y1_true_conducting, y2_true_conducting), axis=0)
        B12 = np.concatenate((B1_minus, B2_minus), axis=0)

        # Compute representativeness error
        err = y12_true_conducting - B12 @ self.u_minus_from_u_minus_plus.dot(u_true)
        err = err.reshape(-1,)
        
        return err        

