import numpy as np
from scipy.special import lpmv
import math


Rc = 3485.0 * 10**0  # CMB radius (km)
Ra = 6371.2 * 10**0  # Earth's reference radius (km)


# =============================================
# calculation of the spectrum
# =============================================


def calculate_spectrum(coef, L):
    spectrum = np.zeros((L,))
    iter = 0
    for l in range(1, L + 1):
        temp = 0
        for m in range(l + 1):
            if m == 0:
                temp += coef[iter] ** 2
                iter += 1
            else:
                temp += coef[iter] ** 2
                temp += coef[iter + 1] ** 2
                iter += 2
        spectrum[l - 1] += temp * (l + 1)
    return spectrum


# =============================================
# calculation Gauss points and weights
# =============================================


def gaussLeg(lower_bound, upper_bound, N):
    M = (N + 1) // 2
    XM = 0.5 * (upper_bound + lower_bound)
    XL = 0.5 * (upper_bound - lower_bound)

    gauss_pts = np.zeros(N)
    gauss_wgths = np.zeros(N)

    pp = 0
    ppp = 0

    EPS = 10**-14

    for i in range(1, M + 1):
        converged = False
        z = np.cos(np.pi * (i - 0.25) / (N + 0.5))
        while not converged:
            p1 = 1.0
            p2 = 0.0

            for j in range(1, N + 1):
                p3 = p2
                p2 = p1
                p1 = ((2.0 * j - 1.0) * z * p2 - (j - 1.0) * p3) / j

            pp = N * (z * p1 - p2) / ((z - 1.0) * (z + 1.0))
            ppp = N * (z * p1 - p2)
            z1 = z
            z = z1 - p1 / pp

            if np.abs(z - z1) < EPS:
                converged = not converged

        gauss_pts[i - 1] = XM - XL * z
        gauss_pts[N + 1 - i - 1] = XM + XL * z
        gauss_wgths[i - 1] = 2.0 * XL * (1.0 - z) * (1.0 + z) / (ppp * ppp)
        gauss_wgths[N + 1 - i - 1] = gauss_wgths[i - 1]

    return gauss_pts, gauss_wgths


# =============================================
# calculation Gauss theta points and weights
# =============================================


def gaussPoints(thetaMin, thetaMax, N):
    gauss_pts, gauss_wgths = gaussLeg(np.cos(thetaMax), np.cos(thetaMin), N)
    gauss_theta = np.arccos(gauss_pts)

    return gauss_theta, gauss_wgths


# =============================================
# calculation norm coefficient for QN Shmidt
# =============================================


def norm_coef(l, m, *args):
    if m == 0:
        return 1
    else:
        return np.sqrt((2) * ((math.factorial(l - m)) / (math.factorial(l + m))))


# =============================================
# calculation norm coefficient full norm
# =============================================


def full_norm(l, m, *args):
    return np.sqrt(2 * l + 1) * np.sqrt(
        np.math.factorial(l - m) / np.math.factorial(l + m)
    )


# =============================================
# calculation of glm is needed for tlm
# =============================================


def calculate_glm_hat(l, m):
    return np.sqrt((l + m) * (l - m))


# =============================================
# Associated Legendre functions Pn,m(x)
# =============================================


def assocLegendrePoly(
    l,
    m,
    theta,
):
    Plm = lpmv(m, l, np.cos(theta)) * ((-1) ** m)
    return Plm


# =============================================
# Derivative of Associated Legendre functions
# dPn,m(x) / dtheta
# =============================================


def derivativeThetaAssocLegendrePoly(l, m, theta):
    Plm = (l + 1) * (-np.cos(theta) / np.sin(theta)) * lpmv(m, l, np.cos(theta)) + (
        l - m + 1
    ) * (1 / np.sin(theta)) * lpmv(m, l + 1, np.cos(theta))
    return Plm * ((-1) ** m)



# =============================================
# Building matrix corresponding qlm = M clm
# =============================================


def build_clm_to_qlm_matrix(Lmax):

    Nu = Lmax * (Lmax + 2)
    clm_to_qlm_matrix = np.zeros((Nu, Nu))

    nu = 0
    for lu in range(1, Lmax + 1):
        for mu in range(0, lu + 1):
            temp_coef = -2 * mu / (lu * (lu + 1))
            if mu == 0:
                nu += 1
            else:
                clm_to_qlm_matrix[nu, nu + 1] = -1 / temp_coef
                clm_to_qlm_matrix[nu + 1, nu] = 1 / temp_coef
                nu += 2

    return clm_to_qlm_matrix


# =============================================
# Building matrix corresponding clm = M qlm
# =============================================


def build_qlm_to_clm_matrix(Lmax):

    Nu = Lmax * (Lmax + 2)
    qlm_to_clm_matrix = np.zeros((Nu, Nu))

    nu = 0
    for lu in range(1, Lmax + 1):
        for mu in range(0, lu + 1):
            temp_coef = -2 * mu / (lu * (lu + 1))
            if mu == 0:
                nu += 1
            else:
                qlm_to_clm_matrix[nu, nu + 1] = -temp_coef
                qlm_to_clm_matrix[nu + 1, nu] = temp_coef
                nu += 2

    return qlm_to_clm_matrix


# =============================================
# Building matrix corresponding tlm = T qlm
# =============================================


def build_clm_to_tlm_matrix(Lmax):

    Nu = Lmax * (Lmax + 2)
    clm_to_tlm_matrix = np.zeros((Nu, Nu))

    lumu_dict = {}

    nu = 0
    for lu in range(1, Lmax + 1):
        for mu in range(0, lu + 1):
            lumu_dict[(lu, mu, "cos")] = nu
            nu += 1
            if mu != 0:
                lumu_dict[(lu, mu, "sin")] = nu
                nu += 1

    nu = 0
    for lu in range(1, Lmax + 1):
        for mu in range(0, lu + 1):
            if lu <= 1:
                pass
            else:
                left_coef = 1 / (2 * lu + 3)
                rigth_coef = 1 / (2 * lu - 1)
                if lu < Lmax:

                    if mu == 0:
                        pass
                    else:
                        temp_coef = -1 / (2 * mu) * (lu - 1) * (lu + 2)
                        nu = lumu_dict[(lu, mu, "cos")]
                        nu_plus = lumu_dict[(lu + 1, mu, "sin")]
                        if (lu - 1, mu, "sin") in lumu_dict:
                            nu_minus = lumu_dict[(lu - 1, mu, "sin")]

                        glm_plus = calculate_glm_hat(lu + 1, mu)
                        glm_minus = calculate_glm_hat(lu, mu)

                        if (lu - 1, mu, "sin") not in lumu_dict:
                            clm_to_tlm_matrix[nu, nu_plus] = (
                                -glm_plus * left_coef * temp_coef
                            )
                        else:
                            clm_to_tlm_matrix[nu, nu_plus] = (
                                -glm_plus * left_coef * temp_coef
                            )
                            clm_to_tlm_matrix[nu, nu_minus] = (
                                -glm_minus * rigth_coef * temp_coef
                            )

                        nu = lumu_dict[(lu, mu, "sin")]
                        nu_plus = lumu_dict[(lu + 1, mu, "cos")]
                        if (lu - 1, mu, "cos") in lumu_dict:
                            nu_minus = lumu_dict[(lu - 1, mu, "cos")]

                        if (lu - 1, mu, "cos") not in lumu_dict:
                            clm_to_tlm_matrix[nu, nu_plus] = (
                                glm_plus * left_coef * temp_coef
                            )
                        else:
                            clm_to_tlm_matrix[nu, nu_plus] = (
                                glm_plus * left_coef * temp_coef
                            )
                            clm_to_tlm_matrix[nu, nu_minus] = (
                                glm_minus * rigth_coef * temp_coef
                            )

                else:
                    if mu == 0:
                        pass
                    else:
                        temp_coef = -1 / (2 * mu) * (lu - 1) * (lu + 2)

                        nu = lumu_dict[(lu, mu, "cos")]
                        if (lu - 1, mu, "sin") in lumu_dict:
                            nu_minus = lumu_dict[(lu - 1, mu, "sin")]

                        glm_minus = calculate_glm_hat(lu, mu)

                        if (lu - 1, mu, "sin") not in lumu_dict:
                            pass
                        else:
                            clm_to_tlm_matrix[nu, nu_minus] = (
                                -glm_minus * rigth_coef * temp_coef
                            )

                        nu = lumu_dict[(lu, mu, "sin")]
                        if mu <= lu - 1:
                            nu_minus = lumu_dict[(lu - 1, mu, "cos")]

                        if (lu - 1, mu, "cos") not in lumu_dict:
                            pass
                        else:
                            clm_to_tlm_matrix[nu, nu_minus] = (
                                glm_minus * rigth_coef * temp_coef
                            )

    return clm_to_tlm_matrix


# =============================================
# Building matrix corresponding tlm = T qlm
# for zonal part ql0 to tl0
# =============================================


def calculation_ql0_to_tl0_zonal_coefs(n):
    def calc_al(l):
        return (-1) * (2 * l + 1) * (2 * l + 2) / (4 * l + 3)

    def calc_bl(l):
        return (2 * l + 2) - (((2 * l + 2) ** 2) / (4 * l + 3))

    leftPartPlus = calc_al(n)
    if n > 0:
        leftPartMinus = calc_bl(n - 1)
    else:
        leftPartMinus = 0

    def calc_cl(l):
        return ((l * (l + 1) * (l - 1)) / ((2 * l + 1) * (2 * l - 1))) - (
            (3 * l * (l - 1)) / ((2 * l + 1) * (2 * l - 1))
        )

    def calc_dl(l):
        return (
            (((l + 1) ** 3) / ((2 * l + 1) * (2 * l + 3)))
            + (((l**2) * (l + 1)) / ((2 * l + 1) * (2 * l - 1)))
            - (((l + 1) ** 2) / (2 * l + 3))
            + 3
            - ((3 * ((l + 1) ** 2)) / ((2 * l + 1) * (2 * l + 3)))
            - ((3 * (l**2)) / ((2 * l + 1) * (2 * l - 1)))
        )

    def calc_el(l):
        return (
            ((((l + 1) ** 2) * (l + 2)) / ((2 * l + 1) * (2 * l + 3)))
            - (((l + 1) * (l + 2)) / (2 * l + 3))
            - ((3 * (l + 1) * (l + 2)) / ((2 * l + 1) * (2 * l + 3)))
        )

    rightPartMin2 = calc_cl(2 * n + 2)
    rightPartZero = calc_dl(2 * n)
    if n > 0:
        rightPartPlus2 = calc_el(2 * n - 2)
    else:
        rightPartPlus2 = 0

    return leftPartPlus, leftPartMinus, rightPartMin2, rightPartZero, rightPartPlus2


def build_qlm_to_tlm_zonal_matrix(Lmax_q):
    Nq = Lmax_q * (Lmax_q + 2) + 1
    Nu = (Lmax_q + 1) * (Lmax_q + 1 + 2)
    matrix = np.zeros((Nu, Nq))
    # gauss_theta, gauss_weights = gaussPoints(0, np.pi, 64)

    u_dict = {}
    nu = 0
    for lu in range(1, (Lmax_q + 1) + 1):
        for mu in range(0, lu + 1):
            u_dict[lu, mu, "cos"] = nu
            nu += 1
            if mu != 0:
                u_dict[lu, mu, "sin"] = nu
                nu += 1

    q_dict = {}
    nq = 0
    for lq in range(0, Lmax_q + 1):
        for mq in range(0, lq + 1):
            q_dict[lq, mq, "cos"] = nq
            nq += 1
            if mq != 0:
                q_dict[lq, mq, "sin"] = nq
                nq += 1

    tprev = 0
    for n in range(0, Lmax_q // 2 + 1):
        a, b, c, d, e = calculation_ql0_to_tl0_zonal_coefs(n)
        if n == 0:
            t2np = u_dict[2 * n + 1, 0, "cos"]
            q2np = q_dict[2 * n + 2, 0, "cos"]
            q2n = q_dict[2 * n, 0, "cos"]
            matrix[t2np, q2np] = c / a
            matrix[t2np, q2n] = d / a
            tprev = t2np
        else:
            t2np = u_dict[2 * n + 1, 0, "cos"]
            if 2 * n + 2 <= Lmax_q:
                q2np = q_dict[2 * n + 2, 0, "cos"]
            q2n = q_dict[2 * n, 0, "cos"]
            q2nm = q_dict[2 * n - 2, 0, "cos"]
            if 2 * n + 2 <= Lmax_q:
                matrix[t2np, q2np] = c / a
            matrix[t2np, q2n] = d / a
            matrix[t2np, q2nm] = e / a
            matrix[t2np, :] -= matrix[tprev, :] * b / a
            tprev = t2np

    return matrix



# =============================================
# calculation of full normalisation coefficient
# =============================================


def norm_coef_Sph(l, m):
    # return np.sqrt(2 * l + 1) * np.sqrt(np.math.factorial(l - m) / np.math.factorial(l + m))
    return (
        np.sqrt(2 * l + 1)
        * np.sqrt(np.math.factorial(l - m) / np.math.factorial(l + m))
        * ((-1) ** m)
    )


# =============================================
# calculation of fully normalised legendre poly
#    Plm hat as in notes
# =============================================


def Plm_hat(l, m, theta):
    return norm_coef_Sph(l, m) * assocLegendrePoly(l, m, theta)


# =============================================
# calculation of magnetic field data at thetas
#    and phis using generalised
#    spherical harmonics
# =============================================


def calculate_Br_from_ReIm(LBr, thetas, phis, Br_glm_r_i, **kwargs):
    if "normFunc" in kwargs:
        normFunc = kwargs["normFunc"]
    else:

        def normFunc(*args):
            return 1

    Br_data_matrix = np.zeros((len(thetas), len(phis)), dtype=np.complex128)
    Br_coef = []
    for lbr in range(1, LBr + 1):
        for mbr in range(-lbr, lbr + 1):
            Br_coef.append(normFunc(lbr, mbr))
            Br_coef.append(Br_coef[-1])

    Br_coef = np.array(Br_coef)

    Br_data_temp_theta = []
    for theta_num in thetas:
        Br_temp_theta = []
        nbr = 0
        for lbr in range(1, LBr + 1):
            for mbr in range(-lbr, lbr + 1):
                Br_temp_theta.append(Plm_hat(lbr, mbr, theta_num))
                nbr += 1
                Br_temp_theta.append(Br_temp_theta[-1])
                nbr += 1

        Br_data_temp_theta.append(np.array(Br_temp_theta))

    Br_data_temp_phi = []
    for phi_num in phis:
        Br_temp_phi = []
        nbr = 0
        for lbr in range(1, LBr + 1):
            for mbr in range(-lbr, lbr + 1):
                Br_temp_phi.append(np.exp(1j * mbr * phi_num))
                nbr += 1
                Br_temp_phi.append(1j * np.exp(1j * mbr * phi_num))
                nbr += 1

        Br_data_temp_phi.append(np.array(Br_temp_phi))

    for tn in range(len(thetas)):
        for pn in range(len(phis)):
            Br_data_matrix[tn, pn] = (
                Br_coef
                * Br_data_temp_theta[tn]
                * Br_data_temp_phi[pn]
                * Br_glm_r_i.reshape(
                    -1,
                )
            ).sum()

    return Br_data_matrix


def calculate_rdBr_dr_from_ReIm(LBr, thetas, phis, Br_glm_r_i, **kwargs):
    if "normFunc" in kwargs:
        normFunc = kwargs["normFunc"]
    else:

        def normFunc(*args):
            return 1

    Br_data_matrix = np.zeros((len(thetas), len(phis)), dtype=np.complex128)
    Br_coef = []
    for lbr in range(1, LBr + 1):
        for mbr in range(-lbr, lbr + 1):
            Br_coef.append(normFunc(lbr, mbr))
            Br_coef.append(Br_coef[-1])

    Br_coef = np.array(Br_coef)

    Br_data_temp_theta = []
    for theta_num in thetas:
        Br_temp_theta = []
        nbr = 0
        for lbr in range(1, LBr + 1):
            for mbr in range(-lbr, lbr + 1):
                Br_temp_theta.append(Plm_hat(lbr, mbr, theta_num))
                nbr += 1
                Br_temp_theta.append(Br_temp_theta[-1])
                nbr += 1

        Br_data_temp_theta.append(np.array(Br_temp_theta))

    Br_data_temp_phi = []
    for phi_num in phis:
        Br_temp_phi = []
        nbr = 0
        for lbr in range(1, LBr + 1):
            for mbr in range(-lbr, lbr + 1):
                Br_temp_phi.append(np.exp(1j * mbr * phi_num))
                nbr += 1
                Br_temp_phi.append(1j * np.exp(1j * mbr * phi_num))
                nbr += 1

        Br_data_temp_phi.append(np.array(Br_temp_phi))

    for tn in range(len(thetas)):
        for pn in range(len(phis)):
            Br_data_matrix[tn, pn] = (
                Br_coef
                * Br_data_temp_theta[tn]
                * Br_data_temp_phi[pn]
                * Br_glm_r_i.reshape(
                    -1,
                )
            ).sum()

    return Br_data_matrix


def calculate_b_plus_from_ReIm(LBr, thetas, phis, b_coeff, **kwargs):
    if "normFunc" in kwargs:
        normFunc = kwargs["normFunc"]
    else:

        def normFunc(*args):
            return 1

    Br_data_matrix = np.zeros((len(thetas), len(phis)), dtype=np.complex128)
    Br_coef = []
    for lbr in range(1, LBr + 1):
        for mbr in range(-lbr, lbr + 1):
            Br_coef.append(normFunc(lbr, mbr))
            Br_coef.append(Br_coef[-1])

    Br_coef = np.array(Br_coef)

    Br_data_temp_theta = []
    for theta_num in thetas:
        Br_temp_theta = []
        nbr = 0
        for lbr in range(1, LBr + 1):
            for mbr in range(-lbr, lbr + 1):
                Br_temp_theta.append(
                    generalised_Plm_plus(l=lbr, m=mbr, theta=theta_num)
                )
                nbr += 1
                Br_temp_theta.append(Br_temp_theta[-1])
                nbr += 1

        Br_data_temp_theta.append(np.array(Br_temp_theta))

    Br_data_temp_phi = []
    for phi_num in phis:
        Br_temp_phi = []
        nbr = 0
        for lbr in range(1, LBr + 1):
            for mbr in range(-lbr, lbr + 1):
                Br_temp_phi.append(np.exp(1j * mbr * phi_num))
                nbr += 1
                Br_temp_phi.append(1j * np.exp(1j * mbr * phi_num))
                nbr += 1

        Br_data_temp_phi.append(np.array(Br_temp_phi))

    for tn in range(len(thetas)):
        for pn in range(len(phis)):
            Br_data_matrix[tn, pn] = (
                Br_coef
                * Br_data_temp_theta[tn]
                * Br_data_temp_phi[pn]
                * b_coeff.reshape(
                    -1,
                )
            ).sum()

    return Br_data_matrix


def calculate_b_minus_from_ReIm(LBr, thetas, phis, b_coeff, **kwargs):
    if "normFunc" in kwargs:
        normFunc = kwargs["normFunc"]
    else:

        def normFunc(*args):
            return 1

    Br_data_matrix = np.zeros((len(thetas), len(phis)), dtype=np.complex128)
    Br_coef = []
    for lbr in range(1, LBr + 1):
        for mbr in range(-lbr, lbr + 1):
            Br_coef.append(normFunc(lbr, mbr))
            Br_coef.append(Br_coef[-1])

    Br_coef = np.array(Br_coef)

    Br_data_temp_theta = []
    for theta_num in thetas:
        Br_temp_theta = []
        nbr = 0
        for lbr in range(1, LBr + 1):
            for mbr in range(-lbr, lbr + 1):
                Br_temp_theta.append(
                    generalised_Plm_minus(l=lbr, m=mbr, theta=theta_num)
                )
                nbr += 1
                Br_temp_theta.append(Br_temp_theta[-1])
                nbr += 1

        Br_data_temp_theta.append(np.array(Br_temp_theta))

    Br_data_temp_phi = []
    for phi_num in phis:
        Br_temp_phi = []
        nbr = 0
        for lbr in range(1, LBr + 1):
            for mbr in range(-lbr, lbr + 1):
                Br_temp_phi.append(np.exp(1j * mbr * phi_num))
                nbr += 1
                Br_temp_phi.append(1j * np.exp(1j * mbr * phi_num))
                nbr += 1

        Br_data_temp_phi.append(np.array(Br_temp_phi))

    for tn in range(len(thetas)):
        for pn in range(len(phis)):
            Br_data_matrix[tn, pn] = (
                Br_coef
                * Br_data_temp_theta[tn]
                * Br_data_temp_phi[pn]
                * b_coeff.reshape(
                    -1,
                )
            ).sum()

    return Br_data_matrix


# =============================================
# calculation of additional information for
#    cos sin and Re Im representation:
#    - number of elements
#    - position of each l and m element
# =============================================


def build_real_imag_and_cossin_dict_pos(L, q=False):
    x_in_real_imag_form_dict = {}
    Nx_real_imag_form = 0
    start_l = 1
    if q:
        start_l = 0
    for lx in range(start_l, L + 1):
        for mx in range(-lx, lx + 1):
            x_in_real_imag_form_dict[Nx_real_imag_form] = (lx, mx, "r")
            Nx_real_imag_form += 1
            x_in_real_imag_form_dict[Nx_real_imag_form] = (lx, mx, "i")
            Nx_real_imag_form += 1

    x_cos_sin_dict = {}
    Nx_cossin_form = 0
    for lx in range(start_l, L + 1):
        for mx in range(0, lx + 1):
            x_cos_sin_dict[Nx_cossin_form] = (lx, mx, "cos")
            Nx_cossin_form += 1
            if mx != 0:
                x_cos_sin_dict[Nx_cossin_form] = (lx, mx, "sin")
                Nx_cossin_form += 1

    return x_in_real_imag_form_dict, Nx_real_imag_form, x_cos_sin_dict, Nx_cossin_form


# =============================================
# building transformation matrix to calculate
#    Re Im form from cos sin
# =============================================


def building_transformation_matrix_from_cossin_to_R_I_form_B(
    Nx_real_imag_form, Nx_cossin_form, x_in_real_imag_form_dict, x_cos_sin_dict
):
    x_Matrix_from_cossin_to_R_I_form = np.zeros((Nx_real_imag_form, Nx_cossin_form))

    for nx_r_i in range(Nx_real_imag_form):
        lx, mx, r_i = x_in_real_imag_form_dict[nx_r_i]
        nx_cs = 0

        if mx == 0:
            if r_i == "r":
                for pos, deg in x_cos_sin_dict.items():
                    if (lx, mx, "cos") == deg:
                        nx_cs = pos
                x_Matrix_from_cossin_to_R_I_form[nx_r_i, nx_cs] = (lx + 1) / np.sqrt(
                    2 * lx + 1
                )

        if mx > 0:
            if r_i == "r":
                for pos, deg in x_cos_sin_dict.items():
                    if (lx, mx, "cos") == deg:
                        nx_cs = pos
                x_Matrix_from_cossin_to_R_I_form[nx_r_i, nx_cs] = (
                    np.sqrt(2) * (lx + 1) / np.sqrt(2 * lx + 1) / 2
                )
            if r_i == "i":
                for pos, deg in x_cos_sin_dict.items():
                    if (lx, mx, "sin") == deg:
                        nx_cs = pos
                x_Matrix_from_cossin_to_R_I_form[nx_r_i, nx_cs] = (
                    -np.sqrt(2) * (lx + 1) / np.sqrt(2 * lx + 1) / 2
                )

        if mx < 0:
            if r_i == "r":
                for pos, deg in x_cos_sin_dict.items():
                    if (lx, -mx, "cos") == deg:
                        nx_cs = pos
                x_Matrix_from_cossin_to_R_I_form[nx_r_i, nx_cs] = (
                    np.sqrt(2) * (lx + 1) / np.sqrt(2 * lx + 1) * ((-1) ** mx) / 2
                )
            if r_i == "i":
                for pos, deg in x_cos_sin_dict.items():
                    if (lx, -mx, "sin") == deg:
                        nx_cs = pos
                x_Matrix_from_cossin_to_R_I_form[nx_r_i, nx_cs] = (
                    np.sqrt(2) * (lx + 1) / np.sqrt(2 * lx + 1) * ((-1) ** mx) / 2
                )

    return x_Matrix_from_cossin_to_R_I_form


def building_transformation_matrix_from_cossin_to_R_I_form(
    Nx_real_imag_form, Nx_cossin_form, x_in_real_imag_form_dict, x_cos_sin_dict
):
    x_Matrix_from_cossin_to_R_I_form = np.zeros((Nx_real_imag_form, Nx_cossin_form))

    for nx_r_i in range(Nx_real_imag_form):
        lx, mx, r_i = x_in_real_imag_form_dict[nx_r_i]
        nx_cs = 0

        if mx == 0:
            if r_i == "r":
                for pos, deg in x_cos_sin_dict.items():
                    if (lx, mx, "cos") == deg:
                        nx_cs = pos
                x_Matrix_from_cossin_to_R_I_form[nx_r_i, nx_cs] = 1 / np.sqrt(
                    2 * lx + 1
                )

        if mx > 0:
            if r_i == "r":
                for pos, deg in x_cos_sin_dict.items():
                    if (lx, mx, "cos") == deg:
                        nx_cs = pos
                x_Matrix_from_cossin_to_R_I_form[nx_r_i, nx_cs] = (
                    np.sqrt(2) / np.sqrt(2 * lx + 1) / 2
                )
            if r_i == "i":
                for pos, deg in x_cos_sin_dict.items():
                    if (lx, mx, "sin") == deg:
                        nx_cs = pos
                x_Matrix_from_cossin_to_R_I_form[nx_r_i, nx_cs] = (
                    -np.sqrt(2) / np.sqrt(2 * lx + 1) / 2
                )

        if mx < 0:
            if r_i == "r":
                for pos, deg in x_cos_sin_dict.items():
                    if (lx, -mx, "cos") == deg:
                        nx_cs = pos
                x_Matrix_from_cossin_to_R_I_form[nx_r_i, nx_cs] = (
                    np.sqrt(2) / np.sqrt(2 * lx + 1) * ((-1) ** mx) / 2
                )
            if r_i == "i":
                for pos, deg in x_cos_sin_dict.items():
                    if (lx, -mx, "sin") == deg:
                        nx_cs = pos
                x_Matrix_from_cossin_to_R_I_form[nx_r_i, nx_cs] = (
                    np.sqrt(2) / np.sqrt(2 * lx + 1) * ((-1) ** mx) / 2
                )

    return x_Matrix_from_cossin_to_R_I_form


# =============================================
# building transformation matrix to calculate
#    cos sin form from Re Im
# =============================================


def building_transformation_matrix_from_R_I_to_cossin_form_B(
    Nx_real_imag_form, Nx_cossin_form, x_in_real_imag_form_dict, x_cos_sin_dict
):
    x_Matrix_from_R_I_to_cossin_form = np.zeros((Nx_cossin_form, Nx_real_imag_form))

    for nx_r_i in range(Nx_real_imag_form):
        lx, mx, r_i = x_in_real_imag_form_dict[nx_r_i]
        nx_cs = 0

        if mx == 0:
            if r_i == "r":
                for pos, deg in x_cos_sin_dict.items():
                    if (lx, mx, "cos") == deg:
                        nx_cs = pos
                x_Matrix_from_R_I_to_cossin_form[nx_cs, nx_r_i] = 1 / (
                    (lx + 1) / np.sqrt(2 * lx + 1)
                )

        if mx > 0:
            if r_i == "r":
                for pos, deg in x_cos_sin_dict.items():
                    if (lx, mx, "cos") == deg:
                        nx_cs = pos
                x_Matrix_from_R_I_to_cossin_form[nx_cs, nx_r_i] = 1 / (
                    np.sqrt(2) * (lx + 1) / np.sqrt(2 * lx + 1)
                )
            if r_i == "i":
                for pos, deg in x_cos_sin_dict.items():
                    if (lx, mx, "sin") == deg:
                        nx_cs = pos
                x_Matrix_from_R_I_to_cossin_form[nx_cs, nx_r_i] = 1 / (
                    -np.sqrt(2) * (lx + 1) / np.sqrt(2 * lx + 1)
                )

        if mx < 0:
            if r_i == "r":
                for pos, deg in x_cos_sin_dict.items():
                    if (lx, -mx, "cos") == deg:
                        nx_cs = pos
                x_Matrix_from_R_I_to_cossin_form[nx_cs, nx_r_i] = 1 / (
                    np.sqrt(2) * (lx + 1) / np.sqrt(2 * lx + 1) * ((-1) ** mx)
                )
            if r_i == "i":
                for pos, deg in x_cos_sin_dict.items():
                    if (lx, -mx, "sin") == deg:
                        nx_cs = pos
                x_Matrix_from_R_I_to_cossin_form[nx_cs, nx_r_i] = 1 / (
                    np.sqrt(2) * (lx + 1) / np.sqrt(2 * lx + 1) * ((-1) ** mx)
                )

    return x_Matrix_from_R_I_to_cossin_form


def building_transformation_matrix_from_R_I_to_cossin_form(
    Nx_real_imag_form, Nx_cossin_form, x_in_real_imag_form_dict, x_cos_sin_dict
):
    x_Matrix_from_R_I_to_cossin_form = np.zeros((Nx_cossin_form, Nx_real_imag_form))

    for nx_r_i in range(Nx_real_imag_form):
        lx, mx, r_i = x_in_real_imag_form_dict[nx_r_i]
        nx_cs = 0

        if mx == 0:
            if r_i == "r":
                for pos, deg in x_cos_sin_dict.items():
                    if (lx, mx, "cos") == deg:
                        nx_cs = pos
                x_Matrix_from_R_I_to_cossin_form[nx_cs, nx_r_i] = 1 / (
                    1 / np.sqrt(2 * lx + 1)
                )

        if mx > 0:
            if r_i == "r":
                for pos, deg in x_cos_sin_dict.items():
                    if (lx, mx, "cos") == deg:
                        nx_cs = pos
                x_Matrix_from_R_I_to_cossin_form[nx_cs, nx_r_i] = 1 / (
                    np.sqrt(2) / np.sqrt(2 * lx + 1)
                )
            if r_i == "i":
                for pos, deg in x_cos_sin_dict.items():
                    if (lx, mx, "sin") == deg:
                        nx_cs = pos
                x_Matrix_from_R_I_to_cossin_form[nx_cs, nx_r_i] = 1 / (
                    -np.sqrt(2) / np.sqrt(2 * lx + 1)
                )

        if mx < 0:
            if r_i == "r":
                for pos, deg in x_cos_sin_dict.items():
                    if (lx, -mx, "cos") == deg:
                        nx_cs = pos
                x_Matrix_from_R_I_to_cossin_form[nx_cs, nx_r_i] = 1 / (
                    np.sqrt(2) / np.sqrt(2 * lx + 1) * ((-1) ** mx)
                )
            if r_i == "i":
                for pos, deg in x_cos_sin_dict.items():
                    if (lx, -mx, "sin") == deg:
                        nx_cs = pos
                x_Matrix_from_R_I_to_cossin_form[nx_cs, nx_r_i] = 1 / (
                    np.sqrt(2) / np.sqrt(2 * lx + 1) * ((-1) ** mx)
                )

    return x_Matrix_from_R_I_to_cossin_form


# =============================================
# Precalculate Polinimials
# =============================================


def precalculatePoly(L, thetas, PolyFunc, **kwargs):
    if "normFunc" in kwargs:
        normFunc = kwargs["normFunc"]
    else:

        def normFunc(*args):
            return 1

    res = {}
    for l in range(1, L + 1):
        for m in range(-l, l + 1):
            for tn in range(len(thetas)):
                res[(l, m, thetas[tn])] = PolyFunc(
                    l=l, m=m, theta=thetas[tn]
                ) * normFunc(l, m)

    return res


# =============================================
# Projecting on spherical harmonics for
#    Re Im form of coefficients
# =============================================


def projectionFFTCoreReIm(
    gauss_theta, gauss_weights, input_matrix, Lmax, tmax, **kwargs
):
    if "precalculatedPolyDict" in kwargs:
        precalculatedPolyDict = kwargs["precalculatedPolyDict"]
    else:
        if "PolyFunc" in kwargs:
            PolyFunc = kwargs["PolyFunc"]

            if "normFunc" in kwargs:
                normFunc = kwargs["normFunc"]
            else:

                def normFunc(*args):
                    return 1

            precalculatedPolyDict = precalculatePoly(
                Lmax, gauss_theta, PolyFunc, normFunc
            )
        else:
            raise Exception("No Polynomial")

    pmax = 2 * tmax

    lsvmsv_dict = {}

    nsv = 0
    for lsv in range(1, Lmax + 1):
        for msv in range(-lsv, lsv + 1):
            lsvmsv_dict[nsv] = (lsv, msv, "r")
            nsv += 1
            lsvmsv_dict[nsv] = (lsv, msv, "i")
            nsv += 1

    N = nsv

    return_matrix = np.zeros((N,))

    for tn in range(len(gauss_theta)):
        theta = gauss_theta[tn]
        weight = gauss_weights[tn]

        ftf = np.fft.fft(input_matrix[tn, :]) / input_matrix.shape[1]

        LM = 0
        for l in range(1, Lmax + 1):

            for m in range(-l, l + 1):
                return_matrix[LM] += (
                    precalculatedPolyDict[(l, m, gauss_theta[tn])]
                    * ftf[m].real
                    * weight
                )

                LM += 1

                return_matrix[LM] += (
                    precalculatedPolyDict[(l, m, gauss_theta[tn])]
                    * ftf[m].imag
                    * weight
                )

                LM += 1

    return_matrix = return_matrix / 2

    return return_matrix


def projectionFFTCoreToEarthReIm(
    gauss_theta, gauss_weights, input_matrix, Lmax, tmax, **kwargs
):
    if "precalculatedPolyDict" in kwargs:
        precalculatedPolyDict = kwargs["precalculatedPolyDict"]
    else:
        if "PolyFunc" in kwargs:
            PolyFunc = kwargs["PolyFunc"]

            if "normFunc" in kwargs:
                normFunc = kwargs["normFunc"]
            else:

                def normFunc(*args):
                    return 1

            precalculatedPolyDict = precalculatePoly(
                Lmax, gauss_theta, PolyFunc, normFunc
            )
        else:
            raise Exception("No Polynomial")

    pmax = 2 * tmax

    lsvmsv_dict = {}

    nsv = 0
    for lsv in range(1, Lmax + 1):
        for msv in range(-lsv, lsv + 1):
            lsvmsv_dict[nsv] = (lsv, msv, "r")
            nsv += 1
            lsvmsv_dict[nsv] = (lsv, msv, "i")
            nsv += 1

    N = nsv

    return_matrix = np.zeros((N,))

    for tn in range(len(gauss_theta)):
        theta = gauss_theta[tn]
        weight = gauss_weights[tn]

        ftf = np.fft.fft(input_matrix[tn, :]) / input_matrix.shape[1]

        LM = 0
        for l in range(1, Lmax + 1):

            for m in range(-l, l + 1):
                temp = precalculatedPolyDict[(l, m, theta)] * weight

                return_matrix[LM] += temp * ftf[m].real

                LM += 1

                return_matrix[LM] += temp * ftf[m].imag

                LM += 1

    n = 0
    for l in range(1, Lmax + 1):
        for m in range(-l, l + 1):
            return_matrix[n] = return_matrix[n] / ((Ra / Rc) ** (l + 2))
            n += 1
            return_matrix[n] = return_matrix[n] / ((Ra / Rc) ** (l + 2))
            n += 1

    return_matrix = return_matrix / 2

    return return_matrix


# =============================================
# generalised spherical harmonics plus
# =============================================


def generalised_Plm_plus(theta, l, m):
    ##############################################
    # Generalised l=0 m=0 not exists at the moment
    ##############################################
    if l != 0:
        norm = -np.sqrt(l * (l + 1))
        hat_norm = norm_coef_Sph(l, m)
        temp = (derivativeThetaAssocLegendrePoly(l, m, theta) * hat_norm) + (
            (m / np.sin(theta)) * Plm_hat(l, m, theta)
        )
        return temp / norm
    else:
        return 0


# =============================================
# generalised spherical harmonics minus
# =============================================


def generalised_Plm_minus(theta, l, m):
    ##############################################
    # Generalised l=0 m=0 not exists at the moment
    ##############################################
    if l != 0:
        norm = np.sqrt(l * (l + 1))
        hat_norm = norm_coef_Sph(l, m)
        temp = (derivativeThetaAssocLegendrePoly(l, m, theta) * hat_norm) - (
            (m / np.sin(theta)) * Plm_hat(l, m, theta)
        )
        return temp / norm
    else:
        return 0


# =============================================
# matrix for multiplication Re Im form by 1i
# =============================================


def building_transformation_matrix_multiply_by_i(
    Nx_real_imag_form, x_in_real_imag_form_dict
):
    matrix_multiply_by_i = np.zeros((Nx_real_imag_form, Nx_real_imag_form))

    for nx_r_i in range(Nx_real_imag_form):
        lx, mx, r_i = x_in_real_imag_form_dict[nx_r_i]

        if r_i == "r":
            matrix_multiply_by_i[nx_r_i + 1, nx_r_i] = 1
        if r_i == "i":
            matrix_multiply_by_i[nx_r_i - 1, nx_r_i] = -1

    return matrix_multiply_by_i


# =============================================
# matrix for calculating u from Re Im form
# =============================================


def calculate_u_for_Re_Im(Lu, thetas, phis, u_r_i, Plm_func):
    u_Re_Im = np.zeros((len(thetas), len(phis)), dtype=np.complex128)
    u_data_temp_theta = []
    nt = 0
    for theta_num in thetas:
        u_temp_theta = []
        nu = 0
        for lu in range(1, Lu + 1):
            for mu in range(-lu, lu + 1):
                # u_temp_theta.append(Plm_func(l=lu, m=mu, theta=theta_num) * gauss_weights[nt])
                u_temp_theta.append(Plm_func(l=lu, m=mu, theta=theta_num))
                u_temp_theta.append(u_temp_theta[-1])
                nu += 1
        nt += 1

        u_data_temp_theta.append(np.array(u_temp_theta))

    u_data_temp_phi = []
    for phi_num in phis:
        u_temp_phi = []
        nbr = 0
        for lu in range(1, Lu + 1):
            for mu in range(-lu, lu + 1):
                u_temp_phi.append(np.exp(1j * mu * phi_num))
                nbr += 1
                u_temp_phi.append(1j * np.exp(1j * mu * phi_num))
                nbr += 1

        u_data_temp_phi.append(np.array(u_temp_phi))

    for tn in range(len(thetas)):
        for pn in range(len(phis)):
            u_Re_Im[tn, pn] = (
                u_data_temp_theta[tn]
                * u_data_temp_phi[pn]
                * u_r_i.reshape(
                    -1,
                )
            ).sum()

    return u_Re_Im


#########################################
# Calculating ulm_plus and ulm_minus
#     forward matrices
#########################################


def calculating_ulm_plus_minus_from_qlm(
    Lu,
    Nu_real_imag_form,
    QtC_matrix_real_imag_form,
    QtT_matrix_real_imag_form,
    Matrix_multiply_by_i,
):
    #########################################
    # Building norm matrix
    #########################################

    func_l = []
    for lu in range(1, Lu + 1):
        for mu in range(-lu, lu + 1):
            func_l.append(np.sqrt(lu * (lu + 1)) / np.sqrt(2))
            func_l.append(np.sqrt(lu * (lu + 1)) / np.sqrt(2))
    func_l = np.array(func_l)

    #########################################
    # Building forward matrices that
    # calculate  ulm+ and ulm- from qlm
    #########################################

    ulm_plus_matrix = (
        QtC_matrix_real_imag_form - Matrix_multiply_by_i.dot(QtT_matrix_real_imag_form)
    ) * func_l.reshape(Nu_real_imag_form, 1)
    ulm_minus_matrix = (
        QtC_matrix_real_imag_form + Matrix_multiply_by_i.dot(QtT_matrix_real_imag_form)
    ) * func_l.reshape(Nu_real_imag_form, 1)

    return ulm_plus_matrix, ulm_minus_matrix


#########################################
# calculating u+ and u-
#########################################


def calculate_u_data_matrices(
    Nu_real_imag_form, u_in_real_imag_form_dict, thetas, phis, debug=False, **kwargs
):
    if "u_plus_from_qlm_matrix" in kwargs:
        u_plus_from_qlm_matrix = kwargs["u_plus_from_qlm_matrix"]
        u_minus_from_qlm_matrix = kwargs["u_minus_from_qlm_matrix"]
        Nu = u_minus_from_qlm_matrix.shape[1]
    else:
        u_plus_from_qlm_matrix = np.eye(Nu_real_imag_form)
        u_minus_from_qlm_matrix = np.eye(Nu_real_imag_form)
        Nu = Nu_real_imag_form

    u_plus_data_matrices = []
    u_minus_data_matrices = []

    u_plus_forward_matrix = np.zeros(
        (len(thetas) * len(phis), Nu_real_imag_form), dtype=np.complex128
    )
    u_minus_forward_matrix = np.zeros(
        (len(thetas) * len(phis), Nu_real_imag_form), dtype=np.complex128
    )
    ntp = 0

    if debug:
        if "u_plus_forward_matrix" in kwargs:
            forward_type = kwargs["forward_type"]
            if forward_type == "complex":
                u_plus_forward_matrix_complex = kwargs["u_plus_forward_matrix"]
                u_minus_forward_matrix_complex = kwargs["u_minus_forward_matrix"]

                for i in range(u_plus_forward_matrix_complex.shape[1]):
                    u_plus_forward_matrix[:, 2 * i] = u_plus_forward_matrix_complex[
                        :, i
                    ]
                    u_plus_forward_matrix[:, 2 * i + 1] = (
                        1j * u_plus_forward_matrix_complex[:, i]
                    )

                    u_minus_forward_matrix[:, 2 * i] = u_minus_forward_matrix_complex[
                        :, i
                    ]
                    u_minus_forward_matrix[:, 2 * i + 1] = (
                        1j * u_minus_forward_matrix_complex[:, i]
                    )

            else:
                u_plus_forward_matrix = kwargs["u_plus_forward_matrix"]
                u_minus_forward_matrix = kwargs["u_minus_forward_matrix"]
        else:
            for tn in range(len(thetas)):
                for pn in range(len(phis)):
                    for nu in range(int(Nu_real_imag_form / 2)):
                        lu, mu, r_i_u = u_in_real_imag_form_dict[2 * nu]
                        u_plus_forward_matrix[ntp, 2 * nu] = generalised_Plm_plus(
                            l=lu, m=mu, theta=thetas[tn]
                        ) * np.exp(1j * mu * phis[pn])
                        u_minus_forward_matrix[ntp, 2 * nu] = generalised_Plm_minus(
                            l=lu, m=mu, theta=thetas[tn]
                        ) * np.exp(1j * mu * phis[pn])
                        u_plus_forward_matrix[ntp, 2 * nu + 1] = (
                            1j * u_plus_forward_matrix[ntp, 2 * nu]
                        )
                        u_minus_forward_matrix[ntp, 2 * nu + 1] = (
                            1j * u_minus_forward_matrix[ntp, 2 * nu]
                        )
                    ntp += 1

        for nu in range(Nu):
            vector = np.zeros((Nu,))
            vector[nu] = 1
            u_plus_data_matrices.append(
                u_plus_forward_matrix.dot(u_plus_from_qlm_matrix.dot(vector)).reshape(
                    (len(thetas), len(phis))
                )
            )
            u_minus_data_matrices.append(
                u_minus_forward_matrix.dot(u_minus_from_qlm_matrix.dot(vector)).reshape(
                    (len(thetas), len(phis))
                )
            )
    else:
        if "u_plus_forward_matrix" in kwargs:
            forward_type = kwargs["forward_type"]
            if forward_type == "complex":
                u_plus_forward_matrix_complex = kwargs["u_plus_forward_matrix"]
                u_minus_forward_matrix_complex = kwargs["u_minus_forward_matrix"]

                for i in range(u_plus_forward_matrix_complex.shape[1]):
                    u_plus_forward_matrix[:, 2 * i] = u_plus_forward_matrix_complex[
                        :, i
                    ]
                    u_plus_forward_matrix[:, 2 * i + 1] = (
                        1j * u_plus_forward_matrix_complex[:, i]
                    )

                    u_minus_forward_matrix[:, 2 * i] = u_minus_forward_matrix_complex[
                        :, i
                    ]
                    u_minus_forward_matrix[:, 2 * i + 1] = (
                        1j * u_minus_forward_matrix_complex[:, i]
                    )

            else:
                u_plus_forward_matrix = kwargs["u_plus_forward_matrix"]
                u_minus_forward_matrix = kwargs["u_minus_forward_matrix"]
        else:
            for tn in range(len(thetas)):
                for pn in range(len(phis)):
                    for nu in range(int(Nu_real_imag_form / 2)):
                        lu, mu, r_i_u = u_in_real_imag_form_dict[2 * nu]
                        u_plus_forward_matrix[ntp, 2 * nu] = generalised_Plm_plus(
                            l=lu, m=mu, theta=thetas[tn]
                        ) * np.exp(1j * mu * phis[pn])
                        u_minus_forward_matrix[ntp, 2 * nu] = generalised_Plm_minus(
                            l=lu, m=mu, theta=thetas[tn]
                        ) * np.exp(1j * mu * phis[pn])
                        u_plus_forward_matrix[ntp, 2 * nu + 1] = (
                            1j * u_plus_forward_matrix[ntp, 2 * nu]
                        )
                        u_minus_forward_matrix[ntp, 2 * nu + 1] = (
                            1j * u_minus_forward_matrix[ntp, 2 * nu]
                        )
                    ntp += 1

        for nu in range(Nu):
            vector = np.zeros((Nu,))
            vector[nu] = 1
            u_plus_data_matrices.append(
                u_plus_forward_matrix.dot(u_plus_from_qlm_matrix.dot(vector)).reshape(
                    (len(thetas), len(phis))
                )
            )
            u_minus_data_matrices.append(
                u_minus_forward_matrix.dot(u_minus_from_qlm_matrix.dot(vector)).reshape(
                    (len(thetas), len(phis))
                )
            )

    return u_plus_data_matrices, u_minus_data_matrices


def calculate_u_zero_data_matrices(
    Nu_real_imag_form, u_in_real_imag_form_dict, thetas, phis, debug=False
):
    u_zero_data_matrices = []

    if debug:
        for nu in range(Nu_real_imag_form):
            u_zero_data_matrix = np.zeros((len(thetas), len(phis)), dtype=np.complex128)

            lu, mu, r_i_u = u_in_real_imag_form_dict[nu]

            norm = lu * (lu + 1)

            u_zero_temp_theta = []
            for theta_num in thetas:
                u_zero_temp_theta.append(Plm_hat(l=lu, m=mu, theta=theta_num))

            u_zero_temp_phi = []
            for phi_num in phis:
                if r_i_u == "r":
                    u_zero_temp_phi.append(np.exp(1j * mu * phi_num))
                else:
                    u_zero_temp_phi.append(1j * np.exp(1j * mu * phi_num))

            for tn in range(len(thetas)):
                for pn in range(len(phis)):
                    u_zero_data_matrix[tn, pn] = (
                        u_zero_temp_theta[tn] * u_zero_temp_phi[pn] * norm
                    ).sum()

            u_zero_data_matrices.append(u_zero_data_matrix)
    else:
        for nu in range(Nu_real_imag_form):
            u_zero_data_matrix = np.zeros((len(thetas), len(phis)), dtype=np.complex128)

            lu, mu, r_i_u = u_in_real_imag_form_dict[nu]

            norm = lu * (lu + 1)

            u_zero_temp_theta = []
            for theta_num in thetas:
                u_zero_temp_theta.append(Plm_hat(l=lu, m=mu, theta=theta_num))

            u_zero_temp_phi = []
            for phi_num in phis:
                if r_i_u == "r":
                    u_zero_temp_phi.append(np.exp(1j * mu * phi_num))
                else:
                    u_zero_temp_phi.append(1j * np.exp(1j * mu * phi_num))

            for tn in range(len(thetas)):
                for pn in range(len(phis)):
                    u_zero_data_matrix[tn, pn] = (
                        u_zero_temp_theta[tn] * u_zero_temp_phi[pn] * norm
                    ).sum()

            u_zero_data_matrices.append(u_zero_data_matrix)

    return u_zero_data_matrices


#########################################
# calculating v+ and v-
#    from u+ and u- via projecting
#    using fft
#########################################


def calculate_v_plus_minus(
    LSV,
    Nu_real_imag_form,
    Br_data_matrix,
    u_plus_data_matrices,
    u_minus_data_matrices,
    gauss_thetas,
    gauss_weights,
    tmax,
    debug=False,
):
    (
        SV_in_real_imag_form_dict,
        Nsv_real_imag_form,
        SV_cos_sin_dict,
        Nsv_cossin_form,
    ) = build_real_imag_and_cossin_dict_pos(LSV)

    SV_Matrix_from_cossin_to_R_I_form = (
        building_transformation_matrix_from_cossin_to_R_I_form_B(
            Nsv_real_imag_form,
            Nsv_cossin_form,
            SV_in_real_imag_form_dict,
            SV_cos_sin_dict,
        )
    )
    SV_Matrix_from_R_I_to_cossin_form = (
        building_transformation_matrix_from_R_I_to_cossin_form_B(
            Nsv_real_imag_form,
            Nsv_cossin_form,
            SV_in_real_imag_form_dict,
            SV_cos_sin_dict,
        )
    )

    v_plus_matrices = np.zeros((Nsv_real_imag_form, Nu_real_imag_form))
    v_minus_matrices = np.zeros((Nsv_real_imag_form, Nu_real_imag_form))

    def one_norm(*args):
        return 1

    precalculatedPolyDictMinus = precalculatePoly(
        LSV, gauss_thetas, generalised_Plm_minus
    )
    precalculatedPolyDictPlus = precalculatePoly(
        LSV, gauss_thetas, generalised_Plm_plus
    )

    if debug:
        for nu in range(Nu_real_imag_form):
            temp_plus = 1j * Br_data_matrix * u_plus_data_matrices[nu]
            temp_minus = -1j * Br_data_matrix * u_minus_data_matrices[nu]

            projected_plus = projectionFFTCoreToEarthReIm(
                gauss_thetas,
                gauss_weights,
                temp_plus,
                LSV,
                tmax,
                precalculatedPolyDict=precalculatedPolyDictPlus,
            )
            projected_minus = projectionFFTCoreToEarthReIm(
                gauss_thetas,
                gauss_weights,
                temp_minus,
                LSV,
                tmax,
                precalculatedPolyDict=precalculatedPolyDictMinus,
            )

            v_plus_matrices[:, nu] = projected_plus
            v_minus_matrices[:, nu] = projected_minus
    else:
        for nu in range(Nu_real_imag_form):
            temp_plus = 1j * Br_data_matrix * u_plus_data_matrices[nu]
            temp_minus = -1j * Br_data_matrix * u_minus_data_matrices[nu]

            projected_plus = projectionFFTCoreToEarthReIm(
                gauss_thetas,
                gauss_weights,
                temp_plus,
                LSV,
                tmax,
                precalculatedPolyDict=precalculatedPolyDictPlus,
            )
            projected_minus = projectionFFTCoreToEarthReIm(
                gauss_thetas,
                gauss_weights,
                temp_minus,
                LSV,
                tmax,
                precalculatedPolyDict=precalculatedPolyDictMinus,
            )

            v_plus_matrices[:, nu] = projected_plus
            v_minus_matrices[:, nu] = projected_minus

    return (
        v_plus_matrices,
        v_minus_matrices,
    )


#########################################
# calculating dBr/dt from v+ and v-
#########################################


def calculating_SV_from_v_matrix(
    LSV,
    Nsv_real_imag_form,
    SV_in_real_imag_form_dict,
    v_plus_matrices_from_qlm,
    v_minus_matrices_from_qlm,
):
    norm_SV_Re_Im = np.ones((Nsv_real_imag_form, 1))

    r_norm = 1
    nsv = 0
    for lsv in range(1, LSV + 1):
        temp = np.sqrt(lsv * (lsv + 1)) / np.sqrt(2) / r_norm
        for m in range(-lsv, lsv + 1):
            norm_SV_Re_Im[nsv] = temp
            norm_SV_Re_Im[nsv + 1] = temp
            nsv += 2

    dBr_dt_matrix = v_plus_matrices_from_qlm - v_minus_matrices_from_qlm

    dBr_dt_matrix = dBr_dt_matrix * norm_SV_Re_Im * (-1)

    Matrix_multiply_by_i_SV = building_transformation_matrix_multiply_by_i(
        Nsv_real_imag_form, SV_in_real_imag_form_dict
    )

    dBr_dt_matrix_from_qlm = Matrix_multiply_by_i_SV.dot(dBr_dt_matrix)

    return dBr_dt_matrix_from_qlm


def calculating_SV_from_v_matrix_ulm(
    LSV,
    Nsv_real_imag_form,
    SV_in_real_imag_form_dict,
    v_plus_matrices_from_ulm,
    v_minus_matrices_from_ulm,
):
    norm_SV_Re_Im = np.ones((Nsv_real_imag_form, 1))

    r_norm = 1
    nsv = 0
    for lsv in range(1, LSV + 1):
        temp = np.sqrt(lsv * (lsv + 1)) / np.sqrt(2) / r_norm
        for m in range(-lsv, lsv + 1):
            norm_SV_Re_Im[nsv] = temp
            norm_SV_Re_Im[nsv + 1] = temp
            nsv += 2

    Nq = v_plus_matrices_from_ulm.shape[1]
    dBr_dt_matrix = np.zeros((Nsv_real_imag_form, Nq * 2))
    dBr_dt_matrix[:, :Nq] = -v_minus_matrices_from_ulm
    dBr_dt_matrix[:, Nq:] = v_plus_matrices_from_ulm

    dBr_dt_matrix = dBr_dt_matrix * norm_SV_Re_Im * (-1)

    Matrix_multiply_by_i_SV = building_transformation_matrix_multiply_by_i(
        Nsv_real_imag_form, SV_in_real_imag_form_dict
    )

    dBr_dt_matrix_from_ulm = Matrix_multiply_by_i_SV.dot(dBr_dt_matrix)

    return dBr_dt_matrix_from_ulm


##########################################################################################################
##########################################################################################################


def calculate_t_from_u(
    LSV,
    LBr,
    Nu_real_imag_form,
    gauss_thetas,
    gauss_weights,
    phis,
    Br_glm_re_im,
    u_plus_data_matrices,
    u_minus_data_matrices,
    tmax=64,
    debug=True,
):
    rdBr_glm_r_i = np.array(Br_glm_re_im)

    n = 0
    for l_ in range(1, LBr + 1):
        for m_ in range(-l_, l_ + 1):
            rdBr_glm_r_i[n, 0] = rdBr_glm_r_i[n, 0] * (-1) * (l_ + 1)
            n += 1
            rdBr_glm_r_i[n, 0] = rdBr_glm_r_i[n, 0] * (-1) * (l_ + 1)
            n += 1

    rdBr_dt_data_matrix = calculate_rdBr_dr_from_ReIm(
        LBr, gauss_thetas, phis, rdBr_glm_r_i
    )

    (
        SV_in_real_imag_form_dict,
        Nsv_real_imag_form,
        SV_cos_sin_dict,
        Nsv_cossin_form,
    ) = build_real_imag_and_cossin_dict_pos(LSV)

    t_plus_matrix = np.zeros((Nsv_real_imag_form, Nu_real_imag_form))
    t_minus_matrix = np.zeros((Nsv_real_imag_form, Nu_real_imag_form))

    def one_norm(*args):
        return 1

    precalculatedPolyDictMinus = precalculatePoly(
        LSV, gauss_thetas, generalised_Plm_minus
    )
    precalculatedPolyDictPlus = precalculatePoly(
        LSV, gauss_thetas, generalised_Plm_plus
    )

    if debug:
        for nu in range(Nu_real_imag_form):
            temp_plus = 1j * rdBr_dt_data_matrix * u_plus_data_matrices[nu]
            temp_minus = -1j * rdBr_dt_data_matrix * u_minus_data_matrices[nu]


            projected_plus = projectionFFTCoreReIm(
                gauss_thetas,
                gauss_weights,
                temp_plus,
                LSV,
                tmax,
                precalculatedPolyDict=precalculatedPolyDictPlus,
            )
            projected_minus = projectionFFTCoreReIm(
                gauss_thetas,
                gauss_weights,
                temp_minus,
                LSV,
                tmax,
                precalculatedPolyDict=precalculatedPolyDictMinus,
            )

            t_plus_matrix[:, nu] = projected_plus
            t_minus_matrix[:, nu] = projected_minus
    else:
        for nu in range(Nu_real_imag_form):
            temp_plus = 1j * rdBr_dt_data_matrix * u_plus_data_matrices[nu]
            temp_minus = -1j * rdBr_dt_data_matrix * u_minus_data_matrices[nu]

            projected_plus = projectionFFTCoreReIm(
                gauss_thetas,
                gauss_weights,
                temp_plus,
                LSV,
                tmax,
                precalculatedPolyDict=precalculatedPolyDictPlus,
            )
            projected_minus = projectionFFTCoreReIm(
                gauss_thetas,
                gauss_weights,
                temp_minus,
                LSV,
                tmax,
                precalculatedPolyDict=precalculatedPolyDictMinus,
            )

            t_plus_matrix[:, nu] = projected_plus
            t_minus_matrix[:, nu] = projected_minus

    return t_plus_matrix, t_minus_matrix


def calculate_t_from_qlm(
    LSV,
    LBr,
    Nu_real_imag_form,
    gauss_thetas,
    gauss_weights,
    phis,
    Br_glm_re_im,
    u_plus_data_matrices,
    u_minus_data_matrices,
    ulm_plus_matrix_from_qlm,
    ulm_minus_matrix_from_qlm,
    tmax=64,
    debug=True,
):
    t_plus_matrix, t_minus_matrix = calculate_t_from_u(
        LSV,
        LBr,
        Nu_real_imag_form,
        gauss_thetas,
        gauss_weights,
        phis,
        Br_glm_re_im,
        u_plus_data_matrices,
        u_minus_data_matrices,
        tmax,
        debug,
    )

    return t_plus_matrix.dot(ulm_plus_matrix_from_qlm), t_minus_matrix.dot(
        ulm_minus_matrix_from_qlm
    )


def calculate_v0_from_u_minus_plus(
    LSV,
    LBr,
    Nsv_real_imag_form,
    gauss_thetas,
    gauss_weights,
    phis,
    blm_re_im,
    u_plus_data_matrices,
    u_minus_data_matrices,
    tmax=64,
    debug=True,
):
    nb = 0
    for lb in range(1, LBr + 1):
        nrm = -np.sqrt(lb / (2 * (lb + 1)))
        for mb in range(-lb, lb + 1):
            blm_re_im[nb] = blm_re_im[nb] * nrm
            blm_re_im[nb + 1] = blm_re_im[nb + 1] * nrm
            nb += 2

    b_plus_matrix = calculate_b_plus_from_ReIm(LBr, gauss_thetas, phis, blm_re_im)
    b_minus_matrix = calculate_b_minus_from_ReIm(LBr, gauss_thetas, phis, blm_re_im)

    v_zero_forward_matrix = np.zeros(
        (Nsv_real_imag_form, len(u_plus_data_matrices) * 2)
    )

    def one_norm(*args):
        return 1

    precalculatedPolyDictHat = precalculatePoly(LSV, gauss_thetas, Plm_hat)

    if debug:
        for nu in range(len(u_plus_data_matrices)):
            temp_1 = 1j * b_minus_matrix * u_plus_data_matrices[nu]
            temp_2 = -1j * b_plus_matrix * u_minus_data_matrices[nu]

            projected_1 = projectionFFTCoreReIm(
                gauss_thetas,
                gauss_weights,
                temp_1,
                LSV,
                tmax,
                precalculatedPolyDict=precalculatedPolyDictHat,
            )
            projected_2 = projectionFFTCoreReIm(
                gauss_thetas,
                gauss_weights,
                temp_2,
                LSV,
                tmax,
                precalculatedPolyDict=precalculatedPolyDictHat,
            )

            v_zero_forward_matrix[:, nu] = projected_2
            v_zero_forward_matrix[:, nu + len(u_plus_data_matrices)] = projected_1
    else:
        for nu in range(len(u_plus_data_matrices)):
            temp_1 = 1j * b_minus_matrix * u_plus_data_matrices[nu]
            temp_2 = -1j * b_plus_matrix * u_minus_data_matrices[nu]

            projected_1 = projectionFFTCoreReIm(
                gauss_thetas,
                gauss_weights,
                temp_1,
                LSV,
                tmax,
                precalculatedPolyDict=precalculatedPolyDictHat,
            )
            projected_2 = projectionFFTCoreReIm(
                gauss_thetas,
                gauss_weights,
                temp_2,
                LSV,
                tmax,
                precalculatedPolyDict=precalculatedPolyDictHat,
            )

            v_zero_forward_matrix[:, nu] = projected_2
            v_zero_forward_matrix[:, nu + len(u_plus_data_matrices)] = projected_1

    return v_zero_forward_matrix


def calculate_v0_from_qlm(
    LSV,
    LBr,
    Nsv_real_imag_form,
    gauss_thetas,
    gauss_weights,
    phis,
    blm_re_im,
    u_plus_data_matrices,
    u_minus_data_matrices,
    u_minus_plus_from_qlm_matrix,
    tmax=64,
    debug=True,
):
    v_zero_forward_matrix = calculate_v0_from_u_minus_plus(
        LSV,
        LBr,
        Nsv_real_imag_form,
        gauss_thetas,
        gauss_weights,
        phis,
        blm_re_im,
        u_plus_data_matrices,
        u_minus_data_matrices,
        tmax,
        debug,
    )
    return v_zero_forward_matrix.dot(u_minus_plus_from_qlm_matrix)


def calculate_p_from_clm(
    LSV,
    LBr,
    Nsv_real_imag_form,
    Nu_real_imag_form,
    Br_glm_re_im,
    gauss_thetas,
    gauss_weights,
    phis,
    u_zero_data_matrices,
    tmax=64,
    debug=True,
):
    blm_re_im = np.array(Br_glm_re_im)

    nb = 0
    for lb in range(1, LBr + 1):
        nrm = -np.sqrt(lb / (2 * (lb + 1)))
        for mb in range(-lb, lb + 1):
            blm_re_im[nb] = blm_re_im[nb] * nrm
            blm_re_im[nb + 1] = blm_re_im[nb + 1] * nrm
            nb += 2

    b_plus_matrix = calculate_b_plus_from_ReIm(LBr, gauss_thetas, phis, blm_re_im)
    b_minus_matrix = calculate_b_minus_from_ReIm(LBr, gauss_thetas, phis, blm_re_im)

    p_plus_matrix = np.zeros((Nsv_real_imag_form, Nu_real_imag_form))
    p_minus_matrix = np.zeros((Nsv_real_imag_form, Nu_real_imag_form))

    temp_pluses = []
    temp_minuses = []

    precalculatedPolyDictMinus = precalculatePoly(
        LSV, gauss_thetas, generalised_Plm_minus
    )
    precalculatedPolyDictPlus = precalculatePoly(
        LSV, gauss_thetas, generalised_Plm_plus
    )

    if debug:
        for nu in range(Nu_real_imag_form):
            temp_plus = -1j * (b_plus_matrix) * u_zero_data_matrices[nu]
            temp_minus = 1j * (b_minus_matrix) * u_zero_data_matrices[nu]

            temp_pluses.append(temp_plus)
            temp_minuses.append(temp_minus)

            projected_plus = projectionFFTCoreReIm(
                gauss_thetas,
                gauss_weights,
                temp_plus,
                LSV,
                tmax,
                precalculatedPolyDict=precalculatedPolyDictPlus,
            )
            projected_minus = projectionFFTCoreReIm(
                gauss_thetas,
                gauss_weights,
                temp_minus,
                LSV,
                tmax,
                precalculatedPolyDict=precalculatedPolyDictMinus,
            )

            p_plus_matrix[:, nu] = projected_plus
            p_minus_matrix[:, nu] = projected_minus
    else:
        for nu in range(Nu_real_imag_form):
            temp_plus = -1j * (b_plus_matrix) * u_zero_data_matrices[nu]
            temp_minus = 1j * (b_minus_matrix) * u_zero_data_matrices[nu]

            temp_pluses.append(temp_plus)
            temp_minuses.append(temp_minus)

            projected_plus = projectionFFTCoreReIm(
                gauss_thetas,
                gauss_weights,
                temp_plus,
                LSV,
                tmax,
                precalculatedPolyDict=precalculatedPolyDictPlus,
            )
            projected_minus = projectionFFTCoreReIm(
                gauss_thetas,
                gauss_weights,
                temp_minus,
                LSV,
                tmax,
                precalculatedPolyDict=precalculatedPolyDictMinus,
            )

            p_plus_matrix[:, nu] = projected_plus
            p_minus_matrix[:, nu] = projected_minus

    return p_plus_matrix, p_minus_matrix


def calculate_p_from_qlm(
    LSV,
    LBr,
    Nsv_real_imag_form,
    Nu_real_imag_form,
    Br_glm_re_im,
    gauss_thetas,
    gauss_weights,
    phis,
    u_zero_data_matrices,
    QtC_matrix_real_imag_form,
    tmax=64,
    debug=True,
):
    p_plus_matrix, p_minus_matrix = calculate_p_from_clm(
        LSV,
        LBr,
        Nsv_real_imag_form,
        Nu_real_imag_form,
        Br_glm_re_im,
        gauss_thetas,
        gauss_weights,
        phis,
        u_zero_data_matrices,
        tmax,
        debug,
    )

    return p_plus_matrix.dot(QtC_matrix_real_imag_form), p_minus_matrix.dot(
        QtC_matrix_real_imag_form
    )


def calculate_u_plus_minus_forward_temp_for_s(Lq, gauss_thetas, phis, debug=False):
    (
        Q_real_imag_dict,
        Nq_real_imag_form,
        Q_cos_sin_dict,
        Nq_cossin_form,
    ) = build_real_imag_and_cossin_dict_pos(Lq, q=True)

    u_plus_forward_matrix = np.zeros(
        (len(gauss_thetas) * len(phis), Nq_real_imag_form), dtype=np.complex128
    )
    u_minus_forward_matrix = np.zeros(
        (len(gauss_thetas) * len(phis), Nq_real_imag_form), dtype=np.complex128
    )
    ntp = 0

    if debug:
        for tn in range(len(gauss_thetas)):
            for pn in range(len(phis)):
                for nq in range(int(Nq_real_imag_form / 2)):
                    lq, mq, r_i_q = Q_real_imag_dict[2 * nq]
                    u_plus_forward_matrix[ntp, 2 * nq] = generalised_Plm_plus(
                        l=lq, m=mq, theta=gauss_thetas[tn]
                    ) * np.exp(1j * mq * phis[pn])
                    u_minus_forward_matrix[ntp, 2 * nq] = generalised_Plm_minus(
                        l=lq, m=mq, theta=gauss_thetas[tn]
                    ) * np.exp(1j * mq * phis[pn])
                    u_plus_forward_matrix[ntp, 2 * nq + 1] = (
                        1j * u_plus_forward_matrix[ntp, 2 * nq]
                    )
                    u_minus_forward_matrix[ntp, 2 * nq + 1] = (
                        1j * u_minus_forward_matrix[ntp, 2 * nq]
                    )
                ntp += 1
    else:
        for tn in range(len(gauss_thetas)):
            for pn in range(len(phis)):
                for nq in range(int(Nq_real_imag_form / 2)):
                    lq, mq, r_i_q = Q_real_imag_dict[2 * nq]
                    u_plus_forward_matrix[ntp, 2 * nq] = generalised_Plm_plus(
                        l=lq, m=mq, theta=gauss_thetas[tn]
                    ) * np.exp(1j * mq * phis[pn])
                    u_minus_forward_matrix[ntp, 2 * nq] = generalised_Plm_minus(
                        l=lq, m=mq, theta=gauss_thetas[tn]
                    ) * np.exp(1j * mq * phis[pn])
                    u_plus_forward_matrix[ntp, 2 * nq + 1] = (
                        1j * u_plus_forward_matrix[ntp, 2 * nq]
                    )
                    u_minus_forward_matrix[ntp, 2 * nq + 1] = (
                        1j * u_minus_forward_matrix[ntp, 2 * nq]
                    )
                ntp += 1

    return u_plus_forward_matrix, u_minus_forward_matrix


def calculate_s_from_delta(
    LSV,
    LBr,
    Lq,
    Br_glm_re_im,
    gauss_thetas,
    gauss_weights,
    phis,
    u_plus_forward_matrix,
    u_minus_forward_matrix,
    tmax=64,
    debug=False,
):
    Br_data_matrix = calculate_Br_from_ReIm(LBr, gauss_thetas, phis, Br_glm_re_im)

    (
        SV_in_real_imag_form_dict,
        Nsv_real_imag_form,
        SV_cos_sin_dict,
        Nsv_cossin_form,
    ) = build_real_imag_and_cossin_dict_pos(LSV)

    (
        Q_real_imag_dict,
        Nq_real_imag_form,
        Q_cos_sin_dict,
        Nq_cossin_form,
    ) = build_real_imag_and_cossin_dict_pos(Lq, q=True)

    s_plus_from_delta_plus_matrix = np.zeros((Nsv_real_imag_form, Nq_real_imag_form))
    s_minus_from_delta_minus_matrix = np.zeros((Nsv_real_imag_form, Nq_real_imag_form))

    def one_norm(*args):
        return 1

    precalculatedPolyDictMinus = precalculatePoly(
        LSV, gauss_thetas, generalised_Plm_minus
    )
    precalculatedPolyDictPlus = precalculatePoly(
        LSV, gauss_thetas, generalised_Plm_plus
    )

    if debug:
        for nq in range(Nq_real_imag_form):
            array = np.zeros((Nq_real_imag_form,))
            array[nq] = 1

            temp_plus = (
                1j
                * Br_data_matrix
                * u_plus_forward_matrix.dot(array).reshape(tmax, tmax * 2)
            )
            temp_minus = (
                -1j
                * Br_data_matrix
                * u_minus_forward_matrix.dot(array).reshape(tmax, tmax * 2)
            )

            projected_plus = projectionFFTCoreReIm(
                gauss_thetas,
                gauss_weights,
                temp_plus,
                LSV,
                tmax,
                precalculatedPolyDict=precalculatedPolyDictPlus,
            )
            projected_minus = projectionFFTCoreReIm(
                gauss_thetas,
                gauss_weights,
                temp_minus,
                LSV,
                tmax,
                precalculatedPolyDict=precalculatedPolyDictMinus,
            )

            s_plus_from_delta_plus_matrix[:, nq] = projected_plus
            s_minus_from_delta_minus_matrix[:, nq] = projected_minus
    else:
        for nq in range(Nq_real_imag_form):
            array = np.zeros((Nq_real_imag_form,))
            array[nq] = 1

            temp_plus = (
                1j
                * Br_data_matrix
                * u_plus_forward_matrix.dot(array).reshape(tmax, tmax * 2)
            )
            temp_minus = (
                -1j
                * Br_data_matrix
                * u_minus_forward_matrix.dot(array).reshape(tmax, tmax * 2)
            )

            projected_plus = projectionFFTCoreReIm(
                gauss_thetas,
                gauss_weights,
                temp_plus,
                LSV,
                tmax,
                precalculatedPolyDict=precalculatedPolyDictPlus,
            )
            projected_minus = projectionFFTCoreReIm(
                gauss_thetas,
                gauss_weights,
                temp_minus,
                LSV,
                tmax,
                precalculatedPolyDict=precalculatedPolyDictMinus,
            )

            s_plus_from_delta_plus_matrix[:, nq] = projected_plus
            s_minus_from_delta_minus_matrix[:, nq] = projected_minus

    return s_plus_from_delta_plus_matrix, s_minus_from_delta_minus_matrix


def calculate_gamma(l, m):
    return np.sqrt(((l - m) * (l + m)) / ((2 * l + 1) * (2 * l - 1)))


def calculate_psi1_to_psi2(L):
    saved_data = {}
    saved_indeces = []

    for m in range(0, L + 1):
        psi_coefs_pos = {(2 * n + m, m): n for n in range(0, int((L - m) / 2) + 1)}
        y_coefs_pos = {(2 * n + m + 1, m): n for n in range(0, int((L - m) / 2) + 1)}
        temp_matrix_sin = np.zeros((int((L - m) / 2) + 1, int((L - m) / 2) + 1))
        temp_matrix_cos = np.zeros((int((L - m) / 2) + 1, int((L - m) / 2) + 1))
        saved_indeces = saved_indeces + list(psi_coefs_pos.keys())
        for n in range(0, int((L - m) / 2) + 1):
            l_2 = 2 * n + m + 1
            l = l_2 - 1
            gamma = calculate_gamma(l_2, m)
            temp_sin = gamma * (l_2 - 1) * 4
            temp_cos = gamma
            ind_psi = psi_coefs_pos[(l, m)]
            ind_y = y_coefs_pos[(l_2, m)]
            temp_matrix_sin[ind_y, ind_psi] = temp_sin
            temp_matrix_cos[ind_y, ind_psi] = temp_cos

            if l_2 + 1 <= L:
                l = l_2 + 1
                gamma = calculate_gamma(l_2 + 1, m)
                temp_sin = gamma * (l_2 + 2) * (-1) * 4
                # temp_sin = gamma * (l_2 + 2) * (-1)
                temp_cos = gamma
                ind_psi = psi_coefs_pos[(l, m)]
                ind_y = y_coefs_pos[(l_2, m)]
                temp_matrix_sin[ind_y, ind_psi] = temp_sin
                temp_matrix_cos[ind_y, ind_psi] = temp_cos

        temp_matrix = np.linalg.inv(temp_matrix_cos).dot(temp_matrix_sin)

        saved_data[m] = {
            "psi_pos": psi_coefs_pos,
            "y_pos": y_coefs_pos,
            "matrix": temp_matrix,
        }

    return {"ind_pairs": saved_indeces, "m_fixed_data": saved_data}


def calculate_psi1_to_psi2_matrix(Lq, data):
    (
        Q_real_imag_dict,
        Nq_real_imag_form,
        Q_cos_sin_dict,
        Nq_cossin_form,
    ) = build_real_imag_and_cossin_dict_pos(Lq, q=True)

    matrix = np.zeros((Nq_real_imag_form, Nq_real_imag_form))

    ind_pairs = data["ind_pairs"]
    data_matrices = data["m_fixed_data"]

    for nq_r_i in range(Nq_real_imag_form):
        lq, mq, r_i = Q_real_imag_dict[nq_r_i]
        if (lq, mq) in ind_pairs:
            ind_psi1 = data_matrices[mq]["psi_pos"][(lq, mq)]
            for psi_2_pair in data_matrices[mq]["psi_pos"].keys():
                ind_psi2 = data_matrices[mq]["psi_pos"][psi_2_pair]
                r_i_2 = r_i
                forw_matrix = data_matrices[mq]["matrix"]

                npsi2_r_i_pos = list(Q_real_imag_dict.keys())[
                    list(Q_real_imag_dict.values()).index(
                        (psi_2_pair[0], psi_2_pair[1], r_i_2)
                    )
                ]
                matrix[npsi2_r_i_pos, nq_r_i] = forw_matrix[ind_psi2, ind_psi1]

                npsi2_r_i_neg = list(Q_real_imag_dict.keys())[
                    list(Q_real_imag_dict.values()).index(
                        (psi_2_pair[0], -psi_2_pair[1], r_i_2)
                    )
                ]
                ind_psi1_neg = list(Q_real_imag_dict.keys())[
                    list(Q_real_imag_dict.values()).index((lq, -mq, r_i))
                ]
                matrix[npsi2_r_i_neg, ind_psi1_neg] = forw_matrix[ind_psi2, ind_psi1]

    return matrix


def calculate_s_tilda_from_qlm_tilda(Lq, rc=1):
    (
        Q_in_real_imag_form_dict,
        Nq_real_imag_form,
        Q_cos_sin_dict,
        Nq_cossin_form,
    ) = build_real_imag_and_cossin_dict_pos(Lq, q=True)

    Matrix_multiply_by_i_Q = building_transformation_matrix_multiply_by_i(
        Nq_real_imag_form, Q_in_real_imag_form_dict
    )

    matrix_plus = np.zeros((Nq_real_imag_form, Nq_real_imag_form))
    matrix_minus = np.zeros((Nq_real_imag_form, Nq_real_imag_form))

    for ls in range(Lq + 1):
        for ms in range(ls + 1):
            for r_i in ["r", "i"]:
                r_i_2 = r_i
                coef = 0
                ###################################
                # l = 0 is under the question
                ###################################
                if ls > 0:
                    coef = np.sqrt(1 / (2 * ls * (ls + 1) * (rc**2)))

                # q_{l-1}^m
                if ls - 1 >= ms:
                    gamma = calculate_gamma(ls, ms)
                    ns_r_i = list(Q_in_real_imag_form_dict.keys())[
                        list(Q_in_real_imag_form_dict.values()).index((ls, ms, r_i))
                    ]
                    nq_r_i = list(Q_in_real_imag_form_dict.keys())[
                        list(Q_in_real_imag_form_dict.values()).index(
                            (ls - 1, ms, r_i_2)
                        )
                    ]
                    temp = (ls + 1) * gamma
                    matrix_plus[ns_r_i, nq_r_i] = temp * coef
                    matrix_minus[ns_r_i, nq_r_i] = -temp * coef

                    if ms != 0:
                        ns_r_i = list(Q_in_real_imag_form_dict.keys())[
                            list(Q_in_real_imag_form_dict.values()).index(
                                (ls, -ms, r_i)
                            )
                        ]
                        nq_r_i = list(Q_in_real_imag_form_dict.keys())[
                            list(Q_in_real_imag_form_dict.values()).index(
                                (ls - 1, -ms, r_i_2)
                            )
                        ]
                        matrix_plus[ns_r_i, nq_r_i] = temp * coef
                        matrix_minus[ns_r_i, nq_r_i] = -temp * coef

                # q_{l}^m
                ns_r_i = list(Q_in_real_imag_form_dict.keys())[
                    list(Q_in_real_imag_form_dict.values()).index((ls, ms, r_i))
                ]
                nq_r_i = list(Q_in_real_imag_form_dict.keys())[
                    list(Q_in_real_imag_form_dict.values()).index((ls, ms, r_i_2))
                ]
                temp = -ms
                matrix_plus[ns_r_i, nq_r_i] = temp * coef
                matrix_minus[ns_r_i, nq_r_i] = temp * coef

                if ms != 0:
                    ns_r_i = list(Q_in_real_imag_form_dict.keys())[
                        list(Q_in_real_imag_form_dict.values()).index((ls, -ms, r_i))
                    ]
                    nq_r_i = list(Q_in_real_imag_form_dict.keys())[
                        list(Q_in_real_imag_form_dict.values()).index((ls, -ms, r_i_2))
                    ]
                    matrix_plus[ns_r_i, nq_r_i] = -temp * coef
                    matrix_minus[ns_r_i, nq_r_i] = -temp * coef
                    # matrix_plus[ns_r_i, nq_r_i] = temp * coef
                    # matrix_minus[ns_r_i, nq_r_i] = temp * coef

                # q_{l+1}^m
                if ls + 1 <= Lq:
                    gamma = calculate_gamma(ls + 1, ms)
                    r_i_2 = r_i
                    ns_r_i = list(Q_in_real_imag_form_dict.keys())[
                        list(Q_in_real_imag_form_dict.values()).index((ls, ms, r_i))
                    ]
                    nq_r_i = list(Q_in_real_imag_form_dict.keys())[
                        list(Q_in_real_imag_form_dict.values()).index(
                            (ls + 1, ms, r_i_2)
                        )
                    ]
                    temp = ls * gamma
                    matrix_plus[ns_r_i, nq_r_i] = -temp * coef
                    matrix_minus[ns_r_i, nq_r_i] = temp * coef

                    if ms != 0:
                        ns_r_i = list(Q_in_real_imag_form_dict.keys())[
                            list(Q_in_real_imag_form_dict.values()).index(
                                (ls, -ms, r_i)
                            )
                        ]
                        nq_r_i = list(Q_in_real_imag_form_dict.keys())[
                            list(Q_in_real_imag_form_dict.values()).index(
                                (ls + 1, -ms, r_i_2)
                            )
                        ]
                        matrix_plus[ns_r_i, nq_r_i] = -temp * coef
                        matrix_minus[ns_r_i, nq_r_i] = temp * coef

    matrix_plus = Matrix_multiply_by_i_Q.dot(matrix_plus)
    matrix_minus = Matrix_multiply_by_i_Q.dot(matrix_minus)

    return matrix_plus, matrix_minus


def calculate_s_hat_from_qlm(Lq, rc=1):
    (
        Q_in_real_imag_form_dict,
        Nq_real_imag_form,
        Q_cos_sin_dict,
        Nq_cossin_form,
    ) = build_real_imag_and_cossin_dict_pos(Lq, q=True)

    Matrix_multiply_by_i = building_transformation_matrix_multiply_by_i(
        Nq_real_imag_form, Q_in_real_imag_form_dict
    )

    matrix_plus = np.zeros((Nq_real_imag_form, Nq_real_imag_form))
    matrix_minus = np.zeros((Nq_real_imag_form, Nq_real_imag_form))

    for m in range(0, Lq + 1):
        for n in range(0, int((Lq - m) / 2) + 1):
            l_2 = 2 * n + m + 1
            if l_2 <= Lq:
                # print(l_2, m)
                coef = np.sqrt((2 * n + m + 1) * (2 * n + m + 2)) / (
                    np.sqrt(2) * (rc**2)
                )
                for r_i_2 in ["r", "i"]:
                    n2_r_i_pos = list(Q_in_real_imag_form_dict.keys())[
                        list(Q_in_real_imag_form_dict.values()).index((l_2, m, r_i_2))
                    ]
                    n2_r_i_neg = list(Q_in_real_imag_form_dict.keys())[
                        list(Q_in_real_imag_form_dict.values()).index((l_2, -m, r_i_2))
                    ]
                    r_i_q = r_i_2
                    l = 2 * n + m
                    nq_r_i_pos = list(Q_in_real_imag_form_dict.keys())[
                        list(Q_in_real_imag_form_dict.values()).index((l, m, r_i_q))
                    ]
                    nq_r_i_neg = list(Q_in_real_imag_form_dict.keys())[
                        list(Q_in_real_imag_form_dict.values()).index((l, -m, r_i_q))
                    ]
                    temp = coef * (2 * n + m) * calculate_gamma(2 * n + m + 1, m)
                    matrix_plus[n2_r_i_pos, nq_r_i_pos] = temp
                    matrix_minus[n2_r_i_pos, nq_r_i_pos] = -temp

                    # temp = coef * (2 * n + (-m)) * calculate_gamma(2 * n + (-m) + 1, (-m))
                    matrix_plus[n2_r_i_neg, nq_r_i_neg] = temp
                    matrix_minus[n2_r_i_neg, nq_r_i_neg] = -temp

                    if (2 * (n + 1) + m) <= Lq:
                        l = 2 * (n + 1) + m
                        nq_r_i_pos = list(Q_in_real_imag_form_dict.keys())[
                            list(Q_in_real_imag_form_dict.values()).index((l, m, r_i_q))
                        ]
                        nq_r_i_neg = list(Q_in_real_imag_form_dict.keys())[
                            list(Q_in_real_imag_form_dict.values()).index(
                                (l, -m, r_i_q)
                            )
                        ]
                        temp = (
                            -coef * (2 * n + m + 3) * calculate_gamma(2 * n + m + 2, m)
                        )
                        matrix_plus[n2_r_i_pos, nq_r_i_pos] = temp
                        matrix_minus[n2_r_i_pos, nq_r_i_pos] = -temp

                        matrix_plus[n2_r_i_neg, nq_r_i_neg] = temp
                        matrix_minus[n2_r_i_neg, nq_r_i_neg] = -temp

    matrix_plus = Matrix_multiply_by_i.dot(matrix_plus)
    matrix_minus = Matrix_multiply_by_i.dot(matrix_minus)

    return matrix_plus, matrix_minus


def calculate_u_forward(Lu, thetas, phis, Plm_func):
    (
        u_in_real_imag_form_dict,
        Nu_real_imag_form,
        u_cos_sin_dict,
        Nu_cossin_form,
    ) = build_real_imag_and_cossin_dict_pos(Lu)

    u_forward_matrix = np.zeros(
        (len(thetas) * len(phis), Nu_real_imag_form), dtype=np.complex128
    )
    ntp = 0
    for tn in range(len(thetas)):
        for pn in range(len(phis)):
            for nu in range(int(Nu_real_imag_form / 2)):
                lu, mu, r_i_u = u_in_real_imag_form_dict[2 * nu]
                u_forward_matrix[ntp, 2 * nu] = Plm_func(
                    l=lu, m=mu, theta=thetas[tn]
                ) * np.exp(1j * mu * phis[pn])
                u_forward_matrix[ntp, 2 * nu + 1] = 1j * u_forward_matrix[ntp, 2 * nu]
            ntp += 1

    return u_forward_matrix


def from_re_im_to_complex(x_in_re_im):
    nu_re_im = x_in_re_im.reshape(
        -1,
    ).shape[0]
    x_in_re_im = x_in_re_im.reshape(-1, 1)
    x_in_complex = np.zeros((int(nu_re_im / 2), 1), dtype=np.complex128)
    for i in range(int(nu_re_im / 2)):
        x_in_complex[i, 0] = x_in_re_im[2 * i, 0] + 1j * x_in_re_im[2 * i + 1, 0]
    return x_in_complex


def from_complex_to_re_im(x_in_complex):
    nu_complex = x_in_complex.reshape(
        -1,
    ).shape[0]
    x_in_complex = x_in_complex.reshape(-1, 1)
    x_in_re_im = np.zeros((int(nu_complex * 2), 1))
    for i in range(int(nu_complex)):
        x_in_re_im[2 * i, 0] = x_in_complex[i, 0].real
        x_in_re_im[2 * i + 1, 0] = x_in_complex[i, 0].imag
    return x_in_re_im


def calculate_u_forward_complex_old(Lu, thetas, phis, Plm_func, debug=False):
    (
        u_in_complex_form_dict,
        Nu_complex_form,
        u_cos_sin_dict,
        Nu_cossin_form,
    ) = build_complex_and_cossin_dict_pos(Lu)

    u_forward_matrix = np.zeros(
        (len(thetas) * len(phis), Nu_complex_form), dtype=np.complex128
    )
    ntp = 0
    if debug == False:
        for tn in range(len(thetas)):
            for pn in range(len(phis)):
                for nu in range(Nu_complex_form):
                    lu, mu = u_in_complex_form_dict[nu]
                    u_forward_matrix[ntp, nu] = Plm_func(
                        l=lu, m=mu, theta=thetas[tn]
                    ) * np.exp(1j * mu * phis[pn])
                ntp += 1
    else:
        for tn in range(len(thetas)):
            for pn in range(len(phis)):
                for nu in range(Nu_complex_form):
                    lu, mu = u_in_complex_form_dict[nu]
                    u_forward_matrix[ntp, nu] = Plm_func(
                        l=lu, m=mu, theta=thetas[tn]
                    ) * np.exp(1j * mu * phis[pn])
                ntp += 1

    return u_forward_matrix


def calculate_u_forward_complex(Lu, thetas, phis, Plm_func, debug=False):
    (
        u_in_complex_form_dict,
        Nu_complex_form,
        u_cos_sin_dict,
        Nu_cossin_form,
    ) = build_complex_and_cossin_dict_pos(Lu)

    u_forward_matrix = np.zeros(
        (len(thetas) * len(phis), Nu_complex_form), dtype=np.complex128
    )
    ntp = 0
    if debug == False:
        for nu in range(Nu_complex_form):
            ntp = 0
            for tn in range(len(thetas)):
                lu, mu = u_in_complex_form_dict[nu]
                Plm_temp = Plm_func(
                        l=lu, m=mu, theta=thetas[tn]
                    ) 
                for pn in range(len(phis)):
                    u_forward_matrix[ntp, nu] = Plm_temp * np.exp(1j * mu * phis[pn])
                    ntp += 1
    else:
        for nu in range(Nu_complex_form):
            ntp = 0
            for tn in range(len(thetas)):
                lu, mu = u_in_complex_form_dict[nu]
                Plm_temp = Plm_func(
                        l=lu, m=mu, theta=thetas[tn]
                    ) 
                for pn in range(len(phis)):
                    u_forward_matrix[ntp, nu] = Plm_temp * np.exp(1j * mu * phis[pn])
                    ntp += 1

    return u_forward_matrix


def build_complex_and_cossin_dict_pos(L):
    x_in_comlex_form_dict = {}
    Nx_comlex_form = 0
    for lx in range(1, L + 1):
        for mx in range(-lx, lx + 1):
            x_in_comlex_form_dict[Nx_comlex_form] = (lx, mx)
            Nx_comlex_form += 1

    x_cos_sin_dict = {}
    Nx_cossin_form = 0
    for lx in range(1, L + 1):
        for mx in range(0, lx + 1):
            x_cos_sin_dict[Nx_cossin_form] = (lx, mx, "cos")
            Nx_cossin_form += 1
            if mx != 0:
                x_cos_sin_dict[Nx_cossin_form] = (lx, mx, "sin")
                Nx_cossin_form += 1

    return x_in_comlex_form_dict, Nx_comlex_form, x_cos_sin_dict, Nx_cossin_form


# =============================================
# Building matrix corresponding clm = M qlm
#  Re Im form
# =============================================


def build_qlm_to_clm_matrix_Re_Im(Lmax):
    Nu = Lmax * (Lmax + 2) * 2
    qlm_to_clm_matrix = np.zeros((Nu, Nu))

    nu = 0
    for lu in range(1, Lmax + 1):
        for mu in range(-lu, lu + 1):
            temp_coef = -2 * mu / (lu * (lu + 1))
            if mu == 0:
                nu += 2
            else:
                qlm_to_clm_matrix[nu, nu + 1] = -temp_coef
                qlm_to_clm_matrix[nu + 1, nu] = temp_coef
                nu += 2

    return qlm_to_clm_matrix


# =============================================
# Building matrix corresponding tlm = T qlm
#   Re Im
# =============================================


def build_qlm_to_tlm_matrix_Re_Im(Lmax):
    def calculate_gamma_lm(l, m):
        return np.sqrt((l - m) * (l + m) / (2 * l + 1) / (2 * l - 1))

    Nq = Lmax * (Lmax + 2) * 2
    Lu = Lmax + 1
    Nu = Lu * (Lu + 2) * 2
    qlm_to_tlm_matrix = np.zeros((Nu, Nq))

    lumu_dict = {}

    nu = 0
    for lu in range(1, (Lmax + 1) + 1):
        for mu in range(-lu, lu + 1):
            lumu_dict[(lu, mu, "r")] = nu
            nu += 1
            lumu_dict[(lu, mu, "i")] = nu
            nu += 1

    nu = 0
    for lu in range(1, Lu + 1):
        for mu in range(-lu, lu + 1):
            if lu <= 1:
                pass
            else:
                left_coef = lu * (lu - 1)
                rigth_coef = (lu + 1) * (lu + 2)
                if lu < Lmax:
                    if mu == 0:
                        pass
                    else:
                        temp_coef = -1 / (lu * (lu + 1))
                        nu = lumu_dict[(lu, mu, "r")]
                        nu_plus = lumu_dict[(lu + 1, mu, "r")]
                        if (lu - 1, mu, "r") in lumu_dict:
                            nu_minus = lumu_dict[(lu - 1, mu, "r")]

                        gamma_l_plus = calculate_gamma_lm(lu + 1, mu)
                        gamma_l = calculate_gamma_lm(lu, mu)

                        if (lu - 1, mu, "r") not in lumu_dict:
                            qlm_to_tlm_matrix[nu, nu_plus] = (
                                gamma_l_plus * left_coef * temp_coef
                            )
                        else:
                            qlm_to_tlm_matrix[nu, nu_plus] = (
                                gamma_l_plus * left_coef * temp_coef
                            )
                            qlm_to_tlm_matrix[nu, nu_minus] = (
                                gamma_l * rigth_coef * temp_coef
                            )

                        nu = lumu_dict[(lu, mu, "i")]
                        nu_plus = lumu_dict[(lu + 1, mu, "i")]
                        if (lu - 1, mu, "i") in lumu_dict:
                            nu_minus = lumu_dict[(lu - 1, mu, "i")]

                        if (lu - 1, mu, "i") not in lumu_dict:
                            qlm_to_tlm_matrix[nu, nu_plus] = (
                                gamma_l_plus * left_coef * temp_coef
                            )
                        else:
                            qlm_to_tlm_matrix[nu, nu_plus] = (
                                gamma_l_plus * left_coef * temp_coef
                            )
                            qlm_to_tlm_matrix[nu, nu_minus] = (
                                gamma_l * rigth_coef * temp_coef
                            )

                else:
                    if mu == 0:
                        pass
                    else:
                        temp_coef = -1 / (lu * (lu + 1))

                        nu = lumu_dict[(lu, mu, "r")]
                        if (lu - 1, mu, "r") in lumu_dict:
                            nu_minus = lumu_dict[(lu - 1, mu, "r")]

                        gamma_l = calculate_gamma_lm(lu, mu)

                        if (lu - 1, mu, "r") not in lumu_dict:
                            pass
                        else:
                            qlm_to_tlm_matrix[nu, nu_minus] = (
                                gamma_l * rigth_coef * temp_coef
                            )

                        nu = lumu_dict[(lu, mu, "i")]
                        if (lu - 1, mu, "i") in lumu_dict:
                            nu_minus = lumu_dict[(lu - 1, mu, "i")]

                        if (lu - 1, mu, "i") not in lumu_dict:
                            pass
                        else:
                            qlm_to_tlm_matrix[nu, nu_minus] = (
                                gamma_l * rigth_coef * temp_coef
                            )

    return qlm_to_tlm_matrix


# =============================================
# Building matrix corresponding tlm = T qlm
# for zonal part ql0 to tl0 Re Im form
# =============================================


def calculation_ql0_to_tl0_zonal_coefs(n):
    def calc_al(l):
        return (
            (-1) * (2 * l + 1) * (2 * l + 2) / np.sqrt(4 * l + 3) / np.sqrt(4 * l + 1)
        )

    def calc_bl(l):
        return (2 * l + 1) * (2 * l + 2) / np.sqrt(4 * l + 3) / np.sqrt(4 * l + 5)

    leftPartPlus = calc_al(n)
    if n > 0:
        leftPartMinus = calc_bl(n - 1)
    else:
        leftPartMinus = 0

    def calc_cl(l):
        return (1 / np.sqrt(2 * l + 1)) * ((l * (l + 1)) / (2 * l - 1)) * (
            (l - 1) / np.sqrt(2 * l - 3)
        ) - (3 / np.sqrt(2 * l + 1)) * (l / (2 * l - 1)) * (
            (l - 1) / np.sqrt(2 * l - 3)
        )

    def calc_dl(l):
        return (
            (-1) * (1 / (2 * l + 1)) * (l * (l + 1) * (l + 1) / (2 * l + 3))
            + (l / (2 * l + 1)) * (l * (l + 1) / (2 * l - 1))
            + 3
            - (3 * (l + 1) * (l + 1) / (2 * l + 1) / (2 * l + 3))
            - (3 * l * l / (2 * l + 1) / (2 * l - 1))
        )

    def calc_el(l):
        return (-1) * (1 / np.sqrt(2 * l + 1)) * (l * (l + 1) / (2 * l + 1)) * (
            (l + 2) / np.sqrt(2 * l + 5)
        ) - (3 / np.sqrt(2 * l + 1)) * ((l + 1) / (2 * l + 3)) * (
            (l + 2) / np.sqrt(2 * l + 5)
        )

    rightPartMin2 = calc_cl(2 * n + 2)
    rightPartZero = calc_dl(2 * n)
    if n > 0:
        rightPartPlus2 = calc_el(2 * n - 2)
    else:
        rightPartPlus2 = 0

    return leftPartPlus, leftPartMinus, rightPartMin2, rightPartZero, rightPartPlus2


def build_qlm_to_tlm_zonal_matrix_Re_Im(Lmax_q):
    Lu_max = Lmax_q + 1
    Nq = Lmax_q * (Lmax_q + 2) * 2 + 2
    Nu = Lu_max * (Lu_max + 2) * 2
    matrix = np.zeros((Nu, Nq))
    # gauss_theta, gauss_weights = gaussPoints(0, np.pi, 64)

    u_dict = {}
    nu = 0
    for lu in range(1, Lu_max + 1):
        for mu in range(-lu, lu + 1):
            u_dict[lu, mu, "r"] = nu
            nu += 1
            u_dict[lu, mu, "i"] = nu
            nu += 1

    q_dict = {}
    nq = 0
    for lq in range(0, Lmax_q + 1):
        for mq in range(-lq, lq + 1):
            q_dict[lq, mq, "r"] = nq
            nq += 1
            q_dict[lq, mq, "i"] = nq
            nq += 1

    tprev = 0
    for n in range(0, Lmax_q // 2 + 1):
        a, b, c, d, e = calculation_ql0_to_tl0_zonal_coefs(n)
        if n == 0:
            t2np = u_dict[2 * n + 1, 0, "r"]
            q2np = q_dict[2 * n + 2, 0, "r"]
            q2n = q_dict[2 * n, 0, "r"]
            matrix[t2np, q2np] = c / a
            matrix[t2np, q2n] = d / a
            tprev = t2np
        else:
            a0, _, _, _, _ = calculation_ql0_to_tl0_zonal_coefs(n - 1)
            t2np = u_dict[2 * n + 1, 0, "r"]
            if 2 * n + 2 <= Lmax_q:
                q2np = q_dict[2 * n + 2, 0, "r"]
            q2n = q_dict[2 * n, 0, "r"]
            q2nm = q_dict[2 * n - 2, 0, "r"]
            if 2 * n + 2 <= Lmax_q:
                matrix[t2np, q2np] = c / a
            matrix[t2np, q2n] = d / a
            matrix[t2np, q2nm] = e / a
            matrix[t2np, :] -= matrix[tprev, :] * b / a0
            tprev = t2np

    return matrix


def build_qlm_to_tlm_zonal_matrix_Re_Im_v2(Lmax_q):
    Lu_max = Lmax_q + 1
    Nq = Lmax_q * (Lmax_q + 2) * 2 + 2
    Nu = Lu_max * (Lu_max + 2) * 2
    matrix = np.zeros((Nu, Nq))

    u_in_re_im_form_dict, Nu_re_im_form, _, _ = build_real_imag_and_cossin_dict_pos(
        Lu_max, q=False
    )
    q_in_re_im_form_dict, Nq_re_im_form, _, _ = build_real_imag_and_cossin_dict_pos(
        Lmax_q, q=True
    )

    nu = 0
    for lu in range(1, Lu_max + 1):
        for mu in range(-lu, lu + 1):
            if mu == 0:
                lq_m1 = lu - 1
                lq_p1 = lu + 1

                if lq_m1 >= 0:
                    gamma = calculate_gamma(lu, 0)
                    nq = list(q_in_re_im_form_dict.keys())[
                        list(q_in_re_im_form_dict.values()).index((lq_m1, 0, "r"))
                    ]
                    matrix[nu, nq] = gamma * (-(lu + 2) / lu)
                    matrix[nu + 1, nq + 1] = gamma * (-(lu + 2) / lu)

                if lq_p1 <= Lmax_q:
                    gamma = calculate_gamma(lu + 1, 0)
                    nq = list(q_in_re_im_form_dict.keys())[
                        list(q_in_re_im_form_dict.values()).index((lq_p1, 0, "r"))
                    ]
                    matrix[nu, nq] = gamma * (-(lu - 1) / (lu + 1))
                    matrix[nu + 1, nq + 1] = gamma * (-(lu - 1) / (lu + 1))

            nu += 2

    return matrix


def compare_plus_minus_coefficients(plus_coefficients, minus_coefficients, re_im_dict):
    for i in range(len(plus_coefficients.reshape((-1,)))):
        l, m, reim = re_im_dict[i]
        if m < 0:
            m_ = -m
            pos = list(re_im_dict.keys())[
                list(re_im_dict.values()).index((l, m_, reim))
            ]
            print(re_im_dict[i])
            if reim == "r":
                print(
                    "plus", plus_coefficients[i], ((-1) ** m_) * minus_coefficients[pos]
                )
                print(
                    "minus",
                    minus_coefficients[i],
                    ((-1) ** m_) * plus_coefficients[pos],
                )
            else:
                print(
                    "plus",
                    plus_coefficients[i],
                    -((-1) ** m_) * minus_coefficients[pos],
                )
                print(
                    "minus",
                    minus_coefficients[i],
                    -((-1) ** m_) * plus_coefficients[pos],
                )
