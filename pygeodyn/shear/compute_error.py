import numpy as np
import os
import time
import logging
from pygeodyn.shear.shear_algo import ShearError
from mpi4py import MPI

# directory of the script
dir = os.path.dirname(os.path.abspath(__file__))

# Computation parameters
Ly = 18
Lu = 18 
Lb = 13
prior_dir_shear = dir + "/../data/priors/CE_augkf" #coupled_earth
prior_type_shear = "coupled_earth"                 #coupled_earth
#prior_dir_shear = dir + "/../data/priors/midpath"  #midpath
#prior_type_shear = "midpath"                       #midpath
#prior_dir_shear = dir + "/../data/priors/71path"   #71path
#prior_type_shear = "71path"                        #71path
TauG = 0

def compute_error(Ly, Lu, Lb, TauG, prior_dir_shear, prior_type_shear):
    """
    Compute the error of representativeness for all prior times and save it in prior_dir_shear

    :param Ly: Maximal spherical harmonic degree of the constraint on the shear
    :type Ly: int
    :param Lu: Maximal spherical harmonic degree of the core flow
    :type Lu: int
    :param Lb: Maximal spherical harmonic degree of the magnetic field
    :type Lb: int
    :param TauG: Time constant for the shear (MUST REMAIN 0 SO FAR)
    :type TauG: int
    :param prior_dir_shear: Directory containing the prior data for the shear computation
    :type prior_dir_shear: str
    :param prior_type_shear: Prior type for the shear computation
    :type prior_type_shear: str
    :param test: Test directory that contains reference data for tests
    :type test: str
    """

    # to improve performance in parrallel
    os.environ["MKL_NUM_THREADS"] = "1"
    os.environ["NUMEXPR_NUM_THREADS"] = "1"
    os.environ["OMP_NUM_THREADS"] = "1"

    # Init MPI
    if not MPI.Is_initialized():
        MPI.Init()
    comm = MPI.COMM_WORLD
    nb_proc = comm.Get_size()
    rank = comm.Get_rank()

    def first_process():
        return rank == 0

    # Log
    logging_level = logging.INFO
    log_format = '%(asctime)s - Process {} - %(levelname)s - %(message)s'.format(rank)
    logging.basicConfig(format=log_format, level=logging_level)
    
    if first_process:
        begin_time = time.time()

    # Init ShearError instance
    error = ShearError(Ly, Lu, Lb, TauG, prior_dir_shear, prior_type_shear)

    # Precompute operators
    error.precompute_operators()

    # Compute 
    U,DU,MF,SV = error.load_prior_for_err_rep()

    N_times = U.shape[0]

    # Build the attribution of times if several processes in parallel
    attributed_times = range(rank, N_times, nb_proc)

    store = np.zeros((N_times, error.Ny2))

    for i in attributed_times:
        logging.info("Computing error at time index " + str(i) + " out of " + str(N_times))
        u = U[i,:]
        du = DU[i,:]
        mf = MF[i,:]
        sv = SV[i,:]

        store[i,:] = error.compute_error(u, du, mf, sv)

    # Recover all errors by summing all arrays
    # Note that Allreduce updates the array of each process with the result so no need to broadcast afterwards.
    logging.info("Waiting all process to finish computing the error of representativeness")
    comm.Allreduce(MPI.IN_PLACE, store, op=MPI.SUM)
    logging.info("Sync done !".format(rank))

    if first_process:
        # Save error in prior_dir_shear
        np.save(prior_dir_shear + "/y12_err_repr_Ly_{}_Lu_{}_Lb_{}.npy".format(Ly,Lu,Lb), store)

        end_time = time.time()
        elapsed_time = end_time - begin_time
        logging.info("Elapsed time : {:.2f}".format(elapsed_time))

if __name__ == '__main__':
    compute_error(Ly, Lu, Lb, TauG, prior_dir_shear, prior_type_shear)