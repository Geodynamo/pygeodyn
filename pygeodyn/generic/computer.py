import numpy as np
from ..corestates import with_core_state_of_dimensions
import geodyn_fortran as fortran
from ..inout.config import ComputationConfig


class GenericComputer(object):
    """
    Mother class of Forecaster and Analyser. Implements methods and members that are common to both.
    """
    def __init__(self, algo):
        """
        Constructor of GenericComputer. Sets the args as members.

        :param algo: Algorithm object
        :type algo: Algo
        """

        if type(algo.config) is not ComputationConfig:
            raise TypeError('{} was initialised with a ComputationConfig argument ! Got an argument of type {} instead.'.format(type(self).__name__, type(algo.config)))

        self.cfg = algo.config
        self.algo = algo

    @with_core_state_of_dimensions(1)
    def compute_Ab(self, input_core_state, specfic_real=False):
        """
        Compute A(b) the operator DivH(uBr) in the spectral domain.

        :param input_core_state: 1D CoreState storing a minima the magnetic field coefficients.
        :type input_core_state: 1D CoreState
        :return: The computed A(b)
        :rtype: 2D numpy.ndarray (dim: Nsv x Nu2)
        """

        gauss_th = np.asfortranarray(self.algo.legendre_polys['thetas'])
        gauss_weights = np.asfortranarray(self.algo.legendre_polys['weights'])
        tmax = gauss_th.shape[0]

        lpsv = np.asfortranarray(self.algo.legendre_polys['SV'][0])
        dlpsv = np.asfortranarray(self.algo.legendre_polys['SV'][1])
        d2lpsv = np.asfortranarray(self.algo.legendre_polys['SV'][2])
        LLsv = d2lpsv.shape[0]

        lpu = np.asfortranarray(self.algo.legendre_polys['U'][0])
        dlpu = np.asfortranarray(self.algo.legendre_polys['U'][1])
        d2lpu = np.asfortranarray(self.algo.legendre_polys['U'][2])
        LLu = d2lpu.shape[0]

        lpb = np.asfortranarray(self.algo.legendre_polys['MF'][0])
        dlpb = np.asfortranarray(self.algo.legendre_polys['MF'][1])
        d2lpb = np.asfortranarray(self.algo.legendre_polys['MF'][2])
        LLb = d2lpb.shape[0]

        # Function radmats computes A(b)^T
        AbT = np.zeros((input_core_state.Nu2, input_core_state.Nsv), order='F')
        fortran.integral.radmats(gauss_th, gauss_weights, lpsv, dlpsv, d2lpsv,
                                 lpu, dlpu, d2lpu, lpb, dlpb, d2lpb,
                                 AbT, input_core_state.B,
                                 input_core_state.Lsv, input_core_state.Lu, input_core_state.Lb,
                                 input_core_state.Nsv, input_core_state.Nu2 // 2, input_core_state.Nb,
                                 LLsv, LLu, LLb, tmax)

        # Return A(b)
        return np.transpose(AbT)
