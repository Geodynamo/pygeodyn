#!/usr/bin/env python3
#-*- coding: utf-8 -*-

from ..inout.config import ComputationConfig

class GenericAlgo:
    """
    Base class that defines the interface of Algo for it to be used in run.py
    """
    name = 'base'

    def __init__(self, config, nb_realisations):
        if not isinstance(config, ComputationConfig):
            raise TypeError('The config used to initialise {} is not a ComputationConfig object ! Got an argument of type {} instead.'.format(type(self).__name__, type(config)))
        self.config = config
        if int(nb_realisations) <= 0:
            raise ValueError('Number of realisation should be a positive int ! Got {} instead.'.format(nb_realisations))
        self.nb_realisations = int(nb_realisations)

    def init_corestates(self, random_state=None):
        raise NotImplementedError()

    def analysis_step(self, input_core_state, analysis_time):
        raise NotImplementedError()

    def forecast_step(self, input_core_state, random_state=None):
        raise NotImplementedError()

    def get_current_misfits(self, measure):
        return 0
