#!/usr/bin/python3

""" Physical constants needed for algo of the diffusion among others. """

# Distance between the core-mantle boundary (CMB) and the layer just below the radial grid of the Coupled-Earth model
delta = 2.7033
# Effective magnetic diffusion coefficient of the Coupled-Earth model
eta_mag = 36.577129


# Radius of core
r_core = 3485.0
# Radius of Earth
r_earth = 6371.2
# Radius usde in covobs decomposition
r_covobs = 6371.2

# Julian Date of Jan 1, 2000 12h
jd2000 = 2451545.0
