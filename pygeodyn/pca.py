import math
import logging
import numpy as np
from . import utilities
from sklearn.decomposition import PCA

class NormedPCAOperator:
    """
    Object handling the transforms with normalisation of the PCA of U.
    Generates the sklearn operator (used to fit the normed data) and the norm matrix used for normalisation.
    Implements the fit_transform, transform and inverse_transform methods with normalisation taken into account.
    """
    def __init__(self, config):
        self.sklearn_operator = PCA(n_components=config.N_pca_u, svd_solver='full')
        self.norm_matrix, self.inverse_norm_matrix = compute_normalisation_matrix(config.Lu, getattr(config, 'pca_norm', None))

        # Assuming that the sklearn_operator was used to do the PCA on the normed data Û = NM*U (NM = norm_matrix):
        # we have U_pca = SKC*(Û - Û0) with SKC = sklearn_operator.components_ and Û0 = sklearn_operator.mean_
        # so Û = SKC^T * U_pca + Û0
        # => U = NM^(-1) SKC^T * U_pca + NM^(-1) * Û0
        # => U = S_u * U_pca + U0
        # => U_pca = inv_S_u * (U - U0)
        # /!\ S_u is no longer orthogonal due to normalisation so the pseudo inverse need to be stored

    @property
    def S_u(self):
        return self.inverse_norm_matrix @ np.transpose(self.sklearn_operator.components_)

    @property
    def inv_S_u(self):
        return self.sklearn_operator.components_ @ self.norm_matrix

    @property
    def U0(self):
        return self.inverse_norm_matrix @ self.sklearn_operator.mean_

    @property
    def n_components(self):
        return self.sklearn_operator.n_components

    def fit(self, realisations_U):
        self.sklearn_operator.fit(realisations_U @ self.norm_matrix)

    def transform(self, U):
        return np.transpose(self.inv_S_u @ np.transpose(U - self.U0))

    def inverse_transform(self, pcaU):
        return np.transpose(self.S_u @ np.transpose(pcaU)) + self.U0
    
    def transform_deriv(self, dU):
        return np.transpose(self.inv_S_u @ np.transpose(dU))

    def inverse_transform_deriv(self, pcadU):
        return np.transpose(self.S_u @ np.transpose(pcadU))

    def __str__(self):
        return type(self).__name__


class NormedNzPCAOperator(NormedPCAOperator):
    def __init__(self, config):
        super().__init__(config)
        # Project into zU,nzU space to recover only the nz part of norm_matrix
        P_U_Ua = utilities.spectral_to_znz_matrix(config.Lu)
        self.norm_matrix = (P_U_Ua @ self.norm_matrix @ P_U_Ua.T)[config.Lu:, config.Lu:]
        self.inverse_norm_matrix = (P_U_Ua @ self.inverse_norm_matrix @ P_U_Ua.T)[config.Lu:, config.Lu:]


def compute_normalisation_matrix(Lu, norm_type=None):
    """
    Computes the normalisation matrix for the PCA according to the asked norm type.
    Default is None: the normalisation matrix (and its inverse) is then the identity.

    :param Lu: max_degree of core flow U
    :type Lu: int
    :param norm_type: Normalisation type
    :type norm_type: "energy" or None
    :return: Normalisation matrix and its inverse
    :rtype: np.array (Nu2 x Nu2), np.array (Nu2 x Nu2)
    """
    if norm_type is not None and norm_type == "energy":
        logging.debug('PCA performed with energy normalisation')
        return compute_energy_normalisation_matrix(Lu)
    else:
        # No normalisation
        logging.debug('PCA performed without any normalisation')
        return np.identity(2*Lu*(Lu+2)), np.identity(2*Lu*(Lu+2))


def compute_energy_normalisation_matrix(Lu):
    """
    Computes the matrix normalising coefficients with respect to energy.
    That is: norm_tnm = sqrt(n*(n + 1)/(2 * n + 1))*tnm

    :param Lu: max_degree of core flow U
    :type Lu: int
    :return: Energy normalisation matrix and its inverse
    :rtype: np.array (Nu2 x Nu2), np.array (Nu2 x Nu2)
    """

    Nu = Lu*(Lu+2)
    norm_factors = np.zeros(2*Nu)
    min_index = 0

    for n in range(1, Lu + 1):
        next_index = n*(n + 2)

        norm_factor = math.sqrt(n * (n + 1) / (2 * n + 1))
        norm_factors[min_index: next_index] = norm_factor
        norm_factors[Nu + min_index: Nu + next_index] = norm_factor

        min_index = next_index

    return np.diag(norm_factors), np.diag(1./norm_factors)
