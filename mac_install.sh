#!/bin/bash

    echo "Pygeodyn and python virtual environment will be installed in the current directory : $PWD"
    while true; do
        read -p "Would you like to continue? " yn
        case $yn in
            [Yy]* ) break;;
            [Nn]* ) exit;;
            * ) echo "Please answer yes or no.";;
        esac
    done
    
    #Install Homebrew if not already installed
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
    
    dependency_list="gcc git python3"

    for d in $dependency_list
    do
        #Update or install dependencies
        brew install $d
    done
    
    #install pip if not already installed
    python3 -m ensurepip
    
    echo "Cloning pygeodyn git repository including pygeodyn dataset"
    git clone --recursive https://gricad-gitlab.univ-grenoble-alpes.fr/Geodynamo/pygeodyn.git

    echo "Installing virtualenv"
    pip3 install virtualenv

    echo "Creating virtual environment"
    virtualenv pygeodyn_venv

    echo "Activating virtual environment"
    source pygeodyn_venv/bin/activate

    echo "Installing dependencies"
    pip3 install "setuptools<=59.8.0"
    pip3 install wheel
    pip3 install "numpy<=1.23.5"
    
    cd pygeodyn

    echo "Installing pygeodyn"
    pip3 install -v .
    
    echo "Deactivating virtual environment"
    deactivate
    
    echo "End of script"