#!/bin/env python3

import setuptools

try:
    from numpy.distutils.core import Extension, setup
except ModuleNotFoundError:
    import sys

    sys.exit("Please install NumPy before pygeodyn !")
import os


def local_file(name):
    return os.path.relpath(os.path.join(os.path.dirname(__file__), name))


# Get version number from _version.py
__version__ = None
with open(local_file("pygeodyn/_version.py")) as o:
    exec(o.read())
assert __version__ is not None


with open(local_file("AUTHORS.txt")) as authors_file:
    authors = authors_file.read().replace("\n", ", ")


setup(
    name="pygeodyn",
    version=__version__,
    author=authors,
    author_email="francois.dall-asta@univ-grenoble-alpes.fr",
    description="Python package for geomagnetic data assimilation",
    long_description=open("README.md").read(),
    long_description_content_type="text/markdown",
    url="https://gricad-gitlab.univ-grenoble-alpes.fr/Geodynamo/pygeodyn",
    packages=setuptools.find_packages(),
    zip_safe=False,
    ext_modules=[
        Extension(
            name="geodyn_fortran",
            sources=[
                "pygeodyn/fortran/Legendre_evals.f90",
                "pygeodyn/fortran/Integral_evals.f90",
            ],
        )
    ],
    install_requires=[
        "h5py",
        "numpy>=1.10",
        "scipy>=0.17",
        "mpi4py",
        "scikit-learn",
        "hypothesis",
        "cdflib"
    ],
    classifiers=[
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Intended Audience :: Science/Research",
        "Topic :: Scientific/Engineering :: Physics",
    ],
)
